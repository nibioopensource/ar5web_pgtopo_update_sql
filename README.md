# PostgreSQL schema and functions to manage Surface/Border topologies used by AR5 Web.

## To checkout code

```
git clone https://gitlab.com/nibioopensource/ar5web_pgtopo_update_sql.git
cd ar5web_pgtopo_update_sql
```

Run `make help` to get a list of available Makefile targets.

Run `make` to generate the scripts to create the `ar5ngis` schemas.

Run `make check` to run the regression testsuite.

## Requirements

In order to use the code and run the tests you need:

	- PostgreSQL 12+
	- GEOS 3.9.1+
	- PostGIS 3.1.0+
	- uuid-ossp PostgreSQL extension
	- https://github.com/NibioOpenSource/pgtopo_update_sql.git
	- On mac, you must set gnused first in your path to run tests (brew info gnu-sed)

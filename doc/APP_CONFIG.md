The format of the JSON app configuration has these elements:

# `version`

Text value expressing version of the application configuration.
At this moment only version `1.0` is supported and is what is
documented here.

# `extensions`

Array of extension names required to define the schema.
The `postgis` and `postgis_topology` are always implicitly
required.


# `topology` (optional)

Object with information about topology configuration.
If missing, all configurable items will be set to their
defaults.

## `srid`

Spatial Reference ID (PostGIS term).
Defaults to 0 (unknown)

## `snap_tolerance`

Snap tolerance to use by default when adding elements
to the topology. The floating point number is expressed
in the units of the spatial reference id.

Defaults to 0 (no or minimal snap))

# `domains` (optional)

An array of objects representing schema domains.
Format of the schema domain objects is the same as
for table attributes (`name`, `type`, `allowed_values`, etc.)

# `tables`

An array of objects representing schema tables.
Each table object has the following fields:

## `name`

Name of the table

## `topogeom_columns` (optional)

An array of objects specifying which topogeometry columns
exist in the table. Each such object has a mandatory `name` and
a mandatory `type` ('puntal', 'lineal', 'areal').

The tables playing the Surface and Border roles need have each
at least one such column, of the 'areal' and 'lineal' type
respectively.

Optional elements each topogeom column are:

	- `allow_multi` boolean, defaults to true

	   Whether the TopoGeometry is allowed to be only possibly
	   represented as a MULTI geometry (ie: a MultiPolygon,
	   MultiLine or MultiPoint). This can happen for example
	   in the case of self-crossing borders or surfaces composed
	   by disjoint faces or by faces only touching at nodes.

	- `allow_overlaps` boolean, defaults to true

	   Whether two TopoGeometry values in the table can share
	   portions of space with other records in the same
	   table (same face used by two surfaces, or same edge
	   used by two borders)

	- `allow_crossings` boolean, defaults to true

	   Whether two TopoGeometry values in the the table can cross
	   each-other. This can only be specified for 'lineal' TopoGeometry
	   columns.

	- `allow_gaps` boolean, defaults to true

	   Whether it is allowed for the union of all values in this
	   column to NOT cover all of the underlying primitives, leaving
	   "gaps".

## `attributes`

Array of objects representing table attributes,
each with the following fields:

### `name`

Name of attribute

### `type`

Datatype of attribute, may refer to name of other tables but
such tables will need to be defined *before* the table using
them as attributes.

### `modifiers` (optional)

Datatype modifiers, an array of modifiers for the
type. For example ["point","4326"] for a "geometry"
type or ["256"] for a "varchar" type.

### `default`

Default value for the attribute.
Interpretation varies based on type of the value, which can be:

	- String/Number:
		a literal value being the canonical representation
		of the default value

	- Array:
		an array of literal values being the canonical
		text representations of each of the attributes
		of a composite type

	- Object
		extensible way to express more complex defaults.

For an "object" values "default", the object MUST have a "type"
attribute expressing how the default is obtained. Supported types are:

#### `type=function`

The default value is obtained by invoking a function,
the name of the functions is in a "name" attribute.

### `allowed_values`

Array of literal values allowed for the field

## `primary_key` (optional)

Name of one field being the primary key of the table

# `surface_layer` (optional)

Value of this element is an object with information about
which table plays the Surface role. It is only required if
operations dealing with surfaces are enabled.

Elements of it are:

## `table_name`

Unqualified name of the table playing the role of Surface.

## `geo_column`

Name of the column containing the Surface TopoGeometry.

## `version_column` (OPTIONAL)

Name of the column containing the version of the Surface record.

# `border_layer`

Value of this element is an object with information about
the Border layer:


## `table_name`

Unqualified name of the table playing the role of Border.

## `geo_column`

Name of the column containing the Border TopoGeometry.

## `version_column` (OPTIONAL)

Name of the column containing the version of the Border record.

# `operations`

Object containing configuration of various operations, as follows.

## `AddBordersSplitSurfaces`

Configuration for the `AddBordersSplitSurfaces` operation.
It currently supports a single `parameters` element
which contains 3 optional elements:

	- "default"
	- "forOpenLines"
	- "forClosedLines"

Each of those elements are objects which can contain the *same*
set of parameters, with the final set of parameters used by
the function being values read from the "default" object (if given)
and overridden by the object matching the input kind (open lines
or closed lines)

These are the parameters:

### `snapTolerance`

Snap tolerance to use for snapping the incoming line to existing
edges/nodes of the underlying topology. It's an object with
elements:

	- `units`: snap tolerance value
	- `unitsAreMeters` (OPTIONAL, defaults to false)
	   if set to true will make `units` be interpreted as meters,
       otherwise they are in topology CRS units.

### `minAllowedNewSurfaceArea`

Minimum allowed area for new surfaces (either resulting from
modification or created new).
It's an object with elements:

	- `units`: min area
	- `unitsAreMeters` (OPTIONAL, defaults to false)
	   if set to true will make `units` be interpreted as square
	   meters, otherwise they are in squared topology CRS units.

### `maxAllowedSplitSurfacesCount`

Maximum number of Surfaces that are allowed to be split by the
operation. Defaults to 5.

### `maxAllowedNewSurfaceCount`

Maximum number of Surfaces that are allowed to be created by the
operation. Defaults to 1.

## `AddBorders`

Configuration for the `AddBorders` operation.
AddBorders does not have any configuration at the moment.
This operation can only be allowed if a `border_layer`
was defined in the appconfig.

## `AddSurface`

Configuration for the `AddSurface` operation.
AddSurface does not have any configuration at the moment.
This operation can only be allowed if a `surface_layer`
was defined in the appconfig.

## `UpdateAttributes`

Configuration for the `UpdateAttributes` operation.
UpdateAttributes does not have any configuration at the moment.

## `GetFeaturesAsTopoJSON`

Configuration for the `GetFeaturesAsTopoJSON` operation.
GetFeaturesAsTopoJSON does not have any configuration at the moment.

## `RemoveBordersMergeSurfaces`

Configuration for the `RemoveBordersMergeSurfaces` operation.
RemoveBordersMergeSurfaces does not have any configuration at the moment.

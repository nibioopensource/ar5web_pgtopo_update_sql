-- Create feature extraction function
CREATE OR REPLACE FUNCTION topo_ar5ngis.feature_extract_position(
	feature JSONB,
	feat_srid INT,
	topo_srid INT,
	OUT role CHAR,
	OUT prop JSONB
) AS $BODY$ -- {
DECLARE
	pos JSONB;
	pos_geom Geometry;
	maxdigits INT := 14;
BEGIN
	prop := feature -> 'properties';

	-- Turn empty "versionId" to a null value
	IF prop -> 'identifikasjon' ->> 'versjonId' = '' THEN
		prop := jsonb_set(prop,'{identifikasjon,versjonId}','null');
	END IF;

	IF feature -> 'geometry' ->> 'type' LIKE '%LineString' THEN
		role := 'B';
		RETURN;
	END IF;

	-- Assuming it's a surface
	role := 'S';
	pos := feature -> 'geometry_properties' -> 'position';

	pos_geom = ST_MakePoint((pos ->> 0)::float,(pos ->> 1)::float);

	IF feat_srid IS NOT NULL THEN
		pos_geom := ST_SetSRID(pos_geom , feat_srid);
		RAISE DEBUG 'Border geometry SRID set to requested SRID %', feat_srid;
	END IF;

	-- Make sure border geometry is using target topology
	-- projection
	IF feat_srid != topo_srid THEN
		pos_geom  := ST_Transform(pos_geom , topo_srid);
		RAISE DEBUG 'pos reprojected to topology CRS: %',
			ST_AsEWKT(pos_geom);
	END IF;

	prop := jsonb_set(
		prop,
		'{ geometry_properties }',
		jsonb_build_object(
			'position',
			to_jsonb(
				ST_AsEWKT(pos_geom, maxdigits)
			)
		)
	);
END;
$BODY$ LANGUAGE 'plpgsql'; --}



CREATE OR REPLACE FUNCTION topo_ar5ngis.add_border_split_surface_java_tmp_ColMapProvider(typ char, act char, usr JSONB)
RETURNS JSONB AS $BODY$
DECLARE
	procName TEXT := 'topo_ar5ngis.add_border_split_surface_java_tmp_ColMapProvider';
	map JSONB;
BEGIN
	map := usr -> format('%s%s', typ, act);

	RAISE DEBUG '%: map for typ:% act:% is %', procName, typ, act, map;

	RETURN map;
END;
$BODY$ LANGUAGE 'plpgsql';
-- }

DROP FUNCTION IF EXISTS topo_ar5ngis.surfaces_as_geojson_java_featureJsonModifierChanged(JSONB, CHAR, TEXT);
DROP FUNCTION IF EXISTS topo_ar5ngis.surfaces_as_geojson_java_featureJsonModifierTouches(JSONB, CHAR, TEXT);

-- Callback function for use with topo_update.surfaces_as_geojson
-- to insert `action` parameter ( Replace or Create )
-- {
CREATE OR REPLACE FUNCTION topo_ar5ngis.surfaces_as_geojson_featureJsonModifier(
	INOUT feat JSONB,
	role CHAR,
	-- A JSONB being the representation of
	-- the recordset returned by topo_ar5ngis.add_border
	-- function limited to the "fid", "typ" and "act"
	-- columns
	usr JSONB)
AS $BODY$
DECLARE
	ffid TEXT;
	action_value TEXT DEFAULT 'Replace';
	featuretype TEXT;
	add_border_results JSONB := usr;
	add_border_result_element JSONB;
BEGIN

	RAISE DEBUG 'topo_ar5ngis.surfaces_as_geojson_featureJsonModifier enter';

	-- Strip NULL values from properties
	-- 	feat := jsonb_set(
	-- 		feat,
	-- 		'{properties}',
	-- 		jsonb_strip_nulls(
	-- 			feat -> 'properties'
	-- 		)
	-- 	);
	-- DO not send NULL values for synbarhet to NGIS
	IF feat -> 'properties' -> 'kvalitet'->> 'synbarhet' IS NULL THEN
		feat := feat #- '{ properties, kvalitet, synbarhet }';
	END IF;

	-- DO not send NULL values for versjonId to NGIS
	IF feat -> 'properties' -> 'identifikasjon' ->> 'versjonId' IS NULL THEN
		feat := feat #- '{ properties, identifikasjon, versjonId }';
	END IF;

	-- Drop crs since it's in the header
	feat := feat #- '{ geometry, crs }';

	-- Extract FID
	ffid := feat #>> ARRAY[ 'properties', 'identifikasjon', 'lokalId' ];
	IF ffid IS NULL THEN
		RAISE EXCEPTION 'Unexpected NULL identifikasjon.lokalId'
			' in feature passed to featureRewriter by surfaces_as_geojson: %',
			feat;
	END IF;

	IF role = 'B'
	THEN -- Border feature
		featuretype := feat #>> ARRAY[ 'properties', 'featuretype' ];
		IF featuretype IN ('ArealressursGrenseFiktiv','Kantutsnitt') THEN
			feat := feat #- '{ properties, kvalitet }';
			feat := feat #- '{ properties, avgrensingType }';
			feat := feat #- '{ properties, registreringsversjon }';
			feat := feat #- '{ properties, verifiseringsdato }';
		END IF;
	ELSIF role = 'S' THEN -- Surface feature
		IF EXISTS (SELECT 1 FROM (
			SELECT json_array_elements(usr::JSON)
		) AS r WHERE (r.json_array_elements)->>'fid' = ffid AND (r.json_array_elements)->>'act' IN ( 'S', 'C' ))
		THEN
			action_value := 'Create';
		END IF;

		-- Apply this to any feature role
		feat := jsonb_set(
			feat,
			'{update}',
			jsonb_build_object(
				'action',
				action_value
			)
		);

		-- DO not send NULL values for synbarhet to NGIS
		IF feat -> 'properties' ->> 'informasjon' IS NULL THEN
			feat := feat #- '{ properties, informasjon }';
		END IF;

	ELSE

		-- DO not send NULL values for synbarhet to NGIS
		IF feat -> 'properties' ->> 'informasjon' IS NULL THEN
			feat := feat #- '{ properties, informasjon }';
		END IF;

	END IF;


	RAISE DEBUG 'Extracting add_border_result item';

	-- Extract add_border_result item for the given feature
	IF EXISTS (SELECT 1 FROM (
		SELECT json_array_elements(usr::JSON)
	) AS r WHERE (r.json_array_elements)->>'fid' = ffid) THEN
		RAISE DEBUG 'Rewriting GeoJSON for Feature with FID: %', ffid;

		IF EXISTS (SELECT 1 FROM (
                SELECT json_array_elements(usr::JSON)
		) AS r WHERE (r.json_array_elements)->>'fid' = ffid AND (r.json_array_elements)->>'act' IN ( 'U', 'T' ))
		THEN
			feat := feat;
		ELSE
			IF EXISTS (SELECT 1 FROM (
	                SELECT json_array_elements(usr::JSON)
			) AS r WHERE (r.json_array_elements)->>'fid' = ffid AND (r.json_array_elements)->>'act' IN ( 'S', 'C' ))
			THEN
				action_value := 'Create';
			END IF;

			-- Apply this to any feature role
			feat := jsonb_set(
				feat,
				'{update}',
				jsonb_build_object(
					'action',
					action_value
				)
			);
		END IF;
	ELSE
		feat := feat;
	END IF;

END;
$BODY$ LANGUAGE 'plpgsql';

-- Function used to prepare NGIS payload for ar5
-- {
CREATE OR REPLACE FUNCTION topo_ar5ngis.update_feature_preparePayload(
        INOUT payload JSONB,
        INOUT bbox GEOMETRY
    )
    AS $BODY$
    DECLARE
        rec RECORD;
        identifikasjon JSONB;
        flate_found INT;
        feat JSONB;
        sql TEXT;
        payload_features_fixed JSONB ='[]';
        id JSONB;
        feature_found int = 0;
        feature_not_found int = 0;
        geometry_properties_position_found int;

    BEGIN

        -- We can only di this quik check if we have bbox
        IF bbox IS NOT NULL THEN
            -- sheck if all surfaces have equal version_id
            FOR rec IN SELECT jsonb_array_elements(payload -> 'features') feat
            LOOP --{
                feat := rec.feat;
                IF feat -> 'properties' ->> 'featuretype' LIKE 'ArealressursFlate' THEN
                    IF feat -> 'properties' -> 'identifikasjon' ->> 'versjonId' = '' THEN
                        feature_not_found := feature_not_found + 1;
                    ELSE
                        identifikasjon := feat -> 'properties' -> 'identifikasjon';
                        sql := format('SELECT 1 from topo_ar5ngis.face_attributes WHERE CAST (%1$L AS UUID) = identifikasjon_lokal_id
                            AND CAST (  %2$L AS timestamp) = identifikasjon_versjon_id',
                            identifikasjon ->> 'lokalId',
                            identifikasjon ->> 'versjonId');
                        EXECUTE sql INTO flate_found;
                        IF flate_found = 1 THEN
                            feature_found := feature_found + 1;
                        ELSE
                            feature_not_found := feature_not_found + 1;
                        END IF;
                    END IF;
                END IF;
            END LOOP; --}

            -- other objects
            SELECT count(identifikasjon_versjon_id) from  topo_ar5ngis.face_attributes where geometry_properties_position && bbox INTO geometry_properties_position_found;
        END IF;


        IF bbox IS NOT NULL AND feature_not_found = 0 AND geometry_properties_position_found = feature_found THEN
            payload := NULL;
            bbox := NULL;
            RAISE NOTICE 'All rows are equal for topo_ar5ngis.face_attributes  feature_not_found %, feature_found %, geometry_properties_position_found % for bbox %',
            feature_not_found, feature_found, geometry_properties_position_found, bbox;
        ELSE
            RAISE NOTICE 'All rows are not equal for topo_ar5ngis.face_attributes  feature_not_found %, feature_found %, geometry_properties_position_found % for bbox %',
            feature_not_found, feature_found, geometry_properties_position_found, bbox;
        END IF;


    END; --}
$BODY$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION topo_ar5ngis.add_border_ColMapProvider(typ char, act char, usr TEXT)
RETURNS JSONB AS $BODY$
DECLARE
	procName TEXT := 'topo_ar5ngis.add_border_ColMapProvider';
	map JSONB;
BEGIN
	IF typ = 'B' THEN
		IF act = 'C' THEN
			map := $$
				{
					"opphav": [ "opphav" ],
					"verifiseringsdato": [ "verifiseringsdato" ],
					"datafangstdato": [ "datafangstdato" ],
					"kvalitet": [ "kvalitet" ],
					"avgrensing_type": [ "border_avgrensing_type" ]
				}
			$$::jsonb;
		ELSE
			map := $$
				{
				}
			$$::jsonb;
		END IF;
	ELSIF typ = 'S' THEN
		IF act = 'C' THEN
			map := $$
				{
					"opphav": [ "opphav" ],
					"verifiseringsdato": [ "verifiseringsdato" ],
					"datafangstdato": [ "datafangstdato" ],
					"kvalitet": [ "kvalitet" ]
				}
			$$::jsonb;
		ELSE
			map := $$
				{
				}
			$$::jsonb;
		END IF;
	ELSE
		RAISE EXCEPTION '%: unexpected feature type %', procName, typ;
	END IF;

	-- Add versjonId mapping
	map := map || $$
		{
			"identifikasjon_versjon_id": [ "_current_timestamp" ],
			"oppdateringsdato": [ "_current_timestamp" ]
		}
	$$::jsonb;

	RETURN map;
END;
$BODY$ LANGUAGE 'plpgsql';

-- A helper function to create UUID so we can make predictable when needed in tests
-- TODO find a better way to this
CREATE OR REPLACE FUNCTION topo_ar5ngis.lock_area_for_update_id()
RETURNS uuid
AS $BODY$
	SELECT * FROM uuid_generate_v1()
$BODY$ LANGUAGE 'sql' VOLATILE
SET search_path TO topo_ar5ngis, public;


CREATE OR REPLACE FUNCTION topo_ar5ngis.add_border(
		feature_in TEXT,
		opphav TEXT DEFAULT NULL, -- this will be sent from the server and controlled java backend server and override settings from the client, TODO mabe make a more generic struture for this ?
		-- to turn on and of locking code
		do_locking boolean DEFAULT true
)
RETURNS TABLE(fid text, typ char, act char, frm text) AS $BODY$
DECLARE
	feature JSONB := feature_in::jsonb;
	-- Test with lower tolerance, this is related https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/186
	-- Test starts failing with 1e-06
	tolerance FLOAT8 := 1e-09;
	minAreaToleranceM2 FLOAT8 = 200;
	minAreaTolerance FLOAT8;
	geom GEOMETRY;
	geom_for_area GEOMETRY;
	geom_as_polygon GEOMETRY;
	geom_as_polygon_area FLOAT8;
	input_line_polygonize_area FLOAT8;
	mline_geo GEOMETRY;
	mline_cnt int;

	-- This is the max number of new surfaces an input operation may changed spatialy. If the area has changed the surface has changed.
	-- Other surfaces may be changed because of new vertex/node, but this are not relevant for this case
	maxAllowedSurfaceSplitCount int = 1;

	-- Two this is the max number of surfaces with new area or changed surface area after the splitt.
	-- but with multline we should accept 3 changed/new surfaces (1 changed and two new ones)
	maxNewSurfacesOrChangedAreaSurfaces int;

	-- for locking
	call_result_pre_check text[][];
	call_result_post_check text[][];
	stmts text[];
	lock_id UUID;
	stmt_result text[];
	featureset_bbox GEOMETRY;
	mbr_direct_bbox_area_after_update GEOMETRY;
	mbr_indirect_bbox_area_after_update GEOMETRY;
	featureset_bbox_before_extend GEOMETRY;
	v_state text;
	v_msg text;
	v_detail text;
	v_hint text;
	v_context text;

	cmd text;

	num_edges_locked int;
	num_mbr_locked int;

	pre_check_lock_area_test boolean = false;
	post_check_lock_area_test boolean = false;
BEGIN

	-- TODO: adapt toleranceless  to incoming line

	-- inject versjonid to feature properties
	feature := jsonb_set(
		feature,
		'{properties, _current_timestamp}',
		to_jsonb(now())
	);

	-- Get value from server side
	IF opphav IS NOT NULL THEN
		feature := jsonb_set(
			feature,
			'{properties, opphav}',
			to_jsonb(opphav)
		);
	END IF;

	-- Get verifiseringsdato for new object
	IF feature -> 'properties' ->> 'datafangstdato' IS NOT NULL THEN
		feature := jsonb_set(
			feature,
			'{properties, verifiseringsdato}',
			to_jsonb(feature -> 'properties' ->> 'datafangstdato')
		);
	END IF;


	-- Set default kvalitet borders
	feature := jsonb_set(
		feature,
		'{properties, kvalitet}',
		to_jsonb('("80",200,"0")'::TEXT)
	);

	-- Set default avgrensing_type, avgrensingType
	feature := jsonb_set(
		feature,
		'{properties, border_avgrensing_type}',
		to_jsonb('4206'::TEXT)
	);

	--RAISE WARNING 'Rewritten feature props: %', jsonb_pretty(feature -> 'properties');

	-- Extract input geometry
	geom := ST_GeomFromGeoJSON(feature -> 'geometry');

	-- TODO What if he drawing one line and that is closed line
	IF ST_GeometryType(geom) = 'ST_MultiLineString' AND ST_NumGeometries(geom) > 1 THEN
		FOR mline_cnt IN 1..ST_NumGeometries(geom) LOOP
			mline_geo = ST_GeometryN(geom,mline_cnt);
			-- sheck if can form a surface
			input_line_polygonize_area := ST_Area(ST_Polygonize(mline_geo));
			IF ST_GeometryType(mline_geo) != 'ST_LineString'
				OR ST_IsSimple(mline_geo) != true
				OR input_line_polygonize_area != 0 THEN
				RAISE EXCEPTION '% number % of MultiLineString with % lines is not valid. Is simple %, has area %. %',
				ST_GeometryType(mline_geo),
				mline_cnt,
				ST_NumGeometries(geom),
				ST_IsSimple(mline_geo),
				input_line_polygonize_area,
				ST_AsText(mline_geo);
			END IF;
		END loop;
		minAreaToleranceM2 = 10;
		maxNewSurfacesOrChangedAreaSurfaces = 3;
	ELSE
		-- sheck if can form a surface
		input_line_polygonize_area := ST_Area(ST_Polygonize(geom));

		-- Default minAreaToleranceM2 should be changed when user is drawing a single non closed line
		IF ST_GeometryType(geom) = 'ST_LineString' AND ST_IsSimple(geom) AND NOT input_line_polygonize_area > 0 THEN
			minAreaToleranceM2 = 10;
		END IF;

		IF input_line_polygonize_area > 0 THEN
			maxNewSurfacesOrChangedAreaSurfaces = 2;
		ELSE
			maxNewSurfacesOrChangedAreaSurfaces = 2;
		END IF;

	END IF;

	RAISE NOTICE 'Min area % and maxNewSurfacesOrChangedAreaSurfaces % for input line GeometryType % IsSimple % PolygonizeArea % wkt %',
	minAreaToleranceM2, maxNewSurfacesOrChangedAreaSurfaces, ST_GeometryType(geom), ST_IsSimple(geom), input_line_polygonize_area, ST_AsEWKT(geom);

	-- Pick point with highest latitude/longitude values
	geom := ST_PointN(
		ST_BoundingDiagonal(
			geom
		),
		1
	);
	-- Make sure we're in latlon projection, which
	-- is required for _ST_BestSRID to work
	geom := ST_Transform(geom, 4258);
	-- Switch to metrical units projection
	geom := ST_Transform(geom, _ST_BestSRID(geom));
	-- Build a rectangle of ~200 square meters around the geometry's highest lat/lon value
	--geom := ST_Buffer(geom, sqrt(200/PI()), 16);
	geom := ST_Expand(geom, sqrt(minAreaToleranceM2)/2.0);
	-- TODO: densify the geometry to have less distortion upon projecting ?
	-- Verify area is ~200 m2
	RAISE NOTICE 'Built geom area in square meters: % (% targeted)', ST_Area(geom), minAreaToleranceM2;
	-- Switch back to latlon (topology) CRS (shall we query it rather than hardcoding it?)
	geom := ST_Transform(geom, 4258);
	-- Compute area in target topology SRID
	minAreaTolerance := ROUND(ST_Area(geom)::numeric, 16);

	RAISE NOTICE 'Computed min area tolerance: %', minAreaTolerance;


	-- Check if area is ready to use
	IF do_locking THEN
		BEGIN
			featureset_bbox = ST_Envelope(geom);

			lock_id := topo_ar5ngis.lock_area_for_update_id();

			cmd := Format('SELECT topo_ar5ngis.pre_check_lock_area(%L,%L,%L,%L)',lock_id,featureset_bbox,'single_line','3600s');
			EXECUTE cmd into pre_check_lock_area_test;


			SELECT ST_Envelope(ST_Collect(f.mbr))
			FROM
			topo_ar5ngis_sysdata_webclient.face f
			WHERE featureset_bbox && f.mbr
			INTO  featureset_bbox_before_extend;

			SELECT count(*) FROM
			(
				SELECT e.geom FROM topo_ar5ngis_sysdata_webclient.edge_data e
				WHERE e.geom && featureset_bbox_before_extend AND ST_Intersects(e.geom,featureset_bbox_before_extend)
				FOR UPDATE
			) r
		 	INTO num_edges_locked;

			SELECT count(*) FROM
			(
				SELECT f.mbr FROM topo_ar5ngis_sysdata_webclient.face f
				WHERE f.mbr && featureset_bbox_before_extend AND ST_Intersects(f.mbr,featureset_bbox_before_extend)
				FOR UPDATE
			) r
		 	INTO num_mbr_locked;
			RAISE NOTICE 'Locked % num_edges_locked and % num_mbr_locked for lockid % on topology % obtained for featureset_bbox %',
			num_edges_locked, num_mbr_locked, lock_id, 'topo_ar5ngis_sysdata_webclient', featureset_bbox_before_extend;


		EXCEPTION WHEN OTHERS THEN
	    	GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT, v_context = PG_EXCEPTION_CONTEXT;
			RAISE EXCEPTION 'Failed to call topo_ar5ngis.pre_check_lock_area with lock_id % for featureset_bbox % , state  : % message: % detail : % hint : % context: %',
			lock_id, ST_AsText(featureset_bbox,10), v_state, v_msg, v_detail, v_hint, v_context;
			--RETURN updated;
			--RETURN;
		END;
	END IF;

	-- IF NOT use lock code lock is OK
	IF NOT do_locking OR pre_check_lock_area_test THEN
		BEGIN

			DROP TABLE IF EXISTS pg_temp.temp_result;
			CREATE TEMP TABLE temp_result AS (
				SELECT * FROM topo_update.add_border_split_surface(

					feature::JSONB,

					'topo_ar5ngis.face_attributes'::regclass,
					'omrade'::name,
					'identifikasjon_lokal_id'::name,

					'topo_ar5ngis.edge_attributes'::regclass,
					'grense'::name,
					'identifikasjon_lokal_id'::name,

					tolerance,

					'topo_ar5ngis.add_border_ColMapProvider'::regproc, -- colMapProviderFunc
					NULL::text,

					minAreaTolerance,

					maxAllowedSurfaceSplitCount,

					maxNewSurfacesOrChangedAreaSurfaces
				)
			);

			-- TODO only call this if requested and add parameters that
			-- TODO move the to add_border_split_surface
			CREATE TEMPORARY TABLE add_border_results_more_info AS (
			SELECT s.fid::uuid, s.typ, s.act
			FROM topo_update.get_border_split_info(
				'topo_ar5ngis.face_attributes'::regclass,
				'omrade'::name,
				'identifikasjon_lokal_id'::name,

				'topo_ar5ngis.edge_attributes'::regclass,
				'grense'::name,
				'identifikasjon_lokal_id'::name,


				-- The result from the split operation
				'pg_temp.temp_result'

				) AS s
			);

			-- This can not be null when sent ngsiOpenApi , may use a older value not sure have to check'
			UPDATE topo_ar5ngis.face_attributes
			SET identifikasjon_versjon_id = '1970-01-01T00:00:00'
			WHERE identifikasjon_lokal_id IN (
				SELECT mi.fid FROM add_border_results_more_info mi
				WHERE mi.typ = 'S' AND mi.act = 'T'
			);

			-- This can not be null when sent back ngsiOpenApi for now we use oppdateringsdato'
			UPDATE topo_ar5ngis.edge_attributes
			SET verifiseringsdato = oppdateringsdato
			WHERE verifiseringsdato IS NULL AND identifikasjon_lokal_id IN (
				SELECT mi.fid FROM add_border_results_more_info mi
				WHERE mi.typ = 'B' AND mi.act = 'U'
			);


			-- This can not be null when sent back ngsiOpenApi for now we use oppdateringsdato'
			-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/165
			UPDATE topo_ar5ngis.edge_attributes
			SET datafangstdato = oppdateringsdato
			WHERE datafangstdato IS NULL AND identifikasjon_lokal_id IN (
				SELECT mi.fid FROM add_border_results_more_info mi
				WHERE mi.typ = 'B' AND mi.act = 'U'
			);

			INSERT INTO temp_result(fid,typ,act)
			SELECT mi.fid::uuid, mi.typ, mi.act
			FROM pg_temp.add_border_results_more_info mi;

		    DROP TABLE IF EXISTS pg_temp.add_border_results_more_info;

			-- IF use lock code lock is OK
			IF do_locking THEN

				-- Get affected area after update, we need this is case this new area been tocuhed while have working here
				-- First get directly mbr with bouding box
				SELECT ST_Envelope(ST_Collect(f.mbr))
				FROM
				topo_ar5ngis_sysdata_webclient.face f
				WHERE featureset_bbox && f.mbr
				INTO  mbr_direct_bbox_area_after_update;

				-- Then use mbr_direct_bbox_area_after_update to get indirectly intersection boxes, to get a secure buffer between other jobs
				SELECT ST_Envelope(ST_Collect(f.mbr))
				FROM
				topo_ar5ngis_sysdata_webclient.face f
				WHERE mbr_direct_bbox_area_after_update && f.mbr
				INTO  mbr_indirect_bbox_area_after_update;

				cmd := Format('SELECT topo_ar5ngis.post_check_lock_area(%L,%L)',lock_id,mbr_indirect_bbox_area_after_update);
				EXECUTE cmd into post_check_lock_area_test;


				RAISE NOTICE 'For lockid % call_result_post_check is %',lock_id, call_result_post_check;

				-- We will get a result for each sql back. We sent only so inly need to check that value two is true
				IF post_check_lock_area_test IS FALSE THEN

					cmd := Format('SELECT topo_ar5ngis.post_mark_failed_lock_area(%L)',lock_id);
					EXECUTE cmd;

					RAISE NOTICE 'For lockid % topo_ar5ngis.post_mark_failed_lock_area is %',lock_id, cmd;

					-- The job is already mark as failed so we roll back
					RAISE EXCEPTION 'Do rollback because of post_check_lock_area failed at timeofday:% topo_ar5ngi with lock_id % for featureset_bbox %',
					Timeofday(), lock_id, ST_AsText(featureset_bbox,10);


				ELSE

					-- perform topo_ar5ngis.post_mark_done_lock_area(lock_id);
					-- COMMIT;
					-- TODO We should have done a commit here but that fails with an error since this function then we have to a procedure,
					-- We may do that in the future.

					-- We need to mark this job as OK to not have jobs marked as NOT DONE in the system for this bbox
					-- This should have been done by the caller but we cannot trust that caller
					-- to do a commit here for instance if we we get an network error

					-- So now we now mark as done OK, even all data not commit yet
					-- TODO checkout if we can use SAVEPOINT

					cmd := Format('SELECT topo_ar5ngis.post_mark_done_lock_area(%L)',lock_id);
					EXECUTE cmd;

					RAISE NOTICE 'For lockid % topo_ar5ngis.post_mark_done_lock_area is %',lock_id, call_result_post_check;


				END IF;

			END IF;




			RETURN QUERY SELECT s.fid, s.typ, s.act, s.frm FROM temp_result s;


		EXCEPTION WHEN OTHERS THEN

			IF do_locking THEN

				cmd := Format('SELECT topo_ar5ngis.post_mark_failed_lock_area(%L)',lock_id);
				EXECUTE cmd;

	        	GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT, v_context = PG_EXCEPTION_CONTEXT;
				RAISE EXCEPTION 'Do rollback at topo_ar5ngis.add_border with lock_id % for featureset_bbox % , state  : % message: % detail : % hint : % context: %',
				lock_id, ST_AsText(featureset_bbox,10), v_state, v_msg, v_detail, v_hint, v_context;
				ROLLBACK;
			ELSE
	        	GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT, v_context = PG_EXCEPTION_CONTEXT;
				RAISE EXCEPTION 'Do rollback at topo_ar5ngis.add_border with lock_id % for featureset_bbox % , state  : % message: % detail : % hint : % context: %',
				lock_id, ST_AsText(featureset_bbox,10), v_state, v_msg, v_detail, v_hint, v_context;
				ROLLBACK;
			END IF;
		END;

	ELSE
		RAISE EXCEPTION 'Failed to call topo_ar5ngis.pre_check_lock_area with lock_id % for featureset_bbox %',
		lock_id, ST_AsText(featureset_bbox,10);
	END IF;

END;
$BODY$ LANGUAGE 'plpgsql';

--{
CREATE OR REPLACE FUNCTION topo_ar5ngis.colMapForFaceAttributes()
RETURNS JSONB AS $BODY$
	SELECT $${
		"featuretype": [ "featuretype" ],
		"identifikasjon_lokal_id": [ "identifikasjon", "lokalId" ],
		"identifikasjon_navnerom": [ "identifikasjon", "navnerom" ],
		"identifikasjon_versjon_id": [ "identifikasjon", "versjonId" ],
		"datafangstdato": [ "datafangstdato" ],
		"kartstandard": [ "kartstandard" ],
		"informasjon": [ "informasjon" ],
		"verifiseringsdato": [ "verifiseringsdato" ],
		"oppdateringsdato": [ "oppdateringsdato" ],
		"opphav": [ "opphav" ],
		"kvalitet.maalemetode": [ "kvalitet", "målemetode" ],
		"kvalitet.noyaktighet": [ "kvalitet", "nøyaktighet" ],
		"kvalitet.synbarhet": [ "kvalitet", "synbarhet" ],
		"arealtype": [ "arealtype" ],
		"treslag": [ "treslag" ],
		"skogbonitet": [ "skogbonitet" ],
		"grunnforhold": [ "grunnforhold" ],
		"registreringsversjon.produkt": [ "registreringsversjon", "produkt" ],
		"registreringsversjon.versjon": [ "registreringsversjon", "versjon" ],
		"geometry_properties_position": [ "geometry_properties", "position" ]
	}$$::jsonb;
$BODY$ LANGUAGE 'sql' IMMUTABLE;
--}

--{
CREATE OR REPLACE FUNCTION topo_ar5ngis.colMapForEdgeAttributes()
RETURNS JSONB AS $BODY$
	SELECT $${
		"featuretype": [ "featuretype" ],
		"identifikasjon_lokal_id": [ "identifikasjon", "lokalId" ],
		"identifikasjon_navnerom": [ "identifikasjon", "navnerom" ],
		"identifikasjon_versjon_id": [ "identifikasjon", "versjonId" ],
		"datafangstdato": [ "datafangstdato" ],
		"verifiseringsdato": [ "verifiseringsdato" ],
		"oppdateringsdato": [ "oppdateringsdato" ],
		"opphav": [ "opphav" ],
		"kvalitet.maalemetode": [ "kvalitet", "målemetode" ],
		"kvalitet.noyaktighet": [ "kvalitet", "nøyaktighet" ],
		"kvalitet.synbarhet": [ "kvalitet", "synbarhet" ],
		"avgrensing_type": [ "avgrensingType" ],
		"registreringsversjon.produkt": [ "registreringsversjon", "produkt" ],
		"registreringsversjon.versjon": [ "registreringsversjon", "versjon" ]
	}$$::jsonb;
$BODY$ LANGUAGE 'sql' IMMUTABLE;
--}

--{
CREATE OR REPLACE FUNCTION topo_ar5ngis.update_features(
	featureset JSONB,
	featureset_bbox GEOMETRY,
	version_column text DEFAULT 'identifikasjon_versjon_id',
	-- to turn on and of locking code
	do_locking boolean DEFAULT true,
	-- If this not null Lines/surface/points that is not covered by this box will not be added or updated.
	-- This is typically used to speed up loading big data sets in the initial face .
	-- When all data are loaded into the database outerBarrierBboxForUpsert will in normal cases be null.
	-- This is temporary solution just to prove that by limiting the number the number of connected edges the performance is quite  stable.
	-- For more info also check out https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/228
	outer_barrier_bbox_for_upsert GEOMETRY DEFAULT NULL
)
RETURNS boolean AS
$BODY$
DECLARE
	updated BOOLEAN := false;
	-- Test with lower tolerance, this is related https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/186
	-- Test starts failing with 1e-06
	tolerance FLOAT8 := 1e-09;
	lock_id UUID;
	-- This
	mbr_direct_bbox_area_after_update GEOMETRY;
	mbr_indirect_bbox_area_after_update GEOMETRY;

	featureset_bbox_before GEOMETRY = featureset_bbox;
	featureset_bbox_before_extend GEOMETRY = featureset_bbox;
	exteriorRing_extend_box GEOMETRY;

	v_state text;
	v_msg text;
	v_detail text;
	v_hint text;
	v_context text;
	num_edges_locked int;
	num_mbr_locked int;
	cmd text;
	pre_check_lock_area_test boolean;
	post_check_lock_area_test boolean;

BEGIN


	BEGIN

		IF do_locking THEN

			lock_id := topo_ar5ngis.lock_area_for_update_id();

			-- Handle case where featureset_bbox is null
			IF featureset_bbox IS NULL THEN
	-- This work OK
				featureset_bbox_before := _postgis_index_extent('topo_ar5ngis_sysdata_webclient.face', 'mbr');
				IF featureset_bbox_before IS NULL THEN
					featureset_bbox_before := ST_SetSrid(ST_GeomFromText('POLYGON EMPTY'),4258);
				END IF;
	-- But with COALESCE it does not work, I have know idea why but featureset_bbox_before null
	--			featureset_bbox_before := COALESCE(
	--				_postgis_index_extent('topo_ar5ngis_sysdata_webclient.face', 'mbr'),
	--				ST_SetSrid(ST_GeomFromText('POLYGON EMPTY'),4258)
	--			);
			ELSE
				SELECT ST_Envelope(ST_Collect(f.mbr))
				FROM
				topo_ar5ngis_sysdata_webclient.face f
				WHERE featureset_bbox && f.mbr
				INTO  featureset_bbox_before_extend;
			END IF;

			IF featureset_bbox_before_extend IS NULL THEN
				featureset_bbox_before_extend := featureset_bbox_before;
			END IF;

			pre_check_lock_area_test := topo_ar5ngis.pre_check_lock_area(lock_id, featureset_bbox_before_extend);

			RAISE NOTICE 'For lockid % call_result_pre_check is %',lock_id, pre_check_lock_area_test;
		END IF;


	EXCEPTION WHEN OTHERS THEN
    	GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT, v_context = PG_EXCEPTION_CONTEXT;

		RAISE EXCEPTION 'Failed to call topo_ar5ngis.pre_check_lock_area with lock_id % for featureset_bbox_before_extend % , state  : % message: % detail : % hint : % context: %',
		lock_id, ST_AsEWKT(featureset_bbox_before_extend,10), v_state, v_msg, v_detail, v_hint, v_context;
	END;


	IF NOT do_locking OR pre_check_lock_area_test IS TRUE THEN
		-- If true nobody else is working is this area
		BEGIN


		-- Do some checking/logging before update on affected area
		IF do_locking THEN
			exteriorRing_extend_box = ST_ExteriorRing(featureset_bbox_before);
			exteriorRing_extend_box = ST_SetSrid(exteriorRing_extend_box,4258);

			SELECT count(*) FROM
			(
				SELECT e.geom FROM topo_ar5ngis_sysdata_webclient.edge_data e
				WHERE e.geom && exteriorRing_extend_box
				AND ST_Intersects(e.geom,exteriorRing_extend_box)
				FOR UPDATE
			) r
		 	INTO num_edges_locked;

			SELECT count(*) FROM
			(
				SELECT f.mbr FROM topo_ar5ngis_sysdata_webclient.face f
				WHERE f.mbr && exteriorRing_extend_box
				AND ST_Intersects(f.mbr,exteriorRing_extend_box)
				--FOR UPDATE
			) r
		 	INTO num_mbr_locked;
			RAISE NOTICE '% num_edges_locked and % mbr in border zone(not locked) for lockid % on topology % found featureset_bbox %',
			num_edges_locked, num_mbr_locked, lock_id, 'topo_ar5ngis_sysdata_webclient', featureset_bbox_before_extend;
		END IF;

			PERFORM topo_update.update_features(
				featureset,

				'topo_ar5ngis.face_attributes'::regclass,
				'omrade',
				'identifikasjon_lokal_id',
				topo_ar5ngis.colMapForFaceAttributes(),

				'topo_ar5ngis.edge_attributes'::regclass,
				'grense',
				'identifikasjon_lokal_id',
				topo_ar5ngis.colMapForEdgeAttributes(),

				tolerance,

				deleteUnknownFeaturesInBbox => featureset_bbox,
				roleAndPropsExtractor => 'topo_ar5ngis.feature_extract_position'::regproc,
				linealLayerVersionColumn => version_column,
				arealLayerVersionColumn => version_column,
				outerBarrierBboxForUpsert => outer_barrier_bbox_for_upsert
			);


			-- Do some checking/logging after update on affected area
			IF do_locking THEN

				-- Get affected area after update, we need this is case this new area been tocuhed while have working here
				-- First get directly mbr with bouding box
				IF featureset_bbox IS NULL OR ST_IsEmpty(featureset_bbox) THEN
					-- if no bbox we have use just have use all rows in the table
					SELECT ST_Envelope(ST_Collect(f.mbr))
					FROM
					topo_ar5ngis_sysdata_webclient.face f
					INTO  mbr_direct_bbox_area_after_update;
				ELSE
					SELECT ST_Envelope(ST_Collect(f.mbr))
					FROM
					topo_ar5ngis_sysdata_webclient.face f
					WHERE featureset_bbox && f.mbr
					INTO  mbr_direct_bbox_area_after_update;
				END IF;


				-- Then use mbr_direct_bbox_area_after_update to get indirectly intersection boxes, to get a secure buffer between other jobs
				SELECT ST_Envelope(ST_Collect(f.mbr))
				FROM
				topo_ar5ngis_sysdata_webclient.face f
				WHERE mbr_direct_bbox_area_after_update && f.mbr
				INTO  mbr_indirect_bbox_area_after_update;


				-- Check that nobody else also working been working this affected area at the same time while we have working here


				cmd := Format('SELECT topo_ar5ngis.post_check_lock_area(%L,%L)',lock_id,mbr_indirect_bbox_area_after_update);
				EXECUTE cmd into post_check_lock_area_test;

				RAISE NOTICE 'For lockid % call_result_post_check is %',lock_id, post_check_lock_area_test;


				-- We will get a result for each sql back. We sent only so inly need to check that value two is true
				IF  post_check_lock_area_test IS FALSE THEN
					-- The job is already mark as failed so we roll back
					RAISE NOTICE 'Do rollback because of post_check_lock_area failed at timeofday:% topo_ar5ngi with lock_id % for featureset_bbox %',
					Timeofday(), lock_id, ST_AsText(featureset_bbox,10);

					-- NB WIll NOT WORK
					cmd := Format('SELECT topo_ar5ngis.post_mark_failed_lock_area(%L)',lock_id);
					EXECUTE cmd into post_check_lock_area_test;

					RAISE EXCEPTION 'Do rollback because of post_check_lock_area failed at timeofday:% topo_ar5ngi with lock_id % for featureset_bbox %',
					Timeofday(), lock_id, ST_AsText(featureset_bbox,10);

				ELSE
					-- We need to mark this job as ok and done
					perform topo_ar5ngis.post_mark_done_lock_area(lock_id);
				END IF;

			END IF;

			updated := true;

		EXCEPTION WHEN OTHERS THEN
			IF do_locking THEN

	        	GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT, v_context = PG_EXCEPTION_CONTEXT;
				RAISE NOTICE 'Do rollback topo_ar5ngis.update_features with lock_id % for featureset_bbox % , state  : % message: % detail : % hint : % context: %',
				lock_id, ST_AsText(featureset_bbox,10), v_state, v_msg, v_detail, v_hint, v_context;

				-- we need to mark jobb as done, so it does not block
				cmd := Format('SELECT topo_ar5ngis.post_mark_failed_lock_area(%L)}',lock_id);
				EXECUTE cmd into post_check_lock_area_test;

				RAISE EXCEPTION 'Failed for lockid % with state % message: %', lock_id , v_state, v_msg;

			ELSE
				RAISE EXCEPTION 'Failed to call topo_ar5ngis.update_features with lock_id % for featureset_bbox %',lock_id, ST_AsText(featureset_bbox,10);
			END IF;

		END;


	END IF;
	RETURN updated;
END;
$BODY$ LANGUAGE 'plpgsql'; --}

-- A wrapper function that takes a array json array and not a table as input
-- Fails with operator overloading so added java at the end
CREATE OR REPLACE FUNCTION topo_ar5ngis.surfaces_as_geojson_java(

	-- The JSON result add_border_split as json
	-- this function is called from java
    -- [{"fid":"9e4324f7-872f-42ad-bbc7-0f1a3961496e","typ":"S","act":"M","frm":"9e4324f7-872f-42ad-bbc7-0f1a3961496e"},{"fid":"e607605f-1967-4cc5-bf11-194f02aae8f0","typ":"S","act":"M"}];

	add_border_results_jsonb TEXT,

	-- Override projection
	--
	-- If value of this parameter is not-null use the
	-- specified CRS for the output GeoJSON, reprojecting
	-- to that system all the geometries.
	--
	outSRID INT DEFAULT NULL,

	-- Overide decimal used geojson
	--
	-- The maxdecimaldigits argument may be used when calling ST_AsGeoJSON,
	-- to changed the number of decimal places used in output
	maxdecimaldigits INT DEFAULT 9

)
RETURNS TEXT AS
$BODY$  -- {
DECLARE

  geojson JSONB;

BEGIN

  CREATE TEMPORARY TABLE add_border_results(
    fid TEXT,
    typ CHAR,
    act CHAR,
    frm TEXT DEFAULT NULL
    );

  insert into add_border_results select * from jsonb_populate_recordset(NULL::add_border_results, add_border_results_jsonb::JSONB );

  alter table add_border_results add column sid serial;


  geojson := topo_ar5ngis.surfaces_as_geojson(
    'add_border_results'::regclass,
    outSRID,
    maxdecimaldigits
    );

  drop table add_border_results;

  RETURN geojson::TEXT;
END;
$BODY$ --}
LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION topo_ar5ngis.surfaces_as_geojson(
	add_border_results REGCLASS,

	-- Override projection
	--
	-- If value of this parameter is not-null use the
	-- specified CRS for the output GeoJSON, reprojecting
	-- to that system all the geometries.
	--
	outSRID INT DEFAULT NULL,

	-- Overide decimal used geojson
	--
	-- The maxdecimaldigits argument may be used when calling ST_AsGeoJSON,
	-- to changed the number of decimal places used in output
	maxdecimaldigits INT DEFAULT 9
)
RETURNS JSONB AS
$BODY$  --{
DECLARE
  sql TEXT;
  geojson JSONB;
  geojson_touched JSONB;
  surfaceLayerTable REGCLASS := 'topo_ar5ngis.face_attributes';
  surfaceLayerView REGCLASS := 'topo_ar5ngis.ngis_export_data_flate_v';
  surfaceLayerTopoGeomColumn NAME := 'omrade';
  surfaceLayerIDColumn NAME := 'identifikasjon_lokal_id';

  borderLayerTable REGCLASS := 'topo_ar5ngis.edge_attributes';
  borderLayerView REGCLASS := 'topo_ar5ngis.ngis_export_data_grense_v';
  borderLayerTopoGeomColumn NAME := 'grense';
  borderLayerIdColumn NAME := 'identifikasjon_lokal_id';

  surfaceLayerColMap JSONB := topo_ar5ngis.colMapForFaceAttributes();
  borderLayerColMap JSONB := topo_ar5ngis.colMapForEdgeAttributes();

  outputSurfaces TEXT[];

  add_border_results_as_json JSONB;

BEGIN

  sql := format(
    $$
			SELECT jsonb_agg(r.*)::text
			FROM (SELECT r.fid AS fid, r.typ, r.act FROM %1$s r) AS r
		$$,
    add_border_results
    );

  SELECT jsonb_agg(r)
    FROM (
    SELECT fid, typ, act
    FROM add_border_results
    ) r
    INTO add_border_results_as_json;
  RAISE DEBUG 'add_border_results_as_json: %', add_border_results_as_json;

  sql := format($$
			SELECT array_agg(fid ORDER BY sid)
			FROM %1$s
			WHERE typ = 'S'
		$$,
    add_border_results
    );
  RAISE DEBUG 'SQL: %', sql;
  EXECUTE sql INTO outputSurfaces;

  RAISE DEBUG 'Generating surfaces JSON';

  -- Produce the GeoJSON for new or modified surfaces
  geojson := topo_update.surfaces_as_geojson(
    surfaceLayerView,
    surfaceLayerTopoGeomColumn,
    surfaceLayerIDColumn,
    surfaceLayerColMap,

    borderLayerView,
    borderLayerTopoGeomColumn,
    borderLayerIdColumn,
    borderLayerColMap,

    -- Array of identifiers of new or modified surfaces
    -- that we want to include in the GeoJSON
    outputSurfaces,

    -- JSON feature modifier
    'topo_ar5ngis.surfaces_as_geojson_featureJsonModifier'::REGPROC,

    -- Parameter passed to JSON feature modifier
    add_border_results_as_json,

    outSRID,

    maxdecimaldigits

    );

  RETURN geojson;
END;
$BODY$ --}
LANGUAGE plpgsql;


-- Return the number nodes that are shared by 3 more edges
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/tree/182-return-nodes-that-are-shared-by-3-faces-or-more
-- Return nodes that are split by 3 surfaces or more
-- The result is transformed to given srid

--{
CREATE OR REPLACE FUNCTION topo_ar5ngis.nodes_3_more_surface_split(
	bbox GEOMETRY,
	srid_out int
)
RETURNS GEOMETRY AS
$BODY$
DECLARE
	node_list GEOMETRY;
BEGIN

	SELECT ST_Collect(nl.geom) FROM (
		SELECT n.geom,
		count(e.*) as num_edges
		FROM
		topo_ar5ngis_sysdata_webclient.node n,
		topo_ar5ngis_sysdata_webclient.edge e
		WHERE n.geom && bbox AND e.geom && bbox AND
		(n.node_id = e.start_node OR n.node_id = e.end_node)
		GROUP BY n.geom
	) nl WHERE nl.num_edges > 2 INTO node_list;

	IF node_list IS NOT NULL THEN
		node_list := ST_Transform(node_list , srid_out);
	END IF;

	RETURN node_list;
END;
$BODY$ LANGUAGE 'plpgsql'; --}

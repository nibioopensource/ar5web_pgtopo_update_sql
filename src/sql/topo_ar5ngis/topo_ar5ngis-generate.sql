SELECT * FROM topo_update.app_CreateSchema(
	'topo_ar5ngis',
	regexp_replace(
	$$
		{
			"version": "1.0",
			"topology": {
				"srid": 4258,
				/* Test with lower tolerance, this is related
				 * https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/186
				 * Test starts failing with 1e-06
				 */
				"snap_tolerance": 1e-9
			},
			"extensions": [
				"uuid-ossp"
			],
			"tables": [
				{
					"name": "sosi_kvalitet",
					"attributes": [
						{
							"name": "maalemetode",
							"type": "varchar",
							"default": "80"
						},
						{
							"name": "noyaktighet",
							"type": "integer",
							"default": "300"
						},
						{
							"name": "synbarhet",
							"type": "varchar",
							"default": "0"
						}
					]
				},
				{
					"name": "sosi_registreringsversjon",
					"attributes": [
						{
							"name": "produkt",
							"type": "varchar",
							"default": "FKB-AR5"
						},
						{
							"name": "versjon",
							"type": "varchar",
							"default": "4.5 20140301"
						}
					]
				},
				{
					"name": "face_attributes",
					"primary_key": "identifikasjon_lokal_id",
					"attributes": [
						{
							"name": "identifikasjon_lokal_id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "identifikasjon_versjon_id",
							"type": "timestamptz",
							"default": {
								"type": "function",
								"name": "now"
							}
						},
						{
							"name": "featuretype",
							"type": "varchar",
							"default": "ArealressursFlate"
						},
						{
							"name": "identifikasjon_navnerom",
							"type": "varchar"
						},
						{
							"name": "kartstandard",
							"type": "text",
							"default": "AR5"
						},
						{
							"name": "datafangstdato",
							"type": "date"
						},
						{
							"name": "informasjon",
							"type": "text"
						},
						{
							"name": "verifiseringsdato",
							"type": "date"
						},
						{
							"name": "oppdateringsdato",
							"type": "timestamptz",
							"default": "now()"
						},
						{
							"name": "opphav",
							"type": "varchar",
							"default": "NIBIO_Client"
						},
						{
							"name": "arealtype",
							"type": "smallint",
							"default": "99"
						},
						{
							"name": "treslag",
							"type": "smallint",
							"default": "98"
						},
						{
							"name": "skogbonitet",
							"type": "smallint",
							"default": "98"
						},
						{
							"name": "grunnforhold",
							"type": "smallint",
							"default": "98"
						},
						{
							"name": "geometry_properties_position",
							"type": "geometry",
							"modifiers": ["Point","4258"]
						},
						{
							"name": "kvalitet",
							"type": "sosi_kvalitet",
							"default": [ 53, 200, 0 ]
						},
						{
							"name": "registreringsversjon",
							"type": "sosi_registreringsversjon",
							"default": [ "FKB-AR5", "4.5 20140301" ]
						}
					],
					"topogeom_columns": [
						{
							"name": "omrade",
							"type": "areal",
							/* FIXME: overlaps is here allowed
							 * as a workaround for bug
							 * https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/187
							 */
							"allow_overlaps": true,
							/* FIXME: gaps is here allowed
							 * as a workaround for bug
							 * https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/190
							 */
							"allow_gaps": true
						}
					]
				},
				{
					"name": "edge_attributes",
					"primary_key": "identifikasjon_lokal_id",
					"attributes": [
						{
							"name": "identifikasjon_lokal_id",
							"type": "uuid",
							"default": {
								"type": "function",
								"name": "uuid_generate_v1"
							}
						},
						{
							"name": "identifikasjon_versjon_id",
							"type": "timestamptz",
							"default": {
								"type": "function",
								"name": "now"
							}
						},
						{
							"name": "featuretype",
							"type": "varchar",
							"default": "ArealressursGrense"
						},
						{
							"name": "identifikasjon_navnerom",
							"type": "varchar",
							"comment": "Will have different values based on where in Norway"
						},
						{
							"name": "datafangstdato",
							"type": "date"
						},
						{
							"name": "verifiseringsdato",
							"type": "date"
						},
						{
							"name": "oppdateringsdato",
							"type": "timestamptz",
							"default": {
								"type": "function",
								"name": "now"
							}
						},
						{
							"name": "opphav",
							"type": "varchar",
							"default": "NIBIO_Client"
						},
						{
							"name": "kvalitet",
							"type": "sosi_kvalitet",
							"default": [ 80, 200, 0]
						},
						{
							"name": "registreringsversjon",
							"type": "sosi_registreringsversjon",
							"default": [ "FKB-AR5", "4.5 20140301" ]
						},
						{
							"name": "avgrensing_type",
							"type": "smallint",
							"default": "9300",
							"allowed_values": [ 4206, 9300, 3310, 9111, 7200, 3001 ]
						}
					],
					"topogeom_columns": [
						{
							"name": "grense",
							"type": "lineal",
							/* FIXME: overlaps is here allowed
							 * as a workaround for bug
							 * https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/187
							 */
							"allow_overlaps": true,
							/* FIXME: gaps is here allowed
							 * as a workaround for bug
							 * https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/190
							 */
							"allow_gaps": true
						}
					]
				}
			],
			"surface_layer": {
				"table_name": "face_attributes",
				"geo_column": "omrade",
				"version_column": "identifikasjon_version_id"
			},
			"border_layer": {
				"table_name": "edge_attributes",
				"geo_column": "grense",
				"version_column": "identifikasjon_version_id"
			},
			"operations": {
				"AddBordersSplitSurfaces": {
					"parameters": {
						"default": {
							"snapTolerance": {
								"units": 1e-10,
								"unitsAreMeters": false
							},
							"minAllowedNewSurfacesArea": {
								"units": 200,
								"unitsAreMeters": true
							},
							"maxAllowedSplitSurfacesCount": 1,
							"maxAllowedNewSurfacesCount": 2
						},
						"forOpenLines": {
							"minAllowedNewSurfacesArea": {
								"units": 10,
								"unitsAreMeters": true
							},
							"maxAllowedNewSurfacesCount": 3
						}
					}
				}
			}
		}
	$$,
	-- strip comments
	'\/\*.*?\*\/', '', 'g'
	)::jsonb
);

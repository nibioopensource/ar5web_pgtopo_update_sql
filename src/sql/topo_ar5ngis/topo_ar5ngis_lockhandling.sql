-- create schema for topo_ar5ngis data, tables, ....

-- NOTE: currently depends on topo_ar5ngis.sql

-- Remove this hack, when this works test/regress/run_test.pl --extension --topology --uuid-ossp test/regress/integration/update_features_java_ar5_mapping_01
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE SCHEMA IF NOT EXISTS topo_ar5ngis;
-- give puclic access
GRANT USAGE ON SCHEMA topo_ar5ngis TO public;


-- this is a temp table used to lock area befor a topology update
CREATE UNLOGGED TABLE topo_ar5ngis.lock_area_for_update (
	id UUID PRIMARY KEY,
	started_sync TIMESTAMPTZ default clock_timestamp(),
	done_sync TIMESTAMPTZ,
	worker_name TEXT,
	result_ok boolean DEFAULT false,
	error_info TEXT,
	locked_area_before public.geometry(Geometry,4258) NOT NULL,
	locked_area_after public.geometry(Geometry,4258) NOT NULL DEFAULT ST_SetSrid('POLYGON EMPTY'::geometry,4258)
);

-- Fails on tests when role does not exist
-- ALTER TABLE topo_ar5ngis.lock_area_for_update OWNER TO topo_ar5;    

-- Add grid table and content to be used by advisory lock
-- TODO we need way to generate this in a more dynamic way
CREATE TABLE topo_ar5ngis.static_meta_grid_metagrid_0001 (
    id serial NOT NULL,
    geo public.geometry(Geometry,4258)
);
INSERT INTO topo_ar5ngis.static_meta_grid_metagrid_0001(geo)
SELECT ST_SetSrid(ST_Transform(s.geom,4258),4258) FROM (
SELECT (ST_SquareGrid(100000,
ST_transform(ST_SetSrid('POLYGON((4.087525900107935 57.759005268413134,4.087525900107935 71.38487872916933,31.761491124439676 71.38487872916933,31.761491124439676 57.759005268413134,4.087525900107935 57.759005268413134))'::geometry,4258),25833) 
)).*) AS s;
CREATE INDEX ON topo_ar5ngis.static_meta_grid_metagrid_0001 USING gist(geo);

-- Added gist indexes locltable
CREATE INDEX ON topo_ar5ngis.lock_area_for_update USING gist(locked_area_before);
CREATE INDEX ON topo_ar5ngis.lock_area_for_update USING gist(locked_area_after);

-- topo_ar5ngis.new_lock_area_for_update is more like safety system in cause of bugs in the code 
-- that handles locking is not correctly implemented when updating Postgis Topology data.

-- This function is used to check that we do have do not have any running jobs is the same area
-- when new job should start. A running job is a job where lockarea.done_sync is null.

-- If we have a error i code and the code and lockarea.done_sync is not set this area will blocked until
-- topo_ar5ngis.clear_pre_check_lock_area is called 
-- TODO find a way better way to handle this ???

CREATE OR REPLACE FUNCTION topo_ar5ngis.new_lock_area_for_update()
  RETURNS trigger AS
  $$
    DECLARE
      locknum_before bigint;
    BEGIN

      SELECT Count(*)
      INTO locknum_before
      FROM topo_ar5ngis.lock_area_for_update lockarea
      WHERE lockarea.done_sync is NULL 
      AND ST_Intersects(lockarea.locked_area_before, NEW.locked_area_before)
      AND lockarea.id != NEW.id;
      IF locknum_before > 0 THEN
        RAISE EXCEPTION 'topo_ar5ngis.new_lock_area_for_update check % found % workers in this area', NEW.id, locknum_before USING HINT='LOCKAREA_FOR_UPDATE_01';
      END IF;

      RETURN NEW;
    END;
  $$
  LANGUAGE 'plpgsql';

-- Trigger new_lock_area_for_update on insert in topo_ar5ngis.lock_area_for_update
CREATE CONSTRAINT TRIGGER topo_ar5ngis_new_job AFTER INSERT ON topo_ar5ngis.lock_area_for_update FOR EACH ROW EXECUTE FUNCTION topo_ar5ngis.new_lock_area_for_update();


-- topo_topo_ar5ngis.after_lock_area_for_update is more like safety system in cause of bugs in the code 
-- that handles locking is not correctly implemented when updating Postgis Topology data.

-- This function is used to check that that no other jobs have been running in the same area
-- when a job is done. A running job is a job where lockarea.done_sync is null.

-- If we have a error i code and the code and lockarea.done_sync is not set this area will blocked until
-- topo_ar5ngis.clear_pre_check_lock_area is called 
-- TODO find a way better way to handle this ???

CREATE OR REPLACE FUNCTION topo_ar5ngis.after_lock_area_for_update()
  RETURNS trigger AS
  $$
    DECLARE
      locknum_after bigint;
      locknum_other_done_jos bigint;
    BEGIN

      SELECT Count(*)
      INTO locknum_after
      FROM topo_ar5ngis.lock_area_for_update lockarea
      WHERE lockarea.done_sync is NULL 
      AND ST_Intersects(lockarea.locked_area_before, NEW.locked_area_after)
      AND lockarea.id != NEW.id;
      IF locknum_after > 0 THEN
      	-- Just insert into log table to know that in failed (_id can not be reused
        RAISE EXCEPTION 'new_lock_area_for_update % found % workers in this area', NEW.id, locknum_after
       	USING HINT='LOCKAREA_FOR_UPDATE_02';
      END IF;

	-- Check that nobody else is working or has been workinng in the same extend area at the same time
	SELECT count(a.id)
	FROM
	topo_ar5ngis.lock_area_for_update a,
	topo_ar5ngis.lock_area_for_update b
	WHERE a.id != NEW.id
	AND ST_Intersects(a.locked_area_before,NEW.locked_area_after)
	AND b.id = NEW.id AND a.started_sync >= b.started_sync AND a.started_sync < clock_timestamp()
	INTO  locknum_other_done_jos;

      IF locknum_other_done_jos > 0 THEN
      	-- Just insert into log table to know that in failed (_id can not be reused
        RAISE EXCEPTION 'topo_ar5ngis.after_lock_area_for_update % found % workers that has been is area and is done', NEW.id, locknum_other_done_jos
        USING HINT='LOCKAREA_FOR_UPDATE_03';
      END IF;

      RETURN NEW;
    END;
  $$
  LANGUAGE 'plpgsql';

-- trigger topo_ar5ngis.after_lock_area_for_update after update in row in topo_ar5ngis.lock_area_for_update 
CREATE CONSTRAINT TRIGGER topo_ar5ngis_after_job AFTER UPDATE ON topo_ar5ngis.lock_area_for_update FOR EACH ROW EXECUTE FUNCTION topo_ar5ngis.after_lock_area_for_update();

 
-- this is a history table to keep track of updates done
CREATE TABLE topo_ar5ngis.lock_area_for_update_history (
	id UUID PRIMARY KEY,
	started_sync TIMESTAMPTZ default clock_timestamp(),
	done_sync TIMESTAMPTZ,
	worker_name TEXT NOT NULL,
	result_ok boolean DEFAULT false,
	error_info TEXT,
	locked_area_before public.geometry(Geometry,4258) NOT NULL,
	locked_area_after public.geometry(Geometry,4258)
); 

-- Fails on tests when role does not exist
-- ALTER TABLE topo_ar5ngis.lock_area_for_update_history OWNER TO topo_ar5;
 
-- Will clear jobs for running jobs at server startup
-- TODO may cause problems if we run in multi server env.
-- We do not need this since locks will be done when database session ends ??? 
--{
CREATE OR REPLACE FUNCTION topo_ar5ngis.clear_pre_check_lock_area(
)
RETURNS INT AS
$BODY$
DECLARE
	num_rows_deleted int := 0;

BEGIN

--	LOCK TABLE topo_ar5ngis.lock_area_for_update IN ACCESS EXCLUSIVE MODE;

	-- Since no jobs are running we can move all jobs to history table
	WITH timed_out_jobs as (
		DELETE FROM topo_ar5ngis.lock_area_for_update
		WHERE started_sync < (clock_timestamp() - '36000s'::interval)
		RETURNING *
	)
	INSERT INTO topo_ar5ngis.lock_area_for_update_history(id, started_sync, done_sync, worker_name, result_ok, error_info, locked_area_before, locked_area_after)
	SELECT id, started_sync, done_sync, worker_name,
	result_ok,
	'topo_ar5ngis.clear_pre_check_lock_area at server startup move all jobs to history table at '||clock_timestamp()  AS error_info,
	locked_area_before, locked_area_after FROM timed_out_jobs;


	GET DIAGNOSTICS num_rows_deleted = ROW_COUNT;

    RETURN num_rows_deleted;

END;
$BODY$ LANGUAGE 'plpgsql'; --}

-- Will check that this area is not locked before starting to update the Postgis Topology structure
--{
CREATE OR REPLACE FUNCTION topo_ar5ngis.pre_check_lock_area(
	_lockid UUID, -- a unique generated by the caller
	_bbox_area_to_update GEOMETRY, -- a bounding box for the update area
	_worker_name TEXT DEFAULT 'bg_job',
	_max_running_secs TEXT DEFAULT '36000s'

)
RETURNS BOOLEAN AS
$BODY$
DECLARE
	ok_to_update BOOLEAN;
	max_lock_try int = 10; 
	-- we need to test with different values here to check how things works
	-- On pre check we should be able to wait some seconds to see if the area becomes free, 
	-- but we need to test that more so for now we only try one and the application has to do a retry.
BEGIN

	
	ok_to_update = topo_ar5ngis.lock_area(_bbox_area_to_update,max_lock_try);

	-- Insert in to the log table so every body else know that I working here
	INSERT INTO topo_ar5ngis.lock_area_for_update(id,started_sync,locked_area_before,worker_name)
	values (_lockid,clock_timestamp(),_bbox_area_to_update,_worker_name);

    RETURN ok_to_update;

END;
$BODY$ LANGUAGE 'plpgsql'; --}

-- Will check that the updated area is ok after update
--{
CREATE OR REPLACE FUNCTION topo_ar5ngis.post_check_lock_area(
	_lockid UUID, -- a unique generated by the caller
	_new_bbox_area_after_update GEOMETRY -- a bounding box for the update area
)
RETURNS BOOLEAN AS
$BODY$
DECLARE
	ok_after_update BOOLEAN;
	in_geo GEOMETRY;
	max_lock_try int = 1; 
	-- we need to test with different values here to check how things works
	-- On the post we should only test one time if somebody else has started to work in the ths area was affected our area we have to give up

	
BEGIN
	
	in_geo = COALESCE (_new_bbox_area_after_update, ST_SetSrid('POLYGON EMPTY'::geometry,4258));

	ok_after_update = topo_ar5ngis.lock_area(in_geo,max_lock_try);

	UPDATE topo_ar5ngis.lock_area_for_update
	SET
		locked_area_after = in_geo
	WHERE id = _lockid;

	RETURN ok_after_update;
END;
$BODY$ LANGUAGE 'plpgsql'; --}


CREATE OR REPLACE FUNCTION topo_ar5ngis.lock_area(
	_bbox_area_to_lock GEOMETRY,
	_max_lock_try int -- If max lock try is one only test time and return if there if there are other working the area
)
RETURNS BOOLEAN AS
$BODY$
DECLARE
	num_rows_intersecting int := 0;
	error_text text;

	counter int = 0;
	max_counter int = 100;
	grid_id_list int[];
	id_to_lock int;
	grid_id_list_locked int[] = '{}';

	my_pid int;
	pg_advisory_lock_status boolean;
	
	lock_try_loop_sleep_time int = 1; -- this is only used if _max_lock_try is bigger than one
	
	
	lock_result BOOLEAN := false;
	pg_advisory_unlock_status BOOLEAN;	

BEGIN

	SELECT pg_backend_pid() INTO my_pid;

	SELECT ARRAY(SELECT gr.id
	FROM topo_ar5ngis.static_meta_grid_metagrid_0001 gr
	WHERE ST_Intersects(gr.geo,_bbox_area_to_lock)) INTO grid_id_list;
	
	RAISE NOTICE 'start lock_area for % ids in grid_id_list % needed for bbox % for pid % ', 
	array_length(grid_id_list,1), grid_id_list, _bbox_area_to_lock, my_pid;

	-- DEFAULT GRID ID is 0 (make global value for default value)
	IF array_length(grid_id_list, 1) IS NULL THEN
		 grid_id_list = '{0}'; 
	END IF;
	

	FOREACH id_to_lock IN ARRAY grid_id_list
	LOOP
		counter = 1;
		
		WHILE counter <= _max_lock_try loop
			--this maybe cause some random issue about what thread that wins that may change the result of the tests but I don understand way ???
			--like here https://gitlab.com/nibioopensource/pgtopo_update_sql/-/jobs/3164377299
			--The pipe line failing if not using locks so mI thinks it's random issuse
			
			--We also have the same error in this run here but here we use dblink and not advisory locks
			--https://gitlab.com/nibioopensource/pgtopo_update_sql/-/jobs/3105132270
			
		
				
			RAISE NOTICE 'CHECK/WAITS FOR LOCK for id_to_lock % in loop % of max % loops ', id_to_lock, counter, _max_lock_try;
		
			-- Just to test to use pg_advisory_lock instead off pg_try_advisory_lock and then not give up and this loop always run only one time.  
			-- EXECUTE pg_advisory_lock(id_to_lock);
			-- When we get here we know that have a valid lock.
			-- pg_advisory_lock_status = true;
			-- pg_advisory_lock can not be used
		
			SELECT pg_try_advisory_lock(id_to_lock) INTO pg_advisory_lock_status;
			
			-- If true exit
			IF pg_advisory_lock_status THEN 
				EXIT;
			END IF;
			
			-- update loop counter
			counter = counter + 1;
			execute pg_sleep(lock_try_loop_sleep_time);
		END loop;
		
		-- if not a valid lock do not check other and exit main loop
		IF (pg_advisory_lock_status = FALSE) THEN 
			lock_result = false;			
			EXIT;
		ELSE 
			lock_result = true;
			grid_id_list_locked = array_append(grid_id_list_locked, id_to_lock);
		END IF;
	END LOOP;	

	IF lock_result = false THEN
		FOREACH id_to_lock IN ARRAY grid_id_list_locked
		LOOP
			SELECT pg_advisory_unlock(id_to_lock) INTO pg_advisory_unlock_status;
		END LOOP;
		RAISE NOTICE 'FAIL status is % done lock_area % locks for grid_id_list % needed for bbox % for pid % with _max_lock_try %', 
		lock_result, counter, grid_id_list, _bbox_area_to_lock, my_pid, _max_lock_try;
	ELSE
		RAISE NOTICE 'OK status is % done lock_area % locks for grid_id_list % needed for bbox % for pid % with _max_lock_try % ', 
		lock_result, counter, grid_id_list, _bbox_area_to_lock, my_pid, _max_lock_try;
	END IF;



	RETURN lock_result;



END;
$BODY$ LANGUAGE 'plpgsql'; --}



-- Set done
--{
CREATE OR REPLACE FUNCTION topo_ar5ngis.post_mark_done_lock_area(
	_lockid UUID
) RETURNS void AS
$BODY$
DECLARE
	bbox_area_to_release geometry;
BEGIN

	UPDATE topo_ar5ngis.lock_area_for_update
	SET
		done_sync = clock_timestamp(),
		result_ok = true
	WHERE id = _lockid;

	SELECT ST_union(locked_area_before,locked_area_after)
	FROM topo_ar5ngis.lock_area_for_update 
	WHERE id = _lockid
	INTO bbox_area_to_release;

	execute topo_ar5ngis.release_lock_area(_lockid,bbox_area_to_release);

	RAISE NOTICE 'topo_ar5ngis.post_mark_done_lock_area for lockid % set OK',_lockid;

	-- move history table will affect running jobs because it will do a rollback
	-- NB this should not be here

END;
$BODY$ LANGUAGE 'plpgsql'; --}

-- Set done
--{
CREATE OR REPLACE FUNCTION topo_ar5ngis.post_mark_failed_lock_area(
	_lockid UUID
) RETURNS void AS
$BODY$
DECLARE
	bbox_area_to_release geometry;
BEGIN

	LOCK TABLE topo_ar5ngis.lock_area_for_update;

	UPDATE topo_ar5ngis.lock_area_for_update
	SET
		done_sync = clock_timestamp()
	WHERE id = _lockid;

	SELECT ST_union(locked_area_before,locked_area_after)
	FROM topo_ar5ngis.lock_area_for_update 
	WHERE id = _lockid
	INTO bbox_area_to_release;


	execute topo_ar5ngis.release_lock_area(_lockid,bbox_area_to_release);

	RAISE NOTICE 'topo_ar5ngis.post_mark_done_lock_area for lockid % set OK',_lockid;
	
	-- move history table will affect running jobs because it will do a rollback
	
	WITH done_jobs as (
		DELETE FROM topo_ar5ngis.lock_area_for_update WHERE id = _lockid RETURNING *
	)
	INSERT INTO topo_ar5ngis.lock_area_for_update_history(id, started_sync, done_sync, worker_name, result_ok, error_info, locked_area_before, locked_area_after)
	SELECT id, started_sync, done_sync, worker_name, result_ok, 
	'topo_ar5ngis.release_lock_area failed',
	locked_area_before, bbox_area_to_release FROM done_jobs;

END;
$BODY$ LANGUAGE 'plpgsql'; --}


CREATE OR REPLACE FUNCTION topo_ar5ngis.release_lock_area(
	_lockid UUID, -- a unique generated by the caller
	_bbox_area_to_release geometry
)
RETURNS void AS
$BODY$
DECLARE
	num_rows_intersecting int := 0;
	error_text text;

	counter int = 0;
	max_counter int = 100;
	grid_id_list int[];
	id_to_release int;
	pg_advisory_unlock_status boolean = false;
	
	my_pid int;
	num_pid_locks int;
	

BEGIN


	SELECT ARRAY(SELECT gr.id
	FROM topo_ar5ngis.static_meta_grid_metagrid_0001 gr
	WHERE ST_Intersects(gr.geo,_bbox_area_to_release))  INTO grid_id_list;

	select pg_backend_pid() INTO my_pid;


	-- DEFAULT GRID ID is 0 (make global value for default value)
	IF array_length(grid_id_list, 1) IS NULL THEN
		 grid_id_list = '{0}'; 
	ELSE 
		RAISE NOTICE 'start release_lock_area for % ids grid_id_list % needed for bbox % for pid % ',
		array_length(grid_id_list,1), grid_id_list, _bbox_area_to_release, my_pid;
	END IF;
	

	FOREACH id_to_release IN ARRAY grid_id_list
	LOOP
	
		num_pid_locks = 1;
		
		while num_pid_locks > 0 loop
		
			SELECT pg_advisory_unlock(id_to_release) INTO pg_advisory_unlock_status;
			IF pg_advisory_unlock_status THEN 
				counter = counter + 1;
			END IF;
			
			--perform pg_sleep(1);
			
			SELECT count(*) 
			FROM  pg_locks 
			WHERE pid = my_pid AND objid = id_to_release AND granted = true AND locktype = 'advisory' 
			INTO num_pid_locks;
	

		end loop;
	
	END LOOP;	


	RAISE NOTICE 'done release_lock_area % locks for grid_id_list % needed for bbox % for pid % ', 
	counter, grid_id_list, _bbox_area_to_release, my_pid;




END;
$BODY$ LANGUAGE 'plpgsql'; --}
 
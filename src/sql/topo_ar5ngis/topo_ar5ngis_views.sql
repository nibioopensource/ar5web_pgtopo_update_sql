
--DROP VIEW IF EXISTS topo_ar5ngis.ngis_export_data_flate_v CASCADE;

CREATE OR REPLACE VIEW topo_ar5ngis.ngis_export_data_flate_v AS    
SELECT 
--    'ArealressursFlate' AS featuretype, 
-- TODO create type    
--    json_build_object(
--        'navnerom', identifikasjon_navnerom, 
--        'lokal_id', identifikasjon_lokal_id,
--        'versjon_id', identifikasjon_versjon_id
--    ) as identifikasjon,
	featuretype,
    identifikasjon_lokal_id::UUID,
    identifikasjon_navnerom,
    identifikasjon_versjon_id::timestamp,
    kartstandard,
    COALESCE(informasjon,'') AS informasjon,
	datafangstdato,
    verifiseringsdato,
    oppdateringsdato::timestamp,
    opphav,  
    kvalitet,
	arealtype::VARCHAR,
	treslag::VARCHAR,
	skogbonitet::VARCHAR,
	grunnforhold::VARCHAR,
	registreringsversjon,
	omrade
	FROM topo_ar5ngis.face_attributes;

--DROP VIEW IF EXISTS topo_ar5ngis.ngis_export_data_grense_v CASCADE;

CREATE OR REPLACE VIEW topo_ar5ngis.ngis_export_data_grense_v AS    
SELECT 
--    'ArealressursGrense' AS featuretype, 
-- TODO create type    
--    json_build_object(
--        'navnerom', identifikasjon_navnerom, 
--        'lokal_id', identifikasjon_lokal_id,
--        'versjon_id', identifikasjon_versjon_id
--    ) as identifikasjon,
	featuretype,
    identifikasjon_lokal_id,
    identifikasjon_navnerom,
    identifikasjon_versjon_id::timestamp,
	datafangstdato,
    verifiseringsdato,
    oppdateringsdato::timestamp,
    opphav,  
    kvalitet,
	avgrensing_type::VARCHAR,
	registreringsversjon,
	grense
FROM topo_ar5ngis.edge_attributes;
	

--  psql -t test_pg_update -f /Users/lop/dev/git/topologi/pgtopo_update_sql/test/data/topo_ar5ngis_views.sql; psql test_pg_update  -tq -c'select * from topo_ar5ngis.ngis_export_data_t3_json'>/tmp/t.sql;cat /tmp/t.sql|jq

CREATE OR REPLACE VIEW topo_ar5ngis.webclient_flate_topojson_flate_v AS
SELECT face_attributes.identifikasjon_lokal_id AS id,
     face_attributes.arealtype,
     face_attributes.treslag,
     face_attributes.skogbonitet,
     face_attributes.grunnforhold,
     'SFKB'::text AS reinbeitebruker_id,
     face_attributes.verifiseringsdato,
     face_attributes.opphav,
     (kvalitet).synbarhet::integer as synbarhet,
     face_attributes.informasjon,
     face_attributes.omrade,
     1 AS editable
FROM topo_ar5ngis.face_attributes;

--{
CREATE OR REPLACE FUNCTION topo_ar5ngis.colMapForFaceAttributes()
RETURNS JSONB AS $BODY$
	SELECT $${
		"featuretype": [ "featuretype" ],
		"identifikasjon_lokal_id": [ "identifikasjon", "lokalId" ],
		"identifikasjon_navnerom": [ "identifikasjon", "navnerom" ],
		"identifikasjon_versjon_id": [ "identifikasjon", "versjonId" ],
		"datafangstdato": [ "datafangstdato" ],
		"kartstandard": [ "kartstandard" ],
		"informasjon": [ "informasjon" ],
		"verifiseringsdato": [ "verifiseringsdato" ],
		"klassifiseringsmetode": [ "klassifiseringsmetode" ],
		"oppdateringsdato": [ "oppdateringsdato" ],
		"opphav": [ "opphav" ],
		"kvalitet.datafangstmetode": [ "kvalitet", "datafangstmetode" ],
		"kvalitet.noyaktighet": [ "kvalitet", "nøyaktighet" ],
		"kvalitet.synbarhet": [ "kvalitet", "synbarhet" ],
		"arealtype": [ "arealtype" ],
		"treslag": [ "treslag" ],
		"skogbonitet": [ "skogbonitet" ],
		"grunnforhold": [ "grunnforhold" ],
		"registreringsversjon": [ "registreringsversjon" ]
	}$$::jsonb;
$BODY$ LANGUAGE 'sql' IMMUTABLE;
--}

--{
CREATE OR REPLACE FUNCTION topo_ar5ngis.colMapForEdgeAttributes()
RETURNS JSONB AS $BODY$
	SELECT $${
		"featuretype": [ "featuretype" ],
		"identifikasjon_lokal_id": [ "identifikasjon", "lokalId" ],
		"identifikasjon_navnerom": [ "identifikasjon", "navnerom" ],
		"identifikasjon_versjon_id": [ "identifikasjon", "versjonId" ],
		"datafangstdato": [ "datafangstdato" ],
		"verifiseringsdato": [ "verifiseringsdato" ],
		"oppdateringsdato": [ "oppdateringsdato" ],
		"opphav": [ "opphav" ],
		"kvalitet.datafangstmetode": [ "kvalitet", "datafangstmetode" ],
		"kvalitet.noyaktighet": [ "kvalitet", "nøyaktighet" ],
		"kvalitet.synbarhet": [ "kvalitet", "synbarhet" ],
		"avgrensing_type": [ "avgrensingType" ],
		"registreringsversjon": [ "registreringsversjon" ]
	}$$::jsonb;
$BODY$ LANGUAGE 'sql' IMMUTABLE;
--}

--{
--
-- Rewrite surface feature coming from FKB50 for use with
-- topo_update.update_features
--
CREATE OR REPLACE FUNCTION topo_ar5ngis.fkb50_rewrite_incoming_surface_feature(
	feature JSONB
)
RETURNS JSONB AS
$BODY$
DECLARE
	newfeature JSONB;
	props JSONB;
	fid TEXT;
	interior_rings JSONB[];
	binding_borders_json JSONB;
	geometry_properties JSONB := '{}'; -- start empty
	rec RECORD;
BEGIN

	-- We need to build the geometry_properties
	-- node as expected by pgtopo_update_sql version 1.0.0, which is a
	-- format like this:
	--
	--		"geometry_properties": {
	--			"exterior": [
	--				"forward_border_id1",
	--				"-backward_border_id1"
	--			],
	--			"interiors": [
	--				[
	--					"forward_border_id2",
	--					"-backward_border_id2"
	--				]
	--			    ...
	--			]
	--		}
	--
	--
	-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/blob/v1.0.0/src/sql/topo_update/function_02_update_features.sql#L72-76
	--
	-- The exterior/interiors information was put, by FKB50, in a field
	-- called "avgrensesAvArealressursGrense" and having a format
	-- like this:
	--
	--     "avgrensesAvArealressursGrense": [
	--       {
	--         "featuretype": "ArealressursGrense",
	--         "lokalId": "a06e01f3-dfe1-4358-9419-44bdb220dfc2",
	--         "reverse": true,
	--         "idx": [
	--           0, -- polygon index (always 0 for single-polygon)
	--           0, -- ring index (0 for shell, 1..n for holes)
	--           0  -- line order, starts at 0
	--         ]
	--       }
	--       ...
	--
	-- See https://github.com/kartverket/NGIS-OpenAPI/blob/master/spec/openapi.yaml#L88
	--

	props := feature -> 'properties';
	IF props IS NULL THEN
		RAISE EXCEPTION 'FKB50 polygon feature has no properties';
	END IF;

	fid := props -> 'identifikasjon' ->> 'lokalId';
	IF fid IS NULL THEN
		RAISE EXCEPTION 'FKB50 polygon feature has no {identifikasjon,lokalId} property';
	END IF;

	RAISE DEBUG 'Preprocessing Surface %', fid;

	binding_borders_json := COALESCE(props -> 'avgrensesAvArealressursGrense','{}'::JSONB)||COALESCE(props -> 'avgrensesAvArealressursGrenseFiktiv','{}'::JSONB);

	IF binding_borders_json IS NULL OR binding_borders_json = '{}'::JSONB THEN
		-- could this be an "empty" Surface ?
		RAISE EXCEPTION
			'Could not find "avgrensesAvArealressursGrense" or "avgrensesAvArealressursGrenseFiktiv" prop in FKB50 Surface %', fid;
	END IF;

	FOR rec IN
		WITH data AS (
			SELECT
				format(
					'%s%s',
					CASE WHEN (bb -> 'reverse')::boolean THEN '-' ELSE '' END,
					bb ->> 'lokalId'
				) as signedId,
				(bb -> 'idx' -> 0)::int as polyNo,
				(bb -> 'idx' -> 1)::int as ringNo,
				(bb -> 'idx' -> 2)::int as lineNo
			FROM jsonb_array_elements(binding_borders_json) bb
		)
		SELECT
			polyNo,
			ringNo,
			array_agg(signedId ORDER BY lineNo) borderIds
		FROM data
		GROUP BY polyNo, ringNo
		ORDER BY polyNo, ringNo
	LOOP
		-- We would have an iteration for each ring
		RAISE DEBUG 'Polygon % ring % formed by: %', rec.polyNo, rec.ringNo, rec.borderIds;
		IF rec.polyNo != 0 THEN
			RAISE EXCEPTION 'Unexpected multi-polygon Surface % found in FKB50 payload', fid;
		END IF;
		IF rec.ringNo = 0 THEN
			geometry_properties := jsonb_set(
				geometry_properties,
				'{exterior}',
				to_jsonb(rec.borderIds)
			);
		ELSE
			interior_rings := interior_rings || to_jsonb(rec.borderIds);
		END IF;
	END LOOP;

	IF interior_rings IS NOT NULL THEN
		geometry_properties := jsonb_set(
			geometry_properties,
			'{interiors}',
			to_jsonb(interior_rings)
		);
	END IF;

	RAISE DEBUG 'Built geometry_properties: %', geometry_properties;

	newfeature := jsonb_set(
		feature,
		'{geometry_properties}',
		geometry_properties
	);

	return newfeature;
END;
$BODY$ LANGUAGE 'plpgsql' STABLE STRICT;
--}

--{
--
-- Preprocess FKB50 features for use with default
-- topo_update.update_features
--
CREATE OR REPLACE FUNCTION topo_ar5ngis.fkb50_featureset_preprocess(
	featureset JSONB
)
RETURNS JSONB AS
$BODY$
DECLARE
	newfeature JSONB;
	newfeatureset JSONB[];
	feature JSONB;
BEGIN
	-- Set position for Surface
	-- Set geometry_properties for Surface
	-- Cleanup props

	FOR feature IN SELECT jsonb_array_elements(featureset)
	LOOP
		IF feature -> 'geometry' ->> 'type' LIKE '%LineString' THEN
			-- This is a Border
			newfeature := feature;
		ELSE
			-- This is a Surface
			newfeature := topo_ar5ngis.fkb50_rewrite_incoming_surface_feature(feature);
		END IF;

		newfeatureset := newfeatureset || newfeature;
	END LOOP;
	return to_jsonb(newfeatureset);
END;
$BODY$ LANGUAGE 'plpgsql' STABLE STRICT;
--}

--{ A wrapper function, to be deleted
CREATE OR REPLACE FUNCTION topo_ar5ngis.update_features_fkb50(
	featureset JSONB,
	featureset_bbox GEOMETRY
)
RETURNS void AS
$BODY$
BEGIN

	PERFORM topo_ar5ngis.update_features_fkb50(
		featureset,
		featureset_bbox,
		'identifikasjon_versjon_id'
	);
END;
$BODY$ LANGUAGE 'plpgsql'; --}

--{
CREATE OR REPLACE FUNCTION topo_ar5ngis.update_features_fkb50(
	featureset JSONB,
	featureset_bbox GEOMETRY,
	version_column text

)
RETURNS void AS
$BODY$
DECLARE
	updated BOOLEAN := false;
	-- Test with lower tolerance, this is related https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/186
	-- Test starts failing with 1e-06
	tolerance FLOAT8 := 1e-09;
	rewritten_features JSONB;
BEGIN

	-- topo_update.update_features expects a "geometry_properties"
	-- property of each Surface feature to describe the Border
	-- features binding the Surface shell and holes, as signed identifiers.
	-- See
	-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/blob/v1.0.0/src/sql/topo_update/function_02_update_features.sql#L72-76
	--
	-- In FKB50 this information is encoded in a different way
	-- so we will need to re-write the featureset to conform
	-- to the pgtopo_update_sql expectance, until the core is updated
	-- to support dynamic extraction of that information.
	--
	rewritten_features := topo_ar5ngis.fkb50_featureset_preprocess(featureset -> 'features');
	featureset := jsonb_set(featureset, '{features}', rewritten_features);

	PERFORM topo_update.update_features(
		featureset,

		'topo_ar5ngis.face_attributes'::regclass,
		'omrade',
		'identifikasjon_lokal_id',
		topo_ar5ngis.colMapForFaceAttributes(),

		'topo_ar5ngis.edge_attributes'::regclass,
		'grense',
		'identifikasjon_lokal_id',
		topo_ar5ngis.colMapForEdgeAttributes(),

		tolerance,

		deleteUnknownFeaturesInBbox => featureset_bbox,
		-- roleAndPropsExtractor => 'topo_ar5ngis.feature_extract_position'::regproc,
		linealLayerVersionColumn => version_column,
		arealLayerVersionColumn => version_column
	);
END;
$BODY$ LANGUAGE 'plpgsql'; --}

--{
-- Given an array of signed border identifiers (a "ring")
-- return an array of FKB50 elements representing the ring
--
CREATE OR REPLACE FUNCTION topo_ar5ngis.fkb50_write_surface_ring(
	poly_idx INT, -- polygon index (for multipolygons, 0 based)
	ring_idx INT, -- ring index (0 for shell)
	ring JSONB -- array of signed border identifiers
)
RETURNS SETOF JSONB AS
$BODY$
DECLARE
	border JSONB;
	line_idx INT := 0;
	rec RECORD;
BEGIN
	FOR rec IN
		WITH
		r AS (
			SELECT
				row_number() over () as seq,
				regexp_replace(signed_id, '^-', '')::uuid id,
				signed_id LIKE '-%' as reversed
			FROM jsonb_array_elements_text(ring) signed_id
		)
		SELECT r.*, b.featuretype FROM r
			LEFT JOIN
		topo_ar5ngis.edge_attributes b
			ON ( r.id = b.identifikasjon_lokal_id )
		ORDER BY r.seq
	LOOP --{

		RAISE DEBUG 'writing border % of ring %', line_idx, ring_idx;

		IF rec.featuretype IS NULL THEN
			-- TODO: use an arbitrary featuretype ?
			RAISE EXCEPTION
				'Border % could not be found in topo_ar5ngis.edge_attributes, '
				'could not determine featuretype', rec.id;
		END IF;

		border := jsonb_build_object(
			'featuretype', rec.featuretype,
			'idx', jsonb_build_array(
				poly_idx, -- polygon index, we're assuming Surface is always single-polygon
				ring_idx, -- ring index
				line_idx
			)
		);

		line_idx := line_idx + 1;

		border := border || jsonb_build_object(
			'reverse', rec.reversed,
			'lokalId', rec.id
		);

		RAISE DEBUG 'Binding border: %', border;

		RETURN NEXT border;
	END LOOP; --}
END;
$BODY$ LANGUAGE 'plpgsql'; --}

--{
--
-- Rewrite border feature coming from topo_update.surfaces_as_geojson
-- for use with FKB50
-- }{
CREATE OR REPLACE FUNCTION topo_ar5ngis.fkb50_rewrite_outgoing_border_feature(
	feature JSONB,
	props JSONB,
	ffid TEXT
)
RETURNS JSONB AS
$BODY$
DECLARE
	newfeature JSONB;
	selfName TEXT := 'topo_ar5ngis.fkb50_rewrite_outgoing_border_feature';
BEGIN

	-- Just strip nulls
	newfeature := jsonb_set(
		feature,
		'{properties}',
		jsonb_strip_nulls(props)
	);

	RETURN newfeature;
END;
$BODY$ LANGUAGE 'plpgsql' STABLE STRICT;
--}

--{
--
-- Rewrite surface feature coming from topo_update.surfaces_as_geojson
-- for use with FKB50
-- }{
CREATE OR REPLACE FUNCTION topo_ar5ngis.fkb50_rewrite_outgoing_surface_feature(
	feature JSONB,
	props JSONB,
	fid TEXT
)
RETURNS JSONB AS
$BODY$
DECLARE
	newfeature JSONB;
	newprops JSONB;
	tmp JSONB;
	ring JSONB;
	ring_idx INT;
	geometry_properties JSONB;
	delimiting_borders JSONB[];
	bid TEXT;
	rec RECORD;
	selfName TEXT := 'topo_ar5ngis.fkb50_rewrite_outgoing_surface_feature';
BEGIN
	-- As of topo_update 1.0.0 the topo_update.surfaces_as_geojson function
	-- writes a "geometry_properties"
	-- property for each Surface feature to describe the Border
	-- features binding the Surface shell and holes, as signed
	-- identifier`s.
	--
	-- format of this object looks like this:
	--
	--		"geometry_properties": {
	--			"exterior": [
	--				"forward_border_id1",
	--				"-backward_border_id1"
	--			],
	--			"interiors": [
	--				[
	--					"forward_border_id2",
	--					"-backward_border_id2"
	--				]
	--			    ...
	--			]
	--		}
	--
	-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/blob/v1.0.0/src/sql/topo_update/function_02_surfaces_as_geojson.sql#L6-10
	--
	-- In FKB50 this information is encoded in a
	-- "avgrensesAvArealressursGrense" field, looking like this:
	--
	--     "avgrensesAvArealressursGrense": [
	--       {
	--         "featuretype": "ArealressursGrense",
	--         "lokalId": "a06e01f3-dfe1-4358-9419-44bdb220dfc2",
	--         "reverse": true,
	--         "idx": [
	--           0, -- polygon index (always 0 for single-polygon)
	--           0, -- ring index (0 for shell, 1..n for holes)
	--           0  -- line order, starts at 0
	--         ]
	--       }
	--       ...
	--
	-- See https://github.com/kartverket/NGIS-OpenAPI/blob/master/spec/openapi.yaml#L88
	--
	-- We will need to re-write the feature to conform
	-- to the FKB50 expectance
	--
	--

	RAISE DEBUG 'Preprocessing Surface %', fid;

	geometry_properties := feature -> 'geometry_properties';
	IF geometry_properties IS NULL THEN
		-- could this be an "empty" Surface ?
		RAISE EXCEPTION
			'Could not find "geometry_properties" property in topo_update Surface %', fid;
	END IF;

	ring := geometry_properties -> 'exterior';
	IF ring IS NULL THEN
		-- could this be an "empty" Surface ?
		RAISE EXCEPTION
			'topo_update Surface % has no exterior ring', fid;
	END IF;

	RAISE DEBUG 'processing exterior ring: %', ring;

	-- Rewrite exterior ring
	SELECT array_agg(b)
	FROM (
		SELECT topo_ar5ngis.fkb50_write_surface_ring(0, 0, ring) b
	) all_borders
	INTO delimiting_borders;
	IF delimiting_borders IS NULL THEN
		-- Programmatic error ?
		RAISE EXCEPTION
			'Could not write surface ring for exterior ring of Surface %', fid;
	END IF;

	-- Rewrite interior rings
	ring_idx := 1;
	FOR ring IN SELECT jsonb_array_elements(geometry_properties -> 'interiors')
	LOOP
		RAISE DEBUG 'processing interior ring %: %', ring_idx, ring;
		SELECT delimiting_borders || array_agg(b) FROM (
			SELECT topo_ar5ngis.fkb50_write_surface_ring(0, ring_idx, ring) b
		) interior
		INTO delimiting_borders;
		IF delimiting_borders IS NULL THEN
			-- Programmatic error ?
			RAISE EXCEPTION
				'Could not write surface ring for interior ring % of Surface %', ring_idx, fid;
		END IF;
		ring_idx := ring_idx + 1;
	END LOOP;

	RAISE DEBUG 'delimiting_borders are: %', delimiting_borders;

	newprops := jsonb_strip_nulls(props);
	RAISE DEBUG 'surface props with nulls stripped are: %', jsonb_pretty(newprops);

	-- Organize delimiting_borders into sets based on featureType,
	-- exporting them into an `avgrensesAv${FEATURETYPE}` element
	--
	-- See https://github.com/kartverket/NGIS-OpenAPI/issues/122
	--

	FOR rec IN
		WITH bb AS ( SELECT unnest( delimiting_borders ) x )
		SELECT
			x ->> 'featuretype' ft,
			array_agg(x) b
		FROM bb
		GROUP BY x ->> 'featuretype'
	LOOP --{
		RAISE DEBUG 'delimiting borders with featuretype = %: %', rec.ft, rec.b;
		newprops := jsonb_set(
			newprops,
			ARRAY[format('avgrensesAv%s', rec.ft)],
			to_jsonb(rec.b)
		);
	END LOOP; --}

	newfeature := jsonb_set(
		feature #- '{geometry_properties}',
		'{properties}',
		newprops
	);

	RETURN newfeature;
END;
$BODY$ LANGUAGE 'plpgsql' STABLE STRICT;
--}

--{
--
-- Rewrite GeoJSON features for use with FKB50
--
--
-- {
CREATE OR REPLACE FUNCTION topo_ar5ngis.surfaces_as_geojson_featureJsonModifier_fkb50(
	INOUT feat JSONB,
	role CHAR,
	-- A relation containing the table
	-- returned by topo_ar5ngis.add_borders_fkb50 function
	usr REGCLASS DEFAULT NULL)
AS $BODY$
DECLARE
	fid TEXT;
	props JSONB;
	selfName TEXT := 'topo_ar5ngis.surfaces_as_geojson_featureJsonModifier_fkb50';
	action_value TEXT;
	sql TEXT;
	rec RECORD;
	add_borders_result_relation REGCLASS := usr;
BEGIN

	-- Drop crs since it's in the header
	feat := feat #- '{ geometry, crs }';

	-- Extract properties
	props := feat -> 'properties';
	IF props IS NULL THEN
		RAISE EXCEPTION 'topo_update feature has no properties';
	END IF;

	-- Extract FID
	fid := props #>> ARRAY[ 'identifikasjon', 'lokalId' ];
	IF fid IS NULL THEN
		RAISE EXCEPTION 'Unexpected NULL identifikasjon.lokalId'
			' in feature passed to featureRewriter by surfaces_as_geojson: %',
			feat;
	END IF;

	IF role = 'B'
	THEN -- Border feature

		feat := topo_ar5ngis.fkb50_rewrite_outgoing_border_feature(feat, props, fid);

	ELSIF role = 'S' THEN -- Surface feature

		feat := topo_ar5ngis.fkb50_rewrite_outgoing_surface_feature(feat, props, fid);

	END IF;

	IF add_borders_result_relation IS NULL THEN
		RAISE DEBUG '%: no add_border_result_relation passed, no action will be added', selfName;
		RETURN;
	END IF;

	-- Apply appropriate action tag to feature, if affected by changes

	-- Column |  Type  |
	----------+--------+
	-- fid    | text   |
	-- typ    | bpchar |
	-- act    | bpchar |
	-- frm    | text   |
	sql := format(
		$$
			SELECT *
			FROM %1$s
			WHERE typ = $1
			AND fid = $2
		$$,
		add_borders_result_relation
	);
	RAISE DEBUG 'SQL: %', sql;
	EXECUTE sql USING role, fid INTO rec;

--		Character representing the action taken on the Feature,
--		with the following possible values:
--
--			M	The Feature was modified loosing space
--				due to a split.
--
--			C	The Feature was created independently from
--				previously existing Features.
--
--			S	The Feature was created by taking up space
--				space previosuly taken by previosly existing
--				Features. When such a Feature is created, there
--				should also be at least another Feature of the
--				same type which is returned as modified (M).
--
--			D	The Feature was deleted. This can currently
--				only happen for Surface features if `keepModifiedSurface`
--				parameter is set to false.
-- T Touhes

	IF rec.act = 'M' THEN
		action_value := 'Replace';
	ELSIF rec.act IN ( 'C', 'S' ) THEN
		action_value := 'Create';
	ELSIF role = 'S' AND rec.act = 'T' THEN
		action_value := 'Replace';
	ELSE
		action_value := NULL;
	END IF;

	IF action_value IS NOT NULL THEN
		feat := jsonb_set(
			feat,
			'{update}',
			jsonb_build_object(
				'action',
				action_value
			)
		);
	ELSE
		feat = NULL;
	END IF;

END; --}
$BODY$ LANGUAGE 'plpgsql';


--
-- Wrapper for topo_update.surfaces_as_geojson to produce
-- valid FKB50 output
--
--{
CREATE OR REPLACE FUNCTION topo_ar5ngis.surfaces_as_geojson_fkb50(
	surfaceIds TEXT[],

	-- Override projection
	--
	-- If value of this parameter is not-null use the
	-- specified CRS for the output GeoJSON, reprojecting
	-- to that system all the geometries.
	--
	outSRID INT DEFAULT NULL,

	-- Overide decimal used geojson
	--
	-- The maxdecimaldigits argument may be used when calling ST_AsGeoJSON,
	-- to changed the number of decimal places used in output
	maxdecimaldigits INT DEFAULT 9
)
RETURNS JSONB AS
$BODY$  --}{
BEGIN

	-- Produce the GeoJSON for new or modified surfaces
	RETURN topo_update.surfaces_as_geojson(
		'topo_ar5ngis.face_attributes_export_fkb50',	-- surfaceLayerView
		'omrade',				-- surfaceLayerTopoGeomColumn
		'identifikasjon_lokal_id',		-- surfaceLayerIDColumn
		topo_ar5ngis.colMapForFaceAttributes(), -- surfaceLayerColMap

		'topo_ar5ngis.edge_attributes_export_fkb50',	-- borderLayerView
		'grense',				-- borderLayerTopoGeomColumn
		'identifikasjon_lokal_id',		-- borderLayerIdColumn
		topo_ar5ngis.colMapForEdgeAttributes(), -- borderLayerColMap

		-- Array of identifiers of new or modified surfaces
		-- that we want to include in the GeoJSON
		surfaceIds,

		-- JSON feature modifier
		'topo_ar5ngis.surfaces_as_geojson_featureJsonModifier_fkb50',

		-- Parameter passed to JSON feature modifier
		NULL::int,

		outSRID,

		maxdecimaldigits

		);

END;
$BODY$ --}
LANGUAGE plpgsql;

--{
--
CREATE OR REPLACE FUNCTION topo_ar5ngis.add_border_ColMapProvider(typ char, act char, usr TEXT)
RETURNS JSONB AS $BODY$ --{
DECLARE
	procName TEXT := 'topo_ar5ngis.add_border_ColMapProvider';
	map JSONB;
BEGIN
	IF typ = 'B' THEN
		IF act = 'C' THEN
			map := $$
				{
					"opphav": [ "opphav" ],
					"verifiseringsdato": [ "verifiseringsdato" ],
					"registreringsversjon": [ "registreringsversjon" ],
					"datafangstdato": [ "datafangstdato" ],
					"kvalitet": [ "kvalitet" ],
					"avgrensing_type": [ "border_avgrensing_type" ]
				}
			$$::jsonb;
		ELSE
			map := $$
				{
				}
			$$::jsonb;
		END IF;
	ELSIF typ = 'S' THEN
		IF act = 'C' THEN
			map := $$
				{
					"opphav": [ "opphav" ],
					"verifiseringsdato": [ "verifiseringsdato" ],
					"registreringsversjon": [ "registreringsversjon" ],
					"datafangstdato": [ "datafangstdato" ],
					"kvalitet": [ "kvalitet" ]
				}
			$$::jsonb;
		ELSE
			map := $$
				{
				}
			$$::jsonb;
		END IF;
	ELSE
		RAISE EXCEPTION '%: unexpected feature type %', procName, typ;
	END IF;

	-- Add versjonId mapping
	map := map || $$
		{
			"identifikasjon_versjon_id": [ "_current_timestamp" ],
			"oppdateringsdato": [ "_current_timestamp" ]
		}
	$$::jsonb;

	RETURN map;
END;
$BODY$ --}
LANGUAGE 'plpgsql'; --}

-- {
-- This function will be called to extract extra info about
-- objects related to a split operation
--
-- # RETURN VALUE
--
-- The function returns a table containig information about
-- the border used by surfaces modified by a split operation
-- and surfaces on the other side of these borders
--
-- returned table has fields:
--
--	- fid text
--
--		Feature identifier (text representation of the value of
--		the specified layer's IdColumn)
--
--	- typ char
--
--		Character representing the type of Feature the record
--		is about, with these possible values:
--
--			S	The Feature is a Surfaces
--
--			B	The Feature is a Border
--
--	- act char
--
--		Character representing the action taken on the Feature,
--		with the following possible values:
--
--			U	For a border, this will give list of
--				borders (U) used by the changed
--              surfaces but not modified themselves.
--
--			T	Only used for surface.	A surfaces that
--              touches any of the changed borders in the input
--				due to a split thats not already in the input list.
--
--
--
-- }{
CREATE OR REPLACE FUNCTION topo_ar5ngis.get_border_split_info_fkb50(

	--TABLE(fid text, typ char, act char, frm text)
	-- Surface layer table
	outputFrom_add_border_split_surface REGCLASS


)
RETURNS TABLE(fid text, typ char, act char) AS $BODY$
DECLARE

	rec RECORD;
	sql text;
	topo topology.topology;
	surfaceLayer topology.layer;
	borderLayer topology.layer;

	-- Surface layer table
	surfaceLayerTable REGCLASS := 'topo_ar5ngis.face_attributes';

	-- Surface layer TopoGeometry column name
	surfaceLayerGeomColumn NAME := 'omrade';

	-- Surface layer primary key column name
	surfaceLayerIdColumn NAME := 'identifikasjon_lokal_id';

	-- Border layer table
	borderLayerTable REGCLASS := 'topo_ar5ngis.edge_attributes';

	-- Name of Border layer TopoGeometry column
	borderLayerGeomColumn NAME := 'grense';

	-- Surface layer primary key column name
	borderLayerIdColumn NAME := 'identifikasjon_lokal_id';

	selfName TEXT := 'topo_ar5ngis.get_border_split_info_fkb50';

BEGIN

	RAISE DEBUG '%: creating old_split_border_result table', selfName;

	-- TODO avoid using temp tables
	-- Create a temp table with last split data
	sql =  format('CREATE TEMP table old_split_border_result AS (SELECT s.fid, s.typ, s.act FROM %I s)', outputFrom_add_border_split_surface );
	EXECUTE sql;

	RAISE DEBUG '%: creating pg_temp.temp_result_get_border_split_info table', selfName;

	-- Create a temp table with new result
	DROP TABLE IF EXISTS pg_temp.temp_result_get_border_split_info;
	CREATE TEMP TABLE temp_result_get_border_split_info(fid text, typ char, act char);

	RAISE DEBUG '%: getting topology info', selfName;

	-- Get info about topology
	SELECT t topo, l layer
	FROM topology.layer l, topology.topology t
	WHERE format('%I.%I', l.schema_name, l.table_name)::REGCLASS = surfaceLayerTable::REGCLASS
	AND l.feature_column = surfaceLayerGeomColumn
	AND l.topology_id = t.id
	INTO rec;

	topo := rec.topo;
	surfaceLayer := rec.layer;

	SELECT t topo, l layer
	FROM topology.layer l, topology.topology t
	WHERE format('%I.%I', l.schema_name, l.table_name)::REGCLASS = borderLayerTable::REGCLASS
	AND l.feature_column = borderLayerGeomColumn
	AND l.topology_id = t.id
	INTO rec;

	borderLayer := rec.layer;

	-- Find list of borders (T) used/touched by the changed surfaces in the input due to a split thats not already in the input list.
	sql := format(
		$$
	CREATE TEMP TABLE result_add_used_borders AS (
		SELECT
		DISTINCT ed_to.%8$s::TEXT AS fid
		FROM
		%2$s fa_from,
		%1$s.relation r_from,
		%1$s.edge_data e_from,
		%1$s.relation r_to,
		%6$s ed_to,
		pg_temp.old_split_border_result tr
		WHERE tr.typ = 'S' AND fa_from.%4$I = tr.fid::UUID AND
		(fa_from.%3$s).type = 3 AND
		(fa_from.%3$s).layer_id = %5$s AND
		(fa_from.%3$s).layer_id = r_from.layer_id AND
		(fa_from.%3$s).id = r_from.topogeo_id AND
		(r_from.element_id = e_from.right_face OR r_from.element_id = e_from.left_face) AND
		(e_from.edge_id = r_to.element_id OR e_from.edge_id = r_to.element_id) AND
		%9$s = r_to.layer_id AND
		(ed_to.%7$s).type = 2 AND
		(ed_to.%7$s).layer_id = %9$s AND
		(ed_to.%7$s).id = r_to.topogeo_id
	)
		$$,
		topo.name,
		surfaceLayerTable,
		surfaceLayerGeomColumn,
		surfaceLayerIdColumn,
		surfaceLayer.layer_id, -- 5
		borderLayerTable,
		borderLayerGeomColumn,
		borderLayerIdColumn,
		borderLayer.layer_id --9
	);

	RAISE DEBUG '%: Finding list of used borders with SQL: %', selfName, sql;

	EXECUTE sql;

	-- TODO, we may also need surfaces touhing changed surfaces
		-- Modify to add toching surface


	-- Find surfaces touching changed borderes
	sql := format(
		$$
	CREATE TEMP TABLE result_add_toched_affected_surfaces AS (
		SELECT
		DISTINCT fa_from.%4$s::TEXT AS fid
		FROM
		%2$s fa_from,
		%1$s.relation r_from,
		%1$s.edge_data e_from,
		%1$s.relation r_to,
		%6$s ed_to,
		pg_temp.old_split_border_result tr
		WHERE tr.typ = 'B' AND ed_to.%8$I = tr.fid::UUID AND
		(fa_from.%3$s).type = 3 AND
		(fa_from.%3$s).layer_id = %5$s AND
		(fa_from.%3$s).layer_id = r_from.layer_id AND
		(fa_from.%3$s).id = r_from.topogeo_id AND
		(r_from.element_id = e_from.right_face OR r_from.element_id = e_from.left_face) AND
		(e_from.edge_id = r_to.element_id OR e_from.edge_id = r_to.element_id) AND
		%9$s = r_to.layer_id AND
		(ed_to.%7$s).type = 2 AND
		(ed_to.%7$s).layer_id = %9$s AND
		(ed_to.%7$s).id = r_to.topogeo_id
	)
		$$,
		topo.name,
		surfaceLayerTable,
		surfaceLayerGeomColumn,
		surfaceLayerIdColumn,
		surfaceLayer.layer_id, -- 5
		borderLayerTable,
		borderLayerGeomColumn,
		borderLayerIdColumn,
		borderLayer.layer_id --9
	);

	RAISE DEBUG '%: Finding list of touching surfaces with SQL %', selfName, sql;


	EXECUTE sql;

	RAISE DEBUG '%: Adding list of used borders to results', selfName;

	-- Add list of used touched borderes
	INSERT INTO temp_result_get_border_split_info(fid,typ,act)
	SELECT s.fid, 'B', 'U'
	FROM result_add_used_borders s
	WHERE
		NOT EXISTS (
				SELECT 1 FROM pg_temp.old_split_border_result a WHERE a.fid = s.fid AND a.typ = 'B'
		);

	DROP TABLE IF EXISTS pg_temp.result_add_used_borders;

	RAISE DEBUG '%: Adding list of touched surfaces to results', selfName;

	-- Add surfaces touching changed borderes
	INSERT INTO temp_result_get_border_split_info(fid,typ,act)
	SELECT s.fid, 'S', 'T'
	FROM result_add_toched_affected_surfaces s
	WHERE
		NOT EXISTS (
				SELECT 1 FROM pg_temp.old_split_border_result a WHERE a.fid = s.fid AND a.typ = 'S'
		);

	RAISE DEBUG '%: Dropping temporary tables', selfName;

		DROP TABLE IF EXISTS pg_temp.result_add_toched_affected_surfaces;

		DROP TABLE IF EXISTS pg_temp.old_split_border_result;

	-- Return result

	RAISE DEBUG '%: Returning results', selfName;

		RETURN QUERY SELECT s.fid, s.typ, s.act FROM temp_result_get_border_split_info s;




END;
$BODY$ LANGUAGE plpgsql; --}



--{
--
-- Wrapper around topo_update.add_borders encoding
-- ar5/fkb50 specific parameters and extending the
-- returned information in a way to allow for
-- encoding action tags in GeoJSON document to be
-- used to update the upstream database.
--
-- The extension to the return codes is the information
-- retrived by topo_update.get_border_split_info and is
-- documented in:
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/blob/v1.0.0/src/sql/topo_update/function_02_get_border_split_info.sql?ref_type=tags#L2
--
-- The base return codes are documented in:
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/blob/v1.0.0/src/sql/topo_update/function_02_add_border_split_surface.sql#L199-241
--
--
CREATE OR REPLACE FUNCTION topo_ar5ngis.add_borders_fkb50(
	feature JSONB
)
RETURNS TABLE(fid text, typ char, act char, frm text) AS
$BODY$
DECLARE
	-- Test with lower tolerance, this is related
	-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/186
	-- Test starts failing with 1e-06
	tolerance FLOAT8 := 1e-09;

	minAreaToleranceM2 FLOAT8 = 200;
	minAreaTolerance FLOAT8;
	geom GEOMETRY;
	geom_for_area GEOMETRY;
	geom_as_polygon GEOMETRY;
	geom_as_polygon_area FLOAT8;
	input_line_polygonize_area FLOAT8;
	mline_geo GEOMETRY;
	mline_cnt int;

	-- This is the max number of new surfaces an input operation may changed spatialy. If the area has changed the surface has changed.
	-- Other surfaces may be changed because of new vertex/node, but this are not relevant for this case
	maxAllowedSurfaceSplitCount int = 1;

	-- Two this is the max number of surfaces with new area or changed surface area after the splitt.
	-- but with multline we should accept 3 changed/new surfaces (1 changed and two new ones)
	maxNewSurfacesOrChangedAreaSurfaces int;


	cmd text;

	num_edges_locked int;
	num_mbr_locked int;

	pre_check_lock_area_test boolean = false;
	post_check_lock_area_test boolean = false;

	default_kvalitet text;

BEGIN

	-- Set default avgrensing_type, avgrensingType
	-- TOTO maybe do this in mapping code ??
		feature := jsonb_set(
						feature,
						'{properties, border_avgrensing_type}',
						to_jsonb('4206'::TEXT)
		);


	-- Get verifiseringsdato for new object
	IF feature -> 'properties' ->> 'datafangstdato' IS NOT NULL THEN
		feature := jsonb_set(
			feature,
			'{properties, verifiseringsdato}',
			to_jsonb(feature -> 'properties' ->> 'datafangstdato')
		);
	END IF;

	-- Set default registreringsversjon
	feature := jsonb_set(
		feature,
		'{properties, registreringsversjon}',
		to_jsonb('2022-01-01'::TEXT)
	);

	-- Set default avgrensing_type, avgrensingType
	-- TOTO maybe do this in mapping code ??
		feature := jsonb_set(
						feature,
						'{properties, opphav}',
						to_jsonb('web'::TEXT)
		);

	-- Set default kvalitet borders
	default_kvalitet = '("'||(feature -> 'properties' ->> 'datafangstmetode')||'","200","0")';

	feature := jsonb_set(
		feature,
		'{properties, kvalitet}',
		to_jsonb(default_kvalitet::TEXT)
	);


	-- Extract input geometry
	geom := ST_GeomFromGeoJSON(feature -> 'geometry');

	-- TODO What if he drawing one line and that is closed line
	IF ST_GeometryType(geom) = 'ST_MultiLineString' AND ST_NumGeometries(geom) > 1 THEN
		FOR mline_cnt IN 1..ST_NumGeometries(geom) LOOP
			mline_geo = ST_GeometryN(geom,mline_cnt);
			-- sheck if can form a surface
			input_line_polygonize_area := ST_Area(ST_Polygonize(mline_geo));
			IF ST_GeometryType(mline_geo) != 'ST_LineString'
				OR ST_IsSimple(mline_geo) != true
				OR input_line_polygonize_area != 0 THEN
				RAISE EXCEPTION '% number % of MultiLineString with % lines is not valid. Is simple %, has area %. %',
				ST_GeometryType(mline_geo),
				mline_cnt,
				ST_NumGeometries(geom),
				ST_IsSimple(mline_geo),
				input_line_polygonize_area,
				ST_AsText(mline_geo);
			END IF;
		END loop;
		minAreaToleranceM2 = 10;
		maxNewSurfacesOrChangedAreaSurfaces = 3;
	ELSE
		-- sheck if can form a surface
		input_line_polygonize_area := ST_Area(ST_Polygonize(geom));

		-- Default minAreaToleranceM2 should be changed when user is drawing a single non closed line
		IF ST_GeometryType(geom) = 'ST_LineString' AND ST_IsSimple(geom) AND NOT input_line_polygonize_area > 0 THEN
			minAreaToleranceM2 = 10;
		END IF;

		IF input_line_polygonize_area > 0 THEN
			maxNewSurfacesOrChangedAreaSurfaces = 2;
		ELSE
			maxNewSurfacesOrChangedAreaSurfaces = 2;
		END IF;

	END IF;

	RAISE NOTICE 'Min area % and maxNewSurfacesOrChangedAreaSurfaces % for input line GeometryType % IsSimple % PolygonizeArea % wkt %',
	minAreaToleranceM2, maxNewSurfacesOrChangedAreaSurfaces, ST_GeometryType(geom), ST_IsSimple(geom), input_line_polygonize_area, ST_AsEWKT(geom);

	-- Pick point with highest latitude/longitude values
	geom := ST_PointN(
		ST_BoundingDiagonal(
			geom
		),
		1
	);
	-- Make sure we're in latlon projection, which
	-- is required for _ST_BestSRID to work
	geom := ST_Transform(geom, 4258);
	-- Switch to metrical units projection
	geom := ST_Transform(geom, _ST_BestSRID(geom));
	-- Build a rectangle of ~200 square meters around the geometry's highest lat/lon value
	--geom := ST_Buffer(geom, sqrt(200/PI()), 16);
	geom := ST_Expand(geom, sqrt(minAreaToleranceM2)/2.0);
	-- TODO: densify the geometry to have less distortion upon projecting ?
	-- Verify area is ~200 m2
	RAISE NOTICE 'Built geom area in square meters: % (% targeted)', ST_Area(geom), minAreaToleranceM2;
	-- Switch back to latlon (topology) CRS (shall we query it rather than hardcoding it?)
	geom := ST_Transform(geom, 4258);
	-- Compute area in target topology SRID
	minAreaTolerance := ROUND(ST_Area(geom)::numeric, 16);

	RAISE NOTICE 'Computed min area tolerance: %', minAreaTolerance;

	RAISE DEBUG 'Creating temp_result table with output from topo_update.add_border_split_surface';

	CREATE TEMP TABLE temp_result ON COMMIT DROP AS
	SELECT * FROM topo_update.add_border_split_surface(

		feature,

		'topo_ar5ngis.face_attributes'::regclass,
		'omrade'::name,
		'identifikasjon_lokal_id'::name,

		'topo_ar5ngis.edge_attributes'::regclass,
		'grense'::name,
		'identifikasjon_lokal_id'::name,

		tolerance,

		'topo_ar5ngis.add_border_ColMapProvider', -- colMapProviderFunc
		NULL::text,

		minAreaTolerance,

		maxAllowedSurfaceSplitCount,

		maxNewSurfacesOrChangedAreaSurfaces
	);

	RAISE DEBUG 'Creating add_border_results_more_info table with output from topo_update.get_border_split_info';

	-- TODO only call this if requested and add parameters that
	-- TODO move the to add_border_split_surface
	CREATE TEMPORARY TABLE add_border_results_more_info ON COMMIT DROP AS
	SELECT s.fid::uuid, s.typ, s.act
	FROM topo_ar5ngis.get_border_split_info_fkb50(

		-- The result from the split operation
		'pg_temp.temp_result'

	) AS s;

	RAISE DEBUG 'Updating face_attributes to set versjon_id';

	-- This can not be null when sent ngsiOpenApi,
	-- may use a older value not sure have to check
	UPDATE topo_ar5ngis.face_attributes
	SET identifikasjon_versjon_id = '1970-01-01T00:00:00'
	WHERE identifikasjon_lokal_id IN (
		SELECT mi.fid FROM add_border_results_more_info mi
		WHERE mi.typ = 'S' AND mi.act = 'T'
	);

	RAISE DEBUG 'Updating edge_attributes to set verifiseringsdato';

	-- This can not be null when sent back ngsiOpenApi for now we use oppdateringsdato
	UPDATE topo_ar5ngis.edge_attributes
	SET verifiseringsdato = oppdateringsdato
	WHERE verifiseringsdato IS NULL AND identifikasjon_lokal_id IN (
		SELECT mi.fid FROM add_border_results_more_info mi
		WHERE mi.typ = 'B' AND mi.act = 'U'
	);

	RAISE DEBUG 'Updating edge_attributes to set datafangstdato';

	-- This can not be null when sent back ngsiOpenApi for now we use oppdateringsdato
	-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/165
	UPDATE topo_ar5ngis.edge_attributes
	SET datafangstdato = oppdateringsdato
	WHERE datafangstdato IS NULL AND identifikasjon_lokal_id IN (
		SELECT mi.fid FROM add_border_results_more_info mi
		WHERE mi.typ = 'B' AND mi.act = 'U'
	);

	RAISE DEBUG 'Updating temp_result with rows from add_border_results_more_info';

	INSERT INTO temp_result(fid,typ,act)
	SELECT mi.fid::uuid, mi.typ, mi.act
	FROM pg_temp.add_border_results_more_info mi;

	RAISE DEBUG 'Dropping add_border_results_more_info table';

	DROP TABLE pg_temp.add_border_results_more_info;

	RAISE DEBUG 'Returning rows in temp_result';

	RETURN QUERY
	SELECT s.fid, s.typ, s.act, s.frm
	FROM pg_temp.temp_result s;

	RAISE DEBUG 'Dropping table temp_result';

	DROP TABLE pg_temp.temp_result;

END;
$BODY$ LANGUAGE 'plpgsql'; --}

--
-- Function to return a GeoJSON document encoding changes
-- introduced by a call to topo_ar5ngis.add_borders_fkb50
-- in FKB50 format
--
--{
CREATE OR REPLACE FUNCTION topo_ar5ngis.add_border_changes_as_geojson_fkb50(

	-- Relation containing results from topo_ar5ngis.add_borders_fkb50
	add_borders_fkb50_results REGCLASS,

	-- Override projection
	--
	-- If value of this parameter is not-null use the
	-- specified CRS for the output GeoJSON, reprojecting
	-- to that system all the geometries.
	--
	outSRID INT DEFAULT NULL,

	-- Overide decimal used geojson
	--
	-- The maxdecimaldigits argument may be used when calling ST_AsGeoJSON,
	-- to changed the number of decimal places used in output
	maxdecimaldigits INT DEFAULT 9
)
RETURNS JSONB AS
$BODY$  --{
DECLARE
	sql TEXT;
	geojson JSONB;
	surfaceIds TEXT[];
BEGIN

	sql := format(
		$$
			SELECT array_agg(fid ORDER BY fid)
			FROM %1$s
			WHERE typ = 'S'
		$$,
		add_borders_fkb50_results
		);
	RAISE DEBUG 'SQL: %', sql;
	EXECUTE sql INTO surfaceIds;

	RAISE DEBUG 'Generating surfaces JSON';

	-- Produce the GeoJSON for new or modified surfaces
	geojson := topo_update.surfaces_as_geojson(
		'topo_ar5ngis.face_attributes_export_fkb50',	-- surfaceLayerView
		'omrade',				-- surfaceLayerTopoGeomColumn
		'identifikasjon_lokal_id',		-- surfaceLayerIDColumn
		topo_ar5ngis.colMapForFaceAttributes(), -- surfaceLayerColMap

		'topo_ar5ngis.edge_attributes_export_fkb50',	-- borderLayerView
		'grense',				-- borderLayerTopoGeomColumn
		'identifikasjon_lokal_id',		-- borderLayerIdColumn
		topo_ar5ngis.colMapForEdgeAttributes(), -- borderLayerColMap

		-- Array of identifiers of new or modified surfaces
		-- that we want to include in the GeoJSON
		surfaceIds,

		-- JSON feature modifier
		'topo_ar5ngis.surfaces_as_geojson_featureJsonModifier_fkb50',

		-- Parameter passed to JSON feature modifier
		add_borders_fkb50_results,

		outSRID,

		maxdecimaldigits

		);

	RETURN geojson;
END;
$BODY$ --}
LANGUAGE plpgsql;


-- A wrapper function that takes a array json array and not a table as input
-- Fails with operator overloading so added java at the end
CREATE OR REPLACE FUNCTION topo_ar5ngis.add_border_changes_as_geojson_fkb50_java(

	-- The JSON result add_border_split as json
	-- this function is called from java
		-- [{"fid":"9e4324f7-872f-42ad-bbc7-0f1a3961496e","typ":"S","act":"M","frm":"9e4324f7-872f-42ad-bbc7-0f1a3961496e"},{"fid":"e607605f-1967-4cc5-bf11-194f02aae8f0","typ":"S","act":"M"}];

	add_border_results_jsonb TEXT,

	-- Override projection
	--
	-- If value of this parameter is not-null use the
	-- specified CRS for the output GeoJSON, reprojecting
	-- to that system all the geometries.
	--
	outSRID INT DEFAULT NULL,

	-- Overide decimal used geojson
	--
	-- The maxdecimaldigits argument may be used when calling ST_AsGeoJSON,
	-- to changed the number of decimal places used in output
	maxdecimaldigits INT DEFAULT 9

)
RETURNS TEXT AS
$BODY$  -- {
DECLARE

	geojson JSONB;

BEGIN

	CREATE TEMPORARY TABLE add_border_results(
		fid TEXT,
		typ CHAR,
		act CHAR,
		frm TEXT DEFAULT NULL
		);

	insert into add_border_results select * from jsonb_populate_recordset(NULL::add_border_results, add_border_results_jsonb::JSONB );

	alter table add_border_results add column sid serial;


	geojson := topo_ar5ngis.add_border_changes_as_geojson_fkb50(
		'add_border_results'::regclass,
		outSRID,
		maxdecimaldigits
		);

	drop table add_border_results;

	RETURN geojson::TEXT;
END;
$BODY$ --}
LANGUAGE plpgsql;

-- Return the number nodes that are shared by 3 more edges
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/tree/182-return-nodes-that-are-shared-by-3-faces-or-more
-- Return nodes that are split by 3 surfaces or more
-- The result is transformed to given srid

--{
CREATE OR REPLACE FUNCTION topo_ar5ngis.nodes_3_more_surface_split(
				bbox GEOMETRY,
				srid_out int
)
RETURNS GEOMETRY AS
$BODY$
DECLARE
				node_list GEOMETRY;
BEGIN

				SELECT ST_Collect(nl.geom) FROM (
								SELECT n.geom,
								count(e.*) as num_edges
								FROM
								topo_ar5ngis_sysdata_webclient.node n,
								topo_ar5ngis_sysdata_webclient.edge e
								WHERE n.geom && bbox AND e.geom && bbox AND
								(n.node_id = e.start_node OR n.node_id = e.end_node)
								GROUP BY n.geom
				) nl WHERE nl.num_edges > 2 INTO node_list;

				IF node_list IS NOT NULL THEN
								node_list := ST_Transform(node_list , srid_out);
				END IF;

				RETURN node_list;
END;
$BODY$ LANGUAGE 'plpgsql'; --}


-- This is wrapper function used by java
--
-- Update a non spatial columns feature in a table by id
--
-- Returns NULL or the ID of the updated rows
-- if a feature was updated the id of the updated row are returned
--
CREATE OR REPLACE FUNCTION topo_ar5ngis.update_feature_attribute_java(

	-- The 'properties' component of a GeoJSON Feature object
	-- See https://geojson.org/
	feature_properties TEXT,

	opphav TEXT DEFAULT NULL -- this will be sent from the server and controlled java backend server and override settings from the client, TODO mabe make a more generic struture for this ?


) RETURNS TEXT
AS $BODY$ -- {
DECLARE
	feature JSONB := feature_properties::jsonb;
BEGIN

	-- TODO should also be wrapper function
	-- Set default kvalitet

	--   kartstandard AR5,

	feature := jsonb_set(
		feature,
		'{properties, "registreringsversjon" }',
		to_jsonb('2022-01-01'::TEXT)
	);

	feature := jsonb_set(
		feature,
		'{properties, "identifikasjon_versjon_id" }',
		to_jsonb(now())
	);

	-- Get value from server side
	IF opphav IS NOT NULL THEN
		feature := jsonb_set(
			feature,
			'{properties, opphav}',
			to_jsonb(opphav)
		);
	END IF;

	-- fails with topo_ar5ngis.colMapForFaceAttributes() -- layerColMap
	-- +ERROR:  query returned more than one row
	-- +ERROR:  current transaction is aborted, commands ignored until end of transaction block


	RETURN topo_update.update_feature_attribute(
	feature::JSONB,
	'topo_ar5ngis.face_attributes'::regclass,
	'identifikasjon_lokal_id',
	$$
	{
		"identifikasjon_lokal_id": [
			"id"
		],
		"identifikasjon_versjon_id": [
			"identifikasjon_versjon_id"
		],
		"kvalitet": [
			"kvalitet"
		],
		"registreringsversjon": [
			"registreringsversjon"
		],
		"informasjon": [
			"informasjon"
		],
		"opphav": [
			"opphav"
		],
		"arealtype": [
			"arealtype"
		],
		"treslag": [
			"treslag"
		],
		"skogbonitet": [
			"skogbonitet"
		],
		"grunnforhold": [
			"grunnforhold"
		],
		"datafangstdato": [
			"datafangstdato"
		],
		"verifiseringsdato": [
			"verifiseringsdato"
		],
		"klassifiseringsmetode": [ "klassifiseringsmetode" ],

		"oppdateringsdato": [
			"oppdateringsdato"
		]
	}
	$$ -- layerColMap

);
END;
$BODY$ --}
LANGUAGE plpgsql;


--{
--
-- Wrapper around topo_update.app_do_RemoveBordersMergeSurfaces
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/blob/develop/src/sql/topo_update/function_03_app_do_RemoveBordersMergeSurfaces.sql

-- The extension to the return codes is the information
-- retrived by topo_update.app_do_RemoveBordersMergeSurfaces and is
--
--
CREATE OR REPLACE FUNCTION topo_ar5ngis.remove_borders_fkb50(
	-- The identifier of a border to remove
	border_id TEXT
)
RETURNS TABLE(fid text, typ char, act char, frm text[]) AS
$BODY$
DECLARE

	-- Application name, will be used to fetch
	-- configuration from application schema.
	-- See doc/APP_CONFIG.md
	appname NAME = 'topo_ar5ngis';

BEGIN

	RETURN QUERY
	SELECT * FROM topo_update.app_do_RemoveBordersMergeSurfaces(
		appname,
		border_id
	);
END;
$BODY$ --}
LANGUAGE plpgsql;


--
-- Function to return a GeoJSON document encoding changes
-- introduced by a call to topo_ar5ngis.remove_borders_fkb50
-- in FKB50 format
--
--{
CREATE OR REPLACE FUNCTION topo_ar5ngis.remove_border_changes_as_geojson_fkb50(

	-- The JSON result add_border_split as json
	-- this function is called from java
		-- [{"fid":"9e4324f7-872f-42ad-bbc7-0f1a3961496e","typ":"S","act":"M","frm":"9e4324f7-872f-42ad-bbc7-0f1a3961496e"},{"fid":"e607605f-1967-4cc5-bf11-194f02aae8f0","typ":"S","act":"M"}];

	remove_border_results_jsonb TEXT,

	-- Override projection
	--
	-- If value of this parameter is not-null use the
	-- specified CRS for the output GeoJSON, reprojecting
	-- to that system all the geometries.
	--
	outSRID INT DEFAULT NULL,

	-- Overide decimal used geojson
	--
	-- The maxdecimaldigits argument may be used when calling ST_AsGeoJSON,
	-- to changed the number of decimal places used in output
	maxdecimaldigits INT DEFAULT 9
)
RETURNS JSONB AS
$BODY$  --{
DECLARE
	sql TEXT;
	geojson_changed JSONB;
	geojson_changed_features JSONB;
	geojson_deleted_features JSONB;
	geojson_features JSONB;

	geojson JSONB;
	surfaceIds TEXT[];
BEGIN

	RAISE DEBUG 'Generating surfaces JSON for deleted borders';


	-- Relation containing results from topo_ar5ngis.remove_borders_fkb50
	CREATE TEMPORARY TABLE remove_borders_fkb50_results(
		fid TEXT,
		typ CHAR,
		act CHAR,
		frm TEXT DEFAULT NULL
		);

	insert into remove_borders_fkb50_results select * from jsonb_populate_recordset(NULL::remove_borders_fkb50_results, remove_border_results_jsonb::JSONB );

	alter table remove_borders_fkb50_results add column sid serial;

	-- Produce the GeoJSON for new or modified surfaces
	SELECt topo_ar5ngis.add_border_changes_as_geojson_fkb50(
		'remove_borders_fkb50_results'::regclass,
		outSRID,
		maxdecimaldigits
	) INTO geojson_changed;

	-- TODO find if we need {geometry} simple featture objects in NGIS OpenAPI
	--geojson_changed_features = ((geojson_changed->'features')->0)-'{geometry}'::text[]	;
	geojson_changed_features = ((geojson_changed->'features')->0);
	geojson = geojson_changed - '{features}'::text[];

	RAISE NOTICE 'geojson_changed_features: %', geojson_changed_features;

	-- add deleted object
	sql := format(
		$$

		SELECT array_to_json(

					array_agg(
							CASE WHEN t.typ = 'B' THEN
								jsonb_build_object('type', 'Feature')||
								jsonb_build_object('properties',
										jsonb_build_object('featuretype','ArealressursGrense')||
										jsonb_build_object('identifikasjon', jsonb_build_object('lokalId',t.fid) ))||
								jsonb_build_object('geometry',%2$L::jsonb)||
								jsonb_build_object('update', jsonb_build_object('action','Erase'))
							ELSE
								jsonb_build_object('type', 'Feature')||
								jsonb_build_object('properties',
										jsonb_build_object('featuretype','ArealressursFlate')||
										jsonb_build_object('identifikasjon', jsonb_build_object('lokalId',t.fid) ))||
								jsonb_build_object('geometry',%2$L::jsonb)||
								jsonb_build_object('update', jsonb_build_object('action','Erase'))
							END
					)
				)
			FROM %1$s t
			WHERE act = 'D'
		$$,
		'remove_borders_fkb50_results'::regclass,
		null,
		null
	);
	EXECUTE sql INTO geojson_deleted_features;

	drop table remove_borders_fkb50_results;

	--RAISE DEBUG 'geojson_deleted: %', geojson_deleted_features;


	geojson_features = jsonb_build_object(
			'features', (geojson_changed_features || geojson_deleted_features)
			)::jsonb;

	geojson = (geojson || geojson_features);
	RETURN geojson;
END;
$BODY$ --}
LANGUAGE plpgsql;



--{
--
-- Wrapper around topo_update.app_do_GetFeaturesAsTopoJSON
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/blob/develop/src/sql/topo_update/topo_update.app_do_GetFeaturesAsTopoJSON
--
-- Returns Border and Surface features in TopoJSON format, see
-- https://github.com/mbostock/topojson-specification/blob/master/README.md
--
CREATE OR REPLACE FUNCTION topo_ar5ngis.get_features_as_topojson(
	-- A Geometry object whose bounding box will be used
	-- to select the features to be returned. Only intesecting
	-- features will be returned.
	bbox GEOMETRY,
	-- Identifier of the spatial reference system to use for the
	-- output arc coordinates. If 0, it will use the app topology SRID.
	out_srid INTEGER,

	-- Maximum number of decimal digits to use for the arc coordinates.
	max_decimal_digits INTEGER

)
RETURNS JSONB AS
$BODY$ --{
DECLARE

	-- Application name, will be used to fetch
	-- configuration from application schema.
	-- See doc/APP_CONFIG.md
	appname NAME = 'topo_ar5ngis';

BEGIN

	RETURN topo_update.app_do_GetFeaturesAsTopoJSON(
		appname,
		bbox,
		out_srid,
		max_decimal_digits
	);
END;
$BODY$ --}
LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION topo_ar5ngis.get_removableborders_as_topojson(
	-- A Geometry object whose bounding box will be used
	-- to select the features to be returned. Only intesecting
	-- features will be returned.
	bbox GEOMETRY,
	-- Identifier of the spatial reference system to use for the
	-- output arc coordinates. If 0, it will use the app topology SRID.
	out_srid INTEGER,

	-- Maximum number of decimal digits to use for the arc coordinates.
	max_decimal_digits INTEGER

)
RETURNS JSONB AS
$BODY$ --{
DECLARE

	-- Application name, will be used to fetch
	-- configuration from application schema.
	-- See doc/APP_CONFIG.md
	appname NAME = 'topo_ar5ngis';

BEGIN

	RETURN topo_update.app_do_GetMergeableSurfaceBordersAsGeoJSON(
		appname,
		bbox,
		out_srid,
		max_decimal_digits
	);
END;
$BODY$ --}
LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION topo_ar5ngis.get_removableborders(
	-- get list of id' that can be deleted
	-- features will be returned.
	bbox GEOMETRY
)
RETURNS TABLE(border_ids text[], side_surfaces text[]) AS
$BODY$ --{
DECLARE

	-- Application name, will be used to fetch
	-- configuration from application schema.
	-- See doc/APP_CONFIG.md
	appname NAME = 'topo_ar5ngis';

BEGIN

	RETURN QUERY SELECT * FROM topo_update.app_do_GetMergeableSurfaceBorders(
		appname,
		bbox
	);
END;
$BODY$ --}
LANGUAGE plpgsql;


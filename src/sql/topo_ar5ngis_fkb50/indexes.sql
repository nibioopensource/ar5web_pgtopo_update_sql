/**

Table "topo_ar5ngis.face_attributes"

Indexes:
    "face_attributes_pkey" PRIMARY KEY, btree (identifikasjon_lokal_id)
    "face_attributes_id_idx" btree (((omrade).id))
    "face_attributes_identifikasjon_lokal_id_idx" btree ((identifikasjon_lokal_id::text))
    "face_attributes_layer_id_idx" btree (((omrade).layer_id))
    "face_attributes_type_idx" btree (((omrade).type))

*/

-- This three indexes did not help very much

CREATE index on topo_ar5ngis.face_attributes (((omrade).id));
CREATE index on topo_ar5ngis.face_attributes (((omrade).layer_id));
CREATE index on topo_ar5ngis.face_attributes (((omrade).type));

-- Adding this seems to help a lot 
-- So we need to fix https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/214

CREATE index on topo_ar5ngis.face_attributes ((identifikasjon_lokal_id::text));



/**
    
Table "topo_ar5ngis.edge_attributes"

Indexes:
    "edge_attributes_pkey" PRIMARY KEY, btree (identifikasjon_lokal_id)
    "edge_attributes_id_idx" btree (((grense).id))
    "edge_attributes_identifikasjon_lokal_id_idx" btree ((identifikasjon_lokal_id::text))
    "edge_attributes_layer_id_idx" btree (((grense).layer_id))
    "edge_attributes_type_idx" btree (((grense).type))
    
*/    

-- This three indexes did not help very much

CREATE index on topo_ar5ngis.edge_attributes (((grense).id));
CREATE index on topo_ar5ngis.edge_attributes (((grense).layer_id));
CREATE index on topo_ar5ngis.edge_attributes (((grense).type));

-- Adding this seems to help a lot 
-- So we need to fix https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/214

CREATE index on topo_ar5ngis.edge_attributes ((identifikasjon_lokal_id::text));



--2023-05-20 08:15:54,313 [pool-10-thread-1] DEBUG PgUpdateApiV2 no.skogoglandskap.topo.client.PgUpdateApiV2.syncPostgisTopologyDBWithJsonPayloadPriv(PgUpdateApiV2.java:336) 336 - leave changedDb:true ms. used:7480 with jsonlength:4644661 checkOnVersionId:true for bbox:POINT (12.261062017478602 61.31772270922819) area m2 3.712593792366899E-4 - 
--2023-05-20 08:15:54,313 [pool-10-thread-1] DEBUG PgUpdateApiV2 no.skogoglandskap.topo.client.PgUpdateApiV2.syncPostgisTopologyDBWithJsonPayload(PgUpdateApiV2.java:148) 148 - after 0 retries, leave changedDb:true ms. used:7480 with jsonlength:4644661 for bbox:POINT (12.261062017478602 61.31772270922819) area m2 3.712593792366899E-4 - 

CREATE OR REPLACE VIEW topo_ar5ngis.face_attributes_export_fkb50 AS
SELECT

    identifikasjon_lokal_id AS id,
    identifikasjon_navnerom,
    -- See https://gitlab.com/nibioopensource/ar5web_pgtopo_update_sql/-/merge_requests/3#note_1323898482
    arealtype::VARCHAR,
    treslag::VARCHAR,
    skogbonitet::VARCHAR,
    grunnforhold::VARCHAR,
    datafangstdato,
    oppdateringsdato::timestamp,
    verifiseringsdato,
    opphav,
    klassifiseringsmetode,
    informasjon,
    1 AS editable,
	featuretype,
	format('%s000', identifikasjon_versjon_id::timestamp) AS identifikasjon_versjon_id,
	registreringsversjon,
	identifikasjon_lokal_id,
	omrade
	FROM topo_ar5ngis.face_attributes;

CREATE OR REPLACE VIEW topo_ar5ngis.edge_attributes_export_fkb50 AS
SELECT
	featuretype,
    identifikasjon_lokal_id,
    identifikasjon_navnerom,
	-- See https://gitlab.com/nibioopensource/ar5web_pgtopo_update_sql/-/merge_requests/3#note_1323898482
	format('%s000', identifikasjon_versjon_id::timestamp)
		identifikasjon_versjon_id,
	datafangstdato,
    verifiseringsdato,
    oppdateringsdato::timestamp,
    opphav,
    kvalitet,
	avgrensing_type::VARCHAR,
	registreringsversjon,
	grense
FROM topo_ar5ngis.edge_attributes;


-- This is a view just used by the client to create TopoJSON
-- TODO remove this

CREATE OR REPLACE VIEW topo_ar5ngis.webclient_flate_topojson_flate_v AS
SELECT
    identifikasjon_lokal_id AS id,
    -- See https://gitlab.com/nibioopensource/ar5web_pgtopo_update_sql/-/merge_requests/3#note_1323898482
    arealtype::VARCHAR,
    treslag::VARCHAR,
    skogbonitet::VARCHAR,
    grunnforhold::VARCHAR,
    datafangstdato,
    oppdateringsdato::timestamp,
    verifiseringsdato,
    opphav,
    klassifiseringsmetode,
    informasjon,
    omrade,
    1 AS editable
FROM topo_ar5ngis.face_attributes;

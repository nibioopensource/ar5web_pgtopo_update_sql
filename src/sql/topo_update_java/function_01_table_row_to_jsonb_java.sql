--
-- Take a PostgreSQL RECORD value from given based on unique primary key and a pgCol JSON mapping
-- file (col_map) and return a JSONB object with keys specified
-- by the mapping file and values taken from the RECORD.
--
-- The format of the pgCol JSON mapping file is the same
-- as used by the topo_update.json_props_to_pg_cols function
-- to allow for re-using the same mapping file specification
-- both ways.
--
-- {
CREATE OR REPLACE FUNCTION topo_update.table_row_to_jsonb_java(
	layerTable TEXT,
	layerIDColumn TEXT,
	layerIDValue TEXT, -- must return on row only
	layerColMap TEXT -- See json_props_to_pg_cols
)
RETURNS TEXT
AS $BODY$ -- }{
DECLARE
	procName TEXT := 'topo_update.table_row_to_jsonb_java';
	sql TEXT;
	result JSONB;
	
BEGIN

	
	sql := format('(SELECT t FROM %1$s t WHERE t.%2$I = %3$L)',layerTable,layerIDColumn,layerIDValue);
	
	RAISE DEBUG 'procName: % sql %', procName, sql;


	EXECUTE format('SELECT topo_update.record_to_jsonb(%1$s,%2$L::JSONB)',sql,layerColMap) INTO result;
    
    RETURN result::TEXT;
END;
$BODY$ --}
LANGUAGE plpgsql;

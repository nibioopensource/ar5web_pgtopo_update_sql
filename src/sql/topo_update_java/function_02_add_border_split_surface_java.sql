
-- {
-- This function will be called from the client when the user
-- draws a line to either create a new surface or extend an
-- existing one.
--
-- This is temporary function used from java to avoid casting
-- }{
CREATE OR REPLACE FUNCTION topo_update.add_border_split_surface_java(

	-- A GeoJSON Feature object representing the
	-- line drawn by the user and the holding the
	-- attributes to use with the border and the
	-- surface layers
	feature text, --JSONB,

	-- Surface layer table
	surfaceLayerTable text, -- REGCLASS,

	-- Surface layer TopoGeometry column name
	surfaceLayerGeomColumn text, -- NAME,

	-- Surface layer primary key column name
	surfaceLayerIdColumn text, -- NAME,

	-- Mapping file for new objects in the surface layer
	-- see json_props_to_pg_cols
	surfaceLayerColMapNewRows text, -- JSONB,

	-- Mapping file for update objects in the surface layer (if no data already exist for where the surface is created)
	-- This can be null and update will use the samme mapping files as for new rows.
	-- see json_props_to_pg_cols
	surfaceLayerColMapUpdateRows text, -- JSONB,

	-- Border layer table
	borderLayerTable text, -- REGCLASS,

	-- Name of Border layer TopoGeometry column
	borderLayerGeomColumn text, -- NAME,

	-- Surface layer primary key column name
	borderLayerIdColumn text, -- NAME,

	-- Mapping file for new objects in the border layer
	-- see json_props_to_pg_cols
	borderLayerColMapNewRows text, -- JSONB,

	-- Mapping file for update objects in the border layer (if no data already exist for where the border is created)
	-- This can be null and update will use the samme mapping files as for new rows.
	-- see json_props_to_pg_cols
	borderLayerColMapUpdateRows text, -- JSONB,

	-- Snap tolerance to use when
	-- inserting the new line
	-- in the topology
	tolerance float8
)
RETURNS TABLE(fid text, typ char, act char) AS $BODY$
DECLARE


BEGIN

	DROP TABLE IF EXISTS pg_temp.temp_result;

	CREATE TEMP TABLE temp_result AS (
		SELECT * FROM topo_update.add_border_split_surface(
	
		-- A GeoJSON Feature object representing the
		-- line drawn by the user and the holding the
		-- attributes to use with the border and the
		-- surface layers
		feature::JSONB,
	
		-- Surface layer table
		surfaceLayerTable::REGCLASS,
	
		-- Surface layer TopoGeometry column name
		surfaceLayerGeomColumn::NAME,
	
		-- Surface layer primary key column name
		surfaceLayerIdColumn::NAME,
	
		-- Border layer table
		borderLayerTable::REGCLASS,
	
		-- Name of Border layer TopoGeometry column
		borderLayerGeomColumn::NAME,
	
		-- Surface layer primary key column name
		borderLayerIdColumn::NAME,
	
		-- Snap tolerance to use when
		-- inserting the new line
		-- in the topology
		tolerance,
	
		-- TODO need to find out how to use BC: borderLayerColMap, SC: surfaceLayerColMapNewRows, SM, SS: surfaceLayerColMapUpdateRows
		'topo_ar5ngis.add_border_split_surface_java_tmp_ColMapProvider'::regproc, -- colMapProviderFunc
		(FORMAT('{ "BM": %1$s, "BC": %2$s, "BS": %1$s, "SM": %3$s , "SC": %4$s , "SS": %3$s }',borderLayerColMapUpdateRows,borderLayerColMapNewRows,surfaceLayerColMapUpdateRows,surfaceLayerColMapNewRows))::jsonb -- colMapProviderParam
		)
	);

	
	-- TODO only call this if requested and add parameters that 
	-- TODO move the to add_border_split_surface
	CREATE TEMPORARY TABLE add_border_results_more_info AS (
	SELECT s.fid, s.typ, s.act
	FROM topo_update.get_border_split_info(
		-- Surface layer table
		surfaceLayerTable::REGCLASS,
	
		-- Surface layer TopoGeometry column name
		surfaceLayerGeomColumn::NAME,
	
		-- Surface layer primary key column name
		surfaceLayerIdColumn::NAME,
	
		-- Border layer table
		borderLayerTable::REGCLASS,
	
		-- Name of Border layer TopoGeometry column
		borderLayerGeomColumn::NAME,
	
		-- Surface layer primary key column name
		borderLayerIdColumn::NAME,
	

		-- The result from the split operation
		'pg_temp.temp_result'

		) AS s
	);

	INSERT INTO temp_result(fid,typ,act)
	SELECT mi.fid, mi.typ, mi.act
	FROM pg_temp.add_border_results_more_info mi;
	
    DROP TABLE IF EXISTS pg_temp.add_border_results_more_info;

	
	RETURN QUERY SELECT s.fid, s.typ, s.act FROM temp_result s;


END;
$BODY$ LANGUAGE plpgsql; --}


-- This is wrapper function used by java
--
-- Update a non spatial columns feature in a table by id 
--
-- Returns NULL or the ID of the updated rows
-- if a feature was updated the id of the updated row are returned
--
CREATE OR REPLACE FUNCTION topo_update.update_feature_attribute_java(

	-- The 'properties' component of a GeoJSON Feature object
	-- See https://geojson.org/
	feature_properties TEXT,

	-- Target layer table (where to update the new feature in)
	layerTable TEXT,

	-- Name of the primary key column in the target layer table
	layerIdColumn TEXT,

	-- JSON mapping of PG columns and values
	-- from JSON attributes and values
	-- See topo_update.json_props_to_pg_cols() function
	layerColMap TEXT, 
	
	opphav TEXT DEFAULT NULL -- this will be sent from the server and controlled java backend server and override settings from the client, TODO mabe make a more generic struture for this ?
	

) RETURNS TEXT
AS $BODY$ -- {
DECLARE
	feature JSONB := feature_properties::jsonb;
BEGIN
	
	-- TODO should also be wrapper function
	-- Set default kvalitet
	feature := jsonb_set(
		feature,
		'{properties, kvalitet}',
		to_jsonb('("80",1,"0")'::TEXT)
	);

	--   kartstandard AR5,
	feature := jsonb_set(
		feature,
		'{properties, kartstandard}',
		to_jsonb('AR5'::TEXT)
	);


	feature := jsonb_set(
		feature,
		'{properties, "identifikasjon_versjon_id" }',
		to_jsonb(now())
	);
	
	-- Get datafangstdato for new object
--	IF feature -> 'properties' ->> 'verifiseringsdato' IS NOT NULL THEN
--		feature := jsonb_set(
--			feature,
--			'{properties, datafangstdato}',
--			to_jsonb(feature -> 'properties' ->> 'verifiseringsdato')
--		);
--	END IF;

	-- Get value from server side
	IF opphav IS NOT NULL THEN
		feature := jsonb_set(
			feature,
			'{properties, opphav}',
			to_jsonb(opphav)
		);
	END IF;

	
	RETURN topo_update.update_feature_attribute(
	feature::JSONB,
	layerTable::REGCLASS,
	layerIdColumn::NAME,
	layerColMap::JSONB
);
END;
$BODY$ --}
LANGUAGE plpgsql;

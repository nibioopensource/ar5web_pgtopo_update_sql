-- Remove dangling edges from the definition of a primitive lineal TopoGeometry
CREATE OR REPLACE FUNCTION topo_update.remove_dangling_topogeometry_edges(

	-- A TopoGeometry only defined by edges
	-- NOTE: it is *assumed* that the TopoGeometry is for
	--       a non-hierarchical layer, failing which the
	--       consequences of calling this function are unspecified
	--       and possibly destructive.
	--
	-- NOTE: it is *assumed* the the TopoGeometry is of LINEAL
	--       type, meaning it is ONLY defined by edges, failing
	--       which the consequences of calling this function are
	--       unspecified and possibly destructive.
	tg topology.TopoGeometry,

	-- Name of the topology the TopoGeometry is based on
	-- NOTE: could be derived by the TopoGeometry object
	--       but we accept the topology name from caller
	--       for performance reasons.
	toponame TEXT

)
RETURNS VOID
AS $BODY$ --{
DECLARE
	sql TEXT;
BEGIN
	sql := format(
		$$
DELETE FROM %1$I.relation r
WHERE r.layer_id = %2$L
AND r.topogeo_id = %3$L
AND r.element_id IN (
	SELECT e.edge_id
	FROM %1$I.edge_data e
	JOIN %1$I.relation r ON (e.edge_id = r.element_id)
	WHERE e.edge_id = r.element_id
	AND e.left_face = e.right_face
	AND r.layer_id = %2$L
	AND r.topogeo_id = %3$L
)
		$$,
		toponame,    -- %1$
		id(tg),      -- %2$
		layer_id(tg) -- %3$
	);
	RAISE DEBUG 'SQL: %', sql;
	EXECUTE sql;
END;
$BODY$ --}
LANGUAGE 'plpgsql';


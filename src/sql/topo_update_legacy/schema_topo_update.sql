-- create schema for topo_update data, tables, ....
CREATE SCHEMA IF NOT EXISTS topo_update;

-- make comment this schema
COMMENT ON SCHEMA topo_update IS 'Is a schema for topo_update attributes and ref to topolygy data. Don´t do any direct update on tables in this schema, all changes should be done using stored proc.';


CREATE TABLE topo_update.json_to_pgcols_maps(
	-- target table
	targetTable REGCLASS,
	-- arbitrary name of a class of JSON payload types
	payloadClass TEXT,
	-- Mapping file
	map JSONB
);

-- make the scema public
GRANT USAGE ON SCHEMA topo_update to public;

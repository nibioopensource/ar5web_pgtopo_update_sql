This the basic for this case. This ascii art below show surfaces with id's . This are generated based on borders.

In the no_over_lap_gap_01.xsd the sequence of SimpleFeatureSurface refers to simple feature surface in this map.

--    ,--+-B4-+--B5---,
--    |       |       |
--    |      B8  S3  B7
--    |       |       |
--    | S2    +--B9---+
--    |       |       |
--    |      B3  S1   |
--    |       |       |
--    `-B1----+--B2---'

set client_min_messages to WARNING;
set timezone to utc;

CREATE OR REPLACE FUNCTION add_border_and_check(lbl text, payload JSONB)
RETURNS TABLE(o TEXT)
AS $BODY$
DECLARE
	oa TEXT[];
	rec RECORD;
BEGIN

	CREATE TEMP TABLE add_border_results AS
	SELECT * FROM topo_ar5ngis.add_border(payload::text);

	FOR rec IN
		SELECT
			ab.fid,
			regexp_replace(ab.fid, '.*-', '')::int::text hrfid,
			ab.typ,
			ab.act,
			regexp_replace(ab.frm, '.*-', '')::int::text frm,
			ST_AsEWKT(ST_Normalize(ST_CollectionHomogenize(ea.grense)), 4) ea_geo,
			date_trunc('second',(now() - ea.identifikasjon_versjon_id))::text ea_age,
			ST_AsEWKT(ST_Normalize(ST_CollectionHomogenize(fa.omrade)), 4) fa_geo,
			date_trunc('second',(now() - fa.identifikasjon_versjon_id))::text fa_age
		FROM add_border_results ab
		LEFT JOIN topo_ar5ngis.edge_attributes ea ON (ab.fid = ea.identifikasjon_lokal_id::text)
		LEFT JOIN topo_ar5ngis.face_attributes fa ON (ab.fid = fa.identifikasjon_lokal_id::text)
		ORDER BY ab.typ, ab.fid
	LOOP
		oa := ARRAY[
					lbl,
					rec.hrfid,
					rec.typ,
					rec.act,
					rec.frm
		];
		IF rec.typ = 'B' THEN
			oa := array_cat(oa,
				ARRAY[
					rec.ea_geo,
					rec.ea_age
				]
			);
		ELSE
			oa := array_cat(oa,
				ARRAY[
					rec.fa_geo,
					rec.fa_age
				]
			);
		END IF;

		o := array_to_string(oa, '|');
		RETURN NEXT;
	END LOOP;

	-- Report Border features not mentioned by add_border result
	FOR rec IN
		SELECT
			identifikasjon_lokal_id,
			regexp_replace(identifikasjon_lokal_id::text, '.*-', '')::int::text hrfid,
			ST_AsEWKT(ST_Normalize(ST_CollectionHomogenize(grense)), 4) geo,
			date_trunc('second',(now() - identifikasjon_versjon_id))::text age
		FROM topo_ar5ngis.edge_attributes
		WHERE identifikasjon_lokal_id::text NOT IN (
			SELECT fid FROM add_border_results
			WHERE typ = 'B'
		)
		ORDER BY identifikasjon_lokal_id
	LOOP
		oa := ARRAY[
					lbl,
					rec.hrfid,
					'B',
					'-',
					rec.geo,
					rec.age
		];
		o := array_to_string(oa, '|');
		RETURN NEXT;
	END LOOP;

	-- Report Surface features not mentioned by add_border result
	FOR rec IN
		SELECT
			identifikasjon_lokal_id,
			regexp_replace(identifikasjon_lokal_id::text, '.*-', '')::int::text hrfid,
			ST_AsEWKT(ST_Normalize(ST_CollectionHomogenize(omrade)), 4) geo,
			date_trunc('second',(now() - identifikasjon_versjon_id))::text age
		FROM topo_ar5ngis.face_attributes
		WHERE identifikasjon_lokal_id::text NOT IN (
			SELECT fid FROM add_border_results
			WHERE typ = 'S'
		)
		ORDER BY identifikasjon_lokal_id
	LOOP
		oa := ARRAY[
					lbl,
					rec.hrfid,
					'B',
					'-',
					rec.geo,
					rec.age
		];
		o := array_to_string(oa, '|');
		RETURN NEXT;
	END LOOP;

	DROP TABLE add_border_results;

END;
$BODY$ LANGUAGE 'plpgsql';

BEGIN;


SELECT * FROM add_border_and_check('T1',
	$$
		{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:4258"
					}
				},
				"type": "LineString",
				"coordinates": [[0,0],[20,0],[20,20],[0,20],[0,0]]
			},
			"properties": {
				"opphav": "T1"
			}
		}
	$$
);

COMMIT;

BEGIN ;

SELECT * FROM add_border_and_check('T2',
	$$
		{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:4258"
					}
				},
				"type": "LineString",
				"coordinates": [[10,5],[30,5],[30,25],[10,25],[10,5]]
			},
			"properties": {
				"opphav": "T2"
			}
		}
	$$
);

COMMIT;


UPDATE topo_ar5ngis.face_attributes SET identifikasjon_versjon_id = now() - '20s'::interval;
UPDATE topo_ar5ngis.edge_attributes SET identifikasjon_versjon_id = now() - '20s'::interval;


SELECT * FROM
add_border_and_check('T3',
	$$
		{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:4258"
					}
				},
				"type": "LineString",
				"coordinates": [[25,8],[40,8],[40,35],[25,35],[25,8]]
			},
			"properties": {
				"opphav": "T2"
			}
		}
	$$
);


DROP FUNCTION add_border_and_check(lbl text, payload JSONB);

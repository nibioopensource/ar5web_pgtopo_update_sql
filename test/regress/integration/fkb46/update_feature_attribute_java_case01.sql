BEGIN;
set client_min_messages to WARNING;
set timezone to utc;

-- the bug desciption is here https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/114#note_559441395


\i :regdir/../../src/sql/topo_update_java/function_01_table_row_to_jsonb_java.sql
\i :regdir/../../src/sql/topo_update_java/function_02_add_border_split_surface_java.sql
\i :regdir/../../src/sql/topo_update_java/function_02_update_feature_attribute_java.sql
\i :regdir/utils/check_overlap.sql

-- Make :datadir available as a variable
SELECT :'regdir' || '/../../test/data/fkb46' as datadir \gset


CREATE TEMP TABLE json_input_01(payload text);
SELECT :'datadir' || '/log_getData_20210909-072751_NIBIO_Lars__NGIS_Json_srid_25832_UTF-8.json' as x \gset
COPY json_input_01 FROM :'x';
SELECT '--json-input-',length(payload) FROM json_input_01;

-- TOPOLOGY NAME: update_features_test_topo
-- AREAL LAYER:   topo_ar5ngis.face_attributes
-- LINEAL LAYER:  topo_ar5ngis.edge_attributes


-- Create mapping schema and table and add mappings

SELECT '--a1--', topo_ar5ngis.update_features(
	(SELECT payload FROM json_input_01)::jsonb,
	ST_GeomFromEWKT('SRID=4258; POLYGON ((11.31492690543025 59.5499012080781, 11.31492690543025 59.55847176806804, 11.350722650159124 59.55847176806804, 11.350722650159124 59.5499012080781, 11.31492690543025 59.5499012080781))')
);



-- Create mapping schema and table and add mappings
CREATE SCHEMA update_features_test;

CREATE TABLE update_features_test.json_mapping(
	target_table regclass PRIMARY KEY,
	mapping_to_ngis json);

INSERT INTO update_features_test.json_mapping (target_table, mapping_to_ngis)
VALUES (
	'topo_ar5ngis.face_attributes',
	'{
      "featuretype": [ "featuretype" ],
      "identifikasjon_lokal_id": [ "identifikasjon", "lokalId" ],
      "identifikasjon_navnerom": [ "identifikasjon", "navnerom" ],
      "identifikasjon_versjon_id": [ "identifikasjon", "versjonId" ],
      "datafangstdato": [ "datafangstdato" ],
      "kartstandard": [ "kartstandard" ],
	  "informasjon": [ "informasjon" ],
      "verifiseringsdato": [ "verifiseringsdato" ],
      "oppdateringsdato": [ "oppdateringsdato" ],
      "opphav": [ "opphav" ],
      "kvalitet.maalemetode": [ "kvalitet", "målemetode" ],
      "kvalitet.noyaktighet": [ "kvalitet", "nøyaktighet" ],
      "kvalitet.synbarhet": [ "kvalitet", "synbarhet" ],
      "arealtype": [ "arealtype" ],
      "treslag": [ "treslag" ],
      "skogbonitet": [ "skogbonitet" ],
      "grunnforhold": [ "grunnforhold" ],
      "registreringsversjon.produkt": [ "registreringsversjon", "produkt" ],
      "registreringsversjon.versjon": [ "registreringsversjon", "versjon" ],
      "geometry_properties_position": [ "geometry_properties", "position" ]
	}'
);

-- Update attribute

SELECT 'before_update_feature_attribute_java_',
   topo_update.table_row_to_jsonb_java(
   'topo_ar5ngis.ngis_export_data_flate_v'::text,
   'identifikasjon_lokal_id'::text,
   '003a736e-ae1a-4fd0-a7a1-f6600b000e44'::text,
   (SELECT mapping_to_ngis FROM update_features_test.json_mapping WHERE target_table = 'topo_ar5ngis.face_attributes'::regclass)::TEXT
   );

SELECT 'upd_feature_attribute_java', (topo_update.update_feature_attribute_java(
	$$
		{
		  "properties" : {
		    "id" : "003a736e-ae1a-4fd0-a7a1-f6600b000e44",
		    "arealtype" : 11,
		    "treslag" : 98,
		    "skogbonitet" : 98,
		    "grunnforhold" : 98,
		    "reinbeitebruker_id" : "SFKB",
		    "verifiseringsdato" : "2021-05-05",
		    "opphav" : "3014",
		    "informasjon" : null
		  }
		}
	$$, -- feature

	-- Surface layer table
	'topo_ar5ngis.face_attributes'::TEXT,

	-- Surface layer primary key column name
	'identifikasjon_lokal_id'::TEXT,
	$$
		{
			"identifikasjon_lokal_id": [
				"id"
			],
			"identifikasjon_versjon_id": [
				"identifikasjon_versjon_id"
			],
			"kvalitet": [
				"kvalitet"
			],
			"kartstandard": [
				"kartstandard"
			],
			"informasjon": [
				"informasjon"
			],
			"opphav": [
				"opphav"
			],
			"arealtype": [
				"arealtype"
			],
			"treslag": [
				"treslag"
			],
			"skogbonitet": [
				"skogbonitet"
			],
			"grunnforhold": [
				"grunnforhold"
			],
			"datafangstdato": [
				"datafangstdato"
			],
			"verifiseringsdato": [
				"verifiseringsdato"
			],
			"oppdateringsdato": [
				"oppdateringsdato"
			]
		}
$$, -- layerColMap
	'webklient_test'
) IS NOT NULL);


-- update result dates to make tests stable
UPDATE topo_ar5ngis.face_attributes f
SET identifikasjon_versjon_id = '1970-01-01T00:00:00'
WHERE f.identifikasjon_versjon_id > (now() - '60s'::interval);

UPDATE topo_ar5ngis.edge_attributes f
SET identifikasjon_versjon_id = '1970-01-01T00:00:00'
WHERE f.identifikasjon_versjon_id > (now() - '60s'::interval);

UPDATE topo_ar5ngis.face_attributes f
SET oppdateringsdato='1970-01-01T00:00:00'
WHERE f.oppdateringsdato > (now() - '60s'::interval);

UPDATE topo_ar5ngis.edge_attributes f
SET oppdateringsdato='1970-01-01T00:00:00'
WHERE f.oppdateringsdato > (now() - '60s'::interval);

SELECT 'after_update_feature_attribute_java_',
   topo_update.table_row_to_jsonb_java(
   'topo_ar5ngis.ngis_export_data_flate_v'::text,
   'identifikasjon_lokal_id'::text,
   '003a736e-ae1a-4fd0-a7a1-f6600b000e44'::text,
   (SELECT mapping_to_ngis FROM update_features_test.json_mapping WHERE target_table = 'topo_ar5ngis.face_attributes'::regclass)::TEXT
   );

------------------------------------
-- Cleanup local stuff
------------------------------------
DROP SCHEMA update_features_test CASCADE;

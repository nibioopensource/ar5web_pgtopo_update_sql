BEGIN;

set client_min_messages to WARNING;
set timezone to utc;

-- the bug description is here https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/191

-- Here we test issue_191_ar5_merged_input_two.json

\i :regdir/../../src/sql/topo_update_java/function_02_add_border_split_surface_java.sql
\i :regdir/../../src/sql/topo_update_java/function_02_update_feature_attribute_java.sql
\i :regdir/../../src/sql/topo_update_legacy/function_01_query_to_topojson.sql
\i :regdir/../../src/sql/topo_update_legacy/function_01_as_topo_json.sql
\i :regdir/../../src/sql/topo_update_legacy/function_01_get_adjusted_edge.sql
\i :regdir/utils/check_overlap.sql

-- Make :datadir available as a variable
SELECT :'regdir' || '/../../test/data/fkb46' as datadir \gset

-- Make :input available as a variable
SELECT 'issue_191_ar5_merged_input_two.json' as input_file_02 \gset

-- Make :bbox available as a variable
SELECT 'SRID=4258;POLYGON ((12.068091259721626 64.99994766115645, 12.068091259721626 65.01105706149139, 12.08703147276633 65.01105706149139, 12.08703147276633 64.99994766115645, 12.068091259721626 64.99994766115645))' as bbox \gset

-- Make :topo_sql_cmd with :bbox available as a variable used by topo_update.query_to_topojson
SELECT 'SELECT DISTINCT a.* FROM topo_ar5ngis.webclient_flate_topojson_flate_v a ,topo_ar5ngis_sysdata_webclient.face fa, topo_ar5ngis_sysdata_webclient.relation re, topology.layer tl WHERE fa.mbr && ST_GeomFromEWKT('''||:'bbox'||''') AND ((a.omrade ).id) = re.topogeo_id AND re.layer_id = tl.layer_id AND tl.schema_name = ''topo_ar5ngis'' AND tl.table_name = ''face_attributes'' and fa.face_id=re.element_id' as topo_sql_cmd \gset

CREATE TEMP TABLE json_input_01(payload text);
SELECT :'datadir' || '/' || :'input_file_02' as x \gset
COPY json_input_01 FROM :'x';
SELECT '--json-input-',length(payload) FROM json_input_01;

-- TOPOLOGY NAME: update_features_test_topo
-- AREAL LAYER:   topo_ar5ngis.face_attributes
-- LINEAL LAYER:  topo_ar5ngis.edge_attributes

SELECT 'input--',:'input_file_02', topo_ar5ngis.update_features(
	(SELECT payload FROM json_input_01)::jsonb,
	ST_GeomFromEWKT(:'bbox'),
	NULL
);

-- Check for topology validity
SELECT
	'before_topo_update.add_border_split_face',
	'validate_topology',
	*
FROM topology.ValidateTopology('topo_ar5ngis_sysdata_webclient');

SELECT :'input_file_02','--t1-- face', count(*)  from topo_ar5ngis.face_attributes;
SELECT :'input_file_02','--t1-- edge', count(*)  from topo_ar5ngis.edge_attributes;
SELECT :'input_file_02','--t1-- test overlaps', count(*) as num_over_lap from topo_update.find_interiors_intersect('topo_ar5ngis.edge_attributes', 'grense', 'identifikasjon_lokal_id');


SELECT :'input_file_02','--t1-- uncovered edges (approximated point on surface)',
ST_AsText(ST_PointOnSurface(geom), 5)
FROM topo_ar5ngis_sysdata_webclient.edge
WHERE geom && ST_GeomFromEWKT(:'bbox')
AND edge_id NOT IN (
	SELECT abs(r.element_id)
	FROM topo_ar5ngis_sysdata_webclient.relation r, topology.layer l
	WHERE r.layer_id = l.layer_id AND l.table_name = 'edge_attributes'
)
ORDER BY 3;

SELECT :'input_file_02','--t1-- uncovered faces (approximated bbox centroid)',
ST_AsText(ST_PointOnSurface(mbr), 5)
FROM topo_ar5ngis_sysdata_webclient.face
WHERE mbr && ST_GeomFromEWKT(:'bbox')
AND face_id NOT IN (
	SELECT abs(r.element_id)
	FROM topo_ar5ngis_sysdata_webclient.relation r, topology.layer l
	WHERE r.layer_id = l.layer_id AND l.table_name = 'face_attributes'
)
ORDER BY 3;

-- make topoJson
\out /tmp/topo_json_result.json
SELECT topo_update.query_to_topojson(
:'topo_sql_cmd',
32632,
0,
0,
ST_GeomFromEWKT(:'bbox')
);

-- test topoJson
\! jq -e . /tmp/topo_json_result.json  > /dev/null

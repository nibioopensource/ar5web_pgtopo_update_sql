
set client_min_messages to WARNING;
set timezone to utc;

-- the bug description is here https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/191

-- Here we test issue_191_ar5_merged_input_one.json

\i :regdir/../../src/sql/topo_update_java/function_02_add_border_split_surface_java.sql
\i :regdir/../../src/sql/topo_update_java/function_02_update_feature_attribute_java.sql
\i :regdir/../../src/sql/topo_update_legacy/function_01_query_to_topojson.sql
\i :regdir/../../src/sql/topo_update_legacy/function_01_as_topo_json.sql
\i :regdir/../../src/sql/topo_update_legacy/function_01_get_adjusted_edge.sql

-- Make :datadir available as a variable
SELECT :'regdir' || '/../../test/data/fkb46' as datadir \gset

-- Make :input available as a variable
SELECT 'issue_192_ar5_input.json' as input_file_01 \gset

-- Make :bbox available as a variable
SELECT 'SRID=4258;POLYGON ((11.35308452047805 59.50072776312051, 11.35308452047805 59.507189728669815, 11.37358530253694 59.507189728669815, 11.37358530253694 59.50072776312051, 11.35308452047805 59.50072776312051))' as bbox \gset

-- Make :topo_sql_cmd with :bbox available as a variable used by topo_update.query_to_topojson
SELECT 'SELECT DISTINCT a.* FROM topo_ar5ngis.webclient_flate_topojson_flate_v a ,topo_ar5ngis_sysdata_webclient.face fa, topo_ar5ngis_sysdata_webclient.relation re, topology.layer tl WHERE fa.mbr && ST_GeomFromEWKT('''||:'bbox'||''') AND ((a.omrade ).id) = re.topogeo_id AND re.layer_id = tl.layer_id AND tl.schema_name = ''topo_ar5ngis'' AND tl.table_name = ''face_attributes'' and fa.face_id=re.element_id' as topo_sql_cmd \gset

CREATE TEMP TABLE json_input_01(payload text);
SELECT :'datadir' || '/' || :'input_file_01' as x \gset
COPY json_input_01 FROM :'x';
SELECT '--json-input-',length(payload) FROM json_input_01;

-- TOPOLOGY NAME: update_features_test_topo
-- AREAL LAYER:   topo_ar5ngis.face_attributes
-- LINEAL LAYER:  topo_ar5ngis.edge_attributes

SELECT 'input--',:'input_file_01', topo_ar5ngis.update_features(
	(SELECT payload FROM json_input_01)::jsonb,
	ST_GeomFromEWKT(:'bbox'),
	NULL
);

-- Check for topology validity
SELECT
	'before_topo_update.add_border_split_face',
	'validate_topology',
	*
FROM topology.ValidateTopology('topo_ar5ngis_sysdata_webclient');

SELECT :'input_file_01','--t1-- face', count(*)  from topo_ar5ngis.face_attributes;
SELECT :'input_file_01','--t1-- edge', count(*)  from topo_ar5ngis.edge_attributes;
SELECT :'input_file_01','--t1-- test overlaps', count(*) as num_over_lap from topo_update.find_interiors_intersect('topo_ar5ngis.edge_attributes', 'grense', 'identifikasjon_lokal_id');


CREATE TEMPORARY TABLE add_border_results(
	test_name TEXT,
	fid TEXT,
	sid SERIAL,
	typ CHAR,
	act CHAR
);


-- CAll with invalid input P0001 message: Non closed linestring does not intersect at least twice with existing edges for input
DO $$
DECLARE
	v_state text;
	v_msg text;
	v_detail text;
	v_hint text;
	v_context text;
BEGIN
	BEGIN
		INSERT INTO add_border_results(test_name, fid, typ, act)
		SELECT '--t1-- run split', fid, typ, act
		FROM topo_ar5ngis.add_border(

		-- A GeoJSON Feature object representing the
		-- line drawn by the user and the holding the
		-- attributes to use with the border and the
		-- surface layers
		CAST ('{
				"type": "Feature",
				"geometry": {
					"crs": {
						"type": "name",
						"properties": {
							"name": "EPSG:25832"
						}
					},
					"type": "LineString",
					"coordinates": [[633655.7161088311,6598490.937443233],[633636.4529237037,6598453.69528532]]
				},
				"properties": {
					"edge_attributes_props": {
						"opphav": "opphav-edge-test-missing-feature", "datafangstdato": "2000-12-01" , "verifiseringsdato": "2001-02-01"
					},
					"face_attributes_props": {
						"opphav": "opphav-face-test-missing-feature"
					}
				}
			}' AS TEXT),
		'server_side_opphav'::TEXT);

	EXCEPTION WHEN OTHERS
	THEN
    	GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT, v_context = PG_EXCEPTION_CONTEXT;
		RAISE WARNING 'Got exception %', substring(v_msg,POSITION('P0001 message' in v_msg),100);
	END;
END;
$$ LANGUAGE 'plpgsql';



select '--t1-- add border',* from add_border_results;

-- Try to add ok line OK line [[633666.507881548,6598679.996850152],[633661.3710321806,6598534.880855526]]

INSERT INTO add_border_results(test_name, fid, typ, act)
SELECT '--t2-- run split', fid, typ, act
FROM topo_ar5ngis.add_border(

	-- A GeoJSON Feature object representing the
	-- line drawn by the user and the holding the
	-- attributes to use with the border and the
	-- surface layers
	$$
		{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:25832"
					}
				},
				"type": "LineString",
				"coordinates": [[633666.507881548,6598679.996850152],[633661.3710321806,6598534.880855526]]
			},
			"properties": {
				"edge_attributes_props": {
					"opphav": "opphav-edge-test-missing-feature", "datafangstdato": "2000-12-01" , "verifiseringsdato": "2001-02-01"
				},
				"face_attributes_props": {
					"opphav": "opphav-face-test-missing-feature"
				}
			}
		}
	$$::TEXT,
	'server_side_opphav'::TEXT);

select '--t2-- add border',* from add_border_results;


SELECT '--t2-- face', count(*)  from topo_ar5ngis.face_attributes;
SELECT '--t2-- edge', count(*)  from topo_ar5ngis.edge_attributes;

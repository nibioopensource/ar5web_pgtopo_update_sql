BEGIN;

set client_min_messages to WARNING;
set timezone to utc;

-- the bug description is here https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/194

-- We here test with valid input

\i :regdir/../../src/sql/topo_update_java/function_02_add_border_split_surface_java.sql
\i :regdir/../../src/sql/topo_update_java/function_02_update_feature_attribute_java.sql
\i :regdir/../../src/sql/topo_update_legacy/function_01_query_to_topojson.sql
\i :regdir/../../src/sql/topo_update_legacy/function_01_as_topo_json.sql
\i :regdir/../../src/sql/topo_update_legacy/function_01_get_adjusted_edge.sql
\i :regdir/utils/check_overlap.sql

-- Make :datadir available as a variable
SELECT :'regdir' || '/../../test/data/fkb46' as datadir \gset

-- Make :input available as a variable
SELECT '/issue_194_ar5_input_test02.json' as input_file_01 \gset

-- Make :bbox available as a variable
SELECT 'SRID=4258;POLYGON ((9.656690352667766 63.13235020714416, 9.656690352667766 63.13495213790528, 9.667230335247455 63.13495213790528, 9.667230335247455 63.13235020714416, 9.656690352667766 63.13235020714416))' as bbox \gset

-- Make :topo_sql_cmd with :bbox available as a variable used by topo_update.query_to_topojson
SELECT 'SELECT DISTINCT a.* FROM topo_ar5ngis.webclient_flate_topojson_flate_v a ,topo_ar5ngis_sysdata_webclient.face fa, topo_ar5ngis_sysdata_webclient.relation re, topology.layer tl WHERE fa.mbr && ST_GeomFromEWKT('''||:'bbox'||''') AND ((a.omrade ).id) = re.topogeo_id AND re.layer_id = tl.layer_id AND tl.schema_name = ''topo_ar5ngis'' AND tl.table_name = ''face_attributes'' and fa.face_id=re.element_id' as topo_sql_cmd \gset

CREATE TEMP TABLE json_input_01(payload text);
SELECT :'datadir' || '/' || :'input_file_01' as x \gset
COPY json_input_01 FROM :'x';
SELECT '--json-input-',length(payload) FROM json_input_01;

-- TOPOLOGY NAME: update_features_test_topo
-- AREAL LAYER:   topo_ar5ngis.face_attributes
-- LINEAL LAYER:  topo_ar5ngis.edge_attributes

SELECT 'input--',:'input_file_01', topo_ar5ngis.update_features(
	(SELECT payload FROM json_input_01)::jsonb,
	ST_GeomFromEWKT(:'bbox'),
	NULL
);

-- Check for topology validity
SELECT
	'before_topo_update.add_border_split_face',
	'validate_topology',
	*
FROM topology.ValidateTopology('topo_ar5ngis_sysdata_webclient');

SELECT :'input_file_01','--t1-- face', count(*)  from topo_ar5ngis.face_attributes;
SELECT :'input_file_01','--t1-- edge', count(*)  from topo_ar5ngis.edge_attributes;

SELECT :'input_file_01','--t1-- test overlaps', count(*) AS num_over_lap
FROM topo_update.find_interiors_intersect('topo_ar5ngis.edge_attributes', 'grense', 'identifikasjon_lokal_id');


SELECT :'input_file_01','--t1-- missing edge attribute', edge_id
FROM
(
	SELECT edge_id FROM
	(SELECT * FROM topo_ar5ngis_sysdata_webclient.edge WHERE geom && ST_GeomFromEWKT(:'bbox') ) f
	except SELECT abs(r.element_id)
	FROM topo_ar5ngis_sysdata_webclient.relation r, topology.layer l
	WHERE r.layer_id = l.layer_id AND l.table_name = 'edge_attributes'
) s
ORDER BY edge_id;

SELECT :'input_file_01','--t1-- missing face attribute', face_id
FROM
(
	SELECT face_id FROM
	(SELECT * FROM topo_ar5ngis_sysdata_webclient.face WHERE mbr && ST_GeomFromEWKT(:'bbox') ) f
	except SELECT abs(r.element_id)
	FROM topo_ar5ngis_sysdata_webclient.relation r, topology.layer l
	WHERE r.layer_id = l.layer_id AND l.table_name = 'face_attributes'
) s
ORDER BY face_id;
;



CREATE TEMPORARY TABLE add_border_results(
	test_name TEXT,
	fid TEXT,
	sid SERIAL,
	typ CHAR,
	act CHAR
);

CREATE TEMPORARY TABLE input_features (
	feature_num SERIAL,
	input_feature TEXT
);

-- Test feature 1
-- add a new line 1 2022-01-10 09:01:38,672
INSERT INTO input_features(input_feature)
VALUES ('		{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:25832"
					}
				},
				"type": "MultiLineString",
				"coordinates": [[[533353.9801663344,7000624.050934396],[533415.777586423,7000638.861886318],[533484.7251212326,7000646.0120010385]],[[533354.4908888144,7000615.368652235],[533397.9022996206,7000620.986599515],[533483.7036762724,7000634.265383997]]]
			},
			"properties": {
				"edge_attributes_props": {
					"opphav": "opphav-edge-test-missing-feature", "datafangstdato": "2000-12-01" , "verifiseringsdato": "2001-02-01"
				},
				"face_attributes_props": {
					"opphav": "opphav-face-test-missing-feature"
				}
			}
		}');

SELECT 1 as test_feature_num \gset

SELECT '--line in feature '||t.feature_num, ST_AsEWKT(ST_GeomFromGeoJSON((t.input_feature::JSONB) -> 'geometry'),13)
FROM input_features t WHERE feature_num = :test_feature_num;
INSERT INTO add_border_results(test_name, fid, typ, act)
SELECT '--feature_num'||t.feature_num, (t.res).fid, (t.res).typ, (t.res).act
FROM (
		SELECT topo_ar5ngis.add_border(t.input_feature, 'server_side_opphav'::TEXT) AS res, feature_num FROM input_features t
		WHERE feature_num = :test_feature_num
	) t;
SELECT 'surfaces touching changed borders ', * FROM  add_border_results WHERE typ ='S' AND act='T' ORDER BY sid ;
SELECT 'other surfaces involved ', * FROM  add_border_results WHERE typ ='S' AND act!='T' ORDER BY sid ;
TRUNCATE TABLE add_border_results;

-- Test feature 2
-- add a new line 2 2022-01-10 09:04:10,155
INSERT INTO input_features(input_feature)
VALUES ('{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:25832"
					}
				},
				"type": "LineString",
				"coordinates": [[533285.543354005,7000629.668881676],[533288.6076888853,7000605.154202634],[533322.8260950501,7000605.154202634],[533350.405108974,7000633.754661517]]
			},
			"properties": {
				"edge_attributes_props": {
					"opphav": "opphav-edge-test-missing-feature", "datafangstdato": "2000-12-01" , "verifiseringsdato": "2001-02-01"
				},
				"face_attributes_props": {
					"opphav": "opphav-face-test-missing-feature"
				}
			}
		}');

SELECT 2 as test_feature_num \gset

SELECT '--line in feature '||t.feature_num, ST_AsEWKT(ST_GeomFromGeoJSON((t.input_feature::JSONB) -> 'geometry'),13)
FROM input_features t WHERE feature_num = :test_feature_num;
INSERT INTO add_border_results(test_name, fid, typ, act)
SELECT '--feature_num'||t.feature_num, (t.res).fid, (t.res).typ, (t.res).act
FROM (
		SELECT topo_ar5ngis.add_border(t.input_feature, 'server_side_opphav'::TEXT) AS res, feature_num FROM input_features t
		WHERE feature_num = :test_feature_num
	) t;
SELECT 'surfaces touching changed borders ', * FROM  add_border_results WHERE typ ='S' AND act='T' ORDER BY sid ;
SELECT 'other surfaces involved ', * FROM  add_border_results WHERE typ ='S' AND act!='T' ORDER BY sid ;
TRUNCATE TABLE add_border_results;


-- Test feature 3
-- add a new line 3 2022-01-10 09:10:01,701
INSERT INTO input_features(input_feature)
VALUES ('{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:25832"
					}
				},
				"type": "LineString",
				"coordinates": [[533285.2183712412,7000630.0313221365],[533287.7922512544,7000605.579462012],[533332.8351514852,7000610.727222038],[533345.7045515511,7000642.2572522005]]
			},
			"properties": {
				"edge_attributes_props": {
					"opphav": "opphav-edge-test-missing-feature", "datafangstdato": "2000-12-01" , "verifiseringsdato": "2001-02-01"
				},
				"face_attributes_props": {
					"opphav": "opphav-face-test-missing-feature"
				}
			}
		}');


SELECT 3 as test_feature_num \gset
SELECT '--line in feature '||t.feature_num, ST_AsEWKT(ST_GeomFromGeoJSON((t.input_feature::JSONB) -> 'geometry'),13)
FROM input_features t WHERE feature_num = :test_feature_num;
DO $$
DECLARE
	v_state text;
	v_msg text;
	v_detail text;
	v_hint text;
	v_context text;
BEGIN
	BEGIN

		INSERT INTO add_border_results(test_name, fid, typ, act)
		SELECT '--feature_num'||t.feature_num, (t.res).fid, (t.res).typ, (t.res).act
		FROM (
			SELECT topo_ar5ngis.add_border(t.input_feature, 'server_side_opphav'::TEXT) AS res, feature_num FROM input_features t
			WHERE feature_num = 3 -- How to use :test_feature_num, seems like we have to create function
		) t;

	EXCEPTION WHEN OTHERS
	THEN
    	GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT, v_context = PG_EXCEPTION_CONTEXT;
		RAISE WARNING 'Got exception %', substring(v_msg,POSITION('P0001 message' in v_msg),70);
	END;
END;
$$ LANGUAGE 'plpgsql';


SELECT 'surfaces touching changed borders ', * FROM  add_border_results WHERE typ ='S' AND act='T' ORDER BY sid ;
SELECT 'other surfaces involved ', * FROM  add_border_results WHERE typ ='S' AND act!='T' ORDER BY sid ;
TRUNCATE TABLE add_border_results;


-- Test feature 4
-- add a new line 4 2022-01-10 09:11:12,232
INSERT INTO input_features(input_feature)
VALUES ('{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:25832"
					}
				},
				"type": "MultiLineString",
				"coordinates": [[[533400.9064671883,7000778.00820065],[533431.1495573432,7000727.817540393],[533515.4441277749,7000655.105430021]],[[533531.5308778574,7000670.5487101],[533460.1057074915,7000741.973880466],[533415.706277264,7000800.5296507655]]]
			},
			"properties": {
				"edge_attributes_props": {
					"opphav": "opphav-edge-test-missing-feature", "datafangstdato": "2000-12-01" , "verifiseringsdato": "2001-02-01"
				},
				"face_attributes_props": {
					"opphav": "opphav-face-test-missing-feature"
				}
			}
		}');

SELECT 4 as test_feature_num \gset

SELECT '--line in feature '||t.feature_num, ST_AsEWKT(ST_GeomFromGeoJSON((t.input_feature::JSONB) -> 'geometry'),13)
FROM input_features t WHERE feature_num = :test_feature_num;
INSERT INTO add_border_results(test_name, fid, typ, act)
SELECT '--feature_num'||t.feature_num, (t.res).fid, (t.res).typ, (t.res).act
FROM (
		SELECT topo_ar5ngis.add_border(t.input_feature, 'server_side_opphav'::TEXT) AS res, feature_num FROM input_features t
		WHERE feature_num = :test_feature_num
	) t;
SELECT 'surfaces touching changed borders ', * FROM  add_border_results WHERE typ ='S' AND act='T' ORDER BY sid ;
SELECT 'other surfaces involved ', * FROM  add_border_results WHERE typ ='S' AND act!='T' ORDER BY sid ;
TRUNCATE TABLE add_border_results;

-- Test feature 5
-- add a new line 5 2022-01-10 09:11:36,724
INSERT INTO input_features(input_feature)
VALUES ('{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:25832"
					}
				},
				"type": "MultiLineString",
				"coordinates": [[[533411.201987241,7000781.86902067],[533454.3144774619,7000732.965300419],[533512.8702477617,7000668.618300091]],[[533497.4269676827,7000656.3923700275],[533456.888357475,7000698.861390245],[533399.6195271816,7000768.999620604]]]
			},
			"properties": {
				"edge_attributes_props": {
					"opphav": "opphav-edge-test-missing-feature", "datafangstdato": "2000-12-01" , "verifiseringsdato": "2001-02-01"
				},
				"face_attributes_props": {
					"opphav": "opphav-face-test-missing-feature"
				}
			}
		}');

SELECT 5 as test_feature_num \gset
SELECT '--line in feature '||t.feature_num, ST_AsEWKT(ST_GeomFromGeoJSON((t.input_feature::JSONB) -> 'geometry'),13)
FROM input_features t WHERE feature_num = :test_feature_num;

DO $$
DECLARE
	v_state text;
	v_msg text;
	v_detail text;
	v_hint text;
	v_context text;
BEGIN
	BEGIN

	INSERT INTO add_border_results(test_name, fid, typ, act)
	SELECT '--feature_num'||t.feature_num, (t.res).fid, (t.res).typ, (t.res).act
	FROM (
		SELECT topo_ar5ngis.add_border(t.input_feature, 'server_side_opphav'::TEXT) AS res, feature_num FROM input_features t
		WHERE feature_num = 5  -- How to use :test_feature_num, seems like we have to create function
	) t;

	EXCEPTION WHEN OTHERS
	THEN
    	GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT, v_context = PG_EXCEPTION_CONTEXT;
		RAISE WARNING 'Got exception %', substring(v_msg,POSITION('P0001 message' in v_msg),90);
	END;
END;
$$ LANGUAGE 'plpgsql';




SELECT 'surfaces touching changed borders ', * FROM  add_border_results WHERE typ ='S' AND act='T' ORDER BY sid ;
SELECT 'other surfaces involved ', * FROM  add_border_results WHERE typ ='S' AND act!='T' ORDER BY sid ;
TRUNCATE TABLE add_border_results;

-- Test feature 6
-- add a new line 5 2022-01-10 09:11:36,724
INSERT INTO input_features(input_feature)
VALUES ('{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:25832"
					}
				},
				"type": "MultiLineString",
				"coordinates": [[[533400.9064671883,7000768.999620604],[533438.2277273794,7000715.591610331],[533509.009427742,7000656.3923700275]],[[533516.0875977782,7000671.835650107],[533476.8359275772,7000712.374260314],[533438.2277273794,7000752.269400518],[533407.3411672212,7000782.512490673]]]
			},
			"properties": {
				"edge_attributes_props": {
					"opphav": "opphav-edge-test-missing-feature", "datafangstdato": "2000-12-01" , "verifiseringsdato": "2001-02-01"
				},
				"face_attributes_props": {
					"opphav": "opphav-face-test-missing-feature"
				}
			}
		}');

SELECT 6 as test_feature_num \gset
SELECT '--line in feature '||t.feature_num, ST_AsEWKT(ST_GeomFromGeoJSON((t.input_feature::JSONB) -> 'geometry'),13)
FROM input_features t WHERE feature_num = :test_feature_num;

DO $$
DECLARE
	v_state text;
	v_msg text;
	v_detail text;
	v_hint text;
	v_context text;
BEGIN
	BEGIN

	INSERT INTO add_border_results(test_name, fid, typ, act)
	SELECT '--feature_num'||t.feature_num, (t.res).fid, (t.res).typ, (t.res).act
	FROM (
		SELECT topo_ar5ngis.add_border(t.input_feature, 'server_side_opphav'::TEXT) AS res, feature_num FROM input_features t
		WHERE feature_num = 6  -- How to use :test_feature_num, seems like we have to create function
	) t;

	EXCEPTION WHEN OTHERS
	THEN
    	GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT, v_context = PG_EXCEPTION_CONTEXT;
		RAISE WARNING 'Got exception %', substring(v_msg,POSITION('P0001 message' in v_msg),90);
	END;
END;
$$ LANGUAGE 'plpgsql';


SELECT 'surfaces touching changed borders ', * FROM  add_border_results WHERE typ ='S' AND act='T' ORDER BY sid ;
SELECT 'other surfaces involved ', * FROM  add_border_results WHERE typ ='S' AND act!='T' ORDER BY sid ;
TRUNCATE TABLE add_border_results;

-- Test feature 7
-- add a new line 7 2022-01-10 09:14:50,940
INSERT INTO input_features(input_feature)
VALUES ('{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:25832"
					}
				},
				"type": "LineString",
				"coordinates": [[533393.8881090459,7000760.155715319],[533403.4249033333,7000720.94889436],[533461.1754909623,7000744.790880078],[533409.7827661915,7000774.4609067505]]
			},
			"properties": {
				"edge_attributes_props": {
					"opphav": "opphav-edge-test-missing-feature", "datafangstdato": "2000-12-01" , "verifiseringsdato": "2001-02-01"
				},
				"face_attributes_props": {
					"opphav": "opphav-face-test-missing-feature"
				}
			}
		}');

SELECT 7 as test_feature_num \gset

SELECT '--line in feature '||t.feature_num, ST_AsEWKT(ST_GeomFromGeoJSON((t.input_feature::JSONB) -> 'geometry'),13)
FROM input_features t WHERE feature_num = :test_feature_num;

DO $$
DECLARE
	v_state text;
	v_msg text;
	v_detail text;
	v_hint text;
	v_context text;
BEGIN
	BEGIN

	INSERT INTO add_border_results(test_name, fid, typ, act)
	SELECT '--feature_num'||t.feature_num, (t.res).fid, (t.res).typ, (t.res).act
	FROM (
		SELECT topo_ar5ngis.add_border(t.input_feature, 'server_side_opphav'::TEXT) AS res, feature_num FROM input_features t
		WHERE feature_num = 7  -- How to use :test_feature_num, seems like we have to create function
	) t;

	EXCEPTION WHEN OTHERS
	THEN
    	GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT, v_context = PG_EXCEPTION_CONTEXT;
		RAISE WARNING 'Got exception %', substring(v_msg,POSITION('P0001 message' in v_msg),90);
	END;
END;
$$ LANGUAGE 'plpgsql';


SELECT 'surfaces touching changed borders ', * FROM  add_border_results WHERE typ ='S' AND act='T' ORDER BY sid ;
SELECT 'other surfaces involved ', * FROM  add_border_results WHERE typ ='S' AND act!='T' ORDER BY sid ;
TRUNCATE TABLE add_border_results;

-- Test feature 8
-- add a new line 8 2022-01-10 09:15:06,549
INSERT INTO input_features(input_feature)
VALUES ('{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:25832"
					}
				},
				"type": "LineString",
				"coordinates": [[533379.5829176148,7000683.86136102],[533393.3582871411,7000714.591031502],[533464.3544223915,7000718.299784835],[533458.526381438,7000678.563141972],[533403.9547252381,7000664.787772446],[533379.5829176148,7000683.86136102]]
			},
			"properties": {
				"edge_attributes_props": {
					"opphav": "opphav-edge-test-missing-feature", "datafangstdato": "2000-12-01" , "verifiseringsdato": "2001-02-01"
				},
				"face_attributes_props": {
					"opphav": "opphav-face-test-missing-feature"
				}
			}
		}');

SELECT 8 as test_feature_num \gset

SELECT '--line in feature '||t.feature_num, ST_AsEWKT(ST_GeomFromGeoJSON((t.input_feature::JSONB) -> 'geometry'),13)
FROM input_features t WHERE feature_num = :test_feature_num;
INSERT INTO add_border_results(test_name, fid, typ, act)
SELECT '--feature_num'||t.feature_num, (t.res).fid, (t.res).typ, (t.res).act
FROM (
		SELECT topo_ar5ngis.add_border(t.input_feature, 'server_side_opphav'::TEXT) AS res, feature_num FROM input_features t
		WHERE feature_num = :test_feature_num
	) t;
SELECT 'surfaces touching changed borders ', * FROM  add_border_results WHERE typ ='S' AND act='T' ORDER BY sid ;
SELECT 'other surfaces involved ', * FROM  add_border_results WHERE typ ='S' AND act!='T' ORDER BY sid ;
TRUNCATE TABLE add_border_results;

-- Check system after update

SELECT :'input_file_01','--t2-- face', count(*)  from topo_ar5ngis.face_attributes;
SELECT :'input_file_01','--t2-- edge', count(*)  from topo_ar5ngis.edge_attributes;

SELECT :'input_file_01','--t2-- test overlaps', count(*) AS num_over_lap
FROM topo_update.find_interiors_intersect('topo_ar5ngis.edge_attributes', 'grense', 'identifikasjon_lokal_id');

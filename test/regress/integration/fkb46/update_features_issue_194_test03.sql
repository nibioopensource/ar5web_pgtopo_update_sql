BEGIN;
set client_min_messages to WARNING;
set timezone to utc;

-- the bug description is here https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/194

-- We here test with valid input

\i :regdir/../../src/sql/topo_update_java/function_02_add_border_split_surface_java.sql
\i :regdir/../../src/sql/topo_update_java/function_02_update_feature_attribute_java.sql
\i :regdir/../../src/sql/topo_update_legacy/function_01_query_to_topojson.sql
\i :regdir/../../src/sql/topo_update_legacy/function_01_as_topo_json.sql
\i :regdir/../../src/sql/topo_update_legacy/function_01_get_adjusted_edge.sql
\i :regdir/utils/check_overlap.sql

-- Make :datadir available as a variable
SELECT :'regdir' || '/../../test/data/fkb46' as datadir \gset

-- Make :input available as a variable
SELECT '/issue_194_ar5_input_test02.json' as input_file_01 \gset

-- Make :bbox available as a variable
SELECT 'SRID=4258;POLYGON ((9.656690352667766 63.13235020714416, 9.656690352667766 63.13495213790528, 9.667230335247455 63.13495213790528, 9.667230335247455 63.13235020714416, 9.656690352667766 63.13235020714416))' as bbox \gset

-- Make :topo_sql_cmd with :bbox available as a variable used by topo_update.query_to_topojson
SELECT 'SELECT DISTINCT a.* FROM topo_ar5ngis.webclient_flate_topojson_flate_v a ,topo_ar5ngis_sysdata_webclient.face fa, topo_ar5ngis_sysdata_webclient.relation re, topology.layer tl WHERE fa.mbr && ST_GeomFromEWKT('''||:'bbox'||''') AND ((a.omrade ).id) = re.topogeo_id AND re.layer_id = tl.layer_id AND tl.schema_name = ''topo_ar5ngis'' AND tl.table_name = ''face_attributes'' and fa.face_id=re.element_id' as topo_sql_cmd \gset

CREATE TEMP TABLE json_input_01(payload text);
SELECT :'datadir' || '/' || :'input_file_01' as x \gset
COPY json_input_01 FROM :'x';
SELECT '--json-input-',length(payload) FROM json_input_01;

-- TOPOLOGY NAME: update_features_test_topo
-- AREAL LAYER:   topo_ar5ngis.face_attributes
-- LINEAL LAYER:  topo_ar5ngis.edge_attributes

SELECT 'input--',:'input_file_01', topo_ar5ngis.update_features(
	(SELECT payload FROM json_input_01)::jsonb,
	ST_GeomFromEWKT(:'bbox'),
	NULL
);

-- Check for topology validity
SELECT
	'before_topo_update.add_border_split_face',
	'validate_topology',
	*
FROM topology.ValidateTopology('topo_ar5ngis_sysdata_webclient');

SELECT :'input_file_01','--t1-- face', count(*)  from topo_ar5ngis.face_attributes;
SELECT :'input_file_01','--t1-- edge', count(*)  from topo_ar5ngis.edge_attributes;

SELECT :'input_file_01','--t1-- test overlaps', count(*) AS num_over_lap
FROM topo_update.find_interiors_intersect('topo_ar5ngis.edge_attributes', 'grense', 'identifikasjon_lokal_id');


SELECT :'input_file_01','--t1-- missing edge attribute', edge_id
FROM
(
	SELECT edge_id FROM
	(SELECT * FROM topo_ar5ngis_sysdata_webclient.edge WHERE geom && ST_GeomFromEWKT(:'bbox') ) f
	except SELECT abs(r.element_id)
	FROM topo_ar5ngis_sysdata_webclient.relation r, topology.layer l
	WHERE r.layer_id = l.layer_id AND l.table_name = 'edge_attributes'
) s
ORDER BY edge_id;

SELECT :'input_file_01','--t1-- missing face attribute', face_id
FROM
(
	SELECT face_id FROM
	(SELECT * FROM topo_ar5ngis_sysdata_webclient.face WHERE mbr && ST_GeomFromEWKT(:'bbox') ) f
	except SELECT abs(r.element_id)
	FROM topo_ar5ngis_sysdata_webclient.relation r, topology.layer l
	WHERE r.layer_id = l.layer_id AND l.table_name = 'face_attributes'
) s
ORDER BY face_id;
;



CREATE TEMPORARY TABLE add_border_results(
	test_name TEXT,
	fid TEXT,
	sid SERIAL,
	typ CHAR,
	act CHAR
);

CREATE TEMPORARY TABLE input_features (
	feature_num SERIAL,
	input_feature TEXT
);


-- Test feature 1
-- add a new line 1 2022-01-10 09:04:10,155
INSERT INTO input_features(input_feature)
VALUES ('{"type":"Feature","geometry":{"type":"LineString","coordinates":[[533285.543354005,7000629.668881676],[533288.6076888853,7000605.154202634],[533322.8260950501,7000605.154202634],[533350.405108974,7000633.754661517]],"crs":{"type":"name","properties":{"name":"EPSG:25832"}}},"properties":{"datafangstdato":"2021-12-27","maalemetode":82}}');

SELECT 1 as test_feature_num \gset

SELECT '--line in feature '||t.feature_num, ST_AsEWKT(ST_GeomFromGeoJSON((t.input_feature::JSONB) -> 'geometry'),13)
FROM input_features t WHERE feature_num = :test_feature_num;
INSERT INTO add_border_results(test_name, fid, typ, act)
SELECT '--feature_num'||t.feature_num, (t.res).fid, (t.res).typ, (t.res).act
FROM (
		SELECT topo_ar5ngis.add_border(t.input_feature, 'server_side_opphav'::TEXT) AS res, feature_num FROM input_features t
		WHERE feature_num = :test_feature_num
	) t;

SELECT 'surfaces touching changed borders ', * FROM  add_border_results WHERE typ ='S' AND act='T' ORDER BY sid ;
SELECT 'other surfaces involved ', * FROM  add_border_results WHERE typ ='S' AND act!='T' ORDER BY sid ;
TRUNCATE TABLE add_border_results;

SELECT 'after feature '||:'test_feature_num',' face attributes', count(*) FROM topo_ar5ngis.face_attributes;
SELECT 'after feature '||:'test_feature_num',' edge attributes', count(*) FROM topo_ar5ngis.edge_attributes;
SELECT 'after feature '||:'test_feature_num',' edges', count(*) FROM topo_ar5ngis_sysdata_webclient.edge;
SELECT 'after feature '||:'test_feature_num',' faces', count(*) FROM topo_ar5ngis_sysdata_webclient.face;
SELECT 'after feature '||:'test_feature_num',' overlaps', count(*) AS num_over_lap
FROM topo_update.find_interiors_intersect('topo_ar5ngis.edge_attributes', 'grense', 'identifikasjon_lokal_id');


-- Test feature 2 which is not valid
-- add a new line 2 2022-01-10 09:10:01,701
INSERT INTO input_features(input_feature)
VALUES ('{"type":"Feature","geometry":{"type":"LineString","coordinates":[[533285.2183712412,7000630.0313221365],[533287.7922512544,7000605.579462012],[533332.8351514852,7000610.727222038],[533345.7045515511,7000642.2572522005]],"crs":{"type":"name","properties":{"name":"EPSG:25832"}}},"properties":{"datafangstdato":"2021-12-27","maalemetode":82}}');

SELECT 2 as test_feature_num \gset


SELECT '--line in feature '||t.feature_num, ST_AsEWKT(ST_GeomFromGeoJSON((t.input_feature::JSONB) -> 'geometry'),13)
FROM input_features t WHERE feature_num = :test_feature_num;

DO $$
DECLARE
	v_state text;
	v_msg text;
	v_detail text;
	v_hint text;
	v_context text;
BEGIN
	BEGIN

		INSERT INTO add_border_results(test_name, fid, typ, act)
		SELECT '--feature_num'||t.feature_num, (t.res).fid, (t.res).typ, (t.res).act
		FROM (
			SELECT topo_ar5ngis.add_border(t.input_feature, 'server_side_opphav'::TEXT) AS res, feature_num FROM input_features t
			WHERE feature_num = 3 -- How to use :test_feature_num, seems like we have to create function
		) t;

	EXCEPTION WHEN OTHERS
	THEN
    	GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT, v_context = PG_EXCEPTION_CONTEXT;
		RAISE WARNING 'Got exception %', substring(v_msg,POSITION('P0001 message' in v_msg),70);
	END;
END;
$$ LANGUAGE 'plpgsql';


SELECT 'surfaces touching changed borders ', * FROM  add_border_results WHERE typ ='S' AND act='T' ORDER BY sid ;
SELECT 'other surfaces involved ', * FROM  add_border_results WHERE typ ='S' AND act!='T' ORDER BY sid ;
TRUNCATE TABLE add_border_results;

-- Check system after invalid update

SELECT 'after feature '||:'test_feature_num',' face attributes', count(*) FROM topo_ar5ngis.face_attributes;
SELECT 'after feature '||:'test_feature_num',' edge attributes', count(*) FROM topo_ar5ngis.edge_attributes;
SELECT 'after feature '||:'test_feature_num',' edges', count(*) FROM topo_ar5ngis_sysdata_webclient.edge;
SELECT 'after feature '||:'test_feature_num',' faces', count(*) FROM topo_ar5ngis_sysdata_webclient.face;
SELECT 'after feature '||:'test_feature_num',' overlaps', count(*) AS num_over_lap
FROM topo_update.find_interiors_intersect('topo_ar5ngis.edge_attributes', 'grense', 'identifikasjon_lokal_id');


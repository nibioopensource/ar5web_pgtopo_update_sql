BEGIN;
set client_min_messages to WARNING;
set timezone to utc;

-- the bug description is here https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/195

-- We here test with valid input

\i :regdir/../../src/sql/topo_update_java/function_02_add_border_split_surface_java.sql
\i :regdir/../../src/sql/topo_update_java/function_02_update_feature_attribute_java.sql
\i :regdir/../../src/sql/topo_update_legacy/function_01_query_to_topojson.sql
\i :regdir/../../src/sql/topo_update_legacy/function_01_as_topo_json.sql
\i :regdir/../../src/sql/topo_update_legacy/function_01_get_adjusted_edge.sql
\i :regdir/utils/check_overlap.sql

-- Make :datadir available as a variable
SELECT :'regdir' || '/../../test/data/fkb46' as datadir \gset

-- Make :input available as a variable
SELECT '/issue_195_ar5_input.json' as input_file_01 \gset

-- Make :bbox available as a variable
SELECT 'SRID=4258;POLYGON ((29.011101066185997 70.12174562773924, 29.01130815296582 70.12387936278337, 29.02083372038418 70.12377206713734, 29.02062565446332 70.12163834455458, 29.011101066185997 70.12174562773924))' as bbox \gset

-- Make :topo_sql_cmd with :bbox available as a variable used by topo_update.query_to_topojson
SELECT 'SELECT DISTINCT a.* FROM topo_ar5ngis.webclient_flate_topojson_flate_v a ,topo_ar5ngis_sysdata_webclient.face fa, topo_ar5ngis_sysdata_webclient.relation re, topology.layer tl WHERE fa.mbr && ST_GeomFromEWKT('''||:'bbox'||''') AND ((a.omrade ).id) = re.topogeo_id AND re.layer_id = tl.layer_id AND tl.schema_name = ''topo_ar5ngis'' AND tl.table_name = ''face_attributes'' and fa.face_id=re.element_id' as topo_sql_cmd \gset

CREATE TEMP TABLE json_input_01(payload text);
SELECT :'datadir' || '/' || :'input_file_01' as x \gset
COPY json_input_01 FROM :'x';
SELECT '--json-input-',length(payload) FROM json_input_01;

-- TOPOLOGY NAME: update_features_test_topo
-- AREAL LAYER:   topo_ar5ngis.face_attributes
-- LINEAL LAYER:  topo_ar5ngis.edge_attributes

SELECT 'input--',:'input_file_01', topo_ar5ngis.update_features(
	(SELECT payload FROM json_input_01)::jsonb,
	ST_GeomFromEWKT(:'bbox'),
	NULL
);

-- Check for topology validity
SELECT
	'before_topo_update.add_border_split_face',
	'validate_topology',
	*
FROM topology.ValidateTopology('topo_ar5ngis_sysdata_webclient');

SELECT :'input_file_01','--t1-- face', count(*)  from topo_ar5ngis.face_attributes;
SELECT :'input_file_01','--t1-- edge', count(*)  from topo_ar5ngis.edge_attributes;

SELECT :'input_file_01','--t1-- test overlaps', count(*) AS num_over_lap
FROM topo_update.find_interiors_intersect('topo_ar5ngis.edge_attributes', 'grense', 'identifikasjon_lokal_id');


SELECT :'input_file_01','--t1-- missing edge attribute', edge_id
FROM
(
	SELECT edge_id FROM
	(SELECT * FROM topo_ar5ngis_sysdata_webclient.edge WHERE geom && ST_GeomFromEWKT(:'bbox') ) f
	except SELECT abs(r.element_id)
	FROM topo_ar5ngis_sysdata_webclient.relation r, topology.layer l
	WHERE r.layer_id = l.layer_id AND l.table_name = 'edge_attributes'
) s
ORDER BY edge_id;

SELECT :'input_file_01','--t1-- missing face attribute', face_id
FROM
(
	SELECT face_id FROM
	(SELECT * FROM topo_ar5ngis_sysdata_webclient.face WHERE mbr && ST_GeomFromEWKT(:'bbox') ) f
	except SELECT abs(r.element_id)
	FROM topo_ar5ngis_sysdata_webclient.relation r, topology.layer l
	WHERE r.layer_id = l.layer_id AND l.table_name = 'face_attributes'
) s
ORDER BY face_id;
;



CREATE TEMPORARY TABLE add_border_results(
	test_name TEXT,
	fid TEXT,
	sid SERIAL,
	typ CHAR,
	act CHAR
);


SELECT 'with no srid', ST_AsEWKT(ST_GeomFromGeoJSON(
	$$
	{
		"type": "MultiLineString",
		"coordinates": [[[576520.524,7780779.784999999],[576556.3970603101,7780743.696550922]],[[576539.9330000001,7780788.727999998],[576578.1829999998,7780750.685]]]
	}
	$$::TEXT));

SELECT 'with srid', ST_AsEWKT(ST_GeomFromGeoJSON(
	$$
	{
		"crs": {
			"type": "name",
			"properties": {
				"name": "EPSG:25835"
			}
		},
		"type": "MultiLineString",
		"coordinates": [[[576520.524,7780779.784999999],[576556.3970603101,7780743.696550922]],[[576539.9330000001,7780788.727999998],[576578.1829999998,7780750.685]]]
	}
	$$::TEXT));

SELECT 'best srid', _ST_BestSRID(ST_Transform(ST_GeomFromGeoJSON(
	$$
	{
		"crs": {
			"type": "name",
			"properties": {
				"name": "EPSG:25835"
			}
		},
		"type": "MultiLineString",
		"coordinates": [[[576520.524,7780779.784999999],[576556.3970603101,7780743.696550922]],[[576539.9330000001,7780788.727999998],[576578.1829999998,7780750.685]]]
	}
	$$::TEXT),4258));


-- add a new line that
-- 3 of point are exact node matches in UTM zone 35, but they are converted to degrees when checked
-- upper left point [576539.9330000001,7780788.727999998]
-- most right point [576578.1829999998,7780750.685]
-- most left point [576520.524,7780779.784999999]
-- we now add extra points to the to be sure that lines crosses the edges

-- this works OK on
-- POSTGIS="3.1.4 ded6c34" [EXTENSION] PGSQL="140" GEOS="3.10.1-CAPI-1.16.0" PROJ="7.2.1" LIBXML="2.9.12" LIBJSON="0.15" LIBPROTOBUF="1.4.0" WAGYU="0.5.0 (Internal)"

INSERT INTO add_border_results(test_name, fid, typ, act)
SELECT '--t1-- run split', fid, typ, act
FROM topo_ar5ngis.add_border(

	$$
		{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:25835"
					}
				},
				"type": "MultiLineString",
				"coordinates": [[[576520.524,7780784.784999999],[576520.524,7780779.784999999],[576556.3970603101,7780743.696550922]],[[576539.9330000001,7780791.727999998],[576539.9330000001,7780788.727999998],[576578.1829999998,7780750.685],[576578.1829999998,7780741.685]]]
			},
			"properties": {
				"edge_attributes_props": {
					"opphav": "opphav-edge-test-missing-feature", "datafangstdato": "2000-12-01" , "verifiseringsdato": "2001-02-01"
				},
				"face_attributes_props": {
					"opphav": "opphav-face-test-missing-feature"
				}
			}
		}
	$$::TEXT,
	'server_side_opphav'::TEXT);


-- Check system after update, here is sample
-- /issue_195_ar5_input.json|--t2-- faces and edges|210
-- +/issue_195_ar5_input.json|--t2-- faces and edges|208
-- The number of faces and edges may change based on Postgis versions and runs as it seems
-- I am not 100% sure why this happens , but I think this related to some tolerance value and
-- if a edge are merged into to existing edge or not or a face is created.
-- We may remove this test, but I keep for now just just to be bale to see what happens
SELECT :'input_file_01','--t2-- faces and edges',
(((SELECT count(*)  FROM topo_ar5ngis.face_attributes) +  (SELECT count(*) FROM topo_ar5ngis.edge_attributes)) > 200);

-- Test on area should not vary that much so this test should be ok
SELECT :'input_file_01','--t2-- faces area',  sum(ST_area(omrade::Geometry,true)::int)  FROM topo_ar5ngis.face_attributes;

-- Since it may different number of edges , also the length may differ
SELECT :'input_file_01','--t2-- edge length',  ((SELECT sum(ST_Length(grense::Geometry,true)::int)  FROM topo_ar5ngis.edge_attributes) > 4900);


SELECT :'input_file_01','--t2-- test overlaps', count(*) AS num_over_lap
FROM topo_update.find_interiors_intersect('topo_ar5ngis.edge_attributes', 'grense', 'identifikasjon_lokal_id');

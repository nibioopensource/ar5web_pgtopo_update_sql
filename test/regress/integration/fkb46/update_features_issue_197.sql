BEGIN;
set client_min_messages to WARNING;
set timezone to utc;

-- the bug description is here https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/197

-- f8ed8d9f-2f81-4983-8172-acac8c11cdb8

-- We here test with valid input

\i :regdir/../../src/sql/topo_update_java/function_02_add_border_split_surface_java.sql
\i :regdir/../../src/sql/topo_update_java/function_02_update_feature_attribute_java.sql
\i :regdir/../../src/sql/topo_update_legacy/function_01_query_to_topojson.sql
\i :regdir/../../src/sql/topo_update_legacy/function_01_as_topo_json.sql
\i :regdir/../../src/sql/topo_update_legacy/function_01_get_adjusted_edge.sql
\i :regdir/utils/check_overlap.sql

-- Make :datadir available as a variable
SELECT :'regdir' || '/../../test/data/fkb46' as datadir \gset

-- issue_197_ar5_input_01.json the input data before update
-- POLYGON ((11.443010091969883 59.40135151821749, 11.443010091969883 59.40657319694998, 11.462139859449708 59.40657319694998, 11.462139859449708 59.40135151821749, 11.443010091969883 59.40135151821749))

-- issue_197_ar5_input_01_after_update.json  the payback from NGIS after upadte
-- POLYGON ((11.445453070630322 59.403355376520224, 11.445453070630322 59.41012501940246, 11.454267862445022 59.41012501940246, 11.454267862445022 59.403355376520224, 11.445453070630322 59.403355376520224))

-- Make :input available as a variable
SELECT '/issue_197_ar5_input_01.json' as input_file_01 \gset

-- Make :bbox available as a variable for input_01
SELECT 'SRID=4258;POLYGON ((11.443010091969883 59.40135151821749, 11.443010091969883 59.40657319694998, 11.462139859449708 59.40657319694998, 11.462139859449708 59.40135151821749, 11.443010091969883 59.40135151821749))' as bbox \gset


-- Make :topo_sql_cmd with :bbox available as a variable used by topo_update.query_to_topojson
SELECT 'SELECT DISTINCT a.* FROM topo_ar5ngis.webclient_flate_topojson_flate_v a ,topo_ar5ngis_sysdata_webclient.face fa, topo_ar5ngis_sysdata_webclient.relation re, topology.layer tl WHERE fa.mbr && ST_GeomFromEWKT('''||:'bbox'||''') AND ((a.omrade ).id) = re.topogeo_id AND re.layer_id = tl.layer_id AND tl.schema_name = ''topo_ar5ngis'' AND tl.table_name = ''face_attributes'' and fa.face_id=re.element_id' as topo_sql_cmd \gset


CREATE TEMP TABLE json_input_01(payload text);
SELECT :'datadir' || '/' || :'input_file_01' as x \gset
COPY json_input_01 FROM :'x';
SELECT '--json-input-',length(payload) FROM json_input_01;

-- TOPOLOGY NAME: update_features_test_topo
-- AREAL LAYER:   topo_ar5ngis.face_attributes
-- LINEAL LAYER:  topo_ar5ngis.edge_attributes

SELECT 'input--',:'input_file_01', topo_ar5ngis.update_features(
	(SELECT payload FROM json_input_01)::jsonb,
	ST_GeomFromEWKT(:'bbox'),
	NULL
);

-- Check for topology validity
SELECT
	'before_topo_update.add_border_split_face',
	'validate_topology',
	*
FROM topology.ValidateTopology('topo_ar5ngis_sysdata_webclient');

SELECT :'input_file_01','--t1-- face', count(*)  from topo_ar5ngis.face_attributes;
SELECT :'input_file_01','--t1-- edge', count(*)  from topo_ar5ngis.edge_attributes;

SELECT :'input_file_01','--t1-- test overlaps', count(*) AS num_over_lap
FROM topo_update.find_interiors_intersect('topo_ar5ngis.edge_attributes', 'grense', 'identifikasjon_lokal_id');




CREATE TEMPORARY TABLE add_border_results(
	test_name TEXT,
	fid TEXT,
	sid SERIAL,
	typ CHAR,
	act CHAR
);

CREATE TEMPORARY TABLE input_features (
	feature_num SERIAL,
	input_feature TEXT
);


-- Test feature 1
-- Jan 18 20:39  log_drawLine_20220118-203943__8D254ED1FB6A5B8FA69C275E443442F8_null__envelope_in_meter_25832_UTF-8.json
INSERT INTO input_features(input_feature)
VALUES ('{"type":"Feature","geometry":{"type":"LineString","coordinates":[[639068.3582610873,6587504.590394735],[639006.3950155943,6587414.374477722]],"crs":{"type":"name","properties":{"name":"EPSG:25832"}}},"properties":{"datafangstdato":"2021-12-27","maalemetode":82}}');

SELECT 1 as test_feature_num \gset

SELECT '--line in feature '||t.feature_num, ST_AsEWKT(ST_GeomFromGeoJSON((t.input_feature::JSONB) -> 'geometry'),13)
FROM input_features t WHERE feature_num = :test_feature_num;
INSERT INTO add_border_results(test_name, fid, typ, act)
SELECT '--feature_num'||t.feature_num, (t.res).fid, (t.res).typ, (t.res).act
FROM (
		SELECT topo_ar5ngis.add_border(t.input_feature, 'server_side_opphav'::TEXT) AS res, feature_num FROM input_features t
		WHERE feature_num = :test_feature_num
	) t;
SELECT 'surfaces touching changed borders ', * FROM  add_border_results WHERE typ ='S' AND act='T' ORDER BY sid ;
SELECT 'other surfaces involved ', * FROM  add_border_results WHERE typ ='S' AND act!='T' ORDER BY sid ;

SELECT 'after feature '||:'test_feature_num',' face attributes', count(*) FROM topo_ar5ngis.face_attributes;
SELECT 'after feature '||:'test_feature_num',' edge attributes', count(*) FROM topo_ar5ngis.edge_attributes;
SELECT 'after feature '||:'test_feature_num',' overlaps', count(*) AS num_over_lap
FROM topo_update.find_interiors_intersect('topo_ar5ngis.edge_attributes', 'grense', 'identifikasjon_lokal_id');



-- update result dates to make tests stable
UPDATE topo_ar5ngis.face_attributes f
SET identifikasjon_versjon_id = '1970-01-01T00:00:00'
WHERE f.identifikasjon_versjon_id > (now() - '60s'::interval);

UPDATE topo_ar5ngis.edge_attributes f
SET identifikasjon_versjon_id = '1970-01-01T00:00:00'
WHERE f.identifikasjon_versjon_id > (now() - '60s'::interval);

UPDATE topo_ar5ngis.face_attributes f
SET oppdateringsdato='1970-01-01T00:00:00'
WHERE f.oppdateringsdato > (now() - '60s'::interval);

UPDATE topo_ar5ngis.edge_attributes f
SET oppdateringsdato='1970-01-01T00:00:00'
WHERE f.oppdateringsdato > (now() - '60s'::interval);

-- map test data to id from NGIS
UPDATE topo_ar5ngis.edge_attributes f
SET identifikasjon_lokal_id='62ccb50a-7896-11ec-8192-3f1375f94b3f'
WHERE identifikasjon_lokal_id = '00000000-0000-0000-0000-000000000003';

UPDATE topo_ar5ngis.edge_attributes f
SET identifikasjon_lokal_id='62cb2c62-7896-11ec-8190-f3dd2c7028ff'
WHERE identifikasjon_lokal_id = '00000000-0000-0000-0000-000000000001';

UPDATE topo_ar5ngis.edge_attributes f
SET identifikasjon_lokal_id='62cc2356-7896-11ec-8191-9f5bab54095b'
WHERE identifikasjon_lokal_id = '00000000-0000-0000-0000-000000000002';


-- just print to tmp to check
\out /tmp/topo_json_result.json
SELECT '--- t3 ---', E'\n' || jsonb_pretty(
	topo_ar5ngis.surfaces_as_geojson(
		'add_border_results',
		25832, -- utm 33
		5
	)::JSONB
);

\out

-- clean up to next update
TRUNCATE TABLE add_border_results;

-- Insert json from NGIS
-- issue_197_ar5_input_01_after_update.json  the payback from NGIS after upadte
-- POLYGON ((11.445453070630322 59.403355376520224, 11.445453070630322 59.41012501940246, 11.454267862445022 59.41012501940246, 11.454267862445022 59.403355376520224, 11.445453070630322 59.403355376520224))
-- Make :input available as a variable
SELECT 'issue_197_ar5_input_01_after_update.json' as input_file_02 \gset

CREATE TEMP TABLE json_input_02(payload text);
SELECT :'datadir' || '/' || :'input_file_02' as x \gset
COPY json_input_02 FROM :'x';
SELECT '--json-input-',length(payload) FROM json_input_01;

-- Make :bbox available as a variable
SELECT 'SRID=4258;POLYGON ((11.443010091969883 59.40135151821749, 11.443010091969883 59.40657319694998, 11.462139859449708 59.40657319694998, 11.462139859449708 59.40135151821749, 11.443010091969883 59.40135151821749))' as bbox_02 \gset

SELECT 'input--',:'input_file_02', topo_ar5ngis.update_features(
	(SELECT payload FROM json_input_02)::jsonb,
	ST_GeomFromEWKT(:'bbox_02'),
	NULL
);

SELECT 'after feature read payload backfrom NGIS'||:'input_file_02',' face attributes', count(*) FROM topo_ar5ngis.face_attributes;
SELECT 'after feature read payload backfrom NGIS'||:'input_file_02',' edge attributes', count(*) FROM topo_ar5ngis.edge_attributes;

-- This returns +after feature read payload backfrom NGISissue_197_ar5_input_01_after_update.json| overlaps|7
-- Should have returned zero rows
SELECT 'after feature read payload backfrom NGIS'||:'input_file_02',' overlaps', count(*) AS num_over_lap
FROM topo_update.find_interiors_intersect('topo_ar5ngis.edge_attributes', 'grense', 'identifikasjon_lokal_id');

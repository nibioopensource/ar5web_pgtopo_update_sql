BEGIN;
set client_min_messages to WARNING;
set timezone to utc;

-- the bug description is here https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/199

-- f8ed8d9f-2f81-4983-8172-acac8c11cdb8

-- We here test with valid input

\i :regdir/../../src/sql/topo_update_java/function_02_add_border_split_surface_java.sql
\i :regdir/../../src/sql/topo_update_java/function_02_update_feature_attribute_java.sql
\i :regdir/../../src/sql/topo_update_legacy/function_01_query_to_topojson.sql
\i :regdir/../../src/sql/topo_update_legacy/function_01_as_topo_json.sql
\i :regdir/../../src/sql/topo_update_legacy/function_01_get_adjusted_edge.sql
\i :regdir/utils/check_overlap.sql

-- Make :datadir available as a variable
SELECT :'regdir' || '/../../test/data/fkb46' as datadir \gset

-- issue_197_ar5_input_01.json the input data before update
-- POLYGON ((11.443010091969883 59.40135151821749, 11.443010091969883 59.40657319694998, 11.462139859449708 59.40657319694998, 11.462139859449708 59.40135151821749, 11.443010091969883 59.40135151821749))

-- issue_197_ar5_input_01_after_update.json  the payback from NGIS after upadte
-- POLYGON ((11.445453070630322 59.403355376520224, 11.445453070630322 59.41012501940246, 11.454267862445022 59.41012501940246, 11.454267862445022 59.403355376520224, 11.445453070630322 59.403355376520224))

-- Make :input available as a variable
SELECT '/issue_199_ar5_input_01.json' as input_file_01 \gset

-- Make :bbox available as a variable for input_01
SELECT 'SRID=4258;POLYGON ((11.245848479680657 59.43343921955089, 11.245936780962772 59.43477128019991, 11.249924319698101 59.43470262383286, 11.249835861964018 59.43337056681029, 11.245848479680657 59.43343921955089))' as bbox \gset


-- Make :topo_sql_cmd with :bbox available as a variable used by topo_update.query_to_topojson
SELECT 'SELECT DISTINCT a.* FROM topo_ar5ngis.webclient_flate_topojson_flate_v a ,topo_ar5ngis_sysdata_webclient.face fa, topo_ar5ngis_sysdata_webclient.relation re, topology.layer tl WHERE fa.mbr && ST_GeomFromEWKT('''||:'bbox'||''') AND ((a.omrade ).id) = re.topogeo_id AND re.layer_id = tl.layer_id AND tl.schema_name = ''topo_ar5ngis'' AND tl.table_name = ''face_attributes'' and fa.face_id=re.element_id' as topo_sql_cmd \gset


CREATE TEMP TABLE json_input_01(payload text);
SELECT :'datadir' || '/' || :'input_file_01' as x \gset
COPY json_input_01 FROM :'x';
SELECT '--json-input-',length(payload) FROM json_input_01;

-- TOPOLOGY NAME: update_features_test_topo
-- AREAL LAYER:   topo_ar5ngis.face_attributes
-- LINEAL LAYER:  topo_ar5ngis.edge_attributes

SELECT 'input--',:'input_file_01', topo_ar5ngis.update_features(
	(SELECT payload FROM json_input_01)::jsonb,
	ST_GeomFromEWKT(:'bbox'),
	NULL,
	FALSE,
	ST_GeomFromEWKT(:'bbox')
);

-- Check for topology validity
SELECT
	'before_topo_update.add_border_split_face',
	'validate_topology',
	*
FROM topology.ValidateTopology('topo_ar5ngis_sysdata_webclient');

SELECT :'input_file_01','--t1-- face', count(*)  from topo_ar5ngis.face_attributes;
SELECT :'input_file_01','--t1-- edge', count(*)  from topo_ar5ngis.edge_attributes;


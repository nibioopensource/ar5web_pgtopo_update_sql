BEGIN;

set client_min_messages to WARNING;
--USED FOR testing
--\set regdir /Users/lop/dev/git/topologi/pgtopo_update_sql/test/regress



\i :regdir/utils/check_overlap.sql

-- TWEAK THESE !
\set wktdigits 10
\set bbox ST_MakeEnvelope(12.138220106969866, 65.15986989011846, 12.142455807015113, 65.16396000490535, 4258)
--\set bbox ST_MakeEnvelope(-1e100, -1e100, 1e100, 1e100, 4258)

-- Make to split a small polygon
\set draw_line_01 ST_GeomFromEWKT('SRID=4258;LINESTRING(12.140187736365434 65.16176989011845,12.140488177619545 65.16206000490536)');


-- TOPOLOGY NAME: update_features_test_topo
-- AREAL LAYER:   topo_ar5ngis.face_attributes
-- LINEAL LAYER:  topo_ar5ngis.edge_attributes

-- Create mapping schema and table and add mappings
CREATE SCHEMA update_features_test;

CREATE TABLE update_features_test.json_mapping(
	target_table regclass PRIMARY KEY,
	mapping_from_ngis json,
	mapping_from_webclient json
);
INSERT INTO update_features_test.json_mapping (target_table, mapping_from_ngis)
VALUES (
	'topo_ar5ngis.face_attributes',
	'{
      "identifikasjon_lokal_id": [ "identifikasjon", "lokalId" ],
      "identifikasjon_navnerom": [ "identifikasjon", "navnerom" ],
      "identifikasjon_versjon_id": [ "identifikasjon", "versjonId" ],
      "datafangstdato": [ "datafangstdato" ],
      "verifiseringsdato": [ "verifiseringsdato" ],
      "oppdateringsdato": [ "oppdateringsdato" ],
      "opphav": [ "opphav" ],
      "kvalitet.maalemetode": [ "kvalitet", "målemetode" ],
      "kvalitet.noyaktighet": [ "kvalitet", "nøyaktighet" ],
      "kvalitet.synbarhet": [ "kvalitet", "synbarhet" ],
      "arealtype": [ "arealtype" ],
      "treslag": [ "treslag" ],
      "skogbonitet": [ "skogbonitet" ],
      "grunnforhold": [ "grunnforhold" ],
      "geometry_properties_position": [ "geometry_properties", "position" ]
	}'
),(
	'topo_ar5ngis.edge_attributes',
	'{
      "identifikasjon_lokal_id": [ "identifikasjon", "lokalId" ],
      "avgrensing_type": [ "avgrensingType" ]
	}'
);


-- Create input json table
CREATE TABLE update_features_test.json_input(
	id serial PRIMARY KEY,
	label text UNIQUE,
	source text,
	payload json
);


\i :regdir/integration/fkb46/update_features_java_add_2_areal_rows_to_json_input.sql

SELECT 'common_bbox', ST_AsText(ST_BoundingDiagonal(:bbox), :wktdigits);


SELECT 'update_features_2_areal_add_rows', topo_ar5ngis.update_features(
	(SELECT payload FROM update_features_test.json_input WHERE label = 'test_2_areal_rows')::jsonb,
	:bbox
);

SELECT 'after_update_features_2_areal_add_rows_areal_attr_geometry_properties',
    identifikasjon_lokal_id,
    arealtype,
	ST_Srid(geometry_properties_position) AS srid_position,
	ST_Srid(omrade::geometry) AS omrade_position,
	ST_IsEmpty(omrade::geometry) AS omrade_isEmpty,
	Round(ST_Area(omrade::geometry,true)::numeric) AS omrade_area
	FROM topo_ar5ngis.face_attributes ORDER BY identifikasjon_lokal_id;

SELECT 'after_update_features_2_areal_add_rows_lines_num_faces',
	count(*) AS num_faces
	FROM topo_ar5ngis_sysdata_webclient.face;



SELECT 'after_update_features_2_areal_add_rows_lines',
	SUM(Round(ST_Length(grense::geometry))),
	count(*),
	ST_AsText(ST_BoundingDiagonal(ST_Union(
		-- workaround to https://trac.osgeo.org/postgis/ticket/4871
		-- can be removed when using PostGIS >= 3.1.2
		ST_SetSRID(
			ST_Envelope(grense),
			0
		)
	)), :wktdigits)
	FROM topo_ar5ngis.edge_attributes;

SELECT 'after_update_features_2_areal_add_rows_areal',
	SUM(Round(ST_Area(omrade::geometry))),
	count(*),
	ST_AsText(ST_BoundingDiagonal(ST_Union(
		-- workaround to https://trac.osgeo.org/postgis/ticket/4871
		-- can be removed when using PostGIS >= 3.1.2
		ST_SetSRID(
			ST_Envelope(omrade),
			0
		)
	)), :wktdigits)
	FROM topo_ar5ngis.face_attributes;


-- Check for topology validity
SELECT
	'before_topo_update.add_border_split_face',
	'validate_topology',
	*
FROM topology.ValidateTopology('topo_ar5ngis_sysdata_webclient');

SELECT
	'before_topo_update.add_border_split_face',
	'surface_overlaps',
	*
FROM check_overlap(
	-- Surface layer table
	'topo_ar5ngis.face_attributes',

	-- Surface layer TopoGeometry column name
	'omrade',

	-- Surface layer primary key column name
	'identifikasjon_lokal_id'
);



--SELECT 'update_features_2_areal_add_rows_areal_attr_geometry_properties_position',
--	ST_Srid(geometry_properties_position),
--	ST_AsText(geometry_properties_position,10),
--	ST_CoveredBy(geometry_properties_position,omrade::geometry) as ok_geometry_properties_position
--	FROM topo_ar5ngis.face_attributes ORDER BY identifikasjon_lokal_id limit 2;


ROLLBACK;

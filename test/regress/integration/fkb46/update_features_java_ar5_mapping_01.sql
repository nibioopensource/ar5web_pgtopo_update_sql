BEGIN;

set client_min_messages to WARNING;
set timezone to utc;

--USED FOR testing
--\set regdir /Users/lop/dev/git/topologi/pgtopo_update_sql/test/regress
\i :regdir/../../src/sql/topo_update_java/function_01_table_row_to_jsonb_java.sql
\i :regdir/../../src/sql/topo_update_java/function_02_add_border_split_surface_java.sql
\i :regdir/../../src/sql/topo_update_java/function_02_update_feature_attribute_java.sql
\i :regdir/utils/check_overlap.sql

-- TWEAK THESE !
\set wktdigits 10
\set bbox ST_MakeEnvelope(12.138220106969866, 65.15986989011846, 12.142455807015113, 65.16396000490535, 4258)
--\set bbox ST_MakeEnvelope(-1e100, -1e100, 1e100, 1e100, 4258)

-- Make to split a small polygon
\set draw_line_01 ST_GeomFromEWKT('SRID=4258;LINESTRING(12.140187736365434 65.16176989011845,12.140488177619545 65.16206000490536)');


-- TOPOLOGY NAME: update_features_test_topo
-- AREAL LAYER:   topo_ar5ngis.face_attributes
-- LINEAL LAYER:  topo_ar5ngis.edge_attributes

-- Create mapping schema and table and add mappings
CREATE SCHEMA update_features_test;

CREATE TABLE update_features_test.json_mapping(
	target_table regclass PRIMARY KEY,
	mapping_from_ngis json,
	mapping_from_webclient json
);
INSERT INTO update_features_test.json_mapping (target_table, mapping_from_ngis)
VALUES (
	'topo_ar5ngis.face_attributes',
	'{
      "identifikasjon_lokal_id": [ "identifikasjon", "lokalId" ],
      "identifikasjon_navnerom": [ "identifikasjon", "navnerom" ],
      "identifikasjon_versjon_id": [ "identifikasjon", "versjonId" ],
      "datafangstdato": [ "datafangstdato" ],
      "verifiseringsdato": [ "verifiseringsdato" ],
      "oppdateringsdato": [ "oppdateringsdato" ],
      "opphav": [ "opphav" ],
      "kvalitet.maalemetode": [ "kvalitet", "målemetode" ],
      "kvalitet.noyaktighet": [ "kvalitet", "nøyaktighet" ],
      "kvalitet.synbarhet": [ "kvalitet", "synbarhet" ],
      "arealtype": [ "arealtype" ],
      "treslag": [ "treslag" ],
      "skogbonitet": [ "skogbonitet" ],
      "grunnforhold": [ "grunnforhold" ]
	}'
),(
	'topo_ar5ngis.edge_attributes',
	'{
      "identifikasjon_lokal_id": [ "identifikasjon", "lokalId" ],
      "identifikasjon_navnerom": [ "identifikasjon", "navnerom" ],
      "identifikasjon_versjon_id": [ "identifikasjon", "versjonId" ],
      "datafangstdato": [ "datafangstdato" ],
      "verifiseringsdato": [ "verifiseringsdato" ],
      "oppdateringsdato": [ "oppdateringsdato" ],
      "opphav": [ "opphav" ],
      "kvalitet.maalemetode": [ "kvalitet", "målemetode" ],
      "kvalitet.noyaktighet": [ "kvalitet", "nøyaktighet" ],
      "kvalitet.synbarhet": [ "kvalitet", "synbarhet" ],
      "avgrensing_type": [ "avgrensingType" ]
	}'
);


-- Create input json table
CREATE TABLE update_features_test.json_input(
	id serial PRIMARY KEY,
	label text UNIQUE,
	source text,
	payload json
);


\i :regdir/integration/fkb46/update_features_java_add_2_areal_rows_to_json_input.sql

SELECT 'common_bbox', ST_AsText(ST_BoundingDiagonal(:bbox), :wktdigits);


SELECT 'update_features_2_areal_add_rows', topo_ar5ngis.update_features(
	(SELECT payload FROM update_features_test.json_input WHERE label = 'test_2_areal_rows')::jsonb,
	:bbox
);


SELECT 'after_update_features_2_areal_add_rows_areal_attr_properties',
   topo_update.table_row_to_jsonb_java(
   'topo_ar5ngis.face_attributes'::text,
   'identifikasjon_lokal_id'::text,
   '0008e6eb-1332-4145-9943-591020916480'::text,
   (SELECT mapping_from_ngis FROM update_features_test.json_mapping WHERE target_table = 'topo_ar5ngis.face_attributes'::regclass)::TEXT
   );


SELECT 'after_update_features_2_areal_add_rows_areal_attr_geometry_properties',
    identifikasjon_lokal_id,
    arealtype,
	ST_Srid(geometry_properties_position) AS srid_position,
	ST_Srid(omrade::geometry) AS omrade_position,
	ST_IsEmpty(omrade::geometry) AS omrade_isEmpty,
	Round(ST_Area(omrade::geometry,true)::numeric) AS omrade_area
	FROM topo_ar5ngis.face_attributes ORDER BY identifikasjon_lokal_id limit 2;


SELECT 'after_update_features_2_areal_add_rows_lines_num_faces',
	count(*) AS num_faces
	FROM topo_ar5ngis_sysdata_webclient.face;


SELECT 'update_features_2_areal_add_rows_lines',
	SUM(Round(ST_Length(grense::geometry))),
	count(*),
	ST_AsText(ST_BoundingDiagonal(ST_Union(
		-- workaround to https://trac.osgeo.org/postgis/ticket/4871
		-- can be removed when using PostGIS >= 3.1.2
		ST_SetSRID(
			ST_Envelope(grense),
			0
		)
	)), :wktdigits)
	FROM topo_ar5ngis.edge_attributes;

SELECT 'update_features_2_areal_add_rows_areal',
	SUM(Round(ST_Area(omrade::geometry))),
	count(*),
	ST_AsText(ST_BoundingDiagonal(ST_Union(
		-- workaround to https://trac.osgeo.org/postgis/ticket/4871
		-- can be removed when using PostGIS >= 3.1.2
		ST_SetSRID(
			ST_Envelope(omrade),
			0
		)
	)), :wktdigits)
	FROM topo_ar5ngis.face_attributes;

SELECT 'update_features_2_areal_add_rows_lines_attr',
	identifikasjon_lokal_id,
	avgrensing_type,
	(kvalitet).maalemetode
	FROM topo_ar5ngis.edge_attributes ORDER BY identifikasjon_lokal_id limit 2;

SELECT 'update_features_2_areal_add_rows_areal_attr',
	identifikasjon_lokal_id,
	identifikasjon_navnerom,
	identifikasjon_versjon_id::timestamp,
	datafangstdato,
    verifiseringsdato,
    oppdateringsdato,
    opphav,
    (kvalitet).maalemetode,
    (kvalitet).noyaktighet,
    (kvalitet).synbarhet,
	arealtype,
	treslag,
	skogbonitet,
	grunnforhold
	FROM topo_ar5ngis.face_attributes ORDER BY identifikasjon_lokal_id limit 2;

SELECT 'update_features_2_areal_add_rows_areal_attr_geometry_properties_position',
	ST_Srid(geometry_properties_position),
	ST_AsText(geometry_properties_position,10),
	ST_CoveredBy(geometry_properties_position,omrade::geometry) as ok_geometry_properties_position
	FROM topo_ar5ngis.face_attributes ORDER BY identifikasjon_lokal_id limit 2;


SELECT 'before_topo_update.add_border_split_surface',
    count(*)
	FROM topo_ar5ngis.face_attributes;

SELECT 'before_topo_update.add_border_split_surface',
    count(*)
	FROM topo_ar5ngis.edge_attributes;

-- Check for topology validity
SELECT
	'before_topo_update.add_border_split_face',
	'validate_topology',
	*
FROM topology.ValidateTopology('topo_ar5ngis_sysdata_webclient');

SELECT
	'before_topo_update.add_border_split_face',
	'surface_overlaps',
	*
FROM check_overlap(
	-- Surface layer table
	'topo_ar5ngis.face_attributes',

	-- Surface layer TopoGeometry column name
	'omrade',

	-- Surface layer primary key column name
	'identifikasjon_lokal_id'
);

SELECT 'date check before after_topo_update.add_border_split_surface',
    datafangstdato, verifiseringsdato, round(ST_Area(omrade::Geometry,true))
	FROM topo_ar5ngis.face_attributes ORDER BY verifiseringsdato,ST_Area(omrade::Geometry,true) ;



SELECT 'topo_update.add_border_split_surface', null from topo_update.add_border_split_surface_java(

	-- A GeoJSON Feature object representing the
	-- line drawn by the user and the holding the
	-- attributes to use with the border and the
	-- surface layers
	$$
		{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:4258"
					}
				},
				"type": "LineString",
				"coordinates": [
					[
						12.140187736,
						65.16176989
					],
					[
						12.140488178,
						65.162060005
					]
				]
			},
			"properties": {
				"edge_attributes_props": {
					"opphav": "opphav-edge", "datafangstdato": "2000-12-01" , "verifiseringsdato": "2001-02-01"
				},
				"face_attributes_props": {
					"opphav": "opphav-face"
				}
			}
		}
	$$::TEXT,

	-- Surface layer table
	'topo_ar5ngis.face_attributes'::TEXT,

	-- Surface layer TopoGeometry column name
	'omrade'::TEXT,

	-- Surface layer primary key column name
	'identifikasjon_lokal_id'::TEXT,

	-- Mapping file for new surface layer
	-- see json_props_to_pg_cols
	'{
	"opphav": [
		"face_attributes_props",
		"opphav"
	],
	"verifiseringsdato": [
		"edge_attributes_props",
		"verifiseringsdato"
	],
	"datafangstdato": [
		"edge_attributes_props",
		"datafangstdato"
	]
	}'::TEXT,

	-- Mapping file for update surface layer
	-- see json_props_to_pg_cols
	'{
	"opphav": [
		"face_attributes_props",
		"opphav"
	],
	"verifiseringsdato": [
		"edge_attributes_props",
		"verifiseringsdato"
	]
	}'::TEXT,

	-- Border layer table
	'topo_ar5ngis.edge_attributes'::TEXT,

	-- Name of Border layer TopoGeometry column
	'grense'::TEXT,

	-- Surface layer primary key column name
	'identifikasjon_lokal_id'::TEXT,

	-- Mapping file for new row border layer
	-- see json_props_to_pg_cols
	'{ "opphav": [ "edge_attributes_props", "opphav" ],
	"verifiseringsdato": [
		"edge_attributes_props",
		"verifiseringsdato"
	] }'::TEXT,

	-- Mapping file updated row in border layer
	-- see json_props_to_pg_cols
	'{ "verifiseringsdato": [
		"edge_attributes_props",
		"verifiseringsdato"
	] }'::TEXT,

	-- Snap tolerance to use when
	-- inserting the new line
	-- in the topology
	0.0000000001::float8
);


SELECT 'after_topo_update.add_border_split_surface',
    count(*)
	FROM topo_ar5ngis.face_attributes;

SELECT 'date check after after_topo_update.add_border_split_surface',
    datafangstdato, verifiseringsdato, round(ST_Area(omrade::Geometry,true))
	FROM topo_ar5ngis.face_attributes ORDER BY verifiseringsdato,ST_Area(omrade::Geometry,true) ;

SELECT 'upd_after_after', (topo_update.update_feature_attribute_java(
	$$
	{
	  "type": "Feature",
	  "properties": {
	    "id": "62bae489-cadb-4999-b7ea-e807149edbc0",
	    "opphav": "opphav_nytt",
	    "kvalitet": {
	      "målemetode": "82"
	    }
	  }
	}
	$$, -- feature

	-- Surface layer table
	'topo_ar5ngis.face_attributes'::TEXT,

	-- Surface layer primary key column name
	'identifikasjon_lokal_id'::TEXT,
	$$
	{
		"identifikasjon_lokal_id": [
			"id"
		],
		"kvalitet": [
			"kvalitet"
		],
		"kartstandard": [
			"kartstandard"
		],
		"opphav": [
			"opphav"
		],
		"arealtype": [
			"arealtype"
		],
		"treslag": [
			"treslag"
		],
		"skogbonitet": [
			"skogbonitet"
		],
		"grunnforhold": [
			"grunnforhold"
		],
		"datafangstdato": [
			"datafangstdato"
		],
		"verifiseringsdato": [
			"verifiseringsdato"
		],
		"oppdateringsdato": [
			"oppdateringsdato"
		]
	}
	$$, -- layerColMap
	'webklient_test'
) IS NOT NULL);

SELECT 'upd_after_after_result',
    opphav,
    kvalitet,
    kartstandard,
    datafangstdato,
    verifiseringsdato
	FROM topo_ar5ngis.face_attributes WHERE identifikasjon_lokal_id = '62bae489-cadb-4999-b7ea-e807149edbc0';

ROLLBACK;

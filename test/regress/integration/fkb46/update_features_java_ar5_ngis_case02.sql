BEGIN;

set client_min_messages to WARNING;
set timezone to utc;

-- the bug desciption is here https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/114#note_559441395


\i :regdir/../../src/sql/topo_update_java/function_02_add_border_split_surface_java.sql
\i :regdir/../../src/sql/topo_update_java/function_02_update_feature_attribute_java.sql
\i :regdir/utils/check_overlap.sql

-- Make :datadir available as a variable
SELECT :'regdir' || '/../../test/data/fkb46' as datadir \gset


CREATE TEMP TABLE json_input_01_utm35(payload text);
SELECT :'datadir' || '/ar5_DatasetFeaturesAsJsonString_OK_srid_25835_UTF-8_1619242313106_.json' as x \gset
COPY json_input_01_utm35 FROM :'x';
SELECT '--json-input-',length(payload) FROM json_input_01_utm35;

-- TOPOLOGY NAME: update_features_test_topo
-- AREAL LAYER:   topo_ar5ngis.face_attributes
-- LINEAL LAYER:  topo_ar5ngis.edge_attributes

SELECT '--a1--', topo_ar5ngis.update_features(
	(SELECT payload FROM json_input_01_utm35)::jsonb,
	ST_GeomFromEWKT('SRID=4258;POLYGON ((28.567214255051546 70.12077381994374, 28.567385375081894 70.12303682108494, 28.577537594924564 70.12294764027095, 28.577365367637622 70.12068465011427, 28.567214255051546 70.12077381994374))')
);

select '--t1-- num ok geometry_properties_position', count(*) from topo_ar5ngis.face_attributes where ST_Intersects(geometry_properties_position,ST_GeomFromEWKT('SRID=4258;POLYGON ((28.567214255051546 70.12077381994374, 28.567385375081894 70.12303682108494, 28.577537594924564 70.12294764027095, 28.577365367637622 70.12068465011427, 28.567214255051546 70.12077381994374))'));

SELECT '--t1-- face_attributes before split',
    count(*) AS num,
    Sum(Round(ST_Area(omrade::geometry::geography)::numeric, 2)) AS area
FROM topo_ar5ngis.face_attributes;


SELECT '--t1-- test overlaps', count(*) as num_over_lap from topo_update.find_interiors_intersect('topo_ar5ngis.edge_attributes', 'grense', 'identifikasjon_lokal_id');


CREATE TEMPORARY TABLE add_border_results(
	test_name TEXT,
	fid TEXT,
	sid SERIAL,
	typ CHAR,
	act CHAR
);




INSERT INTO add_border_results(test_name, fid, typ, act)
SELECT '--t1-- run split', fid, typ, act
FROM topo_update.add_border_split_surface_java(

	-- A GeoJSON Feature object representing the
	-- line drawn by the user and the holding the
	-- attributes to use with the border and the
	-- surface layers
	$$
		{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:25835"
					}
				},
				"type": "LineString",
				"coordinates": [[559682.8510588652,7780215.3876047935],[559693.8535226489,7780142.384770769]]
			},
			"properties": {
				"edge_attributes_props": {
					"opphav": "opphav-edge-test-missing-feature", "datafangstdato": "2000-12-01" , "verifiseringsdato": "2001-02-01"
				},
				"face_attributes_props": {
					"opphav": "opphav-face-test-missing-feature"
				}
			}
		}
	$$::TEXT,

	-- Surface layer table
	'topo_ar5ngis.face_attributes'::TEXT,

	-- Surface layer TopoGeometry column name
	'omrade'::TEXT,

	-- Surface layer primary key column name
	'identifikasjon_lokal_id'::TEXT,

	-- Mapping file for new surface layer
	-- see json_props_to_pg_cols
	'{
	"opphav": [
		"face_attributes_props",
		"opphav"
	],
	"verifiseringsdato": [
		"edge_attributes_props",
		"verifiseringsdato"
	],
	"datafangstdato": [
		"edge_attributes_props",
		"datafangstdato"
	]
	}'::TEXT,

	-- Mapping file for update surface layer
	-- see json_props_to_pg_cols
	'{
	"opphav": [
		"face_attributes_props",
		"opphav"
	],
	"verifiseringsdato": [
		"edge_attributes_props",
		"verifiseringsdato"
	]
	}'::TEXT,

	-- Border layer table
	'topo_ar5ngis.edge_attributes'::TEXT,

	-- Name of Border layer TopoGeometry column
	'grense'::TEXT,

	-- Surface layer primary key column name
	'identifikasjon_lokal_id'::TEXT,

	-- Mapping file for new row border layer
	-- see json_props_to_pg_cols
	'{ "opphav": [ "edge_attributes_props", "opphav" ],
	"verifiseringsdato": [
		"edge_attributes_props",
		"verifiseringsdato"
	] }'::TEXT,

	-- Mapping file updated row in border layer
	-- see json_props_to_pg_cols
	'{ "verifiseringsdato": [
		"edge_attributes_props",
		"verifiseringsdato"
	] }'::TEXT,

	-- Snap tolerance to use when
	-- inserting the new line
	-- in the topology
	1e-10::float8
);

-----------------------------------------
-- t2: check result after split
-----------------------------------------

SELECT '--t2-- face_attributes after split',
    count(*) AS num,
    Sum(Round(ST_Area(omrade::geometry::geography)::numeric, 2)) AS area
	FROM topo_ar5ngis.face_attributes;

SELECT '--t2-- border used but not changed ---', * FROM  add_border_results WHERE typ ='B' AND act='U' ORDER BY sid ;
SELECT '--t2-- surfaces touching changed borders ---', * FROM  add_border_results WHERE typ ='S' AND act='T' ORDER BY sid ;
SELECT '--t2-- check the rest ---', * FROM  add_border_results WHERE act !='T' AND act !='U' ORDER BY sid ;



-----------------------------------------
-- t3: call with feature rewrite function
-----------------------------------------

-- if you need the result as file use  \o /tmp/surfaces_as_geojson.json

SELECT '--- t3 ---', E'\n' || jsonb_pretty(
	topo_ar5ngis.surfaces_as_geojson(
		'add_border_results',
		25835, -- utm 35
		5
	)::JSONB
);



ROLLBACK;

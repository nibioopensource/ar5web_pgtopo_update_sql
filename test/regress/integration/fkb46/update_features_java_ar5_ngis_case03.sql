BEGIN;
-- To test case
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/114#note_558524232

--{"detail":"Commit feilet","errors":[{"reason":"Alle objekter som deler geometrien er ikke med i innsjekken:
--\n\t\Grenselinjen (id: 71931538 globalid: b7fc8dd3-7fa0-4963-b34e-c6b822df2523) som ble sjekket inn mangler følgende tilhørende objekter i innsjekken:
--\n\t\tFlaten (id: 72785647 globalid: 8aa34018-509a-4c9d-9a76-b45b7b08157f)
--\n\tGrenselinjen (id: 72413078 globalid: 1455cac3-53b1-4e2c-87ed-92c70eb0cb4c) som ble sjekket inn mangler følgende tilhørende objekter i innsjekken:
--\n\t\tFlaten (id: 72413234 globalid: 1c85ec9f-c68a-4c54-94de-d5bed0d5556b)
--\n\tGrenselinjen (id:72415503 globalid: 8f64c6bc-be72-477d-a86d-a79f6ef61eae) som ble sjekket inn mangler følgende tilhørende objekter i innsjekken:
--\n\t\tFlaten (id: 72787044 globalid: ddd3606d-c28a-4d8d-90b5-c65cd292c66e)"}]
--,"title":"Commit feilet","type":"http://ngisopenapi.no/errors/commit_error"}

set client_min_messages to WARNING;
set timezone to utc;


\i :regdir/../../src/sql/topo_update_java/function_02_add_border_split_surface_java.sql
\i :regdir/../../src/sql/topo_update_java/function_02_update_feature_attribute_java.sql
\i :regdir/utils/check_overlap.sql

-- Make :datadir available as a variable
SELECT :'regdir' || '/../../test/data/fkb46' as datadir \gset


CREATE TEMP TABLE json_input_01_utm35(payload text);
SELECT :'datadir' || '/ar5_DatasetFeaturesAsJsonString_OK_srid_25835_UTF-8_1619241322168_.json' as x \gset
COPY json_input_01_utm35 FROM :'x';
SELECT '--json-input-',length(payload) FROM json_input_01_utm35;

-- TOPOLOGY NAME: update_features_test_topo
-- AREAL LAYER:   topo_ar5ngis.face_attributes
-- LINEAL LAYER:  topo_ar5ngis.edge_attributes

SELECT '--a1--', topo_ar5ngis.update_features(
	(SELECT payload FROM json_input_01_utm35)::jsonb,
	null
);

SELECT '--t1-- face_attributes before split',
    count(*) AS num,
    Sum(Round(ST_Area(omrade::geometry::geography)::numeric, 2)) AS area
FROM topo_ar5ngis.face_attributes;


SELECT 'upd_after_after', (topo_update.update_feature_attribute_java(
	$$
	{
	  "type": "Feature",
	  "properties": {
	    "id": "dc0773d1-24c5-4077-8bf0-7841bc0263b9",
	    "informasjon": "informasjon old"
	  }
	}
	$$, -- feature

	-- Surface layer table
	'topo_ar5ngis.face_attributes'::TEXT,

	-- Surface layer primary key column name
	'identifikasjon_lokal_id'::TEXT,
	$$
	{
		"identifikasjon_lokal_id": [
			"id"
		],
		"identifikasjon_versjon_id": [
			"identifikasjon_versjon_id"
		],
		"kvalitet": [
			"kvalitet"
		],
		"kartstandard": [
			"kartstandard"
		],
		"informasjon": [
			"informasjon"
		],
		"opphav": [
			"opphav"
		],
		"arealtype": [
			"arealtype"
		],
		"treslag": [
			"treslag"
		],
		"skogbonitet": [
			"skogbonitet"
		],
		"grunnforhold": [
			"grunnforhold"
		],
		"datafangstdato": [
			"datafangstdato"
		],
		"verifiseringsdato": [
			"verifiseringsdato"
		],
		"oppdateringsdato": [
			"oppdateringsdato"
		]
	}
	$$, -- layerColMap
	'webklient_test'
) IS NOT NULL);

UPDATE topo_ar5ngis.face_attributes f
SET identifikasjon_versjon_id = '1970-01-01T00:00:00'
WHERE f.identifikasjon_versjon_id > (now() - '60s'::interval);

SELECT '--t1-- face_attributes before split with informasjon',
	identifikasjon_lokal_id,
	identifikasjon_versjon_id,
  	opphav,
    kvalitet,
    kartstandard,
    datafangstdato,
    verifiseringsdato,
    informasjon
FROM topo_ar5ngis.face_attributes WHERE informasjon is NOT NULL ORDER BY informasjon;

SELECT '--t1-- check b7fc8dd3-7fa0-4963-b34e-c6b822df2523 ',
    Sum(Round(ST_Length(grense::geometry::geography)::numeric, 2)) AS length
FROM topo_ar5ngis.edge_attributes WHERE identifikasjon_lokal_id = 'b7fc8dd3-7fa0-4963-b34e-c6b822df2523';

SELECT '--t1-- test overlaps', count(*) as num_over_lap from topo_update.find_interiors_intersect('topo_ar5ngis.edge_attributes', 'grense', 'identifikasjon_lokal_id');



CREATE TEMPORARY TABLE add_border_results(
	test_name TEXT,
	fid TEXT,
	sid SERIAL,
	typ CHAR,
	act CHAR
);




INSERT INTO add_border_results(test_name, fid, typ, act)
SELECT '--t1-- run split', fid, typ, act
FROM topo_ar5ngis.add_border(

	-- A GeoJSON Feature object representing the
	-- line drawn by the user and the holding the
	-- attributes to use with the border and the
	-- surface layers
	$$
		{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:25835"
					}
				},
				"type": "LineString",
				"coordinates": [[559754.817222428,7780178.7396741975],[559741.692648941,7780152.490527224],[559767.9417959151,7780156.552895208],[559746.6924864599,7780174.989796058]]
			},
			"properties": {
			    "opphav": "test-opphav",
			    "datafangstdato": "2000-12-01"
			}
		}
	$$::TEXT,
	'server_side_opphav'::TEXT);

-----------------------------------------
-- t2: check result after split
-----------------------------------------

SELECT '--t2-- face_attributes after split',
    count(*) AS num,
    Sum(Round(ST_Area(omrade::geometry::geography)::numeric, 2)) AS area
	FROM topo_ar5ngis.face_attributes;

SELECT '--t2-- border used but not changed ---', * FROM  add_border_results WHERE typ ='B' AND act='U' ORDER BY sid ;
SELECT '--t2-- surfaces touching changed borders ---', * FROM  add_border_results WHERE typ ='S' AND act='T' ORDER BY sid ;
SELECT '--t2-- check the rest ---', * FROM  add_border_results WHERE act !='T' AND act !='U' ORDER BY sid ;



SELECT '--t2-- check b7fc8dd3-7fa0-4963-b34e-c6b822df2523 in add_border_results ',
count(*) AS found
FROM add_border_results WHERE fid = 'b7fc8dd3-7fa0-4963-b34e-c6b822df2523';


-- update result dates to make tests stable
UPDATE topo_ar5ngis.face_attributes f
SET identifikasjon_versjon_id = '1970-01-01T00:00:00'
WHERE f.identifikasjon_versjon_id > (now() - '60s'::interval);

UPDATE topo_ar5ngis.edge_attributes f
SET identifikasjon_versjon_id = '1970-01-01T00:00:00'
WHERE f.identifikasjon_versjon_id > (now() - '60s'::interval);

UPDATE topo_ar5ngis.face_attributes f
SET oppdateringsdato='1970-01-01T00:00:00'
WHERE f.oppdateringsdato > (now() - '60s'::interval);

UPDATE topo_ar5ngis.edge_attributes f
SET oppdateringsdato='1970-01-01T00:00:00'
WHERE f.oppdateringsdato > (now() - '60s'::interval);


SELECT '--t1-- check b7fc8dd3-7fa0-4963-b34e-c6b822df2523 ',
    Sum(Round(ST_Length(grense::geometry::geography)::numeric, 2)) AS length
FROM topo_ar5ngis.edge_attributes WHERE identifikasjon_lokal_id = 'b7fc8dd3-7fa0-4963-b34e-c6b822df2523';



-----------------------------------------
-- t3: call with feature rewrite function
-----------------------------------------

-- if you need the result as file use  \o /tmp/surfaces_as_geojson.json

SELECT '--- t3 ---', E'\n' || jsonb_pretty(
	topo_ar5ngis.surfaces_as_geojson(
		'add_border_results',
		25835, -- utm 35
		5
	)::JSONB
);

BEGIN;

-- To test case
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/114#note_558524232

--{"detail":"Commit feilet","errors":[{"reason":"Alle objekter som deler geometrien er ikke med i innsjekken:
--\n\t\Grenselinjen (id: 71931538 globalid: b7fc8dd3-7fa0-4963-b34e-c6b822df2523) som ble sjekket inn mangler følgende tilhørende objekter i innsjekken:
--\n\t\tFlaten (id: 72785647 globalid: 8aa34018-509a-4c9d-9a76-b45b7b08157f)
--\n\tGrenselinjen (id: 72413078 globalid: 1455cac3-53b1-4e2c-87ed-92c70eb0cb4c) som ble sjekket inn mangler følgende tilhørende objekter i innsjekken:
--\n\t\tFlaten (id: 72413234 globalid: 1c85ec9f-c68a-4c54-94de-d5bed0d5556b)
--\n\tGrenselinjen (id:72415503 globalid: 8f64c6bc-be72-477d-a86d-a79f6ef61eae) som ble sjekket inn mangler følgende tilhørende objekter i innsjekken:
--\n\t\tFlaten (id: 72787044 globalid: ddd3606d-c28a-4d8d-90b5-c65cd292c66e)"}]
--,"title":"Commit feilet","type":"http://ngisopenapi.no/errors/commit_error"}

set client_min_messages to WARNING;
set timezone to utc;


\i :regdir/../../src/sql/topo_update_java/function_02_add_border_split_surface_java.sql
\i :regdir/../../src/sql/topo_update_java/function_02_update_feature_attribute_java.sql
\i :regdir/utils/check_overlap.sql

-- Make :datadir available as a variable
SELECT :'regdir' || '/../../test/data/fkb46' as datadir \gset


CREATE TEMP TABLE json_input_01_utm35(payload text);
SELECT :'datadir' || '/ar5_DatasetFeaturesAsJsonString_OK_srid_25832_UTF-8_1620214425346_.json' as x \gset
COPY json_input_01_utm35 FROM :'x';
SELECT '--json-input-',length(payload) FROM json_input_01_utm35;


SELECT '--a1--', topo_ar5ngis.update_features(
	(SELECT payload FROM json_input_01_utm35)::jsonb,
	ST_GeomFromEWKT('SRID=4258;POLYGON((9.55977442789218 58.93571184184625,9.55977442789218 58.9567911189119,9.660503619300684 58.9567911189119,9.660503619300684 58.93571184184625,9.55977442789218 58.93571184184625))')
);

select '--a1-- face_attributes', count(*) from topo_ar5ngis.face_attributes;
select '--a1-- edge_attributes', count(*) from topo_ar5ngis.edge_attributes;



SELECT '--a2--with--preparePayload', topo_ar5ngis.update_features(
	(SELECT payload FROM json_input_01_utm35)::jsonb,
	ST_GeomFromEWKT('SRID=4258;POLYGON((9.55977442789218 58.93571184184625,9.55977442789218 58.9567911189119,9.660503619300684 58.9567911189119,9.660503619300684 58.93571184184625,9.55977442789218 58.93571184184625))')
);

select '--a2-- face_attributes', count(*) from topo_ar5ngis.face_attributes;
select '--a2-- edge_attributes', count(*) from topo_ar5ngis.edge_attributes;

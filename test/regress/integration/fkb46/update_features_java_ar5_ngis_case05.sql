BEGIN;

-- To test case
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/114#note_558524232

--{"detail":"Commit feilet","errors":[{"reason":"Alle objekter som deler geometrien er ikke med i innsjekken:
--\n\t\Grenselinjen (id: 71931538 globalid: b7fc8dd3-7fa0-4963-b34e-c6b822df2523) som ble sjekket inn mangler følgende tilhørende objekter i innsjekken:
--\n\t\tFlaten (id: 72785647 globalid: 8aa34018-509a-4c9d-9a76-b45b7b08157f)
--\n\tGrenselinjen (id: 72413078 globalid: 1455cac3-53b1-4e2c-87ed-92c70eb0cb4c) som ble sjekket inn mangler følgende tilhørende objekter i innsjekken:
--\n\t\tFlaten (id: 72413234 globalid: 1c85ec9f-c68a-4c54-94de-d5bed0d5556b)
--\n\tGrenselinjen (id:72415503 globalid: 8f64c6bc-be72-477d-a86d-a79f6ef61eae) som ble sjekket inn mangler følgende tilhørende objekter i innsjekken:
--\n\t\tFlaten (id: 72787044 globalid: ddd3606d-c28a-4d8d-90b5-c65cd292c66e)"}]
--,"title":"Commit feilet","type":"http://ngisopenapi.no/errors/commit_error"}

set client_min_messages to WARNING;
set timezone to utc;


\i :regdir/../../src/sql/topo_update_java/function_02_add_border_split_surface_java.sql
\i :regdir/../../src/sql/topo_update_java/function_02_update_feature_attribute_java.sql
\i :regdir/utils/check_overlap.sql

-- Make :datadir available as a variable
SELECT :'regdir' || '/../../test/data/fkb46' as datadir \gset


CREATE TEMP TABLE json_input_01_utm33(payload text);
SELECT :'datadir' || '/ar5_DatasetFeaturesAsJsonString_OK_srid_25833_UTF-8_1624014749150_.json' as x \gset
COPY json_input_01_utm33 FROM :'x';
SELECT '--json-input-',length(payload) FROM json_input_01_utm33;


SELECT '--a1--', topo_ar5ngis.update_features(
	(SELECT payload FROM json_input_01_utm33)::jsonb,
	ST_GeomFromEWKT('SRID=4258;POLYGON((21.117261781597527 69.82741596330995, 21.118321174161547 69.83104972553876, 21.141049682610536 69.83025909262167, 21.139986414921893 69.82662548485379, 21.117261781597527 69.82741596330995))')
);

select '--a1-- face_attributes', count(*) from topo_ar5ngis.face_attributes;
select '--a1-- edge_attributes', count(*) from topo_ar5ngis.edge_attributes;

--bbox:POLYGON ((21.117261781597527 69.82741596330995, 21.118321174161547 69.83104972553876, 21.141049682610536 69.83025909262167, 21.139986414921893 69.82662548485379, 21.117261781597527 69.82741596330995))
--- Convert from 4258 to 25833. -


-- 2021-06-18 13:14:32,431 [ajp-nio-8009-exec-8] ERROR TopoClientComImpl no.skogoglandskap.topo.client.TopoClientComImpl.apply_line_on_topo_surface(TopoClientComImpl.java:2142) 2142 -
-- Failed to call topo_update.create_surface_edge_domain_obj for layer topo_ar5ngis.face_attributes using proc topo_update.create_surface_edge_domain_obj  with json_feature
-- {"type":"Feature","geometry":{"type":"LineString","coordinates":[[735538.1076193774,7758559.263737038],[735588.2816888541,7758552.303172486],[735601.0427238655,7758490.818185613],[735565.0798070151,7758486.757856291]],"crs":{"type":"name","properties":{"name":"EPSG:25833"}}},"properties":{"datafangstdato":"2021-06-18","maalemetode":82}}, msg:org.hibernate.exception.GenericJDBCException: Error calling CallableStatement.getMoreResults
-- Caused by: org.postgresql.util.PSQLException: ERROR: Shell is not a line
-- Where: PL/pgSQL function topo_update._surface_as_geojson_features(topology,layer,text,topogeometry,jsonb,regclass,name,name,jsonb,regclass,regproc,anyelement,integer,integer) line 173 at assignment
-- SQL statement "SELECT array_agg(f) FILTER (WHERE f IS NOT NULL)
--                FROM topo_update._surface_as_geojson_features(
--                        topo,

CREATE TEMPORARY TABLE add_border_results(
	test_name TEXT,
	fid TEXT,
	sid SERIAL,
	typ CHAR,
	act CHAR
);



INSERT INTO add_border_results(test_name, fid, typ, act)
SELECT '--t2-- run split', fid, typ, act
FROM topo_ar5ngis.add_border(

	-- A GeoJSON Feature object representing the
	-- line drawn by the user and the holding the
	-- attributes to use with the border and the
	-- surface layers
	$$
		{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:25833"
					}
				},
				"type": "LineString",
				"coordinates": [[735538.1076193774,7758559.263737038],[735588.2816888541,7758552.303172486],[735601.0427238655,7758490.818185613],[735565.0798070151,7758486.757856291]]
			},
			"properties": {
			    "opphav": "test-opphav",
			    "datafangstdato": "2000-12-01"
			}
		}
	$$::TEXT,
	'server_side_opphav'::TEXT);


SELECT '--t2-- border used but not changed ---', * FROM  add_border_results WHERE typ ='B' AND act='U' ORDER BY sid ;
SELECT '--t2-- surfaces touching changed borders ---', * FROM  add_border_results WHERE typ ='S' AND act='T' ORDER BY sid ;
SELECT '--t2-- check the rest ---', * FROM  add_border_results WHERE act !='T' AND act !='U' ORDER BY sid ;



SELECT '--t2-- check b7fc8dd3-7fa0-4963-b34e-c6b822df2523 in add_border_results ',
count(*) AS found
FROM add_border_results WHERE fid = 'b7fc8dd3-7fa0-4963-b34e-c6b822df2523';


-- update result dates to make tests stable
UPDATE topo_ar5ngis.face_attributes f
SET identifikasjon_versjon_id = '1970-01-01T00:00:00'
WHERE f.identifikasjon_versjon_id > (now() - '60s'::interval);

UPDATE topo_ar5ngis.edge_attributes f
SET identifikasjon_versjon_id = '1970-01-01T00:00:00'
WHERE f.identifikasjon_versjon_id > (now() - '60s'::interval);

UPDATE topo_ar5ngis.face_attributes f
SET oppdateringsdato='1970-01-01T00:00:00'
WHERE f.oppdateringsdato > (now() - '60s'::interval);

UPDATE topo_ar5ngis.edge_attributes f
SET oppdateringsdato='1970-01-01T00:00:00'
WHERE f.oppdateringsdato > (now() - '60s'::interval);

-----------------------------------------
-- t3: call with feature rewrite function
-----------------------------------------

-- if you need the result as file use  \o /tmp/surfaces_as_geojson.json

SELECT '--- t3 ---', E'\n' || jsonb_pretty(
	topo_ar5ngis.surfaces_as_geojson(
		'add_border_results',
		25835, -- utm 35
		5
	)::JSONB
);


TRUNCATE TABLE add_border_results;

SELECT 't3-berfore-surface', registreringsversjon, kvalitet,datafangstdato, ST_area(omrade::Geometry,true)::int from topo_ar5ngis.face_attributes where identifikasjon_lokal_id = '1e698326-ce9f-11eb-8195-9779efae6429';
SELECT 't3-begfore-edge', registreringsversjon, kvalitet,datafangstdato, ST_length(grense::Geometry,true)::int from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = '23af0917-0cab-4630-9161-d2d58a83b087';


INSERT INTO add_border_results(test_name, fid, typ, act)
SELECT '--t3-hole-- run split', fid, typ, act
FROM topo_ar5ngis.add_border(

	-- A GeoJSON Feature object representing the
	-- line drawn by the user and the holding the
	-- attributes to use with the border and the
	-- surface layers
	$$
		{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:25833"
					}
				},
				"type": "LineString",
				"coordinates": [[735498.715,7758672.15],[735498.715,7758732.15],[735558.715,7758732.15],[735558.715,7758672.15],[735498.715,7758672.15]]
			},
			"properties": {
			    "opphav": "test-opphav",
			    "datafangstdato": "2000-12-01"
			}
		}
	$$::TEXT,
	'server_side_opphav'::TEXT);


SELECT '--t3-hole-- surfaces touching changed borders ---', * FROM  add_border_results WHERE typ ='S' AND act='C';
SELECT '--t3-hole-- surfaces touching changed borders ---', * FROM  add_border_results WHERE typ ='B' AND act='C';

-- kvalitet should not have changed

SELECT 't3-after-surface', registreringsversjon, kvalitet,datafangstdato, ST_area(omrade::Geometry,true)::int from topo_ar5ngis.face_attributes where identifikasjon_lokal_id = '1e698326-ce9f-11eb-8195-9779efae6429';
SELECT 't3-after-surface', registreringsversjon, kvalitet,datafangstdato, ST_area(omrade::Geometry,true)::int from topo_ar5ngis.face_attributes where identifikasjon_lokal_id = '00000000-0000-0000-0000-000000000018';
SELECT 't3-after-surface', registreringsversjon, kvalitet,datafangstdato, ST_area(omrade::Geometry,true)::int from topo_ar5ngis.face_attributes where identifikasjon_lokal_id = '00000000-0000-0000-0000-000000000019';

SELECT 't3-after-edge', registreringsversjon, kvalitet,datafangstdato, ST_length(grense::Geometry,true)::int from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = '23af0917-0cab-4630-9161-d2d58a83b087';
SELECT 't3-after-edge', registreringsversjon, kvalitet,datafangstdato, ST_length(grense::Geometry,true)::int from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = '00000000-0000-0000-0000-000000000005';
SELECT 't3-after-edge', registreringsversjon, kvalitet,datafangstdato, ST_length(grense::Geometry,true)::int from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = '00000000-0000-0000-0000-000000000006';

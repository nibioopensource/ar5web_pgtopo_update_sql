BEGIN;

-- To test case
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/114#note_558524232

--{"detail":"Commit feilet","errors":[{"reason":"Alle objekter som deler geometrien er ikke med i innsjekken:
--\n\t\Grenselinjen (id: 71931538 globalid: b7fc8dd3-7fa0-4963-b34e-c6b822df2523) som ble sjekket inn mangler følgende tilhørende objekter i innsjekken:
--\n\t\tFlaten (id: 72785647 globalid: 8aa34018-509a-4c9d-9a76-b45b7b08157f)
--\n\tGrenselinjen (id: 72413078 globalid: 1455cac3-53b1-4e2c-87ed-92c70eb0cb4c) som ble sjekket inn mangler følgende tilhørende objekter i innsjekken:
--\n\t\tFlaten (id: 72413234 globalid: 1c85ec9f-c68a-4c54-94de-d5bed0d5556b)
--\n\tGrenselinjen (id:72415503 globalid: 8f64c6bc-be72-477d-a86d-a79f6ef61eae) som ble sjekket inn mangler følgende tilhørende objekter i innsjekken:
--\n\t\tFlaten (id: 72787044 globalid: ddd3606d-c28a-4d8d-90b5-c65cd292c66e)"}]
--,"title":"Commit feilet","type":"http://ngisopenapi.no/errors/commit_error"}

set client_min_messages to WARNING;
set timezone to utc;


\i :regdir/../../src/sql/topo_update_java/function_02_add_border_split_surface_java.sql
\i :regdir/../../src/sql/topo_update_java/function_02_update_feature_attribute_java.sql
\i :regdir/utils/check_overlap.sql

------------------------------------
-- Load ar5ngis stuff
------------------------------------

\i :regdir/../../test/data/topo_ar5ngis/topo_ar5ngis.sql
\i :regdir/../../test/data/topo_ar5ngis/topo_ar5ngis-functions.sql
\i :regdir/../../test/data/topo_ar5ngis/make_predictable.sql
\i :regdir/../../test/data/topo_ar5ngis/function_01_as_geojson_ngis_update.sql
\i :regdir/../../test/data/topo_ar5ngis/function_01_query_to_ngis_geojson_update.sql
\i :regdir/../../test/data/topo_ar5ngis/topo_ar5ngis_views.sql

-- Make :datadir available as a variable
SELECT :'regdir' || '/../../test/data/fkb46' as datadir \gset


CREATE TEMP TABLE json_input_01_utm33(payload text);
SELECT :'datadir' || '/ar5_DatasetFeaturesAsJsonString_OK_srid_25832_UTF-8_1624605946763_.json' as x \gset
COPY json_input_01_utm33 FROM :'x';
SELECT '--json-input-',length(payload) FROM json_input_01_utm33;


SELECT '--a1--', topo_ar5ngis.update_features(
	(SELECT payload FROM json_input_01_utm33)::jsonb,
	ST_GeomFromEWKT('SRID=4258;POLYGON ((11.342587135395066 59.548834637772515, 11.342587135395066 59.55334338620459, 11.354165119497168 59.55334338620459, 11.354165119497168 59.548834637772515, 11.342587135395066 59.548834637772515))')
);

SELECT '--a1-- face_attributes', count(*) from topo_ar5ngis.face_attributes;
SELECT '--a1-- edge_attributes', count(*) from topo_ar5ngis.edge_attributes;

SELECT '--a1-- edge_1_attributes', identifikasjon_lokal_id, datafangstdato, kvalitet from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = 'de49dda8-5224-4668-8f8d-393c382191af';
SELECT '--a1-- edge_2_attributes', identifikasjon_lokal_id, datafangstdato, kvalitet from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = 'bacbcfd9-0889-4a90-acea-1b2f682afd0f';
SELECT '--a1-- edge_3_attributes', identifikasjon_lokal_id, datafangstdato, kvalitet from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = 'c0e37630-2dc7-401e-a85a-39a1c4fdfc10';
SELECT '--a1-- edge_4_attributes', identifikasjon_lokal_id, datafangstdato, kvalitet from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = '97d64e7c-58f8-4f6f-91a5-841fc5f3964d';
SELECT '--a1-- face_attributes', identifikasjon_lokal_id, kvalitet from topo_ar5ngis.face_attributes where identifikasjon_lokal_id = '90d665fe-adc8-4227-bcb4-4ff6d05d2a5b';
SELECT '--a1-- face_attributes', identifikasjon_lokal_id, kvalitet from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = '8fc80fd1-ab24-41fd-96f7-6fb457ccaff8';



CREATE TEMPORARY TABLE add_border_results(
	test_name TEXT,
	fid TEXT,
	sid SERIAL,
	typ CHAR,
	act CHAR
);



INSERT INTO add_border_results(test_name, fid, typ, act)
SELECT '--t2-- run split', fid, typ, act
FROM topo_ar5ngis.add_border(

	-- A GeoJSON Feature object representing the
	-- line drawn by the user and the holding the
	-- attributes to use with the border and the
	-- surface layers
	$$
		{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:25832"
					}
				},
				"type": "LineString",
				"coordinates": [[632853.6767385576,6603778.789402386],[632881.3043024477,6603728.276319155],[632834.0900328148,6603711.369899461]]
			},
			"properties": {
			    "opphav": "test-opphav",
			    "datafangstdato": "2000-12-01"
			}
		}
	$$::TEXT,
	'server_side_opphav'::TEXT);


SELECT '--t2-- border used but not changed ---', * FROM  add_border_results WHERE typ ='B' AND act='U' ORDER BY sid ;
SELECT '--t2-- surfaces touching changed borders ---', * FROM  add_border_results WHERE typ ='S' AND act='T' ORDER BY sid ;
SELECT '--t2-- check the rest ---', * FROM  add_border_results WHERE act !='T' AND act !='U' ORDER BY sid ;

SELECT '--a2-- edge_1_attributes', identifikasjon_lokal_id, datafangstdato, kvalitet from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = 'de49dda8-5224-4668-8f8d-393c382191af';
SELECT '--a2-- edge_2_attributes', identifikasjon_lokal_id, datafangstdato, kvalitet from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = 'bacbcfd9-0889-4a90-acea-1b2f682afd0f';
SELECT '--a2-- edge_3_attributes', identifikasjon_lokal_id, datafangstdato, kvalitet from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = 'c0e37630-2dc7-401e-a85a-39a1c4fdfc10';
SELECT '--a2-- edge_4_attributes', identifikasjon_lokal_id, datafangstdato, kvalitet from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = '97d64e7c-58f8-4f6f-91a5-841fc5f3964d';
SELECT '--a2-- face_attributes', identifikasjon_lokal_id, kvalitet from topo_ar5ngis.face_attributes where identifikasjon_lokal_id = '90d665fe-adc8-4227-bcb4-4ff6d05d2a5b';
SELECT '--a2-- face_attributes', identifikasjon_lokal_id, kvalitet from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = '8fc80fd1-ab24-41fd-96f7-6fb457ccaff8';



SELECT '--a3-before-- edge_1_attributes', identifikasjon_lokal_id, datafangstdato, kvalitet from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = '98b68f51-8654-43dc-812a-769e114d5622';
SELECT '--a3-before-- edge_2_attributes', identifikasjon_lokal_id, datafangstdato, kvalitet from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = 'e62481cb-bbd6-488c-b089-561f0f58b6c2';
SELECT '--a3-before-- edge_3_attributes', identifikasjon_lokal_id, datafangstdato, kvalitet from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = '520180d3-1a1b-46e5-843f-54f80b21f495';
SELECT '--a3-before-- edge_4_attributes', identifikasjon_lokal_id, datafangstdato, kvalitet from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = 'ee8671d7-bf78-499d-a7e2-5b7feab198c1';
SELECT '--a3-before-- edge_4_attributes', identifikasjon_lokal_id, datafangstdato, kvalitet from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = 'b5bf281f-a0c8-4440-bac7-f305fd7816bc';
SELECT '--a3-before-- face_attributes', identifikasjon_lokal_id, kvalitet from topo_ar5ngis.face_attributes where identifikasjon_lokal_id = 'c40e2c74-3916-4465-ba97-23e6848f1675';



TRUNCATE TABLE add_border_results;



INSERT INTO add_border_results(test_name, fid, typ, act)
SELECT '--t3-- run split', fid, typ, act
FROM topo_ar5ngis.add_border(

	-- A GeoJSON Feature object representing the
	-- line drawn by the user and the holding the
	-- attributes to use with the border and the
	-- surface layers
	$$
		{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:25832"
					}
				},
				"type": "LineString",
				"coordinates": [[632909.6627145329,6603709.820310663],[632906.0025448567,6603692.251496218],[632926.3530882564,6603692.251496218],[632907.6130195142,6603708.209836006]]
			},
			"properties": {
			    "opphav": "test-opphav",
			    "datafangstdato": "2000-12-01"
			}
		}
	$$::TEXT,
	'server_side_opphav'::TEXT);


SELECT '--a3-- border used but not changed ---', * FROM  add_border_results WHERE typ ='B' AND act='U' ORDER BY sid ;
SELECT '--a3-- surfaces touching changed borders ---', * FROM  add_border_results WHERE typ ='S' AND act='T' ORDER BY sid ;
SELECT '--a3-- check the rest ---', * FROM  add_border_results WHERE act !='T' AND act !='U' ORDER BY sid ;

SELECT '--a3-- edge_1_attributes', identifikasjon_lokal_id, datafangstdato, kvalitet from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = '98b68f51-8654-43dc-812a-769e114d5622';
SELECT '--a3-- edge_2_attributes', identifikasjon_lokal_id, datafangstdato, kvalitet from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = 'e62481cb-bbd6-488c-b089-561f0f58b6c2';
SELECT '--a3-- edge_3_attributes', identifikasjon_lokal_id, datafangstdato, kvalitet from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = '520180d3-1a1b-46e5-843f-54f80b21f495';
SELECT '--a3-- edge_4_attributes', identifikasjon_lokal_id, datafangstdato, kvalitet from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = 'ee8671d7-bf78-499d-a7e2-5b7feab198c1';
SELECT '--a3-- edge_4_attributes', identifikasjon_lokal_id, datafangstdato, kvalitet from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = 'b5bf281f-a0c8-4440-bac7-f305fd7816bc';
SELECT '--a3-- face_attributes', identifikasjon_lokal_id, kvalitet from topo_ar5ngis.face_attributes where identifikasjon_lokal_id = 'c40e2c74-3916-4465-ba97-23e6848f1675';
SELECT '--a3-- face_attributes', identifikasjon_lokal_id, kvalitet from topo_ar5ngis.face_attributes where identifikasjon_lokal_id = '00000000-0000-0000-0000-000000000006';


ROLLBACK;

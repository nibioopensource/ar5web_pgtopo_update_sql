BEGIN;
-- To test case
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/114#note_558524232

--{"detail":"Commit feilet","errors":[{"reason":"Alle objekter som deler geometrien er ikke med i innsjekken:
--\n\t\Grenselinjen (id: 71931538 globalid: b7fc8dd3-7fa0-4963-b34e-c6b822df2523) som ble sjekket inn mangler følgende tilhørende objekter i innsjekken:
--\n\t\tFlaten (id: 72785647 globalid: 8aa34018-509a-4c9d-9a76-b45b7b08157f)
--\n\tGrenselinjen (id: 72413078 globalid: 1455cac3-53b1-4e2c-87ed-92c70eb0cb4c) som ble sjekket inn mangler følgende tilhørende objekter i innsjekken:
--\n\t\tFlaten (id: 72413234 globalid: 1c85ec9f-c68a-4c54-94de-d5bed0d5556b)
--\n\tGrenselinjen (id:72415503 globalid: 8f64c6bc-be72-477d-a86d-a79f6ef61eae) som ble sjekket inn mangler følgende tilhørende objekter i innsjekken:
--\n\t\tFlaten (id: 72787044 globalid: ddd3606d-c28a-4d8d-90b5-c65cd292c66e)"}]
--,"title":"Commit feilet","type":"http://ngisopenapi.no/errors/commit_error"}

set client_min_messages to WARNING;
set timezone to utc;


\i :regdir/../../src/sql/topo_update_java/function_02_add_border_split_surface_java.sql
\i :regdir/../../src/sql/topo_update_java/function_02_update_feature_attribute_java.sql
\i :regdir/utils/check_overlap.sql

-- Make :datadir available as a variable
SELECT :'regdir' || '/../../test/data/fkb46' as datadir \gset


CREATE TEMP TABLE json_input_01(payload text);
SELECT :'datadir' || '/log_getData_20210804-090850_NIBIO_Lars__NGIS_Json_srid_25832_UTF-8.json' as x \gset
COPY json_input_01 FROM :'x';
SELECT '--json-input-',length(payload) FROM json_input_01;


SELECT '--a1-- length', length(payload) FROM json_input_01;

SELECT '--a1--', topo_ar5ngis.update_features(
	(SELECT payload FROM json_input_01)::jsonb,
	ST_GeomFromEWKT('SRID=4258;POLYGON ((6.39009422008037 60.6242807886776, 6.389916532814004 60.62647846523102, 6.400296273859526 60.62668064329592, 6.400473256446251 60.624482948690336, 6.39009422008037 60.6242807886776))')
);

SELECT '--a1-- face_attributes', count(*) from topo_ar5ngis.face_attributes;
SELECT '--a1-- edge_attributes', count(*) from topo_ar5ngis.edge_attributes;

SELECT '--a1-- edge_1_attributes', identifikasjon_lokal_id, datafangstdato, kvalitet from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = '17074b68-15d4-4560-9eba-5b9c2efa9fd7';
SELECT '--a1-- edge_2_attributes', identifikasjon_lokal_id, datafangstdato, kvalitet from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = '95b444de-bca0-42ed-9dc8-d528b4a12fbc';
SELECT '--a1-- edge_3_attributes', identifikasjon_lokal_id, datafangstdato, kvalitet from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = '978296fb-d95b-4d1b-8663-d0ff1df41254';



CREATE TEMPORARY TABLE add_border_results(
	test_name TEXT,
	fid TEXT,
	sid SERIAL,
	typ CHAR,
	act CHAR
);


-- Draw a closed line where small areas will be rmoved
INSERT INTO add_border_results(test_name, fid, typ, act)
SELECT '--t2-- run split', fid, typ, act
FROM topo_ar5ngis.add_border(

	-- A GeoJSON Feature object representing the
	-- line drawn by the user and the holding the
	-- attributes to use with the border and the
	-- surface layers
	$$
		{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:25832"
					}
				},
				"type": "LineString",
				"coordinates": [[357363.37290397467,6723850.478726661],[357374.81457169255,6723829.003596483],[357382.9117519237,6723830.4118017405],[357387.66444466804,6723837.452828028],[357395.05752227036,6723840.445264201],[357378.51111049374,6723859.103983864],[357361.96469871706,6723848.542444432]]
			},
			"properties": {
			    "opphav": "test-opphav",
			    "datafangstdato": "2000-12-01"
			}
		}
	$$::TEXT,
	'server_side_opphav'::TEXT);


SELECT '--t2-- border used but not changed ---', * FROM  add_border_results WHERE typ ='B' AND act='U' ORDER BY sid ;
SELECT '--t2-- surfaces touching changed borders ---', * FROM  add_border_results WHERE typ ='S' AND act='T' ORDER BY sid ;
SELECT '--t2-- check the rest ---', * FROM  add_border_results WHERE act !='T' AND act !='U' ORDER BY sid ;

-- update result dates to make tests stable
UPDATE topo_ar5ngis.face_attributes f
SET identifikasjon_versjon_id = '1970-01-01T00:00:00'
WHERE f.identifikasjon_versjon_id > (now() - '60s'::interval);

UPDATE topo_ar5ngis.edge_attributes f
SET identifikasjon_versjon_id = '1970-01-01T00:00:00'
WHERE f.identifikasjon_versjon_id > (now() - '60s'::interval);

UPDATE topo_ar5ngis.face_attributes f
SET oppdateringsdato='1970-01-01T00:00:00'
WHERE f.oppdateringsdato > (now() - '60s'::interval);

UPDATE topo_ar5ngis.edge_attributes f
SET oppdateringsdato='1970-01-01T00:00:00'
WHERE f.oppdateringsdato > (now() - '60s'::interval);

-----------------------------------------
-- t3: call with feature rewrite function
-----------------------------------------

-- if you need the result as file use  \o /tmp/surfaces_as_geojson.json

SELECT '--- t3 ---', E'\n' || jsonb_pretty(
	topo_ar5ngis.surfaces_as_geojson(
		'add_border_results',
		25832, -- utm 32
		5 -- num decimals
	)::JSONB
);

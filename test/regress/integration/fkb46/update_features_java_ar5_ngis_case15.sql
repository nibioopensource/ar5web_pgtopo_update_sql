BEGIN;

set client_min_messages to WARNING;
set timezone to utc;

-- the bug desciption is here https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/114#note_559441395


\i :regdir/../../src/sql/topo_update_java/function_02_add_border_split_surface_java.sql
\i :regdir/../../src/sql/topo_update_java/function_02_update_feature_attribute_java.sql
\i :regdir/utils/check_overlap.sql

-- Make :datadir available as a variable
SELECT :'regdir' || '/../../test/data/fkb46' as datadir \gset


CREATE TEMP TABLE json_input_01(payload text);
SELECT :'datadir' || '/log_getData_20211018-135848_jps__NGIS_Json_srid_25832_UTF-8_before.json' as x \gset
COPY json_input_01 FROM :'x';
SELECT '--json-input-',length(payload) FROM json_input_01;


CREATE TEMP TABLE json_input_02(payload text);
SELECT :'datadir' || '/log_getData_20211018-135848_jps__NGIS_Json_srid_25832_UTF-8.json' as x \gset
COPY json_input_02 FROM :'x';
SELECT '--json-input-',length(payload) FROM json_input_02;


-- TOPOLOGY NAME: update_features_test_topo
-- AREAL LAYER:   topo_ar5ngis.face_attributes
-- LINEAL LAYER:  topo_ar5ngis.edge_attributes

SELECT '--a1--', topo_ar5ngis.update_features(
	(SELECT payload FROM json_input_01)::JSONB,
	ST_GeomFromEWKT('SRID=4258;POLYGON ((10.679964864995052 62.87918667486273, 10.679964864995052 62.92808377830601, 10.76897434879247 62.92808377830601, 10.76897434879247 62.87918667486273, 10.679964864995052 62.87918667486273))')
);

SELECT 'p1 before', ST_NPoints(grense::geometry) FROM topo_ar5ngis.edge_attributes e
WHERE e.identifikasjon_lokal_id = 'f3152da4-1f8b-11ec-916e-f7f65531cc3c';

SELECT '--a2--', topo_ar5ngis.update_features(
	(SELECT payload FROM json_input_02)::JSONB,
	ST_GeomFromEWKT('SRID=4258;POLYGON ((10.679964864995052 62.87918667486273, 10.679964864995052 62.92808377830601, 10.76897434879247 62.92808377830601, 10.76897434879247 62.87918667486273, 10.679964864995052 62.87918667486273))'),
	NULL
);

SELECT  'p1 after', ST_NPoints(grense::geometry) FROM topo_ar5ngis.edge_attributes e
WHERE e.identifikasjon_lokal_id = 'f3152da4-1f8b-11ec-916e-f7f65531cc3c';

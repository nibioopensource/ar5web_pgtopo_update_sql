BEGIN;

set client_min_messages to WARNING;
set timezone to utc;

-- the bug desciption is here
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/177


\i :regdir/../../src/sql/topo_update_java/function_02_add_border_split_surface_java.sql
\i :regdir/../../src/sql/topo_update_java/function_02_update_feature_attribute_java.sql
\i :regdir/utils/check_overlap.sql

-- Make :datadir available as a variable
SELECT :'regdir' || '/../../test/data/fkb46' as datadir \gset


CREATE TEMP TABLE json_input_01(payload text);
SELECT :'datadir' || '/log_getData_20211022-074808__Sandro__NGIS_Json_srid_25832_UTF-8.json' as x \gset
COPY json_input_01 FROM :'x';
SELECT '--json-input-',length(payload) FROM json_input_01;

-- TOPOLOGY NAME: update_features_test_topo
-- AREAL LAYER:   topo_ar5ngis.face_attributes
-- LINEAL LAYER:  topo_ar5ngis.edge_attributes


-- Create mapping schema and table and add mappings

SELECT '--a1--', topo_ar5ngis.update_features(
	(SELECT payload FROM json_input_01)::jsonb,
	ST_GeomFromEWKT('SRID=4258;POLYGON ((11.777600812519369 60.706562330759745, 11.777659876735795 60.70724641047967, 11.779774882027212 60.707202536514174, 11.779715772978445 60.7065184580158, 11.777600812519369 60.706562330759745))')
);

select '--t1-- num ok geometry_properties_position', count(*) from topo_ar5ngis.face_attributes where ST_Intersects(geometry_properties_position,ST_GeomFromEWKT('SRID=4258;POLYGON ((28.567214255051546 70.12077381994374, 28.567385375081894 70.12303682108494, 28.577537594924564 70.12294764027095, 28.577365367637622 70.12068465011427, 28.567214255051546 70.12077381994374))'));

SELECT '--t1-- face_attributes before split',
    count(*) AS num,
    Sum(Round(ST_Area(omrade::geometry::geography)::numeric, 1)) AS area
FROM topo_ar5ngis.face_attributes;


SELECT '--t1-- test overlaps', count(*) as num_over_lap from topo_update.find_interiors_intersect('topo_ar5ngis.edge_attributes', 'grense', 'identifikasjon_lokal_id');


CREATE TEMPORARY TABLE add_border_results(
	test_name TEXT,
	fid TEXT,
	sid SERIAL,
	typ CHAR,
	act CHAR
);




INSERT INTO add_border_results(test_name, fid, typ, act)
SELECT '--t1-- run split', fid, typ, act
FROM topo_ar5ngis.add_border(

	-- A GeoJSON Feature object representing the
	-- line drawn by the user and the holding the
	-- attributes to use with the border and the
	-- surface layers
	$$
		{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:25832"
					}
				},
				"type": "MultiLineString",
				"coordinates": [[[651634.7054357309,6733344.747972122],[651628.4449005645,6733340.253228926]],[[651636.8725440577,6733340.895335098],[651630.9330619767,6733337.042698072]]]
			},
			"properties": {
				"edge_attributes_props": {
					"opphav": "opphav-edge-test-missing-feature", "datafangstdato": "2000-12-01" , "verifiseringsdato": "2001-02-01"
				},
				"face_attributes_props": {
					"opphav": "opphav-face-test-missing-feature"
				}
			}
		}
	$$::TEXT,
	'server_side_opphav'::TEXT);

-----------------------------------------
-- t2: check result after split
-----------------------------------------

SELECT '--t2-- face_attributes after split',
    count(*) AS num,
    Sum(Round(ST_Area(omrade::geometry::geography)::numeric, 1)) AS area
	FROM topo_ar5ngis.face_attributes;

SELECT '--t2-- border used but not changed ---', * FROM  add_border_results WHERE typ ='B' AND act='U' ORDER BY sid ;
SELECT '--t2-- surfaces touching changed borders ---', * FROM  add_border_results WHERE typ ='S' AND act='T' ORDER BY sid ;
SELECT '--t2-- check the rest ---', * FROM  add_border_results WHERE act !='T' AND act !='U' ORDER BY sid ;

-- update result dates to make tests stable
UPDATE topo_ar5ngis.face_attributes f
SET identifikasjon_versjon_id = '1970-01-01T00:00:00'
WHERE f.identifikasjon_versjon_id > (now() - '60s'::interval);

UPDATE topo_ar5ngis.edge_attributes f
SET identifikasjon_versjon_id = '1970-01-01T00:00:00'
WHERE f.identifikasjon_versjon_id > (now() - '60s'::interval);

UPDATE topo_ar5ngis.face_attributes f
SET oppdateringsdato='1970-01-01T00:00:00'
WHERE f.oppdateringsdato > (now() - '60s'::interval);

UPDATE topo_ar5ngis.edge_attributes f
SET oppdateringsdato='1970-01-01T00:00:00'
WHERE f.oppdateringsdato > (now() - '60s'::interval);

SELECT '--t2-- edge_attributes e719acd6-ae73-4aed-93eb-2a71a5cdd2b3 after split ', oppdateringsdato, datafangstdato, verifiseringsdato from topo_ar5ngis.edge_attributes where identifikasjon_lokal_id = 'e719acd6-ae73-4aed-93eb-2a71a5cdd2b3'::uuid;

-----------------------------------------
-- t3: call with feature rewrite function
-----------------------------------------

-- if you need the result as file use
-- \o /tmp/surfaces_as_geojson.json

SELECT '--- t3 ---', E'\n' || jsonb_pretty(
	topo_ar5ngis.surfaces_as_geojson(
		'add_border_results',
		25832, -- utm 33
		5
	)::JSONB
);

-- the bug desciption is here
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/177#note_712562635

SELECT 't3 add invalid mulitiline', null
FROM topo_ar5ngis.add_border(

	-- A GeoJSON Feature object representing the
	-- line drawn by the user and the holding the
	-- attributes to use with the border and the
	-- surface layers
	$$
		{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:25832"
					}
				},
				"type": "MultiLineString",
				"coordinates": [[[651485.4157509928,6733314.087402455],[651535.5000323241,6733321.792676506]],[[651550.910580426,6733296.75053584],[651506.605254633,6733287.761049448],[651558.6158544769,6733265.929439637],[651550.910580426,6733296.75053584]]]
			},
			"properties": {
				"edge_attributes_props": {
					"opphav": "opphav-edge-test-missing-feature", "datafangstdato": "2000-12-01" , "verifiseringsdato": "2001-02-01"
				},
				"face_attributes_props": {
					"opphav": "opphav-face-test-missing-feature"
				}
			}
		}
	$$::TEXT,
	'server_side_opphav'::TEXT);

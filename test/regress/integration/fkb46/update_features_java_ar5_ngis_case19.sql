BEGIN;

set client_min_messages to WARNING;
set timezone to utc;

-- the bug desciption is here
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/178 a case like this 'Illegal string value for: versjonId'

-- This is test is not valid any more because do not want suser to splitt many surfaces in one operation
-- In this test we now get 7 new faces and for this operation it's only allowed 2 new faces

\i :regdir/../../src/sql/topo_update_java/function_02_add_border_split_surface_java.sql
\i :regdir/../../src/sql/topo_update_java/function_02_update_feature_attribute_java.sql
\i :regdir/utils/check_overlap.sql

-- Make :datadir available as a variable
SELECT :'regdir' || '/../../test/data/fkb46' as datadir \gset


CREATE TEMP TABLE json_input_01(payload text);
SELECT :'datadir' || '/log_getData_20211026-145154_Jorn__NGIS_Json_srid_25832_UTF-8.json' as x \gset
COPY json_input_01 FROM :'x';
SELECT '--json-input-',length(payload) FROM json_input_01;

-- TOPOLOGY NAME: update_features_test_topo
-- AREAL LAYER:   topo_ar5ngis.face_attributes
-- LINEAL LAYER:  topo_ar5ngis.edge_attributes


-- Create mapping schema and table and add mappings

SELECT '--a1--', topo_ar5ngis.update_features(
	(SELECT payload FROM json_input_01)::jsonb,
	ST_GeomFromEWKT('SRID=4258;POLYGON ((9.664417119625183 63.131488837552915, 9.664493713080999 63.13483920684977, 9.67671890038206 63.13478144340038, 9.676640898038908 63.13143108244481, 9.664417119625183 63.131488837552915))')
);

SELECT '--t1-- before null/empty version_id face', identifikasjon_lokal_id, identifikasjon_versjon_id  from topo_ar5ngis.face_attributes where coalesce( trim(identifikasjon_versjon_id::text),'')='' OR identifikasjon_versjon_id is null ORDER BY identifikasjon_lokal_id;
SELECT '--t1-- before null/empty version_id edge', identifikasjon_lokal_id, identifikasjon_versjon_id  from topo_ar5ngis.edge_attributes where coalesce( trim(identifikasjon_versjon_id::text),'')='' OR identifikasjon_versjon_id is null ORDER BY identifikasjon_lokal_id;

SELECT '--t1-- test overlaps', count(*) as num_over_lap from topo_update.find_interiors_intersect('topo_ar5ngis.edge_attributes', 'grense', 'identifikasjon_lokal_id');

SELECT '--t2-- face_attributes before split',
count(*) AS num,
Sum(Round(ST_Area(omrade::geometry::geography)::numeric, 1)) AS area
FROM topo_ar5ngis.face_attributes;


CREATE TEMPORARY TABLE add_border_results(
	test_name TEXT,
	fid TEXT,
	sid SERIAL,
	typ CHAR,
	act CHAR
);

DO $$
DECLARE
	v_state text;
	v_msg text;
	v_detail text;
	v_hint text;
	v_context text;
BEGIN
	BEGIN


	INSERT INTO add_border_results(test_name, fid, typ, act)
	SELECT '--t1-- run split', fid, typ, act
	FROM topo_ar5ngis.add_border(

	-- A GeoJSON Feature object representing the
	-- line drawn by the user and the holding the
	-- attributes to use with the border and the
	-- surface layers
	CAST ('{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:25832"
					}
				},
				"type": "LineString",
				"coordinates": [[533850.2810274857,7000678.910116729],[533812.0757103164,7000709.089106762],[533776.7598709163,7000714.868062301],[533743.6914031142,7000691.752240147],[533781.8967202835,7000666.710099482],[533808.8651794619,7000659.967984688],[533822.991515222,7000653.867976064],[533850.2810274857,7000678.910116729]]
			},
			"properties": {
				"edge_attributes_props": {
					"opphav": "opphav-edge-test-missing-feature", "datafangstdato": "2000-12-01" , "verifiseringsdato": "2001-02-01"
				},
				"face_attributes_props": {
					"opphav": "opphav-face-test-missing-feature"
				}
			}
		}' AS TEXT),
	'server_side_opphav'::TEXT);

	EXCEPTION WHEN OTHERS
	THEN
    	GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT, v_context = PG_EXCEPTION_CONTEXT;
		RAISE WARNING 'Got exception %', substring(v_msg,POSITION('P0001 message' in v_msg),70);
	END;
END;
$$ LANGUAGE 'plpgsql';


-----------------------------------------
-- t2: check result after split
-----------------------------------------

SELECT '--t2-- face_attributes after split',
    count(*) AS num,
    Sum(Round(ST_Area(omrade::geometry::geography)::numeric, 1)) AS area
	FROM topo_ar5ngis.face_attributes;

SELECT '--t2-- border used but not changed ---', * FROM  add_border_results WHERE typ ='B' AND act='U' ORDER BY sid ;
SELECT '--t2-- surfaces touching changed borders ---', * FROM  add_border_results WHERE typ ='S' AND act='T' ORDER BY sid ;
SELECT '--t2-- check the rest ---', * FROM  add_border_results WHERE act !='T' AND act !='U' ORDER BY sid ;

-- update result dates to make tests stable
UPDATE topo_ar5ngis.face_attributes f
SET identifikasjon_versjon_id = '1970-01-01T00:00:00'
WHERE f.identifikasjon_versjon_id > (now() - '60s'::interval);

UPDATE topo_ar5ngis.edge_attributes f
SET identifikasjon_versjon_id = '1970-01-01T00:00:00'
WHERE f.identifikasjon_versjon_id > (now() - '60s'::interval);

UPDATE topo_ar5ngis.face_attributes f
SET oppdateringsdato='1970-01-01T00:00:00'
WHERE f.oppdateringsdato > (now() - '60s'::interval);

UPDATE topo_ar5ngis.edge_attributes f
SET oppdateringsdato='1970-01-01T00:00:00'
WHERE f.oppdateringsdato > (now() - '60s'::interval);

SELECT '--t1-- after null/empty version_id face', identifikasjon_lokal_id, identifikasjon_versjon_id  from topo_ar5ngis.face_attributes where coalesce( trim(identifikasjon_versjon_id::text),'')='' OR identifikasjon_versjon_id is null ORDER BY identifikasjon_lokal_id;
SELECT '--t1-- after null/empty version_id edge', identifikasjon_lokal_id, identifikasjon_versjon_id  from topo_ar5ngis.edge_attributes where coalesce( trim(identifikasjon_versjon_id::text),'')='' OR identifikasjon_versjon_id is null ORDER BY identifikasjon_lokal_id;


-----------------------------------------
-- t3: call with feature rewrite function
-----------------------------------------

-- if you need the result as file use
-- \o /tmp/surfaces_as_geojson.json

SELECT '--- t3 ---', E'\n' || jsonb_pretty(
	topo_ar5ngis.surfaces_as_geojson(
		'add_border_results',
		25832, -- utm 33
		5
	)::JSONB
);

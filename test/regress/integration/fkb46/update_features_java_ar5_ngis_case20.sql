BEGIN;

set client_min_messages to WARNING;
set timezone to utc;

-- the bug desciption is here
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/183

\i :regdir/../../src/sql/topo_update_java/function_02_add_border_split_surface_java.sql
\i :regdir/../../src/sql/topo_update_java/function_02_update_feature_attribute_java.sql
\i :regdir/utils/check_overlap.sql

SELECT :'regdir' || '/../../test/data/fkb46' as datadir \gset

CREATE TEMP TABLE json_input_01(payload text);
SELECT :'datadir' || '/log_getData_20211029-142643__NIBIO_Sandro__NGIS_Json_srid_25832_UTF-8.json' as x \gset
COPY json_input_01 FROM :'x';
SELECT '--json-input-',length(payload) FROM json_input_01;

-- TOPOLOGY NAME: update_features_test_topo
-- AREAL LAYER:   topo_ar5ngis.face_attributes
-- LINEAL LAYER:  topo_ar5ngis.edge_attributes


-- Create mapping schema and table and add mappings

SELECT '--a1--', topo_ar5ngis.update_features(
	(SELECT payload FROM json_input_01)::jsonb,
	ST_GeomFromEWKT('SRID=4258;POLYGON ((11.31756449098878 59.55355548291095, 11.31794092304785 59.55903103435375, 11.334287253101698 59.55874049057508, 11.333908172265046 59.553265002361044, 11.31756449098878 59.55355548291095))')
);

SELECT '--t1-- before null/empty version_id face', identifikasjon_lokal_id, identifikasjon_versjon_id  from topo_ar5ngis.face_attributes where coalesce( trim(identifikasjon_versjon_id::text),'')='' OR identifikasjon_versjon_id is null;
SELECT '--t1-- before null/empty version_id edge', identifikasjon_lokal_id, identifikasjon_versjon_id  from topo_ar5ngis.edge_attributes where coalesce( trim(identifikasjon_versjon_id::text),'')='' OR identifikasjon_versjon_id is null;

SELECT '--t1-- test overlaps', count(*) as num_over_lap from topo_update.find_interiors_intersect('topo_ar5ngis.edge_attributes', 'grense', 'identifikasjon_lokal_id');

-- FROM issues https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/182
SELECT '--t1-- find number of nodes before ',
	ST_GeometryType(nodes_3_more_surface_split),
	ST_NumGeometries(nodes_3_more_surface_split),
	Round(ST_Area(ST_Envelope(nodes_3_more_surface_split))::numeric, 1)
FROM topo_ar5ngis.nodes_3_more_surface_split(ST_GeomFromEWKT('SRID=4258;POLYGON ((9.651480318061315 63.12898644736155, 9.6516030825561 63.134463227332965, 9.669940654185536 63.13437785304223, 9.66981443544365 63.128901093222105, 9.651480318061315 63.12898644736155))'),25832);


CREATE TEMPORARY TABLE add_border_results(
	test_name TEXT,
	fid TEXT,
	sid SERIAL,
	typ CHAR,
	act CHAR
);


DO $$
DECLARE
	v_state text;
	v_msg text;
	v_detail text;
	v_hint text;
	v_context text;
BEGIN
	BEGIN


INSERT INTO add_border_results(test_name, fid, typ, act)
SELECT '--t1-- run split', fid, typ, act
FROM topo_ar5ngis.add_border(

	-- A GeoJSON Feature object representing the
	-- line drawn by the user and the holding the
	-- attributes to use with the border and the
	-- surface layers
	CAST ('{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:25832"
					}
				},
				"type": "LineString",
				"coordinates": [[631510.7956210651,6604090.330018704],[631552.3984930714,6604135.084623438],[631593.3710185323,6604104.827989252],[631548.6164137982,6604061.334077609]]
			},
			"properties": {
				"edge_attributes_props": {
					"opphav": "opphav-edge-test-missing-feature", "datafangstdato": "2000-12-01" , "verifiseringsdato": "2001-02-01"
				},
				"face_attributes_props": {
					"opphav": "opphav-face-test-missing-feature"
				}
			}
		}' AS TEXT),
	'server_side_opphav'::TEXT);

	EXCEPTION WHEN OTHERS
	THEN
    	GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT, v_context = PG_EXCEPTION_CONTEXT;
		RAISE WARNING 'Got exception %', substring(v_msg,POSITION('P0001 message' in v_msg),140);
	END;
END;
$$ LANGUAGE 'plpgsql';




-----------------------------------------
-- t2: check result after split
-----------------------------------------

SELECT '--t2-- face_attributes after split',
    count(*) AS num,
    Sum(Round(ST_Area(omrade::geometry::geography)::numeric, 1)) AS area
	FROM topo_ar5ngis.face_attributes;

SELECT '--t2-- border used but not changed ---', * FROM  add_border_results WHERE typ ='B' AND act='U' ORDER BY sid ;
SELECT '--t2-- surfaces touching changed borders ---', * FROM  add_border_results WHERE typ ='S' AND act='T' ORDER BY sid ;
SELECT '--t2-- check the rest ---', * FROM  add_border_results WHERE act !='T' AND act !='U' ORDER BY sid ;

-- update result dates to make tests stable
UPDATE topo_ar5ngis.face_attributes f
SET identifikasjon_versjon_id = '1970-01-01T00:00:00'
WHERE f.identifikasjon_versjon_id > (now() - '60s'::interval);

UPDATE topo_ar5ngis.edge_attributes f
SET identifikasjon_versjon_id = '1970-01-01T00:00:00'
WHERE f.identifikasjon_versjon_id > (now() - '60s'::interval);

UPDATE topo_ar5ngis.face_attributes f
SET oppdateringsdato='1970-01-01T00:00:00'
WHERE f.oppdateringsdato > (now() - '60s'::interval);

UPDATE topo_ar5ngis.edge_attributes f
SET oppdateringsdato='1970-01-01T00:00:00'
WHERE f.oppdateringsdato > (now() - '60s'::interval);

SELECT '--t1-- after null/empty version_id face', identifikasjon_lokal_id, identifikasjon_versjon_id  from topo_ar5ngis.face_attributes where coalesce( trim(identifikasjon_versjon_id::text),'')='' OR identifikasjon_versjon_id is null;
SELECT '--t1-- after null/empty version_id edge', identifikasjon_lokal_id, identifikasjon_versjon_id  from topo_ar5ngis.edge_attributes where coalesce( trim(identifikasjon_versjon_id::text),'')='' OR identifikasjon_versjon_id is null;

-- FROM issues https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/182
SELECT '--t1-- find number of nodes after ',
	ST_GeometryType(nodes_3_more_surface_split),
	ST_NumGeometries(nodes_3_more_surface_split),
	Round(ST_Area(ST_Envelope(nodes_3_more_surface_split))::numeric, 1)
FROM topo_ar5ngis.nodes_3_more_surface_split(ST_GeomFromEWKT('SRID=4258;POLYGON ((9.651480318061315 63.12898644736155, 9.6516030825561 63.134463227332965, 9.669940654185536 63.13437785304223, 9.66981443544365 63.128901093222105, 9.651480318061315 63.12898644736155))'),25832);


-----------------------------------------
-- t3: call with feature rewrite function
-----------------------------------------

-- if you need the result as file use
-- \o /tmp/surfaces_as_geojson.json

SELECT '--- t3 ---', E'\n' || jsonb_pretty(
	topo_ar5ngis.surfaces_as_geojson(
		'add_border_results',
		25832, -- utm 33
		5
	)::JSONB
);

BEGIN;

set client_min_messages to WARNING;
set timezone to utc;

-- the bug desciption is here
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/184

\i :regdir/../../src/sql/topo_update_java/function_02_add_border_split_surface_java.sql
\i :regdir/../../src/sql/topo_update_java/function_02_update_feature_attribute_java.sql
\i :regdir/utils/check_overlap.sql

-- Make :datadir available as a variable
SELECT :'regdir' || '/../../test/data/fkb46' as datadir \gset


CREATE TEMP TABLE json_input_01(payload text);
SELECT :'datadir' || '/log_getData_20211029-142643_NIBIO_Sandro__NGIS_Json_srid_25832_UTF-8.json' as x \gset
COPY json_input_01 FROM :'x';
SELECT '--json-input-',length(payload) FROM json_input_01;

-- TOPOLOGY NAME: update_features_test_topo
-- AREAL LAYER:   topo_ar5ngis.face_attributes
-- LINEAL LAYER:  topo_ar5ngis.edge_attributes


-- Create mapping schema and table and add mappings

SELECT '--a1--', topo_ar5ngis.update_features(
	(SELECT payload FROM json_input_01)::jsonb,
	ST_GeomFromEWKT('SRID=4258;POLYGON ((11.31756449098878 59.55355548291095, 11.31794092304785 59.55903103435375, 11.334287253101698 59.55874049057508, 11.333908172265046 59.553265002361044, 11.31756449098878 59.55355548291095))')
);

SELECT '--t1-- before null/empty version_id face', identifikasjon_lokal_id, identifikasjon_versjon_id  from topo_ar5ngis.face_attributes where coalesce( trim(identifikasjon_versjon_id::text),'')='' OR identifikasjon_versjon_id is null;
SELECT '--t1-- before null/empty version_id edge', identifikasjon_lokal_id, identifikasjon_versjon_id  from topo_ar5ngis.edge_attributes where coalesce( trim(identifikasjon_versjon_id::text),'')='' OR identifikasjon_versjon_id is null;

SELECT '--t1-- test overlaps', count(*) as num_over_lap from topo_update.find_interiors_intersect('topo_ar5ngis.edge_attributes', 'grense', 'identifikasjon_lokal_id');

-- FROM issues https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/182
SELECT '--t1-- find number of nodes before ',
	ST_GeometryType(nodes_3_more_surface_split),
	ST_NumGeometries(nodes_3_more_surface_split),
	Round(ST_Area(ST_Envelope(nodes_3_more_surface_split))::numeric, 1)
FROM topo_ar5ngis.nodes_3_more_surface_split(ST_GeomFromEWKT('SRID=4258;POLYGON ((28.854525140044043 70.14380127434582, 28.854881166261713 70.1477744062521, 28.86913287443928 70.1476263008112, 28.868774116612954 70.14365320096361, 28.854525140044043 70.14380127434582))'),25835);


CREATE TEMPORARY TABLE add_border_results(
	test_name TEXT,
	fid TEXT,
	sid SERIAL,
	typ CHAR,
	act CHAR
);


-- Do spilt where fist and last point are nodes in the Postgis Topology layer

INSERT INTO add_border_results(test_name, fid, typ, act)
SELECT '--t1-- run split', fid, typ, act
FROM topo_ar5ngis.add_border(

	-- A GeoJSON Feature object representing the
	-- line drawn by the user and the holding the
	-- attributes to use with the border and the
	-- surface layers
	$$
		{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:25835"
					}
				},
				"type": "LineString",
				"coordinates": [[570660.8699999999,7783169.9399999995],[570692.8669933317,7783201.049750816],[570647.308,7783202.794999998]]
			},
			"properties": {
				"edge_attributes_props": {
					"opphav": "opphav-edge-test-missing-feature", "datafangstdato": "2000-12-01" , "verifiseringsdato": "2001-02-01"
				},
				"face_attributes_props": {
					"opphav": "opphav-face-test-missing-feature"
				}
			}
		}
	$$::TEXT,
	'server_side_opphav'::TEXT);

BEGIN;

set client_min_messages to WARNING;
set timezone to utc;

-- the bug desciption is here
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/184

\i :regdir/../../src/sql/topo_update_java/function_02_add_border_split_surface_java.sql
\i :regdir/../../src/sql/topo_update_java/function_02_update_feature_attribute_java.sql
\i :regdir/utils/check_overlap.sql

-- Make :datadir available as a variable
SELECT :'regdir' || '/../../test/data/fkb46' as datadir \gset


CREATE TEMP TABLE json_input_01(payload text);
SELECT :'datadir' || '/log_getData_20211102-072540_NIBIO_Sandro__NGIS_Json_srid_25832_UTF-8.json' as x \gset
COPY json_input_01 FROM :'x';
SELECT '--json-input-',length(payload) FROM json_input_01;

-- TOPOLOGY NAME: update_features_test_topo
-- AREAL LAYER:   topo_ar5ngis.face_attributes
-- LINEAL LAYER:  topo_ar5ngis.edge_attributes


-- Create mapping schema and table and add mappings

SELECT '--a1--', topo_ar5ngis.update_features(
	(SELECT payload FROM json_input_01)::jsonb,
	ST_GeomFromEWKT('SRID=4258;POLYGON ((11.316258811892428 59.5527450776301, 11.316772333400193 59.56021865857926, 11.335616669498577 59.55988371065107, 11.335098980289224 59.55241022918863, 11.316258811892428 59.5527450776301))')
);

SELECT '--t1-- before null/empty version_id face', identifikasjon_lokal_id, identifikasjon_versjon_id  from topo_ar5ngis.face_attributes where coalesce( trim(identifikasjon_versjon_id::text),'')='' OR identifikasjon_versjon_id is null;
SELECT '--t1-- before null/empty version_id edge', identifikasjon_lokal_id, identifikasjon_versjon_id  from topo_ar5ngis.edge_attributes where coalesce( trim(identifikasjon_versjon_id::text),'')='' OR identifikasjon_versjon_id is null;

SELECT '--t1-- test overlaps', count(*) as num_over_lap from topo_update.find_interiors_intersect('topo_ar5ngis.edge_attributes', 'grense', 'identifikasjon_lokal_id');

-- FROM issues https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/182
SELECT '--t1-- find number of nodes before ',
	ST_GeometryType(nodes_3_more_surface_split),
	ST_NumGeometries(nodes_3_more_surface_split),
	Round(ST_Area(ST_Envelope(nodes_3_more_surface_split))::numeric, 1)
FROM topo_ar5ngis.nodes_3_more_surface_split(ST_GeomFromEWKT('SRID=4258;POLYGON ((11.316258811892428 59.5527450776301, 11.316772333400193 59.56021865857926, 11.335616669498577 59.55988371065107, 11.335098980289224 59.55241022918863, 11.316258811892428 59.5527450776301))'),25832);


CREATE TEMPORARY TABLE add_border_results(
	test_name TEXT,
	fid TEXT,
	sid SERIAL,
	typ CHAR,
	act CHAR
);


-- Do spilt where fist and last point are nodes in the Postgis Topology layer
DO $$
DECLARE
	v_state text;
	v_msg text;
	v_detail text;
	v_hint text;
	v_context text;
BEGIN
	BEGIN


INSERT INTO add_border_results(test_name, fid, typ, act)
SELECT '--t1-- run split', fid, typ, act
FROM topo_ar5ngis.add_border(

	-- A GeoJSON Feature object representing the
	-- line drawn by the user and the holding the
	-- attributes to use with the border and the
	-- surface layers
	CAST ('{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:25832"
					}
				},
				"type": "LineString",
				"coordinates": [[631036.9026321144,6604175.941680136],[631121.0185405038,6604180.436423332],[631149.271212024,6604201.625926973]]
			},
			"properties": {
				"edge_attributes_props": {
					"opphav": "opphav-edge-test-missing-feature", "datafangstdato": "2000-12-01" , "verifiseringsdato": "2001-02-01"
				},
				"face_attributes_props": {
					"opphav": "opphav-face-test-missing-feature"
				}
			}
		}' AS TEXT),
	'server_side_opphav'::TEXT);

	EXCEPTION WHEN OTHERS
	THEN
    	GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT, v_context = PG_EXCEPTION_CONTEXT;
		RAISE WARNING 'Got exception %', substring(v_msg,POSITION('P0001 message' in v_msg),140);
	END;
END;
$$ LANGUAGE 'plpgsql';


BEGIN;

set client_min_messages to WARNING;
set timezone to utc;

--USED FOR testing
--\set regdir /Users/lop/dev/git/topologi/pgtopo_update_sql/test/regress

\i :regdir/../../src/sql/topo_update_java/function_02_add_border_split_surface_java.sql
\i :regdir/../../src/sql/topo_update_java/function_02_update_feature_attribute_java.sql
\i :regdir/utils/check_overlap.sql

-- TWEAK THESE !
\set wktdigits 10
\set bbox ST_MakeEnvelope(12.138220106969866, 65.15986989011846, 12.142455807015113, 65.16396000490535, 4258)
--\set bbox ST_MakeEnvelope(-1e100, -1e100, 1e100, 1e100, 4258)

-- Make to split a small polygon
\set draw_line_01 ST_GeomFromEWKT('SRID=4258;LINESTRING(12.140187736365434 65.16176989011845,12.140488177619545 65.16206000490536)');


-- TOPOLOGY NAME: update_features_test_topo
-- AREAL LAYER:   topo_ar5ngis.face_attributes
-- LINEAL LAYER:  topo_ar5ngis.edge_attributes

-- Create mapping schema and table and add mappings
CREATE SCHEMA update_features_test;

-- Create input json table
CREATE TABLE update_features_test.json_input(
	id serial PRIMARY KEY,
	label text UNIQUE,
	source text,
	payload json
);


\i :regdir/integration/fkb46/update_features_java_add_2_areal_rows_to_json_input.sql

SELECT 'common_bbox', ST_AsText(ST_BoundingDiagonal(:bbox), :wktdigits);


SELECT 'update_features_2_areal_add_rows', topo_ar5ngis.update_features(
	(SELECT payload FROM update_features_test.json_input WHERE label = 'test_2_areal_rows')::jsonb,
	:bbox
);

SELECT 'id before_split', identifikasjon_lokal_id, identifikasjon_versjon_id FROM topo_ar5ngis.face_attributes;


CREATE TEMPORARY TABLE add_border_results(
	test_name TEXT,
	fid TEXT,
	sid SERIAL,
	typ CHAR,
	act CHAR
);

SELECT 'face_attributes_before_topo_update.add_border_split_surface',
    count(*)
	FROM topo_ar5ngis.face_attributes;

INSERT INTO add_border_results(test_name, fid, typ, act)
SELECT 't1', fid, typ, act
FROM topo_update.add_border_split_surface_java(

	-- A GeoJSON Feature object representing the
	-- line drawn by the user and the holding the
	-- attributes to use with the border and the
	-- surface layers
	$$
		{
			"type": "Feature",
			"geometry": {
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:4258"
					}
				},
				"type": "LineString",
				"coordinates": [
					[
						12.140187736,
						65.16176989
					],
					[
						12.140488178,
						65.162060005
					]
				]
			},
			"properties": {
				"edge_attributes_props": {
					"opphav": "opphav-edge", "datafangstdato": "2000-12-01" , "verifiseringsdato": "2001-02-01"
				},
				"face_attributes_props": {
					"opphav": "opphav-face"
				}
			}
		}
	$$::TEXT,

	-- Surface layer table
	'topo_ar5ngis.face_attributes'::TEXT,

	-- Surface layer TopoGeometry column name
	'omrade'::TEXT,

	-- Surface layer primary key column name
	'identifikasjon_lokal_id'::TEXT,

	-- Mapping file for new surface layer
	-- see json_props_to_pg_cols
	'{
	"opphav": [
		"face_attributes_props",
		"opphav"
	],
	"verifiseringsdato": [
		"edge_attributes_props",
		"verifiseringsdato"
	],
	"datafangstdato": [
		"edge_attributes_props",
		"datafangstdato"
	]
	}'::TEXT,

	-- Mapping file for update surface layer
	-- see json_props_to_pg_cols
	'{
	"opphav": [
		"face_attributes_props",
		"opphav"
	],
	"verifiseringsdato": [
		"edge_attributes_props",
		"verifiseringsdato"
	]
	}'::TEXT,

	-- Border layer table
	'topo_ar5ngis.edge_attributes'::TEXT,

	-- Name of Border layer TopoGeometry column
	'grense'::TEXT,

	-- Surface layer primary key column name
	'identifikasjon_lokal_id'::TEXT,

	-- Mapping file for new row border layer
	-- see json_props_to_pg_cols
	'{ "opphav": [ "edge_attributes_props", "opphav" ],
	"verifiseringsdato": [
		"edge_attributes_props",
		"verifiseringsdato"
	] }'::TEXT,

	-- Mapping file updated row in border layer
	-- see json_props_to_pg_cols
	'{ "verifiseringsdato": [
		"edge_attributes_props",
		"verifiseringsdato"
	] }'::TEXT,

	-- Snap tolerance to use when
	-- inserting the new line
	-- in the topology
	0.0000000001::float8
);

SELECT 'topo_update.add_border_split_surface', (fid is NOT NULL) as ok_fid, typ, act
FROM add_border_results
WHERE test_name = 't1';

SELECT 't1 more info from splitt ', fid, typ, act
FROM topo_update.get_border_split_info(
	-- Surface layer table
	'topo_ar5ngis.face_attributes'::REGCLASS,

	-- Surface layer TopoGeometry column name
	'omrade'::NAME,

	-- Surface layer primary key column name
	'identifikasjon_lokal_id'::NAME,

	-- Border layer table
	'topo_ar5ngis.edge_attributes'::REGCLASS,

	-- Name of Border layer TopoGeometry column
	'grense'::NAME,

	-- Surface layer primary key column name
	'identifikasjon_lokal_id'::NAME,

	-- The result from the split operation
	'pg_temp.add_border_results'

);

SELECT 'face_attributes_after_topo_update.add_border_split_surface',
    count(*)
	FROM topo_ar5ngis.face_attributes;


SELECT 'identifikasjon_versjon_id after_split', identifikasjon_versjon_id FROM topo_ar5ngis.face_attributes;

-----------------------------------------
-- t2: call with feature rewrite function
-----------------------------------------

--SELECT 'XXX', array_agg(fid) FROM add_border_results WHERE typ = 'S';

CREATE FUNCTION featureRewriter(INOUT feat JSONB, role CHAR, usr TEXT)
AS $BODY$
DECLARE
	ffid TEXT;
	action TEXT;
	rec RECORD;
BEGIN

	IF role = 'S'
	THEN -- Surface feature

		-- Drop the coordinates from surface objects
		feat := feat #- '{ geometry, coordinates }';

	END IF;

	-- Extract FID
	ffid := feat #>> ARRAY[ 'properties', 'identifikasjon', 'lokalId' ];
	IF ffid IS NULL THEN
		RAISE EXCEPTION 'Unexpected NULL identifikasjon.lokalId'
			' in feature passed to featureRewriter by surfaces_as_geojson: %',
			feat;
	END IF;

	RAISE DEBUG 'Rewriting GeoJSON for Feature with FID: %', ffid;

	-- Check what affected the feature in the add_border_results
	-- table for the test name passed with 'usr' parameter
	SELECT typ, act
	FROM add_border_results
	WHERE test_name = usr
	AND fid = ffid
	INTO rec;

	RAISE DEBUG 'Operation on fid % in test % was %', ffid, usr, rec;

	-- Determine update action
	IF ( rec.typ = 'B' AND rec.act IN ( 'S', 'C' ) )
	OR ( rec.typ = 'S' AND rec.act IN ( 'S', 'C' ) )
	THEN
		action := 'Create';
	ELSE
		action := 'Replace';
	END IF;

	-- Apply this to any feature role
	feat := jsonb_set(
		feat,
		'{update}',
		jsonb_build_object(
			'action',
			action
		)
	);

END;
$BODY$ LANGUAGE 'plpgsql';


SELECT '--- t2 ---', E'\n' || jsonb_pretty(
       topo_update.surfaces_as_geojson(
               'topo_ar5ngis.ngis_export_data_flate_v',
               'omrade',
               'identifikasjon_lokal_id',
               topo_ar5ngis.colMapForFaceAttributes(),
               'topo_ar5ngis.ngis_export_data_grense_v',
               'grense',
               'identifikasjon_lokal_id',
               topo_ar5ngis.colMapForEdgeAttributes(),
               (SELECT array_agg(fid) FROM add_border_results WHERE typ = 'S' AND act != 'T'),
               'featureRewriter'::regproc,
               't1'::text, -- name of test for which we stored results in add_border_results
		25833,
		5
	)
);


SELECT 't2 Create Action java', regexp_matches(
	topo_ar5ngis.surfaces_as_geojson(
		'add_border_results',
		25835, -- utm 35
		5

	)::TEXT	,'Create','g'
);

------------------------------------
-- Cleanup local stuff
------------------------------------
DROP SCHEMA update_features_test CASCADE;
DROP FUNCTION featureRewriter(INOUT feat JSONB, role CHAR, usr TEXT);

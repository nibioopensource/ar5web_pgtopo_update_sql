BEGIN;

-- Uncomment the following line to enable gathering stats
-- about function calls, pass --nodrop to the ./run_pgtopo_test.sh
-- call, then connect to nibio_reg and run:
--
-- nibio_reg=# select * from pg_stat_user_functions  order by total_time desc;
--
--set track_functions to 'all';

-- Uncommment the following two lines to have a chance to
-- attach a profiler to the backend running the test
-- For example (replace $PID with that's printed by the select):
--
--		sudo operf --pid $PID
--
--SELECT pg_backend_pid();
--SELECT pg_sleep(10);

--LOAD 'auto_explain';
--SET auto_explain.log_min_duration = 10;
--SET auto_explain.log_analyze = true;
--SET auto_explain.log_buffers = true;
--SET auto_explain.log_timing = true;
--SET auto_explain.log_verbose = true;
--SET auto_explain.log_nested_statements = true;


set client_min_messages to WARNING;

-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)

-- TWEAK THESE !
\set wktdigits 10
\set bbox ST_MakeEnvelope(12.138220106969866, 65.15986989011846, 12.142455807015113, 65.16396000490535, 4258)
--\set bbox ST_MakeEnvelope(-1e100, -1e100, 1e100, 1e100, 4258)


-- TARGET TABLES:
-- topo_ar5.webclient_flate
-- topo_ar5.webclient_grense


--CREATE EXTENSION IF NOT EXISTS postgis_topology CASCADE;
--\i function_02_update_features_test.sql

-- (Test) schema setup

-- TOPOLOGY NAME: update_features_test_topo
-- AREAL LAYER:   topo_ar5.webclient_flate
-- LINEAL LAYER:  topo_ar5.webclient_grense

CREATE SCHEMA update_features_test;

SELECT NULL FROM CreateTopology('update_features_test_topo', 4258);

CREATE TABLE update_features_test.composite (
  a int,
  b text
);

-- This is like topo_ar5.webclient_flate but has an UUID field
CREATE TABLE update_features_test.areal (
    "id" UUID PRIMARY KEY,
	"version_id" TIMESTAMP,
    "arealtype" smallint,
    "treslag" smallint,
    "skogbonitet" smallint,
    "grunnforhold" smallint,
    "status" integer not null default 0,
    "composite" update_features_test.composite,
    "informasjon" text not null default '',
    "saksbehandler" varchar,
    "slette_status_kode" smallint not null default 0,
    "reinbeitebruker_id" varchar
);
SELECT NULL FROM AddTopoGeometryColumn('update_features_test_topo', 'update_features_test', 'areal', 'omrade', 'AREAL');

CREATE TABLE update_features_test.lineal (
    "id" uuid PRIMARY KEY,
	"version_id" TIMESTAMP,
    "avgrensingType" int,
    "datafangstdato" date,
    "oppdateringsdato" timestamp,
    "opphav" text,
    "verifiseringsdato" date
);
SELECT NULL FROM AddTopoGeometryColumn('update_features_test_topo', 'update_features_test', 'lineal', 'omrade', 'LINEAL');


-- Create mapping table
CREATE TABLE update_features_test.json_mapping(
	target_table regclass PRIMARY KEY,
	mapping_from_ngis json,
	mapping_from_webclient json
);
INSERT INTO update_features_test.json_mapping (target_table, mapping_from_ngis)
VALUES (
	'update_features_test.areal',
	'{
      "id": [ "identifikasjon", "lokalId" ],
      "version_id": [ "identifikasjon", "versjonId" ],
      "arealtype": [ "arealtype" ],
      "treslag": [ "treslag" ],
      "skogbonitet": [ "skogbonitet" ],
      "grunnforhold": [ "grunnforhold" ],
      "composite.a": [ "kvalitet", "synbarhet" ],
      "composite.b": [ "identifikasjon", "navnerom" ]
		}'
),(
	'update_features_test.lineal',
	'{
      "id": [ "identifikasjon", "lokalId" ],
      "version_id": [ "identifikasjon", "versjonId" ],
      "avgrensingType": [ "avgrensingType" ],
      "datafangstdato": [ "datafangstdato" ],
      "oppdateringsdato": [ "oppdateringsdato" ],
      "verifiseringsdato": [ "verifiseringsdato" ]
	}'
);

-- Create input json table
CREATE TABLE update_features_test.json_input(
	id serial PRIMARY KEY,
	label text UNIQUE,
	source text,
	payload json
);


\i :regdir/integration/fkb46/update_features_java_add_2_areal_rows_to_json_input.sql
\i :regdir/integration/fkb46/update_features_java_add_8_areal_rows_to_json_input.sql

SELECT 'common_bbox', ST_AsText(ST_BoundingDiagonal(:bbox), :wktdigits);

CREATE FUNCTION features_summary(
	lbl TEXT,
	wktdigits INT
)
RETURNS SETOF TEXT
AS $BODY$
DECLARE
	rec RECORD;
BEGIN
	FOR rec IN
		WITH feats AS (
			SELECT omrade::geometry g
			FROM update_features_test.lineal
		)
		SELECT
			ARRAY[
				lbl || '_lines',
				SUM(Round(ST_Length(g)))::text,
				count(*)::text,
				ST_AsText(ST_BoundingDiagonal(ST_Union(
					-- workaround to https://trac.osgeo.org/postgis/ticket/4871
					-- can be removed when using PostGIS >= 3.1.2
					ST_SetSRID(
						ST_Envelope(g),
						0
					)
				)), wktdigits)
			]::text[] a
		FROM feats
	LOOP
		RETURN NEXT array_to_string(rec.a, '|');
	END LOOP;

	FOR rec IN
		WITH feats AS (
			SELECT omrade::geometry g
			FROM update_features_test.areal
		)
		SELECT
			ARRAY[
				lbl || '_areal',
				SUM(Round(ST_Length(g)))::text,
				count(*)::text,
				ST_AsText(ST_BoundingDiagonal(ST_Union(
					-- workaround to https://trac.osgeo.org/postgis/ticket/4871
					-- can be removed when using PostGIS >= 3.1.2
					ST_SetSRID(
						ST_Envelope(g),
						0
					)
				)), wktdigits)
			]::text[] a
		FROM feats
	LOOP
		RETURN NEXT array_to_string(rec.a, '|');
	END LOOP;
END;
$BODY$ LANGUAGE 'plpgsql';


SELECT 'update_features_2_areal_add_rows', topo_ar5ngis.update_features(
	(SELECT payload FROM update_features_test.json_input WHERE label = 'test_2_areal_rows')::jsonb,
	:bbox
);

SELECT * from features_summary('update_features_2_areal_add_rows', :wktdigits);


SELECT 'update_features_8_areal_add_rows', topo_ar5ngis.update_features(
	(SELECT payload FROM update_features_test.json_input WHERE label = 'test_8_areal_rows')::jsonb,
	:bbox
);

SELECT * from features_summary('update_features_8_areal_add_rows', :wktdigits);

SELECT 'update_features_2_areal_update_rows', topo_ar5ngis.update_features(
	(SELECT payload FROM update_features_test.json_input WHERE label = 'test_2_areal_rows')::jsonb,
	:bbox
);

SELECT * from features_summary('update_features_2_areal_update_rows', :wktdigits);

------------------------------------
-- Cleanup local stuff
------------------------------------
SELECT topology.DropTopology('update_features_test_topo');
DROP SCHEMA update_features_test CASCADE;
DROP FUNCTION features_summary(
	lbl TEXT,
	wktdigits INT
);


set client_min_messages to WARNING;
set timezone to utc;

-- TODO create a master xsd format and a sample

-- TODO create JSON schema like https://gitlab.com/nibioopensource/pgtopo_update_sql/-/blob/app_config/doc/APP_CONFIG_EXAMPLE.js
-- based on master xsd

-- TODO create XSD to validate XML distribution for simplefeature format (heleid geometri)
-- like https://gitlab.com/nibioopensource/pgtopo_update_sql/-/blob/196-use-xsd-to-describe-overlap-and-gap-and-a-sample-with-xml-output-with-simple-feature/test/xsd/samples/no_overlap_and_gap/no_over_lap_gap_01.xsd
-- based on master xsd

-- TODO create XSD to validate XML for Topological distribution based on master xsd

-- TODO use topo_update.app_CreateSchema to create Postgis Topology tables and functions based on given JSON
-- (In https://gitlab.com/nibioopensource/pgtopo_update_sql/-/blob/app_config/test/regress/app_CreateSchema.sql  you can see different test cases and what's possible now )

-- TODO make test and data and the validate from these like below for different formats

\! xmllint --noout -schema ../../../data/fkb46/xsd/samples/no_overlap_and_gap/no_over_lap_gap_01.xsd ../../../data/fkb46/xsd/samples/no_overlap_and_gap/no_over_lap_gap_example_01.xml



BEGIN;

set client_min_messages to WARNING;

\i :regdir/utils/make_predictable.sql

-- Load data
\set dataset 'fkb50/split-sample2'
\i :regdir/utils/load_update_features_payload.sql

SELECT topo_ar5ngis.update_features_fkb50(payload, bbox)
FROM json_input;

-- Validate data
\i :regdir/utils/validate_update_features_load.sql


\o :regdir/../data/:dataset.export.json
SELECT
  topo_ar5ngis.surfaces_as_geojson_fkb50(
    (
      SELECT array_agg("identifikasjon_lokal_id"::text ORDER BY "identifikasjon_lokal_id")
      FROM topo_ar5ngis.face_attributes
    ),
    25835, -- utm 35
    5 -- num decimals
  ) geojson
;

-- enable output again
\o

-- add1
CREATE TABLE add1_border_res AS
SELECT * FROM topo_ar5ngis.add_borders_fkb50($FEATURE$
{
  "type": "Feature",
  "geometry": {
    "type": "MultiLineString",
    "coordinates": [
      [[654138.96,7812482.78],[654149.67,7812480.51]],
      [[654140.295,7812474.477],[654151.662,7812476.751]]
    ],
    "crs": {
      "type": "name",
      "properties": {
        "name": "EPSG:25835"
      }
    }
  },
  "properties": {
    "datafangstmetode": "dig",
    "datafangstdato": "2023-04-17"
  }
}
$FEATURE$);

SELECT 'add1 rows', * FROM add1_border_res
ORDER BY 2;

--add2
CREATE TABLE add2_border_res AS
SELECT * FROM topo_ar5ngis.add_borders_fkb50($FEATURE$
{
  "type": "Feature",
  "geometry": {
    "type": "LineString",
    "coordinates": [
      [654144.517,7812482.380],[654145.600,7812474.586]
    ],
    "crs": {
      "type": "name",
      "properties": {
        "name": "EPSG:25835"
      }
    }
  },
  "properties": {
    "datafangstmetode": "dig",
    "datafangstdato": "2023-04-17"
  }
}
$FEATURE$);

SELECT 'add2 rows', * FROM add2_border_res
ORDER BY 2;

-- remove add 2
SELECT * FROM topo_ar5ngis.remove_borders_fkb50('00000000-0000-0000-0000-000000000011');

-- check for remoavble border id, should be zero , beacuse opphav = 'samf'
SELECT 'removableborders id opphav=samf', * FROM topo_ar5ngis.get_removableborders(
'SRID=4258;POLYGON((31.114045470683845 70.37111263907262,31.114045470683845 70.37118204778173,31.114340190815284 70.37118204778173,31.114340190815284 70.37111263907262,31.114045470683845 70.37111263907262))'
) ORDER BY border_ids;

-- set opphave to web
UPDATE topo_ar5ngis.face_attributes set opphav = 'web' where ST_Intersects(omrade,'SRID=4258;POLYGON((31.114045470683845 70.37111263907262,31.114045470683845 70.37118204778173,31.114340190815284 70.37118204778173,31.114340190815284 70.37111263907262,31.114045470683845 70.37111263907262))');

-- check for remoavble border id, we shold find some'
SELECT 'removableborders id opphav=web', * FROM topo_ar5ngis.get_removableborders(
'SRID=4258;POLYGON((31.114045470683845 70.37111263907262,31.114045470683845 70.37118204778173,31.114340190815284 70.37118204778173,31.114340190815284 70.37111263907262,31.114045470683845 70.37111263907262))'
) ORDER BY border_ids;

-- change arealtype from 12 to 81/water
UPDATE topo_ar5ngis.face_attributes set arealtype = 81 where arealtype = 12 AND
ST_Intersects(omrade,'SRID=4258;POLYGON((31.114045470683845 70.37111263907262,31.114045470683845 70.37118204778173,31.114340190815284 70.37118204778173,31.114340190815284 70.37111263907262,31.114045470683845 70.37111263907262))');

-- check for remoavble border id, should be zero , beacuse areal water is not mergebale
SELECT 'removableborders id arealtype = 81', * FROM topo_ar5ngis.get_removableborders(
'SRID=4258;POLYGON((31.114045470683845 70.37111263907262,31.114045470683845 70.37118204778173,31.114340190815284 70.37118204778173,31.114340190815284 70.37111263907262,31.114045470683845 70.37111263907262))'
) ORDER BY border_ids;

-- change arealtype bac from 81 to 12
UPDATE topo_ar5ngis.face_attributes set arealtype = 12 where arealtype = 81 AND
ST_Intersects(omrade,'SRID=4258;POLYGON((31.114045470683845 70.37111263907262,31.114045470683845 70.37118204778173,31.114340190815284 70.37118204778173,31.114340190815284 70.37111263907262,31.114045470683845 70.37111263907262))');

-- check for remoavble border id, ok
SELECT 'removableborders id is ok', * FROM topo_ar5ngis.get_removableborders(
'SRID=4258;POLYGON((31.114045470683845 70.37111263907262,31.114045470683845 70.37118204778173,31.114340190815284 70.37118204778173,31.114340190815284 70.37111263907262,31.114045470683845 70.37111263907262))'
) ORDER BY border_ids;

-- get remoavble border for topojson
-- \out /tmp/ts.json
CREATE TABLE get_removebale_borders_as_topojson_results AS
SELECT topo_ar5ngis.get_removableborders_as_topojson(
'SRID=4258;POLYGON((31.114045470683845 70.37111263907262,31.114045470683845 70.37118204778173,31.114340190815284 70.37118204778173,31.114340190815284 70.37111263907262,31.114045470683845 70.37111263907262))',
25835,
5);
-- Print result summaries
SELECT 'features_as_topojson1', count(*) FROM get_removebale_borders_as_topojson_results;


-- remove borders
CREATE TABLE remove_border_results1 AS
SELECT * FROM topo_ar5ngis.remove_borders_fkb50('00000000-0000-0000-0000-000000000005');

SELECT 'remove1', * FROM remove_border_results1
ORDER BY 2;

WITH x AS (
  SELECT topo_ar5ngis.remove_border_changes_as_geojson_fkb50(
    (SELECT JSON_AGG(r.*) FROM remove_border_results1 r)::TEXT,
    25832,
    3
    ) j
)
SELECT
  'geojson',
  f -> 'geometry' ->> 'type' as typ,
  f -> 'properties' -> 'identifikasjon' ->> 'lokalId' as id,
  f -> 'properties' -> 'klassifiseringsmetode' as klassifiseringsmetode,
  f -> 'properties' -> 'avgrensingType' as avgrensingType,
  f -> 'properties' -> 'opphav' as opphav,
  CASE WHEN (f -> 'properties' -> 'verifiseringsdato') IS NULL THEN 'not_ok_verifiseringsdato'
  ELSE 'ok_verifiseringsdato'
  END AS verifiseringsdato,
  f -> 'properties' -> 'kvalitet' as opphav,
  f -> 'properties' -> 'registreringsversjon' as registreringsversjon,
  f -> 'update' ->> 'action' as action
FROM x, jsonb_array_elements( j -> 'features' ) f
ORDER BY 2 desc,3;


ROLLBACK;

/**

brew services restart postgresql@16; \
rm -fr /tmp/pgis_reg; \
sleep 1; \
echo '' > /opt/homebrew/var/log/postgresql@16.log; \
make clean; \
make; \
sh ./test/regress/run_pgtopo_ar5ngis_fkb50_test.sh --nodrop  ./test/regress/integration/fkb50/remove_border_feature_01.sql; \
psql postgres -c"drop database if exists test_pg_update_t1;" -c"CREATE database test_pg_update_t1 TEMPLATE nibio_ar5ngis_reg;";
cat /tmp/pgis_reg/test_1_diff;

 */
BEGIN;

set client_min_messages to WARNING;

\i :regdir/utils/make_predictable.sql

-- Load data
\set dataset 'fkb50/remove_border_feature-sample_03'
\i :regdir/utils/load_update_features_payload.sql

SELECT topo_ar5ngis.update_features_fkb50(payload, bbox)
FROM json_input;

-- Validate data
\i :regdir/utils/validate_update_features_load.sql


\o :regdir/../data/:dataset.export.json
SELECT
  topo_ar5ngis.surfaces_as_geojson_fkb50(
    (
      SELECT array_agg("identifikasjon_lokal_id"::text ORDER BY "identifikasjon_lokal_id")
      FROM topo_ar5ngis.face_attributes
    ),
    25832, -- utm 32
    5 -- num decimals
  ) geojson
;

-- enable output again
\o


-- check for remoavble border id, should be zero , beacuse opphav = 'samf'
-- Test regress/integration/fkb50/remove_border_feature_03 should return zero borderes to merge becuse surface is between 500 and 2000 m2 and opphave is not web
SELECT 'removableborders id opphav=samf', * FROM topo_ar5ngis.get_removableborders(
'SRID=4258; POLYGON ((13.169346671215193 64.92716653481773, 13.169644576206911 64.92908064767641, 13.177698496020673 64.92885479613336, 13.17740001968789 64.92694070285334, 13.169346671215193 64.92716653481773))'
) ORDER BY border_ids;




ROLLBACK;

/**

brew services restart postgresql@16; \
rm -fr /tmp/pgis_reg; \
sleep 1; \
echo '' > /opt/homebrew/var/log/postgresql@16.log; \
make clean; \
make; \
sh ./test/regress/run_pgtopo_ar5ngis_fkb50_test.sh --nodrop  ./test/regress/integration/fkb50/remove_border_feature_01.sql; \
psql postgres -c"drop database if exists test_pg_update_t1;" -c"CREATE database test_pg_update_t1 TEMPLATE nibio_ar5ngis_reg;";
cat /tmp/pgis_reg/test_1_diff;

 */
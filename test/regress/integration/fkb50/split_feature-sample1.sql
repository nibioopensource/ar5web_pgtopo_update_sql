BEGIN;

set client_min_messages to ERROR;

\i :regdir/utils/make_predictable.sql

-- Load data
\set dataset 'fkb50/split-sample1'
\i :regdir/utils/load_update_features_payload.sql

SELECT topo_ar5ngis.update_features_fkb50(payload, bbox)
FROM json_input;

-- Validate data
\i :regdir/utils/validate_update_features_load.sql


\o :regdir/../data/:dataset.export.json
SELECT
	topo_ar5ngis.surfaces_as_geojson_fkb50(
		(
			SELECT array_agg("identifikasjon_lokal_id"::text ORDER BY "identifikasjon_lokal_id")
			FROM topo_ar5ngis.face_attributes
		),
		25832, -- utm 32
		5 -- num decimals
	) geojson
;

-- enable output again
\o

DO $$
DECLARE
        v_state text;
        v_msg text;
        v_detail text;
        v_hint text;
        v_context text;
BEGIN

BEGIN

CREATE TABLE add_border_results1 AS
SELECT * FROM topo_ar5ngis.add_borders_fkb50($FEATURE$
{
  "type": "Feature",
  "geometry": {
    "type": "LineString",
    "coordinates": [
      [
        633192.4674673436,
        6603299.595540076
      ],
      [
        633243.7651605743,
        6603297.003844773
      ]
    ],
    "crs": {
      "type": "name",
      "properties": {
        "name": "EPSG:25832"
      }
    }
  },
  "properties": {
    "datafangstmetode": "dig",
    "datafangstdato": "2023-04-17"
  }
}
$FEATURE$);


-- Check num rows
SELECT 'add num rows', count(*) FROM add_border_results1 ;

-- Print result summaries
SELECT 'add1 rows', * FROM add_border_results1
ORDER BY 2;



-- Get changes json
WITH x AS (
	SELECT topo_ar5ngis.add_border_changes_as_geojson_fkb50(
        'add_border_results1',
        25832,
		3
    ) j
)
SELECT
	'geojson',
	f -> 'geometry' ->> 'type' as typ,
	f -> 'properties' -> 'identifikasjon' ->> 'lokalId' as id,
	f -> 'properties' -> 'klassifiseringsmetode' as klassifiseringsmetode,
	f -> 'properties' -> 'avgrensingType' as avgrensingType,
	f -> 'properties' -> 'opphav' as opphav,
	CASE WHEN (f -> 'properties' -> 'verifiseringsdato') IS NULL THEN 'not_ok_verifiseringsdato'
	ELSE 'ok_verifiseringsdato' 
	END AS verifiseringsdato,
	f -> 'properties' -> 'kvalitet' as opphav,
	f -> 'properties' -> 'registreringsversjon' as registreringsversjon,
	f -> 'update' ->> 'action' as action
FROM x, jsonb_array_elements( j -> 'features' ) f
ORDER BY 2 desc,3;


        EXCEPTION WHEN OTHERS
        THEN
        GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT, v_context = PG_EXCEPTION_CONTEXT;
                RAISE WARNING 'Got exception %', substring(v_msg,POSITION('P0001 message' in v_msg),90);
        END;


END$$;

ROLLBACK;

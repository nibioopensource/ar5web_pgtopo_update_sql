BEGIN;

set client_min_messages to WARNING;

\i :regdir/utils/make_predictable.sql

-- Load data
\set dataset 'fkb50/split-sample2'
\i :regdir/utils/load_update_features_payload.sql

SELECT topo_ar5ngis.update_features_fkb50(payload, bbox)
FROM json_input;

-- Validate data
\i :regdir/utils/validate_update_features_load.sql


\o :regdir/../data/:dataset.export.json
SELECT
	topo_ar5ngis.surfaces_as_geojson_fkb50(
		(
			SELECT array_agg("identifikasjon_lokal_id"::text ORDER BY "identifikasjon_lokal_id")
			FROM topo_ar5ngis.face_attributes
		),
		25835, -- utm 35
		5 -- num decimals
	) geojson
;

-- enable output again
\o

-- Split it with an horizontal line
CREATE TABLE add_border_results1 AS
SELECT * FROM topo_ar5ngis.add_borders_fkb50($FEATURE$
{
  "type": "Feature",
  "geometry": {
    "type": "LineString",
    "coordinates": [
      [
        653982.2000010489,
        7812357.891995837
      ],
      [
		654037.4186265768,
		7812365.981947789      
      ]
    ],
    "crs": {
      "type": "name",
      "properties": {
        "name": "EPSG:25835"
      }
    }
  },
  "properties": {
    "datafangstmetode": "dig",
    "datafangstdato": "2023-04-17"
  }
}
$FEATURE$);

/*

Zoom to Vardø, By, Vardø

500 Internal Server Error: "{"detail":"Commit feilet","errors":[{"reason":"Alle objekter som deler geometrien er ikke med i innsjekk
en:\n\tGrenselinjen (id: 41354948 globalid: d914330a-d134-4b19-924d-a3a16da126db) som ble sjekket inn mangler følgende tilhørende objekter i innsjekken:\n\t\t
Flaten (id: 41336126 globalid: 7088f47a-890e-4f63-b52d-1d69f9f269bf)\n\tGrenselinjen (id: 41354964 globalid: 6f716f91-09eb-4165-b89f-bfaf73f177f9) som ble sjekket inn mangler følgende tilhørende objekter i innsjekken:\n\t\tFlaten (id: 41337998 globalid: 9556d9
3b-69e4-4c8c-a510-e7dfed78ab00)"}],"title":"Commit feilet","type":"http://ngisopenapi.no/errors/commit_error"}"  

*/
-- Check num rows
SELECT 'add num rows', count(*) FROM add_border_results1 ;

-- Print result summaries
SELECT 'add1 rows', * FROM add_border_results1
ORDER BY 2;



-- Get changes json
WITH x AS (
	SELECT topo_ar5ngis.add_border_changes_as_geojson_fkb50(
        'add_border_results1',
        25832,
		3
    ) j
)
SELECT
	'geojson',
	f -> 'geometry' ->> 'type' as typ,
	f -> 'properties' -> 'identifikasjon' ->> 'lokalId' as id,
	f -> 'properties' -> 'klassifiseringsmetode' as klassifiseringsmetode,
	f -> 'properties' -> 'avgrensingType' as avgrensingType,
	f -> 'properties' -> 'opphav' as opphav,
	CASE WHEN (f -> 'properties' -> 'verifiseringsdato') IS NULL THEN 'not_ok_verifiseringsdato'
	ELSE 'ok_verifiseringsdato' 
	END AS verifiseringsdato,
	f -> 'properties' -> 'kvalitet' as opphav,
	f -> 'properties' -> 'registreringsversjon' as registreringsversjon,
	f -> 'update' ->> 'action' as action
FROM x, jsonb_array_elements( j -> 'features' ) f
ORDER BY 2 desc,3;

;


--\o :regdir/../data/:dataset.ngis_split_payload.json;
--SELECT * FROM topo_ar5ngis.add_border_changes_as_geojson_fkb50('add_border_results1',25832,3);--
--SELECT * FROM topo_ar5ngis.add_border_changes_as_geojson_fkb50_java((SELECT JSON_AGG(r.*) FROM add_border_results1 r)::TEXT,25832,3);


SELECT '-- geojson diff input output --';
\echo `:regdir/utils/fkb50_geojson_diff.sh :regdir/../data/:dataset.json :regdir/../data/:dataset.export.json`

ROLLBACK;

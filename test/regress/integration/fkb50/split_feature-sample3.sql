
/**



request:{"request_id":null,"session_id":"45B3A84B5A3BCA443A679DEACE6614DE","user_id":null,"user_token":null,"response_srid":25832,"request_time":null,"layer_id":"AR5_WEBCLIENT_F","envelope":null,
"bbox":{"typ
e":"Polygon","coordinates":[[[632333.2309544123,6602945.299422214],[632333.2309544123,6603585.607695848],[633304.095484832,6603585.607695848],[633304.095484832,6602945.299422214],[632333.2309544123,6602945.299
422214]]],"crs":{"type":"name","properties":{"name":"EPSG:25832"}}},"input_line":
{"type":
"LineString","coordinates":
[[632809.6737332294,6603411.468502296],[632841.1369356042,6603320.931532198]],"crs":{"type":"
name","propertiexs":{"name":"EPSG:25832"}}},"map_update":"\"{\"type\":\"Feature\",\"geometry\":{\"type\":\
"LineString\",\"coordinates\":[[632809.6737332294,6603411.468502296],[632841.1369356042,6603320.931532198]],\"crs\":{\"type\":\"name\",\"properties\":{\"name\":\"EPSG:25832\"}}},\"properties\":
{\"datafangstmetode\":\"dig\",\"datafangstdato\":\"2023-04-22\"}}\"","updSurfaceWithPolyReqAttrValuesStringEntities":[],
"updSurfaceWithPolyReqAttrValuesDecimalEntities":[],"updSurfaceWithPolyReqAttrValuesDateEntities":[],"updSurfaceWithPolyReqAttrValuesIntEntities":[]} - 

ERROR NgisJobHandler no.skogoglandskap.topo.jobhandler.NgisJobHandler$NgisAr5UpadteDatasetFeaturesImpl.call(NgisJobHandler.java:1128) 1128 - failed when calling UpadteDatasetFeatures job for session_id 45B3A84B5A3BCA443A679DEACE6614DE with job uuid f0321f71-5d40-43f5-8301-bdc16ad0d138 and dataset uuid [23babdf3-ed5c-41f9-ad53-c5c1c007090e] registered at Sat Apr 22 09:38:00 CEST 2023 ngisError:{"detail":"Et eller flere objekter ble ikke godkjent av valideringsrutinene","errors":[{"lokalid":"265c4772-e2ca-4181-a8a5-67a63e46d394","reason":"Error 467: Objekt av type ArealressursFlate mangler egenskap verifiseringsdato "},{"lokalid":"265c4772-e2ca-4181-a8a5-67a63e46d394","reason":"Error 467: Objekt av type ArealressursFlate mangler egenskap datafangstdato "}],"title":"Data avvist i validering","type":"http://ngisopenapi.no/errors/validation_error"} - 
org.springframework.web.client.HttpClientErrorException$BadRequest: 400 Bad Request: "{"detail":"Et eller flere objekter ble ikke godkjent av valideringsrutinene","errors":[{"lokalid":"265c4772-e2ca-4181-a8a5-67a63e46d394","reason":"Error 467: Objekt av type ArealressursFlate mangler egenskap verifiseringsdato "},{"lokalid":"265c4772-e2ca-4181-a8a5-67a63e46d394","reason":"Error 467: Objekt av type ArealressursFlate mangler egenskap datafangstdato "}],"title":"Data avvist i validering","type":"http://ngisopenapi.no/errors/validation_error"}"
        at org.springframework.web.client.HttpClientErrorException.create(HttpClientErrorException.java:101) ~[spring-web-5.2.22.RELEASE.jar:5.2.22.RELEASE]
        at org.springframework.web.client.DefaultResponseErrorHandler.handleError(DefaultResponseErrorHandler.java:169) ~[spring-web-5.2.22.RELEASE.jar:5.2.22.RELEASE]
        at org.springframework.web.client.DefaultResponseErrorHandler.handleError(DefaultResponseErrorHandler.java:122) ~[spring-web-5.2.22.RELEASE.jar:5.2.22.RELEASE]
        at org.springframework.web.client.ResponseErrorHandler.handleError(ResponseErrorHandler.java:63) ~[spring-web-5.2.22.RELEASE.jar:5.2.22.RELEASE]
        at org.springframework.web.client.RestTemplate.handleResponse(RestTemplate.java:780) ~[spring-web-5.2.22.RELEASE.jar:5.2.22.RELEASE]
        at org.springframework.web.client.RestTemplate.doExecute(RestTemplate.java:738) ~[spring-web-5.2.22.RELEASE.jar:5.2.22.RELEASE]
        at org.springframework.web.client.RestTemplate.exchange(RestTemplate.java:649) ~[spring-web-5.2.22.RELEASE.jar:5.2.22.RELEASE]
        at ngis.openapi.client.client.invoker.ApiClient.invokeAPI(ApiClient.java:508) ~[ngis-openapi-api-client-0.0.3-SNAPSHOT.jar:?]
        at ngis.openapi.client.client.api.FeaturesApi.updateDatasetFeaturesWithHttpInfo(FeaturesApi.java:493) ~[ngis-openapi-api-client-0.0.3-SNAPSHOT.jar:?]
        at ngis.openapi.client.client.api.FeaturesApi.updateDatasetFeatures(FeaturesApi.java:425) ~[ngis-openapi-api-client-0.0.3-SNAPSHOT.jar:?]
        at no.skogoglandskap.topo.client.Ar5NgisOpenApiIntergration.upadteDatasetFeatures(Ar5NgisOpenApiIntergration.java:676) ~[pgtopo_update-1.6-SNAPSHOT.jar:?]
        at no.skogoglandskap.topo.jobhandler.NgisJobHandler$NgisAr5UpadteDatasetFeaturesImpl.call(NgisJobHandler.java:1112) [pgtopo_update-1.6-SNAPSHOT.jar:?]
        at no.skogoglandskap.topo.jobhandler.NgisJobHandler$NgisAr5UpadteDatasetFeaturesImpl.call(NgisJobHandler.java:982) [pgtopo_update-1.6-SNAPSHOT.jar:?]
        at java.util.concurrent.FutureTask.run(FutureTask.java:264) [?:?]
        at java.util.concurrent.ScheduledThreadPoolExecutor$ScheduledFutureTask.run(ScheduledThreadPoolExecutor.java:304) [?:?]
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1130) [?:?]
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:630) [?:?]
        at java.lang.Thread.run(Thread.java:832) [?:?]

*/

BEGIN;

set client_min_messages to WARNING;

\i :regdir/utils/make_predictable.sql

-- Load data
\set dataset 'fkb50/split-sample3'
\i :regdir/utils/load_update_features_payload.sql

SELECT topo_ar5ngis.update_features_fkb50(payload, bbox)
FROM json_input;

-- Validate data
\i :regdir/utils/validate_update_features_load.sql


\o :regdir/../data/:dataset.export.json
SELECT
	topo_ar5ngis.surfaces_as_geojson_fkb50(
		(
			SELECT array_agg("identifikasjon_lokal_id"::text ORDER BY "identifikasjon_lokal_id")
			FROM topo_ar5ngis.face_attributes
		),
		25832, -- utm 32
		5 -- num decimals
	) geojson
;

-- enable output again
\o

-- Split it with an horizontal line
CREATE TABLE add_border_results1 AS
SELECT * FROM topo_ar5ngis.add_borders_fkb50($FEATURE$
{
  "type": "Feature",
  "geometry": {
    "type": "LineString",
    "coordinates": [
      [
        632809.6737332294,6603411.468502296
      ],
      [
        632841.1369356042,6603320.931532198     
      ]
    ],
    "crs": {
      "type": "name",
      "properties": {
        "name": "EPSG:25832"
      }
    }
  },
  "properties": {
    "datafangstmetode": "dig",
    "datafangstdato": "2023-04-17"
  }
}
$FEATURE$);

-- Check num rows
SELECT 'add num rows', count(*) FROM add_border_results1 ;

-- Print result summaries
SELECT 'add1 rows', * FROM add_border_results1
ORDER BY 2;



-- Get changes json
WITH x AS (
	SELECT topo_ar5ngis.add_border_changes_as_geojson_fkb50(
        'add_border_results1',
        25832,
		3
    ) j
)
SELECT
	'geojson',
	f -> 'geometry' ->> 'type' as typ,
	f -> 'properties' -> 'identifikasjon' ->> 'lokalId' as id,
	f -> 'properties' -> 'klassifiseringsmetode' as klassifiseringsmetode,
	f -> 'properties' -> 'avgrensingType' as avgrensingType,
	f -> 'properties' -> 'opphav' as opphav,
	CASE WHEN (f -> 'properties' -> 'verifiseringsdato') IS NULL THEN 'not_ok_verifiseringsdato'
	ELSE 'ok_verifiseringsdato' 
	END AS verifiseringsdato,
	CASE WHEN (f -> 'properties' -> 'datafangstdato') IS NULL THEN 'not_ok_datafangstdato'
	ELSE 'ok_datafangstdato' 
	END AS datafangstdato,
	f -> 'properties' -> 'kvalitet' as opphav,
	f -> 'properties' -> 'registreringsversjon' as registreringsversjon,
	f -> 'update' ->> 'action' as action
FROM x, jsonb_array_elements( j -> 'features' ) f
ORDER BY 2 desc,3;

;


--Takes to long time ro run
--
--SELECT '-- geojson diff input output --';
--\echo `:regdir/utils/fkb50_geojson_diff.sh :regdir/../data/:dataset.json :regdir/../data/:dataset.export.json`

ROLLBACK;

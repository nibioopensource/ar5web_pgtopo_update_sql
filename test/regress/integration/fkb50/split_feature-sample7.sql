
/**

Issue ref https://gitlab.com/nibioopensource/ar5web_pgtopo_update_sql/-/issues/38


Some info from server log files

2024-08-22 07:57:15,187 [ajp-nio-127.0.0.1-8009-exec-18] DEBUG TopoClientUtil no.skogoglandskap.topo.client.TopoClientUtil.setUpdateSessionAuthentication(TopoClientUtil.java:34) 34 - SET pgtopo_update.session_id = '35F90937902523690B1420FF48F89149' -
2024-08-22 07:57:15,187 [ajp-nio-127.0.0.1-8009-exec-18] DEBUG TopoClientUtil no.skogoglandskap.topo.client.TopoClientUtil.setUpdateSessionAuthentication(TopoClientUtil.java:44) 44 - User 5021_anbo has write access because of role Forvaltning. -
2024-08-22 07:57:15,230 [ajp-nio-127.0.0.1-8009-exec-18] ERROR SqlExceptionHelper org.hibernate.engine.jdbc.spi.SqlExceptionHelper.logExceptions(SqlExceptionHelper.java:142) 142 -
ERROR: Border 56b4d0ab-f2c3-4a06-8206-a8793b9c299a in layer topo_ar5ngis.edge_attributes_export_fkb50.grense 	 5132 also extends to edge 5934 at
POINT(9.298273159>
  Where: PL/pgSQL function topo_update.get_surface_borders(topology,topogeometry,regclass,name,name) line 346 at RAISE



2024-08-22 10:01:06,122 [ajp-nio-127.0.0.1-8009-exec-14] ERROR SqlExceptionHelper org.hibernate.engine.jdbc.spi.SqlExceptionHelper.logExceptions(SqlExceptionHelper.java:142) 142 - ERROR: Border 56b4d0ab-f2c3-4a06-8206-a8793b9c299a in layer topo_ar5ngis.edge_attributes_export_fkb50.grense covering surface boundary edges 56030 also extends to edge 14185 at POINT(9.2988335>
  Where: PL/pgSQL function topo_update.get_surface_borders(topology,topogeometry,regclass,name,name) line 346 at RAISE
PL/pgSQL function topo_update._surface_as_geojson_features(topology,layer,text,topogeometry,jsonb,regclass,name,name,jsonb,regclass,regproc,anyelement,integer,integer) line 27 at FOR over SELECT rows
SQL statement "SELECT array_agg(f) FILTER (WHERE f IS NOT NULL)
                FROM topo_update._surface_as_geojson_features(


2024-08-22 10:01:06,122 [ajp-nio-127.0.0.1-8009-exec-14] ERROR SqlExceptionHelper org.hibernate.engine.jdbc.spi.SqlExceptionHelper.logExceptions(SqlExceptionHelper.java:142) 142 - ERROR: Border 56b4d0ab-f2c3-4a06-8206-a8793b9c299a in layer topo_ar5ngis.edge_attributes_export_fkb50.grense covering surface boundary edges


*/

BEGIN;

set client_min_messages to WARNING;

\i :regdir/utils/make_predictable.sql

-- Load data
\set dataset 'fkb50/split-sample7'
\i :regdir/utils/load_update_features_payload.sql

SELECT topo_ar5ngis.update_features_fkb50(payload, bbox)
FROM json_input;

-- Validate data
\i :regdir/utils/validate_update_features_load.sql


\o :regdir/../data/:dataset.export.json
SELECT
	topo_ar5ngis.surfaces_as_geojson_fkb50(
		(
			SELECT array_agg("identifikasjon_lokal_id"::text ORDER BY "identifikasjon_lokal_id")
			FROM topo_ar5ngis.face_attributes
		),
		25832, -- utm 32
		5 -- num decimals
	) geojson
;

-- enable output again
\o

-- Split it with an horizontal line
CREATE TABLE add_border_results1 AS
SELECT * FROM topo_ar5ngis.add_borders_fkb50($FEATURE$
{
  "type": "Feature",
  "geometry": {
    "type": "LineString",
    "coordinates": [[703020.9500000001,7224756.359999999],[703004.5580226367,7224780.843796474],[703023.7736,7224786.680500003]],
    "crs": {
      "type": "name",
      "properties": {
        "name": "EPSG:25832"
      }
    }
  },
  "properties": {
    "datafangstmetode": "dig",
    "datafangstdato": "2024-08-16"
  }
}
$FEATURE$);

-- Check num rows
SELECT 'add num rows', count(*) FROM add_border_results1 ;

-- Print result summaries
SELECT 'add1 rows', * FROM add_border_results1
ORDER BY 2;

SELECT * FROM topology.ValidateTopology('topo_ar5ngis_sysdata_webclient');

-- Get changes json
WITH x AS (
	SELECT topo_ar5ngis.add_border_changes_as_geojson_fkb50(
        'add_border_results1',
        25832,
		5
    ) j
)
SELECT
	'geojson',
	f -> 'geometry' ->> 'type' as typ,
	f -> 'properties' -> 'identifikasjon' ->> 'lokalId' as id,
	f -> 'properties' -> 'klassifiseringsmetode' as klassifiseringsmetode,
	f -> 'properties' -> 'avgrensingType' as avgrensingType,
	f -> 'properties' -> 'opphav' as opphav,
	CASE WHEN (f -> 'properties' -> 'verifiseringsdato') IS NULL THEN 'not_ok_verifiseringsdato'
	ELSE 'ok_verifiseringsdato'
	END AS verifiseringsdato,
	CASE WHEN (f -> 'properties' -> 'datafangstdato') IS NULL THEN 'not_ok_datafangstdato'
	ELSE 'ok_datafangstdato'
	END AS datafangstdato,
	f -> 'properties' -> 'kvalitet' as opphav,
	f -> 'properties' -> 'registreringsversjon' as registreringsversjon,
	f -> 'geometry' -> 'coordinates' as coordinates,
	f -> 'update' ->> 'action' as action
FROM x, jsonb_array_elements( j -> 'features' ) f
ORDER BY 2 desc,3;

;


SELECT ST_ASText(ST_Transform(grense::geometry,25832)) from topo_ar5ngis.edge_attributes WHERE datafangstdato = '2023-05-15';


--Takes to long time ro run
--
--SELECT '-- geojson diff input output --';
--\echo `:regdir/utils/fkb50_geojson_diff.sh :regdir/../data/:dataset.json :regdir/../data/:dataset.export.json`

ROLLBACK;

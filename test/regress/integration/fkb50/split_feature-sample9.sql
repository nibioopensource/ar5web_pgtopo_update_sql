
/**

Issue ref https://gitlab.com/nibioopensource/ar5web_pgtopo_update_sql/-/issues/


Some info from server log files

request:{"request_id":null,"session_id":"A0C78753B14B4113B0AF1096F1C6A106","user_id":null,"user_token":null,"resp
onse_srid":25832,"request_time":null,"layer_id":"AR5_WEBCLIENT_F","envelope":null,"bbox":null,"input_line":{"type":"LineString","coordinates":
[[702500.2869118316,7224291.286140252],[702512.6684333065,7224287.618428993],[702512.1750656195,7224272.872005591]],"crs":{"type":"name","properties":{"name":"EPSG:25832"}}},"remove
_line_id":null,"map_update":"\"{\"type\":\"Feature\",\"geometry\":{\"type\":\"LineString\",\"coordinates\":
[[702500.2869118316,7224291.286140252],[702512.6684333065,7224287.618428993],[702512.1750656195,7224272.872005591]],\"crs\":{\"type\":\"name\",\"properties\":{\"name\":\"EPSG:25832\"}}},\"properties\":{\"datafangstme
tode\":\"dig\",\"datafangstdato\":\"2025-01-30\"}}\"","updSurfaceWithPolyReqAttrValuesStringEntities":[],"updSurfaceWithPolyReqAttrValuesDecimalEntities":[],"updSurfaceWithPolyReqAttrValuesDateEntities":[],"updSurfaceWithPolyReqAttrValuesIntEntities":[]} -
2025-01-30 13:05:50,209 [ajp-nio-127.0.0.1-8009-exec-1] DEBUG JSonEndpointController no.skogoglandskap.topo.client.handler.json.JSonEndpointController.getServerSideInfo(JSonEndpointController.java:379) 379 - sakbehandler:NIBIO_Lars, opphav:NGISOPENAPI -
2025-01-30 13:05:50,209 [ajp-nio-127.0.0.1-8009-exec-1] DEBUG TopoClientComImpl no.skogoglandskap.topo.client.TopoClientComImpl.getUpdTopoLayer(TopoClientComImpl.java:713) 713 - enter -


-
2025-01-30 13:05:50,327 [ajp-nio-127.0.0.1-8009-exec-1] DEBUG JSonEndpointController no.skogoglandskap.topo.client.handler.json.JSonEndpointController.edit_layers_request(JSonEndpointController.java:351) 351 - response:{"request_id":null,"session_id":"A0C78753B14B4113B0AF1096F1C6A106","done_time":"2025-01-30T12:05:50.326+
00:00","status":0,"statusErrorServerMsg":"ERROR: Surface split count exceeded limit of 1\n  Where: PL/pgSQL function topo_update.add_border_split_surface(jsonb,regclass,name,name,regclass,name,name,double precision,regproc,anyelement,double precision,integer,integer,boolean) line 904 at RAISE\nSQL statement \"CREATE TEMP
TABLE temp_result ON COMMIT DROP AS\n  SELECT * FROM topo_update.add_border_split_surface(\n\n    feature,\n\n    'topo_ar5ngis.face_attributes'::regclass,\n    'omrade'::name,\n    'identifikasjon_lokal_id'::name,\n\n    'topo_ar5ngis.edge_attributes'::regclass,\n    'grense'::name,\n    'identifikasjon_lokal_id'::name,\
n\n    tolerance,\n\n    'topo_ar5ngis.add_border_ColMapProvider', -- colMapProviderFunc\n    NULL::text,\n\n    minAreaTolerance,\n\n    maxAllowedSurfaceSplitCount,\n\n    maxNewSurfacesOrChangedAreaSurfaces\n  )\"\nPL/pgSQL ","changed_area":null,"updSurfaceWithPolyResAttrEntities":[]} -
2025-01-30 13:05:50,327 [ajp-nio-127.0.0.1-8009-exec-1] DEBUG JSonEndpointController no.skogoglandskap.topo.client.handler.json.JSonEndpointController.edit_layers_request(JSonEndpointController.java:355) 355 - leave -
2025-01-30 13:05:59,406 [ajp-nio-127.0.0.1-8009-exec-7] DEBUG JSonEndpointController no.skogoglandskap.topo.client.handler.json.JSonEndpointController.get_surface_attribute(JSonEndpointController.java:467) 467 - enter GetSurfaceAttributeRequestEntity  no.skogoglandskap.topo._topo_client_com.GetSurfaceAttributeRequestEntit
y@11e32202 -
2025-01-30 13:05:59,406 [ajp-nio-127.0.0.1-8009-exec-7] DEBUG JSonEndpointController no.skogoglandskap.topo.client.handler.json.JSonEndpointController.get_surface_attribute(JSonEndpointController.java:472)

*/

BEGIN;

set client_min_messages to WARNING;

\i :regdir/utils/make_predictable.sql

-- Load data
\set dataset 'fkb50/split-sample9'
\i :regdir/utils/load_update_features_payload.sql

SELECT topo_ar5ngis.update_features_fkb50(payload, bbox)
FROM json_input;

-- Validate data
\i :regdir/utils/validate_update_features_load.sql


\o :regdir/../data/:dataset.export.json
SELECT
	topo_ar5ngis.surfaces_as_geojson_fkb50(
		(
			SELECT array_agg("identifikasjon_lokal_id"::text ORDER BY "identifikasjon_lokal_id")
			FROM topo_ar5ngis.face_attributes
		),
		25832, -- utm 32
		5 -- num decimals
	) geojson
;



-- with snap and 1 cm longer line
-- [[702500.2869118316,7224291.286140252],[702512.6684333065,7224287.618428993],[702512.1750656195,7224272.872005591]]
-- ERROR: Surface split count exceeded limit of 1

-- change this from 1 to 2 , maxAllowedSurfaceSplitCount int = 2;
-- gives this error message
--+ERROR:  Border a6aee2c0-8bad-11ef-a5a0-43e5a26e5b7a in layer topo_ar5ngis.edge_attributes_export_fkb50.grense covering surface boundary edges 822 also extends to edge 908 at POINT(13.309724921137025 65.08008550445261)
--+ERROR:  current transaction is aborted, commands ignored until end of transaction block

-- with exact snapto ???
--[[702500.2964999999,7224291.2833],[702510.982904608,7224285.61184721],[702512.1754000001,7224272.881999999]]
-- it works ok

-- enable output again
\o

-- Split it with an horizontal line
CREATE TABLE add_border_results1 AS
SELECT * FROM topo_ar5ngis.add_borders_fkb50($FEATURE$
{
	"type": "Feature",
	"geometry": {
		"type": "LineString",
		"coordinates": [[702500.2869118316,7224291.286140252],[702512.6684333065,7224287.618428993],[702512.1750656195,7224272.872005591]],
		"crs": {
			"type": "name",
			"properties": {
				"name": "EPSG:25832"
			}
		}
	},
	"properties": {
		"datafangstmetode": "dig",
		"datafangstdato": "2025-01-30"
	}
}
$FEATURE$);

-- Check num rows
SELECT 'add num rows', count(*) FROM add_border_results1 ;

-- Print result summaries
SELECT 'add1 rows', * FROM add_border_results1
ORDER BY 2;

SELECT * FROM topology.ValidateTopology('topo_ar5ngis_sysdata_webclient');

-- Get changes json
WITH x AS (
	SELECT topo_ar5ngis.add_border_changes_as_geojson_fkb50(
				'add_border_results1',
				25832,
		5
		) j
)
SELECT
	'geojson',
	f -> 'geometry' ->> 'type' as typ,
	f -> 'properties' -> 'identifikasjon' ->> 'lokalId' as id,
	f -> 'properties' -> 'klassifiseringsmetode' as klassifiseringsmetode,
	f -> 'properties' -> 'avgrensingType' as avgrensingType,
	f -> 'properties' -> 'opphav' as opphav,
	CASE WHEN (f -> 'properties' -> 'verifiseringsdato') IS NULL THEN 'not_ok_verifiseringsdato'
	ELSE 'ok_verifiseringsdato'
	END AS verifiseringsdato,
	CASE WHEN (f -> 'properties' -> 'datafangstdato') IS NULL THEN 'not_ok_datafangstdato'
	ELSE 'ok_datafangstdato'
	END AS datafangstdato,
	f -> 'properties' -> 'kvalitet' as opphav,
	f -> 'properties' -> 'registreringsversjon' as registreringsversjon,
	f -> 'geometry' -> 'coordinates' as coordinates,
	f -> 'update' ->> 'action' as action
FROM x, jsonb_array_elements( j -> 'features' ) f
ORDER BY 2 desc,3;

;


SELECT ST_ASText(ST_Transform(grense::geometry,25832)) from topo_ar5ngis.edge_attributes WHERE datafangstdato = '2023-05-15';


--Takes to long time ro run
--
--SELECT '-- geojson diff input output --';
--\echo `:regdir/utils/fkb50_geojson_diff.sh :regdir/../data/:dataset.json :regdir/../data/:dataset.export.json`

ROLLBACK;

BEGIN;

set client_min_messages to WARNING;

\i :regdir/utils/make_predictable.sql

/** get data files

echo "SRID=4258;" > test/data/fkb50/update_feature_attribute-sample1.bbox  

ssh ???@????? cat '/data/ngis_files/2023_05_25_07/ar5_pg_OK_USER_REQUEST_E48A85DB43E69FC4063B3CA26D39C870_cbe4392a-671e-4f9a-89e0-c0a298505c3d_dataset_f8283036-6979-47cb-9988-eca6200505cf__UTF-8_1684992251024\
.bbox\
'>>test/data/fkb50/update_feature_attribute-sample1.bbox

ssh ???@????? cat '/data/ngis_files/2023_05_25_07/ar5_pg_OK_USER_REQUEST_E48A85DB43E69FC4063B3CA26D39C870_cbe4392a-671e-4f9a-89e0-c0a298505c3d_dataset_f8283036-6979-47cb-9988-eca6200505cf__UTF-8_1684992251024\
.json\
'|jq . >test/data/fkb50/update_feature_attribute-sample1.json


2023-05-25 07:23:40,356 [ajp-nio-127.0.0.1-8009-exec-14] DEBUG JSonEndpointController no.skogoglandskap.topo.client.handler.json.JSonEndpointController.get_surface_attribute(JSonEndpointController.java:454) 454 - request:


 json_feature for surface or line update: 
 {"properties":{"id":"f8b67e95-c0da-4246-8f5b-8bed975d8c52","arealtype":"82","treslag":"98","skogbonitet":"98","grunnforhold":"98","datafangstdato":"2023-05-24","verifiser
ingsdato":"2023-05-24","opphav":"nibio","klassifiseringsmetode":"sFelt","informasjon":""}} - 

2023-05-25 07:23:54,804 [ajp-nio-127.0.0.1-8009-exec-11] DEBUG PgUpdateApiV2 no.skogoglandskap.topo.client.PgUpdateApiV2.updateFeaturePostgisTopologyDBWithJsonPayload(PgUpdateApiV2.java:882) 882 - enter - 
2023-05-25 07:23:54,806 [ajp-nio-127.0.0.1-8009-exec-11] DEBUG TopoClientUtil no.skogoglandskap.topo.client.TopoClientUtil.setUpdateSessionAuthentication(TopoClientUtil.java:34) 34 - SET pgtopo_update.session_id = 'E48A85DB43E69FC4063B3CA26D39C870' - 
2023-05-25 07:23:54,806 [ajp-nio-127.0.0.1-8009-exec-11] DEBUG TopoClientUtil no.skogoglandskap.topo.client.TopoClientUtil.setUpdateSessionAuthentication(TopoClientUtil.java:44) 44 - User NIBIO_Lars has write access because of role Forvaltning. - 
2023-05


2023-05-25 07:23:54,866 [pool-19-thread-10] DEBUG NgisJobHandler no.skogoglandskap.topo.jobhandler.NgisJobHandler$NgisAr5UpadteDatasetFeatureAttributeImpl.call(NgisJobHandler.java:1427) 1427 - ngis UpadteDatasetFeatureAttribute job for session_id E48A85DB43E69FC4063B3CA26D39C870 with job uuid 2eb8e904-47ee-4e45-ae00-ce9773aa55bc and dataset uuid 23babdf3-ed5c-41f9-ad53-c5c1c007090e registered at Thu May 25 07:23:54 CEST 2023 update datalayer 23babdf3-ed5c-41f9-ad53-c5c1c007090e with new attributes object f8b67e95-c0da-4246-8f5b-8bed975d8c52 - 
2023-05-25 07:23:54,866 [pool-19-thread-10] DEBUG Ar5NgisOpenApiIntergration no.skogoglandskap.topo.client.Ar5NgisOpenApiIntergration.upadteDatasetFeatureAttribute(Ar5NgisOpenApiIntergration.java:809) 809 - enter - 


 - 

*/

-- Load data
\set dataset 'fkb50/update_feature_attribute-sample1'
\i :regdir/utils/load_update_features_payload.sql

SELECT topo_ar5ngis.update_features_fkb50(payload, bbox)
FROM json_input;

-- Validate data
\i :regdir/utils/validate_update_features_load.sql


\o :regdir/../data/:dataset.export.json
SELECT
	topo_ar5ngis.surfaces_as_geojson_fkb50(
		(
			SELECT array_agg("identifikasjon_lokal_id"::text ORDER BY "identifikasjon_lokal_id")
			FROM topo_ar5ngis.face_attributes
		),
		25832, -- utm 32
		5 -- num decimals
	) geojson
;

-- enable output again
\o


SELECT registreringsversjon, arealtype, informasjon FROM topo_ar5ngis.face_attributes WHERE identifikasjon_lokal_id = 'f8b67e95-c0da-4246-8f5b-8bed975d8c52';


SELECT 'upd_after_after', (topo_ar5ngis.update_feature_attribute_java(
	$$
	{
	  "type": "Feature",
	  "properties": {
	  	"id":"f8b67e95-c0da-4246-8f5b-8bed975d8c52","arealtype":"82","treslag":"98","skogbonitet":"98","grunnforhold":"98","datafangstdato":"2023-05-24","verifiseringsdato":"2023-05-24","opphav":"nibio","klassifiseringsmetode":"uMeld","informasjon":"nyyyee"
	  }
	}
	$$, -- feature
	'webklient_test'
) IS NOT NULL);


SELECT registreringsversjon, arealtype, informasjon, klassifiseringsmetode FROM topo_ar5ngis.face_attributes WHERE identifikasjon_lokal_id = 'f8b67e95-c0da-4246-8f5b-8bed975d8c52';

--

--{"properties":{"id":"f8b67e95-c0da-4246-8f5b-8bed975d8c52","arealtype":"82","treslag":"98","skogbonitet":"98","grunnforhold":"98","datafangstdato":"2023-05-24","verifiseringsdato":"2023-05-24","opphav":"nibio","klassifiseringsmetode":"sFelt","informasjon":""}}

ROLLBACK;

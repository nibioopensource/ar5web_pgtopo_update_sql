BEGIN;

set client_min_messages to WARNING;

-- Load data
\set dataset 'fkb50/getData-sample1'
\i :regdir/utils/load_update_features_payload.sql

SELECT topo_ar5ngis.update_features_fkb50(payload, bbox)
FROM json_input;

-- Validate data
\i :regdir/utils/validate_update_features_load.sql

-- Output the data
\o :regdir/../data/:dataset.export.json
SELECT
	topo_ar5ngis.surfaces_as_geojson_fkb50(
		(
			SELECT array_agg("identifikasjon_lokal_id"::text ORDER BY "identifikasjon_lokal_id")
			FROM topo_ar5ngis.face_attributes
		),
		25832, -- utm 32
		5 -- num decimals
	) geojson
;
\o

SELECT '-- geojson diff input output --';
\echo `:regdir/utils/fkb50_geojson_diff.sh :regdir/../data/:dataset.json :regdir/../data/:dataset.export.json`

ROLLBACK;

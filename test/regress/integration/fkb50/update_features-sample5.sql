BEGIN;

set client_min_messages to ERROR;

/**

2023-05-04 14:15:28.346 UTC user topo_rein_crud1 [2981898] topo_rein_crud1@ar5ngis_utv_fkb50_ws ERROR:  parse error - invalid geometry at character 515
2023-05-04 14:15:28.346 UTC user topo_rein_crud1 [2981898] topo_rein_crud1@ar5ngis_utv_fkb50_ws HINT:  "[4" <-- parse error at position 2 within geometry
2023-05-04 14:15:28.346 UTC user topo_rein_crud1 [2981898] topo_rein_crud1@ar5ngis_utv_fkb50_ws QUERY:  INSERT INTO topo_ar5ngis.face_attributes (opphav,treslag,arealtype,featuretype,skogbonitet,grunnforhold,datafangstdato,oppdateringsdato,verifiseringsdato,registreringsversjon,klassifiseringsmetode,identifikasjon_lokal_id,identifikasjon_navnerom,identifikasjon_versjon_id,geometry_properties_position,omrade) VALUES('nibio','98','11','ArealressursFlate','98','98','1970-08-13','2023-05-04T14:35:10','2019-07-27','2014-03-01','sFelt','6f7b8826-7ab3-43b6-9d8c-e2cffd67d4e9','test.ar5','2023-05-04 14:35:10.234710000','[481421.9127532891, 7432243.479612575]','(20,1,2053,3)') RETURNING identifikasjon_lokal_id
2023-05-04 14:15:28.346 UTC user topo_rein_crud1 [2981898] topo_rein_crud1@ar5ngis_utv_fkb50_ws CONTEXT:  PL/pgSQL function topo_update.insert_feature(topogeometry,jsonb,regclass,name,name,jsonb) line 31 at EXECUTE

6f7b8826-7ab3-43b6-9d8c-e2cffd67d4e9

481421.9127532891, 7432243.479612575


seringsdato,registreringsversjon,klassifiseringsmetode,identifikasjon_lokal_id,identifikasjon_navnerom,identifikasjon_versjon_id,geometry_properties_position,omrade) VALUES('nibio','98','11','ArealressursFlate
','98','98','1970-08-13','2023-05-04T14:35:10','2019-07-27','2014-03-01','sFelt','6f7b8826-7ab3-43b6-9d8c-e2cffd67d4e9','test.ar5','2023-05-04 14:35:10.234710000','[481421.9127532891, 7432243.479612575]','(1,1
,37,3)') RETURNING identifikasjon_lokal_id


"geometry_properties": {
          "position": [
            481421.9127532891,
            7432243.479612575
          ]
        }


Relates to https://github.com/kartverket/NGIS-OpenAPI/issues/120

*/

-- Load data a smaler data set it the same area
\set dataset 'fkb50/getData-sample5'
\i :regdir/utils/load_update_features_payload.sql

SELECT topo_ar5ngis.update_features_fkb50(payload, bbox)
FROM json_input;


-- Validate data
\i :regdir/utils/validate_update_features_load.sql

-- Output the data
\o :regdir/../data/:dataset.export.json
SELECT
	topo_ar5ngis.surfaces_as_geojson_fkb50(
		(
			SELECT array_agg("identifikasjon_lokal_id"::text ORDER BY "identifikasjon_lokal_id")
			FROM topo_ar5ngis.face_attributes
		),
		25835, -- utm 33
		5 -- num decimals
	) geojson
;
\o

--SELECT '-- geojson diff input output --';
--\echo `:regdir/utils/fkb50_geojson_diff.sh :regdir/../data/:dataset.json :regdir/../data/:dataset.export.json`

ROLLBACK;

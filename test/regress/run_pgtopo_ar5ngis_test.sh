#!/bin/sh

REGDIR=$(dirname $0)
TOPDIR=${REGDIR}/../..
DATADIR=${REGDIR}/../data

PATH=$PATH:${TOPO_UPDATE_DIR}/test/regress

which run_pgtopo_test.sh || {
	echo "Cannot find run_pgtopo_test.sh, please set TOPO_UPDATE_DIR" >&2
	exit 1
}

export POSTGIS_REGRESS_DB=${POSTGIS_REGRESS_DB-nibio_ar5ngis_reg}

if echo $@ | grep -q -- '--nocreate'; then
	:
else
	OPTS="--after-create-script ${TOPDIR}/topo_ar5ngis.sql"
	OPTS="${OPTS} --after-create-script ${REGDIR}/utils/make_predictable.sql"
	OPTS="${OPTS} --before-test-script ${REGDIR}/utils/topo_ar5ngis-schema_reset.sql"
fi

if echo $@ | grep -q -- '--nodrop'; then
	:
else
	OPTS="${OPTS} --before-uninstall-script ${TOPDIR}/topo_ar5ngis-uninstall.sql"
fi

export POSTGIS_REGRESS_DIR=$(cd ${REGDIR}; pwd -P)
run_pgtopo_test.sh ${OPTS} $@

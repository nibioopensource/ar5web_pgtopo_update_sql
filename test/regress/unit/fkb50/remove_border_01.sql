BEGIN;

\i :regdir/utils/make_predictable.sql

set client_min_messages to WARNING;

-- Load data
\set dataset 'fkb50/synteticSample1'
\i :regdir/utils/load_update_features_payload.sql

SELECT topo_ar5ngis.update_features_fkb50(payload, bbox)
FROM json_input;

-- Validate data
\i :regdir/utils/validate_update_features_load.sql

-- Split it with an horizontal line
CREATE TABLE add_border_results1 AS
SELECT * FROM topo_ar5ngis.add_borders_fkb50($FEATURE$
    {
        "geometry" : {
            "crs" : {
                "properties" : {
                    "name" : "EPSG:25832"
                },
                "type" : "name"
            },
            "coordinates" : [ [ -1, 5 ], [ 11, 5 ] ],
            "type" : "LineString"
        },
        "properties": {
          "_current_timestamp": "2023-04-07 00:00:00",
        "datafangstmetode": "dig",
        "datafangstdato": "2023-04-17"
    },
        "type" : "Feature"
    }
$FEATURE$);

-- Print result summaries
SELECT 'add1', * FROM add_border_results1
ORDER BY 2;


-- Remove border created line
CREATE TABLE remove_border_results1 AS
SELECT * FROM topo_ar5ngis.remove_borders_fkb50('00000000-0000-0000-0000-000000000002');


-- Print result summaries
SELECT 'remove1', * FROM remove_border_results1
ORDER BY 2;


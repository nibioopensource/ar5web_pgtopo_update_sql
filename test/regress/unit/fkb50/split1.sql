BEGIN;

\i :regdir/utils/make_predictable.sql

set client_min_messages to WARNING;

-- Load data
\set dataset 'fkb50/synteticSample1'
\i :regdir/utils/load_update_features_payload.sql

SELECT topo_ar5ngis.update_features_fkb50(payload, bbox)
FROM json_input;

-- Validate data
\i :regdir/utils/validate_update_features_load.sql

-- Split it with an horizontal line
CREATE TABLE add_border_results1 AS
SELECT * FROM topo_ar5ngis.add_borders_fkb50($FEATURE$
    {
        "geometry" : {
            "crs" : {
                "properties" : {
                    "name" : "EPSG:25832"
                },
                "type" : "name"
            },
            "coordinates" : [ [ -1, 5 ], [ 11, 5 ] ],
            "type" : "LineString"
        },
        "properties": {
        	"_current_timestamp": "2023-04-07 00:00:00",
		    "datafangstmetode": "dig",
		    "datafangstdato": "2023-04-17"
		},
        "type" : "Feature"
    }
$FEATURE$);

-- Print result summaries
SELECT 'add1', * FROM add_border_results1
ORDER BY 2;

-- Get changes json
WITH x AS (
	SELECT topo_ar5ngis.add_border_changes_as_geojson_fkb50(
        'add_border_results1',
		maxdecimaldigits => 0
    ) j
)
SELECT
	'geojson1',
	f -> 'geometry' ->> 'type' as typ,
	f -> 'properties' -> 'identifikasjon' ->> 'lokalId' as id,
	f -> 'update' ->> 'action' as action
FROM x, jsonb_array_elements( j -> 'features' ) f
ORDER BY 3;

-- Split the upper rectangle with a vertical line
CREATE TABLE add_border_results2 AS
SELECT * FROM topo_ar5ngis.add_borders_fkb50($FEATURE$
    {
        "geometry" : {
            "crs" : {
                "properties" : {
                    "name" : "EPSG:25832"
                },
                "type" : "name"
            },
            "coordinates" : [ [ 5, 4 ], [ 5, 11 ] ],
            "type" : "LineString"
        },
        "properties": {
        	"_current_timestamp": "2023-04-11 00:00:00",
		    "datafangstmetode": "dig",
		    "datafangstdato": "2023-04-17"
		},
        
        "type" : "Feature"
    }
$FEATURE$);

-- Print result summaries
SELECT 'add2', * FROM add_border_results2
ORDER BY 2;

-- Get changes json
WITH x AS (
	SELECT topo_ar5ngis.add_border_changes_as_geojson_fkb50(
        'add_border_results2',
		maxdecimaldigits => 0
    ) j
)
SELECT
	'geojson2',
	f -> 'geometry' ->> 'type' as typ,
	f -> 'properties' -> 'identifikasjon' ->> 'lokalId' as id,
	f -> 'update' ->> 'action' as action
FROM x, jsonb_array_elements( j -> 'features' ) f
ORDER BY 3;


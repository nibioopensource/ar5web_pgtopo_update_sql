BEGIN;

\i :regdir/utils/make_predictable.sql

SET client_min_messages TO WARNING;

-- This is a test for
-- https://gitlab.com/nibioopensource/ar5web_pgtopo_update_sql/-/issues/19
-- The bug does not show up if the Border table (edge_attributes)
-- contains only 100 rows, but do show up if it contains 1000 rows.
-- TODO: find a way to make this more predictable
--
INSERT INTO topo_ar5ngis.edge_attributes(
	featuretype
)
SELECT
	CASE
	WHEN i < 100 THEN
		'ArealressursGrense'
	WHEN i < 200 THEN
		'ArealressursGrenseFiktiv'
	WHEN i % 2 = 0 THEN
		'Even'
	ELSE
		'Odd'
	END
FROM generate_series(1,1000,1) i;

-- Test for https://gitlab.com/nibioopensource/ar5web_pgtopo_update_sql/-/issues/19
SELECT 't1', jsonb_pretty(j -> 'properties' -> 'avgrensesAvArealressursGrense' )
FROM topo_ar5ngis.surfaces_as_geojson_featureJsonModifier_fkb50(
	$FEATURE$
        {
            "type": "Feature",
            "geometry": {
                "type": "Polygon",
                "coordinates": [
                    [ [ 0, 0 ], [ 10, 0 ], [ 5, 10 ], [ 0, 0 ] ]
                ]
            },
            "properties": {
                "identifikasjon": {
                    "lokalId": "10000000-0000-0000-0000-000000000001"
                }
            },
            "geometry_properties": {
                "exterior": [
					"00000000-0000-0000-0000-000000000091",
					"00000000-0000-0000-0000-000000000002"
                ]
            }
        }
	$FEATURE$,
	'S', -- role
	NULL -- add_borders_result_relation
) j;

-- Test for
-- https://gitlab.com/nibioopensource/ar5web_pgtopo_update_sql/-/merge_requests/13#note_1362141538
SELECT 't2', jsonb_pretty(j -> 'properties' -> 'avgrensesAvArealressursGrense' )
FROM topo_ar5ngis.surfaces_as_geojson_featureJsonModifier_fkb50(
	$FEATURE$
        {
            "type": "Feature",
            "geometry": {
                "type": "Polygon",
                "coordinates": [
                    [ [ 0, 0 ], [ 10, 0 ], [ 5, 10 ], [ 0, 0 ] ]
                ]
            },
            "properties": {
                "identifikasjon": {
                    "lokalId": "10000000-0000-0000-0000-000000000001"
                }
            },
            "geometry_properties": {
                "exterior": [
					"00000000-0000-0000-0000-000000000080"
                ],
                "interiors": [
					[ "00000000-0000-0000-0000-000000000010" ],
					[ "-00000000-0000-0000-0000-000000000020" ]
                ]
            }
        }
	$FEATURE$,
	'S', -- role
	NULL -- add_borders_result_relation
) j;


-- Test for https://gitlab.com/nibioopensource/ar5web_pgtopo_update_sql/-/issues/30
WITH surfaces AS (
	SELECT topo_ar5ngis.surfaces_as_geojson_featureJsonModifier_fkb50(
		$FEATURE$
			{
				"type": "Feature",
				"geometry": {
					"type": "Polygon",
					"coordinates": [
						[ [ 0, 0 ], [ 10, 0 ], [ 5, 10 ], [ 0, 0 ] ]
					]
				},
				"properties": {
					"identifikasjon": {
						"lokalId": "10000000-0000-0000-0000-000000000001"
					}
				},
				"geometry_properties": {
					"exterior": [
						"00000000-0000-0000-0000-000000000200",
						"00000000-0000-0000-0000-000000000201",
						"00000000-0000-0000-0000-000000000202"
					]
				}
			}
		$FEATURE$,
		'S', -- role
		NULL -- add_borders_result_relation
	) -> 'properties' j
), keys AS (
	SELECT jsonb_object_keys(j) k FROM surfaces
)
SELECT 't3', k, jsonb_array_length(j -> k) FROM keys, surfaces
WHERE k like 'avgrensesAv%'
ORDER BY 2
;


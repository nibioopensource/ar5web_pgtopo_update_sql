CREATE SCHEMA util;

CREATE FUNCTION util.check_generated_schema(lbl text, appname text)
RETURNS TABLE(o TEXT)
AS $BODY$ --{
DECLARE
	sql TEXT;
	appconfig JSONB;
BEGIN

	-- print schemas
	RETURN QUERY SELECT array_to_string(ARRAY[
			lbl, 'schema', nspname
		], '|')
		FROM pg_namespace
		WHERE nspname like appname || '%'
		ORDER BY nspname;

	-- print topology info
	RETURN QUERY SELECT array_to_string(ARRAY[
			lbl, 'topology', name, srid::text, precision::text
		], '|')
		FROM topology.topology
		WHERE name like appname || '%';

	-- print layers
	RETURN QUERY SELECT array_to_string(ARRAY[
			lbl, 'layer', schema_name, table_name,
			feature_column, feature_type::text
		], '|')
		FROM topology.layer
		WHERE topology_id = (
			SELECT id FROM topology.topology
			WHERE name like appname || '%'
		);

	-- print tables
	RETURN QUERY SELECT array_to_string(ARRAY[
			lbl, 'table', relname
		], '|')
		FROM pg_class
		WHERE relnamespace = appname::regnamespace
		AND relkind = 'r'
		ORDER BY oid
		;

	-- Print constraints
	RETURN QUERY SELECT array_to_string(ARRAY[
			lbl, 'constraint', tgrelid::regclass::text, tgname
		], '|')
		FROM pg_trigger
		WHERE tgrelid::regclass::text like appname || '.%'
		ORDER BY tgrelid, tgname
		;

	-- Print domains
	RETURN QUERY SELECT array_to_string(ARRAY[
			lbl,
			'domain',
			t.typname, -- "Name"
			pg_catalog.format_type(
				t.typbasetype,
				t.typtypmod
			), -- "Type"
			--t.typdefault, -- "Default"
			pg_catalog.array_to_string(
				ARRAY(
					SELECT pg_catalog.pg_get_constraintdef(r.oid, true)
					FROM pg_catalog.pg_constraint r
					WHERE t.oid = r.contypid
				),
				' '
			) -- "Check"
		], '|')
		FROM pg_catalog.pg_type t
		LEFT JOIN pg_catalog.pg_namespace n
		ON n.oid = t.typnamespace
		WHERE t.typtype = 'd'
		AND n.nspname = appname
		ORDER BY t.typname
	;

	-- print appconfig version
	sql := format(
			'SELECT cfg FROM %I.app_config',
			appname || '_sysdata_webclient_functions'
		);
	--RAISE DEBUG 'SQL: %', sql;
	EXECUTE sql INTO appconfig;

	RETURN QUERY SELECT array_to_string(ARRAY[
		lbl, 'appconfig_version', appconfig ->> 'version'
		], '|');

END;
$BODY$ LANGUAGE 'plpgsql'; --}


CREATE FUNCTION util.drop_generated_schema(appname text)
RETURNS VOID
AS $BODY$ --{
DECLARE
	sql TEXT;
BEGIN
	PERFORM DropTopology(appname || '_sysdata_webclient');
	sql := format('DROP SCHEMA %I CASCADE', appname);
	--RAISE WARNING 'SQL: %', sql;
	EXECUTE sql;
	sql := format('DROP SCHEMA %I CASCADE', appname || '_sysdata_webclient_functions');
	--RAISE WARNING 'SQL: %', sql;
	EXECUTE sql;
END;
$BODY$ LANGUAGE 'plpgsql'; --}



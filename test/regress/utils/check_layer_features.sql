-- {
CREATE OR REPLACE FUNCTION check_layer_features(
	layerTable regclass,
	geomcol name,
	check_orphaned BOOL DEFAULT FALSE
)
RETURNS TABLE(o TEXT)
AS $BODY$
DECLARE
  rec RECORD;
  sql TEXT;
  attrs TEXT[];
  topo topology.topology;
  layer topology.layer;
BEGIN

  SELECT t, l
	FROM topology.layer l, topology.topology t, pg_class c, pg_namespace n
	WHERE l.schema_name = n.nspname
	AND l.table_name = c.relname
	AND c.oid = layerTable
	AND c.relnamespace = n.oid
	AND l.feature_column = geomcol
	AND l.topology_id = t.id
  INTO rec;

  topo := rec.t;
  layer := rec.l;

  SELECT array_agg(quote_ident(attname) ORDER BY attnum)
  FROM pg_attribute
  WHERE attrelid = layerTable
  AND attnum > 0
  AND NOT attisdropped
  AND attname != geomcol
  INTO attrs;

  sql := format($$
    SELECT
		%1$s,
		CASE
			WHEN ST_Dimension(%2$I) = 2 THEN
				ST_AsEWKT(ST_Normalize(%2$I), 4)
			ELSE
				ST_AsEWKT(%2$I, 4)
		END
	FROM %3$s
	ORDER BY 1,2
  $$, array_to_string(attrs, ', '), geomcol, layerTable);
  --RAISE DEBUG 'SQL: %', sql;
  FOR rec IN EXECUTE sql
  LOOP
    o := (rec)::text;
    RETURN NEXT;
  END LOOP;

	IF NOT check_orphaned THEN
		RETURN;
	END IF;

	-- Check for the presence of orphaned TopoGeometry
	-- for the target layer
	sql := format(
		$$
SELECT DISTINCT topogeo_id
FROM %1$I.relation
WHERE layer_id = %2$L
AND topogeo_id NOT IN (
	SELECT id(%3$I)
	FROM %4$s
)
		$$,
		topo.name,
		layer.layer_id,
		geomcol,
		layerTable
	);

	--RAISE DEBUG 'SQL: %', sql;

	FOR rec IN EXECUTE sql
	LOOP
		o := 'MISSING defined TopoGeometry ' || rec.topogeo_id::text;
	RETURN NEXT;
	END LOOP;

END;
$BODY$ LANGUAGE 'plpgsql';
--}


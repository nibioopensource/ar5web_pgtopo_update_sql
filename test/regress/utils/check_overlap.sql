CREATE OR REPLACE FUNCTION check_overlap(
	layerTable REGCLASS,
	layerTopoGeomColumn NAME,
	layerIdColumn NAME
)
RETURNS TABLE(fid1 TEXT, fid2 TEXT)
AS $BODY$ --{
DECLARE
	sql TEXT;
	rec RECORD;
BEGIN

	sql := format(
		$$
SELECT
	l1.%1$I fid1,
	l2.%1$I fid2
FROM
	%2$s l1,
	%2$s l2
WHERE l1.%1$I != l2.%1$I
AND ST_Overlaps(l1.%3$I, l2.%3$I)
ORDER BY l1, l2
		$$,
		layerIdColumn,
		layerTable,
		layerTopoGeomColumn
	);
	FOR rec IN EXECUTE sql
	LOOP
		fid1 = rec.fid1;
		fid2 = rec.fid2;
		RETURN NEXT;
	END LOOP;

END;
$BODY$ LANGUAGE 'plpgsql'; --}

#!/bin/sh

TMPDIR=${TMPDIR:-/tmp}
DIR="${TMPDIR}/$(basename $0 .sh)-$$"
KEEPNORMALIZED=
FID_PROP=identifikasjon.lokalId
BID_PROP=lokalId
DEBUG=0
DECIMAL_DIGITS=5
j1=
j2=

usage() {
	echo "Usage: $0 [OPTIONS] <geojson1> <geojson2>"
	echo "Options:"
	echo "	-k"
	echo "		Keep normalized version of the files"
	echo "	-i <prop>"
	echo "		Specify property containing feature id."
	echo "		Defaults to 'identifikasjon.lokalId'"
	echo "	-v"
	echo "		Enable debug"
}

debug() {
	if [ "${DEBUG}" -ge 1 ]; then
		echo "DEBUG: $*"
	fi
}

fatal() {
	echo "FATAL: $*"
	exit 1
}

pathsafe() {
	echo "$*" | sed 's|/|_|g'
}

while [ -n "$1" ]; do
	if [ "$1" = '-k' ]; then
		KEEPNORMALIZED=1
	elif [ "$1" = '-i' ]; then
		fatal "-i switch not implemented in the perl normalizer";
		shift
		FID_PROP="$1"
	elif [ "$1" = '-v' ]; then
		DEBUG=$((DEBUG+1))
	elif [ -z "$j1" ]; then
		j1="$1"
	elif [ -z "$j2" ]; then
		j2="$1"
	fi
	shift
done

debug "Debug level is ${DEBUG}"

if test -z "$j2" -o -z "$FID_PROP"; then
	usage >&2
	exit 1
fi

test -e "$j1" || {
	echo "$j1: no such file or directory" >&2
	exit 1
}

test -e "$j2" || {
	echo "$j2: no such file or directory" >&2
	exit 1
}

cleanup() {
	if [ "x$KEEPNORMALIZED" = "x1" ]; then
		echo "Keeping normalized files in ${DIR}"
		# provide names matching inputs
		cd ${DIR}
	else
		rm -rf ${DIR}
	fi
}

mkdir -p ${DIR} || fatal "Could not create dir '${DIR}'"

trap cleanup 0

normalize() {
	I=$1
	ND=$2
	# TODO: pass verbosity switch to the normalizer too
	perl $(dirname $0)/fkb50_geojson_normalize.pl "${I}" "${ND}"
}

# Normalize the json
j1bn=$(basename "$j1" .json)
j1n="${DIR}/${j1bn}-normalized"
debug "---- Normalizing ${j1} into ${j1n} ------"
normalize "$j1" "${j1n}" || exit 1
debug "Normalized $j1 to $j1n"
if [ ${DEBUG} -ge 2 ]; then
	debug "Printing left-over files in ${j1n}"
	find "${j1n}" -printf "DEBUG: file left for ${j1bn}: %P\n"
fi

j2bn=$(basename "$j2" .json)
j2n="${DIR}/${j2bn}-normalized"
debug "---- Normalizing ${j2} into ${j2n} ------"
normalize "$j2" "${j2n}" || exit 1
debug "Normalized $j2 to $j2n"
if [ ${DEBUG} -ge 2 ]; then
	debug "Printing left-over files in ${j2n}"
	find "${j2n}" -printf "DEBUG: file left in ${j2bn}: %P\n"
fi

diff -x _* -rU2 "${j1n}" "${j2n}" || exit 1

echo "GeoJSON files are equal for the purpose of FKB50 use"

# print some summary now that we verified normalized
# form is the same for the two input
echo "Total number of features: "$('ls' "${j1n}"/features | wc -l | tr -d "[:space:]")
echo "Number of surfaces: "$(find "${j1n}"/features/ -name boundary -type d | wc -l | tr -d "[:space:]")

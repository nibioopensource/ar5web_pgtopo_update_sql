#!/bin/env perl

use JSON;
use Data::Dumper;
use File::Path qw(make_path);

my $DECIMAL_DIGITS = 5; # TODO: make parametrizable

my $json = JSON->new->pretty->canonical;

sub usage
{
	die "Usage: $0 <jsonfile> <outputdir>\n";
}

sub pathsafe
{
	my $path = shift;
	$path =~ s|[/ "'()]|-|g;
	return $path;
}

sub debug
{
	my $msg = shift;
	print "$msg\n";
}

sub normalize_boundaries
{
	my $fid = shift;
	my $BDIR = shift; # boundary dir
	my @boundaries = @_;

	my $BN=0;
	my %RING;
	foreach my $bound ( @boundaries )
	{
		++$BN;
		#debug "Boundary ${BN}: " . Dumper($bound);

		# Extract border feature identifier
		my $BID = $bound->{'lokalId'}
			or die "Element ${BN} of feature ${fid} 'avgrensesAvArealressursGrense' or 'avgrensesAvArealressursGrenseFiktiv' property lacks a 'lokalId' property, can't normalize\n";
		#debug "Found BID: ${BID}";

		# Extract idx element
		my @IDX = @{ $bound->{'idx'} }
			or die "Boundary ${BID} of feature ${fid} lacks an idx property\n";
		#debug "Found IDX: @IDX";

		die "Boundary ${BID} of feature ${fid} has " . @IDX . " elements, at least 3 expected\n"
			if ( @IDX < 3 );

		# Extract polygon number
		my $POLYID = $IDX[0];
		#debug "Found POLYID: ${POLYID}";

		# Extract ring number
		my $RID = $IDX[1];
		#debug "Found RINGID: ${RID}";

		# Extract component number
		my $CID = $IDX[2];
		#debug "Found COMPONENTID: ${CID}";
		#$CID = sprintf("%.3d", ${CID}); # zero-pad to 3 digits
		#debug "Padded COMPONENTID: ${CID}";

		delete $bound->{'idx'};
		$RING{$POLYID}{$RID}{$CID} = $bound;

	}

	# Now for each polygon:
	#  For each ring:
	#	set the "prev" element each component
	#	write boundary to file
	while( my($pid,$poly) = each %RING )
	{
		my $PDIR="${BDIR}/p${pid}";
		make_path( ${PDIR} );

		#debug "poly ${pid}"; # . Dumper(${poly});

		while( my ($rid,$ring) = each %{ $poly } )
		{
			# Sort ring components numerically by component id
			my @CIDS = sort { $a <=> $b } keys ( %{ $ring } );

			#debug "ring ${pid}.${rid} has components: @CIDS";

			my $least_id = undef;
			my $prev_id = undef;
			foreach my $cid (@CIDS)
			{
				my $comp = $ring->{$cid};
				my $curr_id = $comp->{'lokalId'};

				#debug "component $cid of ring ${pid}.${rid} has id $curr_id";

				if ( defined($prev_id) )
				{
					#debug "previous component of $cid in ring ${pid}.${rid} had id $prev_id";
					$comp->{'prev'} = $prev_id;
					$least_id = $curr_id if ( $curr_id lt $least_id );
				}
				else
				{
					#debug "component $cid of ring ${pid}.${rid} is the first component";
					$least_id = $curr_id;
				}

				$prev_id = $curr_id;
			}
			#debug "last component of ring ${pid}.${rid} had id $prev_id";
			$ring->{$CIDS[0]}->{'prev'} = $prev_id;

			my $RDIR = $rid eq 0 ? "${PDIR}/shell" : "${PDIR}/hole-${least_id}";

			# Save boundary files
			make_path( ${RDIR} );
			foreach my $cid (@CIDS)
			{
				my $comp = $ring->{$cid};
				my $cid = $comp->{'lokalId'};
				my $BF = "${RDIR}/" . pathsafe($cid) . ".json"; # component file

				open $FH, '>', $BF or die "Can't open $BF for writing: $!\n";
				print $FH $json->encode( $comp );
				close $FH;
			}
		}
	}

	return \%RING;
}

sub round
{
	my $in = shift;
	my $out = sprintf("%.${DECIMAL_DIGITS}f", $in);
	$out =~ s/\.?0+$//;
	return $out;
}


if ( @ARGV < 2 )
{
	usage();
}

my $INP = $ARGV[0];
my $ND = $ARGV[1];

make_path( ${ND} );

open my $FH, '<', $INP or die "Can't open $INP for reading: $!\n";
my $json_unparsed = do { local $/; <$FH> };
close $FH;

my $json_parsed = $json->decode ( $json_unparsed );

# Iterate over features
my $FN=0;
my @features = @{ $json_parsed->{'features'} };
foreach my $feat ( @features )
{
	++$FN;

	#print 'DUMP FEATURE: ' . Dumper($feat) . "\n";
	#print "FEATURE: " . $json->encode( $feat ) . "\n";

	# Extract full properties
	my $props = $feat->{'properties'}
		or die "Feature ${FN} in ${INP} lacks a 'properties' key, can't normalize\n";
	#print 'DUMP FEATURE PROPS: ' . Dumper($props) . "\n";

	# Extract feature identifier
	# TODO: make this parametrizable ?
	my $fid = $props->{'identifikasjon'}->{'lokalId'}
		or die "Feature ${FN} in ${INP} lacks an 'identifikasjon.lokalId' property, can't normalize\n";
	#print 'FEATURE ID: ' . Dumper($fid) . "\n";

	# Extract full geometry
	my $geom = $feat->{'geometry'};

	# Round geometry coordinates
	my $geomtype = $geom->{'type'};
	if ( $geomtype eq 'LineString' )
	{
		#debug "Feature $fid is a LineString";
		foreach my $point ( @{ $geom->{'coordinates'} } )
		{
			$point->[0] = round($point->[0]);
			$point->[1] = round($point->[1]);
		}
	}
	elsif ( $geomtype eq 'Polygon' )
	{
		#debug "Feature $fid is a Polygon";
		# Strip coordinates from polygons
		delete $geom->{'coordinates'};
	}
	else
	{
		die "Feature $fid has unsupported geometry type '$geomtype'\n";
	}

	my $FDIR="${ND}/features/" . pathsafe($fid); # feature directory
	make_path( ${FDIR} );

	# Dump geometry data
	my $GF="${FDIR}/geometry.json";
	open $FH, '>', $GF or die "Can't open $GF for writing: $!\n";
	print $FH $json->encode( $geom );
	close $FH;

	# Extract boundaries of Surfaces.
	#
	# These are in the "avgrensesAvArealressursGrense"
	# or "avgrensesAvArealressursGrenseFiktiv"
	# property of Surface features, depending on their
	# featuretype.
	#
	# We merge the two properties here to be able to find
	# full rings.
	#

	my @boundaryTypes = ( 'avgrensesAvArealressursGrense', 'avgrensesAvArealressursGrenseFiktiv' );

	my @boundaries;
	foreach my $bt ( @boundaryTypes ) {
		push ( @boundaries, @{ $props->{$bt} } );
	}
	if ( @boundaries )
	{
		$RING = normalize_boundaries($fid, "${FDIR}/boundary", @boundaries);
		#debug "Rings: " . Dumper($RING);
	}

	# Cleanup properties data

	# Remove avgrensesAvArealressursGrense and avgrensesAvArealressursGrenseFiktiv
	# TODO: keep them in some form, just a list of lokalId in them, and ordered
	# See https://gitlab.com/nibioopensource/ar5web_pgtopo_update_sql/-/issues/30
	foreach my $bt ( @boundaryTypes ) {
		my @newType;
		foreach my $avgrenses ( $props->{$bt} ) {
			foreach my $el ( @{ $avgrenses } ) {
				push( @newType, $el->{'lokalId'} );
			}
		}
		$props->{$bt} = @newType; # just keep a count for now, add backslash to
		#$props->{$bt} = \@newType; # this would add the actual identifiers
		#sort @newType;
	}

	# Remove null-valued elements
	map { delete $props->{$_} unless defined $props->{$_} } keys %{$props};

	my $PF="${FDIR}/props.json";
	open $FH, '>', $PF or die "Can't open $PF for writing: $!\n";
	print $FH $json->encode( $props );
	close $FH;

}

# Remove features from top-level file
delete $json_parsed->{'features'};

# Write stripped-down json to header file
my $HF="${ND}/header.json";
open $FH, '>', $HF or die "Can't open $HF for writing: $!\n";
print $FH $json->encode( $json_parsed );
close $FH;

-- Requires caller to define a "dataset" variable.
--
-- Read the contents of :'dataset'.json and :'dataset'.bbox
-- files from regress/data dir into a "json_input" table
-- (created if doesn't exist) with columns:
--
--   name - name of dataset
--   bbox - bounding box geometry
--   payload - jsonb payload
--
-- The .json file can be prettified
--

-- Error out if :dataset is not set
\set dataset_is_set :dataset
select :'dataset_is_set' = ':dataset' as dataset_is_unset \gset
\if :dataset_is_unset
\echo FATAL: The "dataset" variable is unset upon running load_update_features_payload.sql
\echo 'HINT: Use something like the following:'
\echo 'HINT: \\set dataset ''fkb50/getData-sample1'''
\echo 'HINT: \\i \:regdir/utils/load_update_features_payload.sql'
\quit
\endif

\set dataset_base_path :'regdir'/../data/:'dataset'

-- Read bbox file, error out if file is missing
\set dataset_content_bbox `cat :dataset_base_path.bbox`
select :'dataset_content_bbox' = '' as dataset_lacks_bbox \gset
\if :dataset_lacks_bbox
\echo FATAL: Cannot read bbox from :dataset dataset .bbox file
\quit
\endif
-- \echo Dataset content bbox is :dataset_content_bbox

-- Read json file
\set dataset_content_json `cat :dataset_base_path.json`
select :'dataset_content_json' = '' as dataset_lacks_json \gset
\if :dataset_lacks_json
\echo FATAL: Cannot read payload from :dataset dataset .json file
\quit
\endif
-- \echo Dataset content json is :dataset_content_json

CREATE TABLE IF NOT EXISTS json_input(name text, bbox geometry, payload jsonb);

INSERT INTO json_input(name, bbox, payload)
VALUES (
	:'dataset',
	:'dataset_content_bbox',
	:'dataset_content_json'
);

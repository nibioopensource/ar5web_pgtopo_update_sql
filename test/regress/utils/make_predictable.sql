
--------------------------------------------
-- Make default lokal_id values predictable
--------------------------------------------

CREATE SEQUENCE IF NOT EXISTS topo_ar5ngis.predictable_uuid_seq;

CREATE OR REPLACE FUNCTION topo_ar5ngis.uuid_generate_predictable()
RETURNS uuid
AS $BODY$
	SELECT (
		'00000000-0000-0000-0000-' ||
		LPAD(
			nextval('topo_ar5ngis.predictable_uuid_seq')::text,
			12,
			'0'
		)
	)::uuid;
$BODY$ LANGUAGE 'sql' VOLATILE;

ALTER TABLE topo_ar5ngis.face_attributes
	ALTER COLUMN identifikasjon_lokal_id
		SET DEFAULT topo_ar5ngis.uuid_generate_predictable();

ALTER TABLE topo_ar5ngis.edge_attributes
	ALTER COLUMN identifikasjon_lokal_id
		SET DEFAULT topo_ar5ngis.uuid_generate_predictable();


------------------------------------------------------
-- Make default identifikasjon_versjon_id predictable
------------------------------------------------------

ALTER TABLE topo_ar5ngis.face_attributes
	ALTER COLUMN identifikasjon_versjon_id
		SET DEFAULT '1970-01-01 00:00:00'::timestamptz;

ALTER TABLE topo_ar5ngis.edge_attributes
	ALTER COLUMN identifikasjon_versjon_id
		SET DEFAULT '1970-01-01 00:00:00'::timestamptz;

------------------------------------------------------
-- Make default oodateringsdato predictable
------------------------------------------------------

ALTER TABLE topo_ar5ngis.face_attributes
	ALTER COLUMN oppdateringsdato
		SET DEFAULT '1970-01-01 00:00:00'::timestamptz;

ALTER TABLE topo_ar5ngis.edge_attributes
	ALTER COLUMN oppdateringsdato
		SET DEFAULT '1970-01-01 00:00:00'::timestamptz;


--------------------------------------------
-- Make default id values predictable for topo_ar5ngis.lock_area_for_update and topo_ar5ngis.lock_area_for_update_history
--------------------------------------------

CREATE SEQUENCE IF NOT EXISTS topo_ar5ngis.predictable_lock_area_for_update_id_seq;

CREATE OR REPLACE FUNCTION topo_ar5ngis.uuid_generate_lock_area_for_update_id_predictable()
RETURNS uuid
AS $BODY$
	SELECT (
		'00000000-0000-0000-0000-' ||
		LPAD(
			nextval('topo_ar5ngis.predictable_lock_area_for_update_id_seq')::text,
			12,
			'0'
		)
	)::uuid;
$BODY$ LANGUAGE 'sql' VOLATILE;

CREATE OR REPLACE FUNCTION topo_ar5ngis.lock_area_for_update_id()
RETURNS uuid
AS $BODY$
	SELECT * FROM topo_ar5ngis.uuid_generate_lock_area_for_update_id_predictable()
$BODY$ LANGUAGE 'sql' VOLATILE;




CREATE OR REPLACE FUNCTION reset_topology(topo TEXT)
RETURNS VOID
AS $BODY$
DECLARE
  sql TEXT;
BEGIN
  sql := format(
    $$
DELETE FROM %1$I.relation;
DELETE FROM %1$I.edge_data;
DELETE FROM %1$I.node;
DELETE FROM %1$I.face WHERE face_id > 0;
    $$,
    topo
  );
  EXECUTE sql;
END;
$BODY$ LANGUAGE 'plpgsql';


-- Reset topo_ar5ngis schema to empty state
TRUNCATE topo_ar5ngis.face_attributes CASCADE;
TRUNCATE topo_ar5ngis.edge_attributes CASCADE;

-- Use reset code behave the same was as topo_ar5ngis reset.sql
TRUNCATE topo_ar5ngis.lock_area_for_update CASCADE;
TRUNCATE topo_ar5ngis.lock_area_for_update_history CASCADE;
SELECT NULL FROM setval('topo_ar5ngis.predictable_uuid_seq', 1, false);
SELECT NULL FROM setval('topo_ar5ngis.predictable_lock_area_for_update_id_seq', 1, false);

-- Reset topo_ar5ngis_sysdata_webclient topology to empty state
TRUNCATE topo_ar5ngis_sysdata_webclient.relation CASCADE;
TRUNCATE topo_ar5ngis_sysdata_webclient.edge_data CASCADE;
TRUNCATE topo_ar5ngis_sysdata_webclient.node CASCADE;
DELETE FROM topo_ar5ngis_sysdata_webclient.face WHERE face_id > 0;
SELECT NULL FROM setval('topo_ar5ngis_sysdata_webclient.topogeo_s_1', 1, false);
SELECT NULL FROM setval('topo_ar5ngis_sysdata_webclient.topogeo_s_2', 1, false);
SELECT NULL FROM setval('topo_ar5ngis_sysdata_webclient.edge_data_edge_id_seq', 1, false);
SELECT NULL FROM setval('topo_ar5ngis_sysdata_webclient.node_node_id_seq', 1, false);
SELECT NULL FROM setval('topo_ar5ngis_sysdata_webclient.face_face_id_seq', 1, false);


SELECT 'validate', * FROM topology.ValidateTopology('topo_ar5ngis_sysdata_webclient');

SELECT 'Surface count', count(*)  from topo_ar5ngis.face_attributes;
SELECT 'Borders count', count(*)  from topo_ar5ngis.edge_attributes;

-- We need to make sure statistics are up to date here
-- or the plan could be so bad to take over 1h instead of 200ms
-- for the topo_update.find_interiors_intersect query.
--
-- TODO: have ANALYZE called by topo_update.update_features ?
--
ANALYZE;

SELECT 'Borders overlap count', count(*) as num_over_lap from topo_update.find_interiors_intersect('topo_ar5ngis.edge_attributes', 'grense', 'identifikasjon_lokal_id');
SELECT 'Surfaces overlap count', count(*) as num_over_lap from topo_update.find_interiors_intersect('topo_ar5ngis.face_attributes', 'omrade', 'identifikasjon_lokal_id');


SELECT 'Uncovered edges (approximated point on surface)',
	ST_AsText(ST_PointOnSurface(geom), 5)
	FROM topo_ar5ngis_sysdata_webclient.edge e, json_input j
	WHERE e.geom && j.bbox
	AND edge_id NOT IN (
		SELECT abs(r.element_id)
		FROM topo_ar5ngis_sysdata_webclient.relation r, topology.layer l
		WHERE r.layer_id = l.layer_id AND l.table_name = 'edge_attributes'
	)
	ORDER BY 2;

SELECT 'Uncovered faces (approximated bbox centroid)',
	ST_AsText(ST_PointOnSurface(mbr), 5)
	FROM topo_ar5ngis_sysdata_webclient.face f, json_input j
	WHERE f.mbr && j.bbox
	AND face_id NOT IN (
		SELECT abs(r.element_id)
		FROM topo_ar5ngis_sysdata_webclient.relation r, topology.layer l
		WHERE r.layer_id = l.layer_id AND l.table_name = 'face_attributes'
	)
	ORDER BY 2;


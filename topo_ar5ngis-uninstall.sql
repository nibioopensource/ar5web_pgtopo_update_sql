-- TODO: use a topo_update.app_Drop function when available
SELECT topology.DropTopology('topo_ar5ngis_sysdata_webclient');
DROP schema IF EXISTS topo_ar5ngis CASCADE;
DROP schema IF EXISTS topo_ar5ngis_sysdata_webclient_functions CASCADE;

-- To handle when rollback is not used
DROP FUNCTION IF EXISTS check_overlap(
        layerTable REGCLASS,
        layerTopoGeomColumn NAME,
        layerIdColumn NAME
);
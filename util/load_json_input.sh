#!/bin/sh

usage() {
	echo "Usage: $0 <json_input_file>"
}

JSON_INPUT="$1"

if test -z "$JSON_INPUT"; then
	usage >&2
	exit 1
fi

cat <<EOF
-- Store input into a json_input table under topology
BEGIN;
CREATE TABLE json_input(payload jsonb);
\COPY json_input FROM '$JSON_INPUT'
COMMIT;
EOF

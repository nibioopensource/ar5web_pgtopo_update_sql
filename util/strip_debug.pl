#!/usr/bin/env perl

my $IN_DEBUG = 0;

while ($line = <>) {

	if ( not $IN_DEBUG )
	{
		if ( $line =~ /RAISE DEBUG/ )
		{
			if ( $line =~ /;/ )
			{
				print "-- STRIP -- $line";
				$line =~ s/.*;//;
				print $line;
			}
			else
			{
				print "-- STRIP -- $line";
				$IN_DEBUG = 1;
			}
			next;
		}
	}

	if ( $IN_DEBUG )
	{
		if ( $line =~ m/([^;]*);(.*)/ )
		{
			print "-- STRIP -- $1";
			$line = $2 . "\n";
			$IN_DEBUG = 0;
		}
		else
		{
			print "-- STRIP -- $line";
			next;
		}
	}

	print $line;
}


#!/bin/sh

usage() {
	echo "Usage: $0 <json_input_file> <bbox>"
}

JSON_INPUT=$1
BBOX=$2

if test -z "$BBOX"; then
	usage >&2
	exit 1
fi

#CRS=`jq -r .crs.properties.name $JSON_INPUT`
#echo "CRS: $CRS" >&2


cat <<EOF

-- Store input into a json_input temp table
CREATE TEMP TABLE json_input(payload jsonb);
\COPY pg_temp.json_input FROM '$JSON_INPUT'

SELECT '$BBOX' as bbox \gset

-- Call update_features
SELECT topo_ar5ngis.update_features(
	(SELECT payload FROM pg_temp.json_input),
	:'bbox'
);

EOF

#!/bin/sh

usage() {
	echo "Usage: $0 <json_input_file> <border_id> <surface_id>"
}

JSON_INPUT=$1
BORDER_ID=$2
SURFACE_ID=$3

if test -z "$SURFACE_ID"; then
	usage >&2
	exit 1
fi

BORDER_ID_ARRAY=`echo $BORDER_ID | sed 's/\./","/g;s/^/["/;s/$/"]/'`
SURFACE_ID_ARRAY=`echo $SURFACE_ID | sed 's/\./","/g;s/^/["/;s/$/"]/'`

CRS=`jq -r .crs.properties.name $JSON_INPUT`
#echo "CRS: $CRS" >&2

TOPO=topo$$
TOPO=topo
echo "Topo: $TOPO" >&2

cat <<EOF

SELECT DropTopology('$TOPO');

BEGIN;

-- Find source SRID
SELECT srid FROM spatial_ref_sys
WHERE ( auth_name || ':' || auth_srid )
= '$CRS' \gset

-- Create topology
SELECT CreateTopology('$TOPO', :srid);

-- Create border table
CREATE TABLE $TOPO.border(id TEXT primary key);
SELECT AddTopoGeometryColumn('$TOPO', '$TOPO', 'border', 'tg', 'LINE');

-- Create surface table
CREATE TABLE $TOPO.surface(id TEXT primary key);
SELECT AddTopoGeometryColumn('$TOPO', '$TOPO', 'surface', 'tg', 'POLYGON');

-- Store input into a json_input table under topology
CREATE TABLE $TOPO.json_input(payload jsonb);
\COPY $TOPO.json_input FROM '$JSON_INPUT'

-- Call update_features
SELECT topo_update.update_features(
	(SELECT payload FROM $TOPO.json_input),
	'$TOPO.surface', 'tg', 'id',
	'{ "id": $SURFACE_ID_ARRAY }',
	'$TOPO.border', 'tg', 'id',
	'{ "id": $BORDER_ID_ARRAY }',
	0, -- tolerance
	NULL -- deleteUnknownInBbox
);

COMMIT;

EOF
